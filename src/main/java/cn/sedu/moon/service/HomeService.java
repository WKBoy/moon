/*
 * Dian.com Inc.
 * Copyright (c) 2004-2019 All Rights Reserved.
 */
package cn.sedu.moon.service;

import cn.sedu.moon.controller.response.NewsResponse;
import cn.sedu.moon.service.context.IndexAskContext;
import cn.sedu.moon.service.context.QueryNewsContext;

import java.util.List;

/**
 *
 * @author ${wukong}
 * @version $Id: HomeService.java, v 0.1 2019年03月27日 7:32 PM Exp $
 */
public interface HomeService {

    /**
     * 首页新闻列表
     *
     * @param context 上下文
     * @return 新闻列表
     */
    List<NewsResponse> indexNews(QueryNewsContext context);


    /**
     * 首页的问答
     *
     * @return 新闻response
     */
    List<NewsResponse> indexAsk(IndexAskContext context);

    /**
     * 检查用户是否阅读过引导内容
     *
     * @param userId 用户id
     * @return true: 阅读过
     */
    Boolean checkIntroRead(Long userId);

    /**
     * 记录用过户已读引导内容
     *
     * @param userId 用户id
     */
    void introRead(Long userId);
}