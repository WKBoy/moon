/*
 * Dian.com Inc.
 * Copyright (c) 2004-2018 All Rights Reserved.
 */
package cn.sedu.moon.service;

import cn.sedu.moon.controller.request.district.UserAddDistrictRequest;
import cn.sedu.moon.controller.response.ArchiveChooseSchoolInfoResponse;
import cn.sedu.moon.controller.response.ArchiveDescResponse;
import cn.sedu.moon.controller.response.DistrictResponse;
import cn.sedu.moon.controller.response.GradeResponse;
import cn.sedu.moon.controller.response.QueryGrowDirectionResponse;
import cn.sedu.moon.controller.response.StudentArchiveResponse;
import cn.sedu.moon.controller.response.district.UserAddDistrictResponse;
import cn.sedu.moon.service.context.ArchiveReportContext;
import cn.sedu.moon.service.context.QueryDistrictContext;

import java.util.List;

/**
 * 学生档案服务的接口
 *
 * @author ${wukong}
 * @version $Id: ArchiveService.java, v 0.1 2018年12月10日 10:03 PM Exp $
 */
public interface ArchiveService {

    /**
     * 查询基本信息
     *
     * @param userId
     * @return
     */
    StudentArchiveResponse queryArchive(Long userId);

    /**
     * 查询择校信息
     *
     * @param userId
     * @return
     */
    ArchiveChooseSchoolInfoResponse queryChooseSchoolInfo(Long userId);

    /**
     * 查询年级列表
     *
     * @return 年级列表
     */
    List<GradeResponse> queryGradeList();

    /**
     * 上报学生档案信息
     *
     * @param context 上下文对象
     */
    void report(ArchiveReportContext context);

    /**
     * 上报基本信息的接口
     *
     * @param context 上下文对象
     */
    void reportBaseInfo(ArchiveReportContext context);

    /**
     * 获取小区列表
     *
     * @param context 上下文
     * @return 小区列表
     */
    List<DistrictResponse> districtNearby(QueryDistrictContext context);


    /**
     * 用户自己添加小区
     *
     * @param request request
     * @return 小区id
     */
    UserAddDistrictResponse userAddDistrict(UserAddDistrictRequest request);

    /**
     * 根据区编码获取小区列表
     *
     * @param context 上下文
     * @return 小区列表
     */
    List<DistrictResponse> districtNearbyArea(QueryDistrictContext context);

    /**
     * 获取成长方向列表
     *
     * @return response
     */
    QueryGrowDirectionResponse growDirectionList();

    /**
     * 获取问题的描述
     *
     * @param userId 用户ID
     * @return response
     */
    ArchiveDescResponse getAnswerDesc(Long userId);
}