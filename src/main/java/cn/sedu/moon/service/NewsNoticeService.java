/*
 * Dian.com Inc.
 * Copyright (c) 2004-2019 All Rights Reserved.
 */
package cn.sedu.moon.service;

import cn.sedu.moon.dao.model.NewsNotice;

/**
 * @author wukong
 * @version : NewsNoticeService.java, v 0.1 2019年10月27日 12:40 上午 Exp $
 */
public interface NewsNoticeService {

    boolean checkUserSubscribe(Long userId, Long newsId);

    NewsNotice getNoticeInfoByNewsId(Long newsId);

    void subscribeNotice(Long userId, Long noticeId);

    void addNotice(NewsNotice addModel);

    void deleteNotice(Long noticeId);

    void updateNotice(NewsNotice notice);
}