/*
 * Dian.com Inc.
 * Copyright (c) 2004-2019 All Rights Reserved.
 */
package cn.sedu.moon.service;

import cn.sedu.moon.controller.response.census.EditCensusResponse;
import cn.sedu.moon.controller.request.CmsNewsQueryRequest;
import cn.sedu.moon.controller.request.news.AddNewsRequest;
import cn.sedu.moon.controller.request.news.NewsCheckRequest;
import cn.sedu.moon.controller.response.SchoolNewsCheckDetailResponse;
import cn.sedu.moon.controller.response.SchoolNewsCheckResponse;
import cn.sedu.moon.controller.response.SchoolNewsDetailResponse;
import cn.sedu.moon.controller.response.SchoolNewsResponse;
import cn.sedu.moon.controller.response.SearchCollectNewsResponse;
import cn.sedu.moon.service.context.QueryStickNewsContext;
import cn.sedu.moon.service.context.SearchNewsContext;
import cn.sedu.moon.service.context.StickNewsContext;

import java.util.List;

/**
 * 新闻服务
 *
 * @author ${wukong}
 * @version $Id: NewsService.java, v 0.1 2019年02月15日 12:04 PM Exp $
 */
public interface NewsService {

    void updateDetail(NewsCheckRequest request);

    SchoolNewsCheckDetailResponse checkDetail(Long newsId);

    List<SchoolNewsResponse> searchNews(SearchNewsContext context);

    List<SchoolNewsCheckResponse> checkList(CmsNewsQueryRequest request);

    Integer checkListTotal(CmsNewsQueryRequest request);

    void deleteNews(Long newsId);

    void batchDeleteNews(List<Long> idList);

    void addNews(AddNewsRequest request);

    List<SchoolNewsResponse> queryNews(Long userId, Integer offset, Integer pageSize);

    void read(Long userId, Long newsId);

    /**
     * 获取新闻详情 (为前端提供的服务)
     *
     * @param newsId 新闻ID
     * @return 新闻详情
     */
    SchoolNewsDetailResponse newsDetail(Long newsId, Long userId);

    /**
     * 将新闻设置为置顶
     *
     * @param context 上下文
     */
    void stickNews(StickNewsContext context);

    /**
     * 查询置顶的文章列表
     *
     * @param context 上下文
     */
    List<SchoolNewsCheckResponse> queryStickNewsList(QueryStickNewsContext context);

    /**
     * 删除置顶文章
     *
     * @param context 上下文
     */
    void deleteStick(StickNewsContext context);

    void collect(Long userId, Long newsId);

    SearchCollectNewsResponse searchCollect(Long userId, Integer offset, Integer pageSize);

    void deleteCollect(Long userId, Long newsId);

    void stickLastDayCreatedNews();

    /**
     * 文章按地区编辑情况：（近30天）
     */
    EditCensusResponse newsEditCensus(String cityCode);

    /**
     * 每周精选列表
     *
     * @param userId 用户id
     * @return 新闻列表
     */
    List<SchoolNewsResponse> weeklySelectedList(Long userId);

}