/*
 * Dian.com Inc.
 * Copyright (c) 2004-2019 All Rights Reserved.
 */
package cn.sedu.moon.service;

import cn.sedu.moon.controller.request.cms.CmsLoginRequest;

/**
 *
 * @author ${wukong}
 * @version $Id: CmsService.java, v 0.1 2019年02月20日 7:39 PM Exp $
 */
public interface CmsService {

    /**
     * cms 端用户登陆，登陆成功后返回token
     *
     * @param request request
     * @return 返回token
     */
    String login(CmsLoginRequest request);
}