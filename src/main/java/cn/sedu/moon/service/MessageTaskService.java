/*
 * Dian.com Inc.
 * Copyright (c) 2004-2019 All Rights Reserved.
 */
package cn.sedu.moon.service;

import cn.sedu.moon.dao.model.NewsNotice;

import java.util.Date;

/**
 * 具体的消息发送接口定义
 *
 * @author ${wukong}
 * @version $Id: MessageTaskService.java, v 0.1 2019年04月22日 10:12 PM Exp $
 */
public interface MessageTaskService {

    /**
     * 消息。两种：1、提了问题有人回答；2、点了想问的问题有人回答
     *
     * @param userId 消息推送目标用户ID
     */
    void askPushTemplateMessage(Long userId);

    /**
     * 提问创建成功通知
     *
     * @param askId 问题id
     */
    void askCreatedMessage(Long askId);

    /**
     * 问答收到答案通知
     *
     * @param answerId 问题id
     */
    void askReceivedAnswerMessage(Long answerId);

    /**
     * 用户提问进展通知
     *
     * @param userId 用户id
     */
    void askChangeMessage(Long userId);


    /**
     * 每周精选推送
     *
     * @param userId
     */
    void bestWeekPushTemplateMessage(final Long userId);

    /**
     * 用户想上的学校文章推送
     * @param id
     * @param title
     * @param userId
     * @param schoolName
     */
    boolean pushNewNewsNotice(Long id, String title, Long userId, String schoolName);

    /**
     * 用户想上的学校评论推送
     * @param userId
     * @param schoolName
     */
    void pushNewCommentNotice(Long schoolId, String comment, Long userId, String schoolName);

    /**
     * 在校生家长评论通知
     * @param schoolId
     * @param schoolName
     * @param parentId
     * @param userName
     */
    void pushHelpToParentNotice(Long schoolId, String schoolName, Long parentId, String userName);

    /**
     * 学校问题有回应通知
     * @param askId
     * @param title
     * @param userId
     * @param schoolName
     */
    void pushAskConcernUserNotice(Long askId, String title, Long userId, String schoolName);

    /**
     * 推送newsNotice
     *
     * @param newsNotice newsNotice
     */
    void pushNewsNoticeMessage(NewsNotice newsNotice);

    /**
     * 新用户注册成功推送
     * @param msg
     * @param userId
     * @param cityName
     * @param date
     */
    void pushRegisterSuccessMsg(String msg, Long userId, String cityName, Date date);

    /**
     * 向未填写信息的人发送通知
     * @param userId
     */
    void pushNotArchiveMsg(String userId);

    /**
     * 推送注册完成一小时还未阅读过新闻的用户
     * @param userId
     * @param newsId
     * @param schoolName
     * @param title
     */
    void pushArchiveNotReadNewsMsg(Long userId, Long newsId, String schoolName, String title);
}