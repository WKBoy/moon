package cn.sedu.moon.service;

import java.util.Date;

/**
 * 消息推送
 *
 * @author guanzhong
 * @version : MessagePushServiceImpl.java, v 0.1 2019年11月13日01:32:17
 */
public interface MessagePushService {

    /**
     * 推送每周精选
     */
    void pushBestNewsWeekMessage();

    /**
     * 用户想上的学校文章推送
     * @param id
     * @param title
     * @param userId
     * @param schoolName
     */
    boolean pushNewNewsNotice(Long id, String title, Long userId, String schoolName);

    /**
     * 用户想上的学校评论推送
     * @param schoolId
     * @param comment
     * @param userId
     * @param schoolName
     */
    void pushNewCommentNotice(Long schoolId, String comment, Long userId, String schoolName);

    /**
     * 在校生家长评论通知
     * @param schoolId
     * @param schoolName
     * @param parentId
     * @param userName
     */
    void pushHelpToParentNotice(Long schoolId, String schoolName, Long parentId, String userName);

    /**
     * 学校问题有回应通知
     * @param askId
     * @param title
     * @param userId
     * @param schoolName
     */
    void pushAskConcernUserNotice(Long askId, String title, Long userId, String schoolName);

    /**
     * 用户注册成功通知
     * @param msg
     * @param userId
     * @param cityName
     * @param date
     */
    void pushRegisterSuccessMsg(String msg, Long userId, String cityName, Date date);

    /**
     * 向未填写信息的人发送通知
     * @param userId
     */
    void pushNotArchiveMsg(String userId);

    /**
     * 推送注册完成一小时还未阅读过新闻的用户
     * @param userId
     * @param newsId
     * @param schoolName
     * @param title
     */
    void pushArchiveNotReadNewsMsg(Long userId, Long newsId, String schoolName, String title);
}
