/*
 * Dian.com Inc.
 * Copyright (c) 2004-2019 All Rights Reserved.
 */
package cn.sedu.moon.service;

import cn.sedu.moon.controller.request.DistrictUserReportRequest;
import cn.sedu.moon.controller.request.SchoolUserReportRequest;

/**
 *
 * @author ${wukong}
 * @version $Id: UserReportService.java, v 0.1 2019年03月12日 2:11 AM Exp $
 */
public interface UserReportService {

    void reportSchool(SchoolUserReportRequest request);

    void reportDistrict(DistrictUserReportRequest request);
}