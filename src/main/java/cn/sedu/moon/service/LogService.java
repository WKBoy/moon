/*
 * Dian.com Inc.
 * Copyright (c) 2004-2019 All Rights Reserved.
 */
package cn.sedu.moon.service;

/**
 * @author wukong
 * @version : LogService.java, v 0.1 2019年08月24日 16:43 Exp $
 */
public interface LogService {

    void logNewDetail(Long newsId, Long userId);
}