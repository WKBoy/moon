package cn.sedu.moon.service.helper;

import cn.sedu.moon.dao.DistrictGdDAO;
import cn.sedu.moon.dao.StudentArchivesDAO;
import cn.sedu.moon.dao.model.DistrictGd;
import cn.sedu.moon.dao.model.StudentArchives;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Slf4j
@Service
public class UserHelper {


    @Autowired
    private StudentArchivesDAO archivesDAO;


    @Autowired
    private DistrictGdDAO districtGdDAO;


    /**
     * 获取用户的城市code
     *
     * @param userId
     * @return
     */
    public String getUserCityCode(Long userId) {
        // 获取用户家的地理位置
        DistrictGd userDistrict = queryUserDistrictInfo(userId);
        if (userDistrict == null || userDistrict.getLat() == null || userDistrict.getLng() == null) {
            return "";
        }
        return userDistrict.getCityCode();
    }


    /**
     * 获取高德地图信息
     *
     * @param userId
     * @return
     */
    private DistrictGd queryUserDistrictInfo(Long userId) {
        // 获取用户家的地理位置
        StudentArchives queryArchiveModel = new StudentArchives();
        queryArchiveModel.setUserId(userId);
        queryArchiveModel.setDeleted(0);

        List<StudentArchives> queryArchiveResult = archivesDAO.listRecord(queryArchiveModel);
        if (queryArchiveResult == null || queryArchiveResult.size() == 0 || queryArchiveResult.get(0).getDistrictHouseId() == null) {
            return null;
        }

        DistrictGd queryDistrictModel = new DistrictGd();
        queryDistrictModel.setId(queryArchiveResult.get(0).getDistrictHouseId());
        queryDistrictModel.setDeleted(0);

        return districtGdDAO.getRecord(queryDistrictModel);
    }
}
