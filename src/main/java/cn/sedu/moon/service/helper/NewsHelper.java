package cn.sedu.moon.service.helper;

import cn.sedu.moon.dao.NewsReadDAO;
import cn.sedu.moon.dao.UserCollectNewsDAO;
import cn.sedu.moon.dao.model.NewsRead;
import cn.sedu.moon.dao.model.UserCollectNews;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Slf4j
@Service
public class NewsHelper {

    @Autowired
    private UserCollectNewsDAO userCollectNewsDAO;

    @Autowired
    private NewsReadDAO newsReadDAO;

    /**
     * 检查∂用户是否收藏新闻
     *
     * @param userId
     * @param newsId
     * @return
     */
    public Boolean checkUserCollectNews(Long userId, Long newsId) {
        if (userId <= 0 || newsId <= 0) return false;
        UserCollectNews queryCollectModel = new UserCollectNews();
        queryCollectModel.setNewsId(newsId);
        queryCollectModel.setUserId(userId);
        queryCollectModel.setDeleted(0);
        List<UserCollectNews> collectNewsList = userCollectNewsDAO.listRecord(queryCollectModel);
        return collectNewsList != null && collectNewsList.size() > 0 ? true : false;
    }

    /**
     * 检查用户是否阅读过文章
     *
     * @param userId
     * @param newsId
     * @return
     */
    public Boolean checkUserReadNews(Long userId, Long newsId) {
        if (userId <= 0 || newsId <= 0) return false;
        NewsRead queryNewsRead = new NewsRead();
        queryNewsRead.setNewsId(newsId);
        queryNewsRead.setUserId(userId);
        queryNewsRead.setDeleted(0);
        NewsRead newsRead = newsReadDAO.getRecord(queryNewsRead);
        return newsRead != null ? true : false;
    }
}
