/*
 * Dian.com Inc.
 * Copyright (c) 2004-2019 All Rights Reserved.
 */
package cn.sedu.moon.service;

import cn.sedu.moon.common.enums.GloryChangeBizTypeEnum;

/**
 *
 * @author ${wukong}
 * @version $Id: GloryService.java, v 0.1 2019年04月01日 3:07 PM Exp $
 */
public interface GloryService {

    /**
     * 添加荣誉值
     *
     * @param userId 用户id
     * @param value 荣誉值
     */
    void addGlory(Long userId, Long value, GloryChangeBizTypeEnum bizType);

    /**
     * 查询用户的荣誉值
     *
     * @param userId 用户id
     * @return 用户荣誉值
     */
    Long getUserGlory(Long userId);

}