package cn.sedu.moon.service;

import cn.sedu.moon.controller.request.subscriber.CheckSubscribeBestWeekRequest;
import cn.sedu.moon.controller.request.subscriber.ListBestWeekRequest;
import cn.sedu.moon.controller.request.subscriber.SubscribeBestWeekRequest;
import cn.sedu.moon.controller.request.subscriber.UnsubscribeBestWeekRequest;
import cn.sedu.moon.controller.response.subscriber.CheckSubscribeBestWeekResponse;
import cn.sedu.moon.controller.response.subscriber.ListBestWeekResponse;

import java.util.List;

/**
 * 订阅者服务接口
 *
 * @author guanzhong
 * @version : SubscriberServiceImpl.java, v 0.1 2019年11月13日16:21:26
 */
public interface SubscriberService {


    /**
     * 订阅每周精选
     *
     * @param request
     */
    void subscribeBestWeek(SubscribeBestWeekRequest request);


    /**
     * 取消订阅每周精选
     *
     * @param request
     */
    void unsubscribeBestWeek(UnsubscribeBestWeekRequest request);


    /**
     * 检查用户订阅每周精选状态
     *
     * @param request
     * @return
     */
    CheckSubscribeBestWeekResponse checkSubscribeBestWeek(CheckSubscribeBestWeekRequest request);


    /**
     * 获取每周精选文章列表
     *
     * @param request
     * @return
     */
    List<ListBestWeekResponse> listBestWeek(ListBestWeekRequest request);

}
