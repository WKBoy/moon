/*
 * Dian.com Inc.
 * Copyright (c) 2004-2019 All Rights Reserved.
 */
package cn.sedu.moon.service.impl;

import cn.hutool.core.date.DateUtil;
import cn.sedu.moon.common.constant.BizConstants;
import cn.sedu.moon.common.enums.ErrorCodeEnum;
import cn.sedu.moon.common.exception.BizException;
import cn.sedu.moon.dao.*;
import cn.sedu.moon.dao.model.*;
import cn.sedu.moon.service.MessageService;
import cn.sedu.moon.service.MessageTaskService;
import cn.sedu.moon.service.context.WxTemplateMessageContext;
import com.github.pagehelper.ISelect;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author ${wukong}
 * @version $Id: MessageTaskServiceImpl.java, v 0.1 2019年04月22日 10:17 PM Exp $
 */
@Service
public class MessageTaskServiceImpl implements MessageTaskService {

    @Autowired
    private MessageService messageService;

    @Autowired
    private AskDAO askDAO;

    @Autowired
    private AskAnswerDAO askAnswerDAO;

    @Autowired
    private AskAttentionDAO askAttentionDAO;

    @Autowired
    private UserDAO userDAO;

    @Autowired
    private NewsDAO newsDAO;

    @Autowired
    private StudentArchivesDAO studentArchivesDAO;

    @Override
    public void askPushTemplateMessage(final Long userId) {

        WxTemplateMessageContext context = new WxTemplateMessageContext();
        context.setUserId(userId);
        context.setTemplateId("3FqK3KHYcOd1FTri32-u5PWKnRhQCNlBPVOf-MhdJBM");

        Map<String, Map<String, String>> dataMap = new HashMap<>();

        Map<String, String> valueMap1 = new HashMap<>();
        valueMap1.put("value", "赶紧去看看大家的答案吧");
        dataMap.put("keyword1", valueMap1);

        Map<String, String> valueMap2 = new HashMap<>();
        valueMap2.put("value", "2018年的新年愿望");
        dataMap.put("keyword2", valueMap2);

        Map<String, String> valueMap3 = new HashMap<>();
        valueMap3.put("value", "13:32:35");
        dataMap.put("keyword3", valueMap3);

        context.setData(dataMap);
        context.setPage("");

        messageService.sendWechatAppTemplateMessage(context);
    }

    @Override
    public void askCreatedMessage(final Long askId) {

        // 查询问题信息
        Ask queryAskModel = new Ask();
        queryAskModel.setDeleted(0);
        queryAskModel.setId(askId);

        Ask ask = askDAO.getRecord(queryAskModel);
        if (ask == null) {
            throw new BizException(ErrorCodeEnum.SYS_EXP, "问题不存在");
        }

        WxTemplateMessageContext context = new WxTemplateMessageContext();
        context.setUserId(ask.getPublisher());
        context.setTemplateId("usnf9xIIL6AISvxNQcwDeR1NO0M6MojzQZZ5t2r");

        // 消息内容
        Map<String, Map<String, String>> dataMap = new HashMap<>();

        // 问题title
        Map<String, String> valueMap1 = new HashMap<>();
        valueMap1.put("value", ask.getTitle());
        dataMap.put("keyword1", valueMap1);

        // 问题创建时间
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        String dateString = formatter.format(ask.getCreateTime());

        Map<String, String> valueMap2 = new HashMap<>();
        valueMap2.put("value", dateString);
        dataMap.put("keyword2", valueMap2);

        context.setData(dataMap);
        context.setPage("pages/index/questionDetail/questionDetail?id=" + askId);

        messageService.sendWechatAppTemplateMessage(context);
    }

    @Override
    @Async
    public void askReceivedAnswerMessage(final Long answerId) {

        // 查询答案信息
        AskAnswer queryAnswerModel = new AskAnswer();
        queryAnswerModel.setDeleted(0);
        queryAnswerModel.setId(answerId);

        AskAnswer answer = askAnswerDAO.getRecord(queryAnswerModel);
        if (answer == null) {
            throw new BizException(ErrorCodeEnum.SYS_EXP, "答案不存在");
        }

        // 查询问题信息
        Ask queryAskModel = new Ask();
        queryAskModel.setId(answer.getAskId());
        queryAskModel.setDeleted(0);

        Ask ask = askDAO.getRecord(queryAskModel);
        if (ask == null) {
            throw new BizException(ErrorCodeEnum.SYS_EXP, "问题不存在");
        }

        // 向发布问题的人发送消息
        sendAnswerAskMessage(ask.getPublisher(), ask, answer);

        // 向关注问题的人发送消息
        AskAttention queryAttentionModel = new AskAttention();
        queryAttentionModel.setAskId(ask.getId());
        queryAttentionModel.setDeleted(0);

        List<AskAttention> attentionList = askAttentionDAO.listRecord(queryAttentionModel);
        if (attentionList != null && attentionList.size() > 0) {
            for (AskAttention askAttention : attentionList) {
                sendAnswerAskMessage(askAttention.getAttentionUserId(), ask, answer);
            }
        }
    }

    @Override
    public void askChangeMessage(final Long userId) {

    }

    /**
     * 向指定的用户发送问题被回复的消息
     *
     * @param userId userId
     * @param ask    问题
     * @param answer 答案
     */
    public void sendAnswerAskMessage(Long userId, Ask ask, AskAnswer answer) {

        WxTemplateMessageContext context = new WxTemplateMessageContext();
        context.setUserId(userId);
        context.setTemplateId("LSE3k3_Xgwtq-evMANmw7UOLo5pGiaIHIfIf_K8UpSQ");

        // 消息内容
        Map<String, Map<String, String>> dataMap = new HashMap<>();

        // 问题title
        Map<String, String> valueMap1 = new HashMap<>();
        valueMap1.put("value", ask.getTitle());
        dataMap.put("keyword1", valueMap1);

        // 答案内容
        Map<String, String> valueMap2 = new HashMap<>();
        valueMap2.put("value", answer.getContent());
        dataMap.put("keyword2", valueMap2);

        // 问答创建时间
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        String dateString = formatter.format(answer.getCreateTime());

        Map<String, String> valueMap3 = new HashMap<>();
        valueMap3.put("value", dateString);
        dataMap.put("keyword3", valueMap3);

        context.setData(dataMap);
        context.setPage("pages/index/questionDetail/questionDetail?id=" + ask.getId());

        messageService.sendWechatAppTemplateMessage(context);
    }


    /**
     * 每周精选推送
     *
     * @param userId
     */
    @Override
    public void bestWeekPushTemplateMessage(final Long userId) {


        WxTemplateMessageContext context = new WxTemplateMessageContext();
        context.setUserId(userId);
        context.setTemplateId(BizConstants.WX_MESSAGE_PUSH_TEMPLATE_ID_BESTWEEK);

        Map<String, Map<String, String>> dataMap = new HashMap<>();


        Map<String, String> valueMap1 = new HashMap<>();
        valueMap1.put("value", "每周精选");
        dataMap.put("keyword1", valueMap1);

        Map<String, String> valueMap2 = new HashMap<>();
        valueMap2.put("value", "【每周精选】专栏，专家推荐升学必读");
        dataMap.put("keyword2", valueMap2);

        Map<String, String> valueMap3 = new HashMap<>();
        valueMap3.put("value", "13:32:35");
        dataMap.put("keyword3", valueMap3);

        // TODO 添加小程序页面
        context.setData(dataMap);
        context.setPage("");

        messageService.sendWechatAppTemplateMessage(context);
    }

    @Override
    public void pushNewsNoticeMessage(final NewsNotice newsNotice) {

        // 获取用户列表
        User queryUserModel = new User();
        queryUserModel.setDeleted(0);
        queryUserModel.setMock(0);

        Integer total = userDAO.countRecord(queryUserModel);
        if (total == null || total == 0) {
            return;
        }

        // 查询文章数据
        News queryNewsModel = new News();
        queryNewsModel.setId(newsNotice.getNewsId());
        queryNewsModel.setDeleted(0);

        News news = newsDAO.getRecord(queryNewsModel);
        if (news == null) {
            return;
        }

        int offset = 0;
        int pageSize = 20;

        while (offset < total) {

            Page<User> page = PageHelper.offsetPage(offset, pageSize).doSelectPage(new ISelect() {
                @Override
                public void doSelect() {
                    userDAO.listRecord(queryUserModel);
                }
            });

            if (page.size() > 0) {
                for (User user : page.getResult()) {

                    // 获取档案信息
                    StudentArchives queryArchiveModel = new StudentArchives();
                    queryArchiveModel.setUserId(user.getId());
                    queryArchiveModel.setDeleted(0);
                    List<StudentArchives> archivesList = studentArchivesDAO.listRecord(queryArchiveModel);
                    if (CollectionUtils.isEmpty(archivesList)) {
                        continue;
                    }

                    if (!archivesList.get(0).getCityCode().equals(news.getCityCode())) {
                        continue;
                    }

                    WxTemplateMessageContext context = new WxTemplateMessageContext();

                    context.setUserId(user.getId());
                    context.setTemplateId(BizConstants.WX_MESSAGE_PUSH_TEMPLATE_ID_NEWS_NOTICE);

                    Map<String, Map<String, String>> dataMap = new HashMap<>();


                    Map<String, String> valueMap1 = new HashMap<>();
                    valueMap1.put("value", news.getTitle());
                    dataMap.put("keyword1", valueMap1);

                    // 往后加10分钟
                    Date afterDate = new Date(newsNotice.getPushTime().getTime() + 600000);
                    SimpleDateFormat smt = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    String afterDateString = smt.format(afterDate);

                    Map<String, String> valueMap2 = new HashMap<>();
                    valueMap2.put("value", afterDateString);
                    dataMap.put("keyword2", valueMap2);

                    Map<String, String> valueMap3 = new HashMap<>();
                    valueMap3.put("value", "专家答疑专场即将开始，赶紧联系【小鹿上学】加群哦～");
                    dataMap.put("keyword3", valueMap3);

                    context.setData(dataMap);
                    context.setPage("pages/index/newsDetail/newsDetail?id=" + news.getId());

                    messageService.sendWechatAppTemplateMessage(context);
                }
            }

            offset += pageSize;
        }
    }

    @Override
    public boolean pushNewNewsNotice(Long id, String title, Long userId, String schoolName) {

        WxTemplateMessageContext context = new WxTemplateMessageContext();
        context.setUserId(userId);
        context.setTemplateId(BizConstants.NEW_NEWS_NOTICE_PUSH_TEMPLATE_ID);

        Map<String, Map<String, String>> dataMap = new HashMap<>();

        Map<String, String> valueMap1 = new HashMap<>();
        valueMap1.put("value", schoolName);
        dataMap.put("keyword1", valueMap1);

        Map<String, String> valueMap2 = new HashMap<>();
        valueMap2.put("value", title);
        dataMap.put("keyword2", valueMap2);

        context.setData(dataMap);
        context.setPage("pages/index/newsDetail/newsDetail?id="+id);

        return messageService.sendWechatAppTemplateMessage(context);
    }

    @Override
    public void pushNewCommentNotice(Long schoolId, String comment, Long userId, String schoolName) {
        WxTemplateMessageContext context = new WxTemplateMessageContext();
        context.setUserId(userId);
        context.setTemplateId(BizConstants.NEW_NEWS_NOTICE_PUSH_TEMPLATE_ID);

        Map<String, Map<String, String>> dataMap = new HashMap<>();

        Map<String, String> valueMap1 = new HashMap<>();
        valueMap1.put("value", schoolName);
        dataMap.put("keyword1", valueMap1);

        Map<String, String> valueMap2 = new HashMap<>();
        valueMap2.put("value", comment);
        dataMap.put("keyword2", valueMap2);

        context.setData(dataMap);
        context.setPage("pages/school/index?id="+schoolId);

        messageService.sendWechatAppTemplateMessage(context);
    }

    @Override
    public void pushHelpToParentNotice(Long schoolId, String schoolName, Long parentId, String userName) {
        WxTemplateMessageContext context = new WxTemplateMessageContext();
        context.setUserId(parentId);
        context.setTemplateId(BizConstants.COMMENT_HELP_NOTICE_PUSH_TEMPLATE_ID);

        Map<String, Map<String, String>> dataMap = new HashMap<>();

        Map<String, String> valueMap1 = new HashMap<>();
        valueMap1.put("value", "多位家长想邀请您，对" + schoolName + "提出宝贵的评价。评价学校，将奖励荣誉值+2");
        dataMap.put("keyword1", valueMap1);

        Map<String, String> valueMap2 = new HashMap<>();
        valueMap2.put("value", userName);
        dataMap.put("keyword2", valueMap2);

        context.setData(dataMap);
        context.setPage("pages/school/index?id="+schoolId);

        messageService.sendWechatAppTemplateMessage(context);
    }

    @Override
    public void pushAskConcernUserNotice(Long askId, String title, Long userId, String schoolName) {
        WxTemplateMessageContext context = new WxTemplateMessageContext();
        context.setUserId(userId);
        context.setTemplateId(BizConstants.NEW_NEWS_NOTICE_PUSH_TEMPLATE_ID);

        Map<String, Map<String, String>> dataMap = new HashMap<>();

        Map<String, String> valueMap1 = new HashMap<>();
        valueMap1.put("value", schoolName);
        dataMap.put("keyword1", valueMap1);

        Map<String, String> valueMap2 = new HashMap<>();
        valueMap2.put("value", title);
        dataMap.put("keyword2", valueMap2);

        context.setData(dataMap);
        context.setPage("pages/index/questionDetail/questionDetail?id="+askId);

        messageService.sendWechatAppTemplateMessage(context);
    }

    @Override
    public void pushRegisterSuccessMsg(String msg, Long userId, String cityName, Date date) {
        WxTemplateMessageContext context = new WxTemplateMessageContext();
        context.setUserId(userId);
        context.setTemplateId(BizConstants.REGISTER_SUCCESS_PUSH_TEMPLATE_ID);

        Map<String, Map<String, String>> dataMap = new HashMap<>();

        Map<String, String> valueMap1 = new HashMap<>();
        valueMap1.put("value", msg);
        dataMap.put("keyword1", valueMap1);

        Map<String, String> valueMap2 = new HashMap<>();
        valueMap2.put("value", cityName);
        dataMap.put("keyword2", valueMap2);

        Map<String, String> valueMap3 = new HashMap<>();
        valueMap3.put("value", DateUtil.formatDateTime(date));
        dataMap.put("keyword2", valueMap3);

        context.setData(dataMap);
        context.setPage("pages/index/questionDetail/questionDetail?id="+userId);

        messageService.sendWechatAppTemplateMessage(context);
    }

    @Override
    public void pushNotArchiveMsg(String userId) {
        WxTemplateMessageContext context = new WxTemplateMessageContext();
        context.setUserId(Long.parseLong(userId));
        context.setTemplateId(BizConstants.NOT_ARCHIVE_TEMPLATE_ID);

        Map<String, Map<String, String>> dataMap = new HashMap<>();

        Map<String, String> valueMap1 = new HashMap<>();
        valueMap1.put("value", "小鹿上学");
        dataMap.put("keyword1", valueMap1);

        Map<String, String> valueMap2 = new HashMap<>();
        valueMap2.put("value", "2020年孩子升学读书最新资讯播报");
        dataMap.put("keyword2", valueMap2);

        context.setData(dataMap);
        // todo 跳转页面未填写
        context.setPage("pages/index/questionDetail/questionDetail?id="+userId);

        messageService.sendWechatAppTemplateMessage(context);
    }

    @Override
    public void pushArchiveNotReadNewsMsg(Long userId, Long newsId, String schoolName, String title) {
        WxTemplateMessageContext context = new WxTemplateMessageContext();
        context.setUserId(userId);
        context.setTemplateId(BizConstants.ARCHIVE_BUT_NOT_READ_NEWS_TEMPLATE_ID);

        Map<String, Map<String, String>> dataMap = new HashMap<>();

        Map<String, String> valueMap1 = new HashMap<>();
        valueMap1.put("value", schoolName);
        dataMap.put("keyword1", valueMap1);

        Map<String, String> valueMap2 = new HashMap<>();
        valueMap2.put("value", title);
        dataMap.put("keyword2", valueMap2);

        context.setData(dataMap);
        // todo 跳转页面未填写
        context.setPage("pages/index/newsDetail/newsDetail?id="+newsId);

        messageService.sendWechatAppTemplateMessage(context);
    }
}