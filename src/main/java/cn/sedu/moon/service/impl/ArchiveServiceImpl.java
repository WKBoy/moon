/*
 * Dian.com Inc.
 * Copyright (c) 2004-2018 All Rights Reserved.
 */
package cn.sedu.moon.service.impl;

import cn.sedu.moon.common.cache.CacheKey;
import cn.sedu.moon.common.constant.BizConstants;
import cn.sedu.moon.common.enums.AreaLevelEnum;
import cn.sedu.moon.common.enums.ErrorCodeEnum;
import cn.sedu.moon.common.exception.BizException;
import cn.sedu.moon.controller.request.district.UserAddDistrictRequest;
import cn.sedu.moon.controller.response.ArchiveChooseSchoolInfoResponse;
import cn.sedu.moon.controller.response.ArchiveDescResponse;
import cn.sedu.moon.controller.response.DistrictResponse;
import cn.sedu.moon.controller.response.GradeResponse;
import cn.sedu.moon.controller.response.GrowDirectionResponse;
import cn.sedu.moon.controller.response.QueryGrowDirectionResponse;
import cn.sedu.moon.controller.response.StudentArchiveResponse;
import cn.sedu.moon.controller.response.district.UserAddDistrictResponse;
import cn.sedu.moon.dao.*;
import cn.sedu.moon.dao.model.*;
import cn.sedu.moon.service.ArchiveService;
import cn.sedu.moon.service.DataCacheService;
import cn.sedu.moon.service.context.ArchiveReportContext;
import cn.sedu.moon.service.context.QueryDistrictContext;
import com.alibaba.fastjson.JSON;
import com.github.pagehelper.ISelect;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.redisson.api.RLock;
import org.redisson.api.RedissonClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * 学生档案的实现类
 *
 * @author ${wukong}
 * @version $Id: ArchiveServiceImpl.java, v 0.1 2018年12月10日 10:05 PM Exp $
 */
@Service
@Slf4j
public class ArchiveServiceImpl implements ArchiveService {

    @Autowired
    private StudentGradeDAO gradeDAO;

    @Autowired
    private StudentArchivesDAO archivesDAO;

    @Autowired
    private GrowDirectionDAO growDirectionDAO;

    @Autowired
    private SchoolDAO schoolDAO;

    @Autowired
    private DistrictGdDAO districtGdDAO;

    @Autowired
    private StuGrowDirectionDAO stuGrowDirectionDAO;

    @Autowired
    private AreaDataDAO areaDataDAO;

    @Autowired
    private DataCacheService cacheService;

    @Resource
    private RedissonClient redissonClient;

    @Override
    public StudentArchiveResponse queryArchive(final Long userId) {

        StudentArchives queryModel = new StudentArchives();
        queryModel.setUserId(userId);
        queryModel.setDeleted(0);
        StudentArchives archives = archivesDAO.getRecord(queryModel);
        if (archives == null) {
            throw new BizException(ErrorCodeEnum.SYS_EXP, "查询不到档案信息");
        }

        StudentArchiveResponse response = new StudentArchiveResponse();

        response.setSchoolId(archives.getSchoolId());
        response.setSchoolName(archives.getSchoolName());

        response.setProvinceCode(archives.getProvinceCode());
        response.setProvinceName(archives.getProvinceName());

        response.setCityCode(archives.getCityCode());
        response.setCityName(archives.getCityName());

        response.setAreaCode(archives.getAreaCode());
        response.setAreaName(archives.getAreaName());

        response.setDistrictId(archives.getDistrictHouseId());
        response.setDistrictName(archives.getDistrictHouseName());

        response.setGradeId(archives.getGradeId());
        response.setGrade(archives.getGrade());

        response.setGender(archives.getGender());

        return response;
    }

    @Override
    public ArchiveChooseSchoolInfoResponse queryChooseSchoolInfo(final Long userId) {

        ArchiveChooseSchoolInfoResponse response = new ArchiveChooseSchoolInfoResponse();

        // 住宅地址，学校属性
        StudentArchives queryModel = new StudentArchives();
        queryModel.setUserId(userId);
        queryModel.setDeleted(0);
        StudentArchives archives = archivesDAO.getRecord(queryModel);
        if (archives == null) {
            throw new BizException(ErrorCodeEnum.SYS_EXP, "查询不到档案信息");
        }

        response.setDistrictId(archives.getDistrictHouseId());
        response.setDistrictName(archives.getDistrictHouseName());
        response.setSchoolProperty(archives.getSchoolProperty());

        // 培养方向
        StuGrowDirection queryGrowDirectionModel = new StuGrowDirection();
        queryGrowDirectionModel.setUserId(userId);
        queryGrowDirectionModel.setDeleted(0);
        List<StuGrowDirection> growDirectionList = stuGrowDirectionDAO.listRecord(queryGrowDirectionModel);

        if (growDirectionList != null || growDirectionList.size() > 0) {
            List<GrowDirection> growList = new ArrayList<>();
            for (StuGrowDirection direction : growDirectionList) {
                GrowDirection growDirection = new GrowDirection();
                growDirection.setId(direction.getId());
                growDirection.setTitle(direction.getGrowDirectionTitle());
                growList.add(growDirection);
            }

            response.setGrowDirectionList(growList);
        }

        return response;
    }

    @Override
    public List<GradeResponse> queryGradeList() {
        List<StudentGrade> gradeList = gradeDAO.listRecord(new StudentGrade());

        if (gradeList != null && gradeList.size() > 0) {
            List<GradeResponse> responseList = new ArrayList<>();
            for (StudentGrade grade : gradeList) {
                GradeResponse response = new GradeResponse();
                response.setId(grade.getId());
                response.setTitle(grade.getTitle());
                response.setValue(grade.getId());
                responseList.add(response);
            }

            return responseList;
        }

        return new ArrayList<>();
    }

    @Override
    public void reportBaseInfo(final ArchiveReportContext context) {

        RLock lock = redissonClient.getLock(CacheKey.REPORT_ARCHIVE_LOCK_KEY_PREFIX + context.getUserId());
        try {
            if (lock.tryLock(BizConstants.REDIS_LOCK_WAIT_SECONDS, BizConstants.REDIS_LOCK_RELEASE_SECONDS, TimeUnit.SECONDS)) {

                // 删除历史上报记录
                StudentArchives queryModel = new StudentArchives();
                queryModel.setUserId(context.getUserId());
                queryModel.setDeleted(0);
                List<StudentArchives> resultList = archivesDAO.listRecord(queryModel);

                StudentArchives currentArchive = null;

                if (resultList != null && resultList.size() > 0) {

                    currentArchive = resultList.get(0);

                    // 将记录逻辑删除
                    for (StudentArchives student : resultList) {
                        StudentArchives updateModel = new StudentArchives();
                        updateModel.setId(student.getId());
                        updateModel.setUpdateTime(new Date());
                        updateModel.setDeleted(1);
                        int result = archivesDAO.updateRecord(updateModel);
                        if (result == 0) {
                            throw new BizException(ErrorCodeEnum.PARAM_CHECK_EXP, "上报失败");
                        }
                    }
                }

                // 添加新的上报记录
                StudentArchives saveModel = new StudentArchives();
                saveModel.setUserId(context.getUserId());

                StudentGrade queryGradeModel = new StudentGrade();
                queryGradeModel.setId(context.getGrade());
                queryGradeModel.setDeleted(0);
                StudentGrade gradeResult = gradeDAO.getRecord(queryGradeModel);

                if (gradeResult == null) {
                    throw new BizException(ErrorCodeEnum.SYS_EXP, "年级信息有误");
                }

                saveModel.setGradeId(gradeResult.getId());
                saveModel.setGrade(gradeResult.getTitle());

                AreaData queryAreaModel = new AreaData();
                queryAreaModel.setDeleted(0);
                if (StringUtils.isNotBlank(context.getProvinceCode())) {
                    queryAreaModel.setAreaCode(context.getProvinceCode());
                    AreaData areaData = areaDataDAO.getRecord(queryAreaModel);
                    if (areaData != null) {
                        saveModel.setProvinceCode(areaData.getAreaCode());
                        saveModel.setProvinceName(areaData.getAreaName());
                    }
                }

                if (StringUtils.isNotBlank(context.getCityCode())) {
                    queryAreaModel.setAreaCode(context.getCityCode());
                    AreaData areaData = areaDataDAO.getRecord(queryAreaModel);
                    if (areaData != null) {
                        saveModel.setCityCode(areaData.getAreaCode());
                        saveModel.setCityName(areaData.getAreaName());
                    }
                }

                if (StringUtils.isNotBlank(context.getAreaCode())) {
                    queryAreaModel.setAreaCode(context.getAreaCode());
                    AreaData areaData = areaDataDAO.getRecord(queryAreaModel);
                    if (areaData != null) {
                        saveModel.setAreaCode(areaData.getAreaCode());
                        saveModel.setAreaName(areaData.getAreaName());
                    }
                }

                saveModel.setGender(context.getGender());

                School querySchoolModel = new School();
                querySchoolModel.setId(context.getSchoolId());
                querySchoolModel.setDeleted(0);
                School school = schoolDAO.getRecord(querySchoolModel);

                if (school == null) {
                    throw new BizException(ErrorCodeEnum.SYS_EXP, "学校信息有误");
                }
                saveModel.setSchoolId(school.getId());
                saveModel.setSchoolName(school.getName());

                saveModel.setCreateTime(new Date());
                saveModel.setUpdateTime(new Date());
                saveModel.setDeleted(0);

                // 小区信息
                DistrictGd queryDistrictModel = new DistrictGd();
                queryDistrictModel.setId(context.getDistrictId());
                DistrictGd districtGd = districtGdDAO.getRecord(queryDistrictModel);
                if (districtGd != null) {
                    saveModel.setDistrictHouseId(districtGd.getId());
                    saveModel.setDistrictHouseName(districtGd.getName());
                }

                if (currentArchive != null) {
                    //saveModel.setDistrictHouseName(currentArchive.getDistrictHouseName());
                    //saveModel.setDistrictHouseId(currentArchive.getDistrictHouseId());
                    saveModel.setSchoolProperty(currentArchive.getSchoolProperty());
                }

                int result = archivesDAO.saveRecord(saveModel);
                if (result == 0) {
                    throw new BizException(ErrorCodeEnum.PARAM_CHECK_EXP, "上报失败");
                }

                // 更新缓存推荐数据
                reloadNewsData(context.getUserId());
                reloadSchoolRecommendData(context.getUserId());
                cacheService.clearSchoolTabData(context.getUserId());
            }
        } catch(Exception e){
            e.printStackTrace();
            throw new BizException(ErrorCodeEnum.SYS_EXP, "变更荣誉值出错");
        } finally{
            if (lock.isLocked()) {
                lock.unlock();
            }
        }

    }

    @Async
    void reloadNewsData(Long userId) {
        cacheService.reloadNewsCache(userId);
        log.info("刷新用户新闻缓存数据完成 userId is " + userId);

    }

    @Async
    void reloadSchoolRecommendData(Long userId) {
        cacheService.reloadRecommendSchoolCache(userId);
        log.info("刷新用户推荐缓存数据完成 userId is " + userId);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void report(final ArchiveReportContext context) {

        // 1 查询archive记录
        StudentArchives queryArchiveModel = new StudentArchives();
        queryArchiveModel.setUserId(context.getUserId());
        queryArchiveModel.setDeleted(0);

        StudentArchives queryResultModel = archivesDAO.getRecord(queryArchiveModel);
        if (queryResultModel == null) {
            throw new BizException(ErrorCodeEnum.PARAM_CHECK_EXP, "请先上报基本信息");
        }

        StudentArchives archives = new StudentArchives();
        archives.setId(queryResultModel.getId());
        archives.setName(context.getName());

        //// 年级
        //StudentGrade queryGradeModel = new StudentGrade();
        //queryGradeModel.setId(context.getGrade());
        //StudentGrade gradeResult = gradeDAO.getRecord(queryGradeModel);
        //
        //if (gradeResult == null) {
        //    throw new BizException(ErrorCodeEnum.SYS_EXP, "年级信息有误");
        //}
        //
        //archives.setGradeId(gradeResult.getId());
        //archives.setGrade(gradeResult.getTitle());
        //
        //// 学校
        //School querySchoolModel = new School();
        //querySchoolModel.setId(context.getSchoolId());
        //School schoolResult = schoolDAO.getRecord(querySchoolModel);
        //
        //if (schoolResult == null) {
        //    throw new BizException(ErrorCodeEnum.SYS_EXP, "学校信息有误");
        //}

        //archives.setSchoolId(schoolResult.getId());

        // 性别
        //archives.setGender(context.getGender());

        // 学校属性
        archives.setSchoolProperty(context.getSchoolProperty());

        // 培养方向
        if (context.getGrowDirectionList() != null && context.getGrowDirectionList().size() > 0) {
            // 删除用户过去的培养方向记录
            StuGrowDirection deletedModel = new StuGrowDirection();
            deletedModel.setUserId(context.getUserId());
            deletedModel.setDeleted(1);
            deletedModel.setUpdateTime(new Date());
            stuGrowDirectionDAO.updateRecordByUserId(deletedModel);

            for (Long growDirectionId : context.getGrowDirectionList()) {

                GrowDirection queryModel = new GrowDirection();
                queryModel.setId(growDirectionId);
                queryModel.setDeleted(0);
                GrowDirection growDirection = growDirectionDAO.getRecord(queryModel);
                if (growDirection == null) {
                    throw new BizException(ErrorCodeEnum.SYS_EXP, "学生培养方向信息有误");
                }

                StuGrowDirection stuGrowDirection = new StuGrowDirection();
                stuGrowDirection.setCreateTime(new Date());
                stuGrowDirection.setUpdateTime(new Date());
                stuGrowDirection.setDeleted(0);
                stuGrowDirection.setUserId(context.getUserId());
                stuGrowDirection.setGrowDirectionId(growDirectionId);
                stuGrowDirection.setGrowDirectionTitle(growDirection.getTitle());

                int result = stuGrowDirectionDAO.saveRecord(stuGrowDirection);
                if (result == 0) {
                    throw new BizException(ErrorCodeEnum.SYS_EXP, "保存学生培养信息失败");
                }
            }
        }

        // 小区信息
        //DistrictGd queryDistrict = new DistrictGd();
        //queryDistrict.setId(context.getDistrictId());
        //queryDistrict.setDeleted(0);
        //DistrictGd district = districtGdDAO.getRecord(queryDistrict);
        //if (district != null) {
        //    archives.setDistrictHouseId(district.getId());
        //    archives.setDistrictHouseName(district.getName());
        //}

        archives.setUpdateTime(new Date());
        int result = archivesDAO.updateRecord(archives);
        if (result == 0) {
            throw new BizException(ErrorCodeEnum.SYS_EXP, "更新信息失败" + JSON.toJSON(archives));
        }

        // 更新缓存推荐数据
        reloadNewsData(context.getUserId());
        reloadSchoolRecommendData(context.getUserId());
    }

    @Override
    public List<DistrictResponse> districtNearby(final QueryDistrictContext context) {

        // 根据关键字搜索
        Page<DistrictGd> page;
        if (StringUtils.isNotBlank(context.getKeyName())) {

            page = PageHelper.offsetPage(context.getOffset(), context.getPageSize()).doSelectPage(new ISelect() {
                @Override
                public void doSelect() {
                    districtGdDAO.queryByKeyName(context.getKeyName());
                }
            });

        } else {
            DistrictGd queryDistrictModel = new DistrictGd();

            // 根据用户上报的区信息，查询小区
            StudentArchives queryArchiveModel = new StudentArchives();
            queryArchiveModel.setUserId(context.getUserId());
            queryArchiveModel.setDeleted(0);
            List<StudentArchives> archivesList = archivesDAO.listRecord(queryArchiveModel);
            if (archivesList != null && archivesList.size() > 0) {
                //queryDistrictModel.set
                queryDistrictModel.setArea(archivesList.get(0).getAreaName());
            }

            // 如果没有传关键字，则全部返回
            page = PageHelper.offsetPage(context.getOffset(), context.getPageSize()).doSelectPage(new ISelect() {
                @Override
                public void doSelect() {
                    districtGdDAO.listRecord(queryDistrictModel);
                }
            });
        }

        // 构造response
        if (page != null && page.size() > 0) {
            List<DistrictResponse> responseList = new ArrayList<>();
            for (DistrictGd district : page.getResult()) {
                DistrictResponse response = new DistrictResponse();
                response.setName(district.getName());
                response.setDistrictId(district.getId());
                response.setValue(district.getId());
                responseList.add(response);
            }

            return responseList;
        }

        return new ArrayList<>();
    }

    @Override
    public List<DistrictResponse> districtNearbyArea(final QueryDistrictContext context) {

        // 根据关键字搜索
        Page<DistrictGd> page;
        if (StringUtils.isNotBlank(context.getKeyName())) {

            page = PageHelper.offsetPage(context.getOffset(), context.getPageSize()).doSelectPage(new ISelect() {
                @Override
                public void doSelect() {
                    districtGdDAO.queryByKeyName(context.getKeyName());
                }
            });

        } else {
            DistrictGd queryDistrictModel = new DistrictGd();

            // 根据areaCode 查询areaName
            AreaData queryAreaInfoModel = new AreaData();
            queryAreaInfoModel.setAreaCode(context.getAreaCode());
            List<AreaData> areaDataList = areaDataDAO.listRecord(queryAreaInfoModel);
            if (areaDataList != null && areaDataList.size() > 0) {
                queryDistrictModel.setArea(areaDataList.get(0).getAreaName());
            }

            // 如果没有传关键字，则全部返回
            page = PageHelper.offsetPage(context.getOffset(), context.getPageSize()).doSelectPage(new ISelect() {
                @Override
                public void doSelect() {
                    districtGdDAO.listRecord(queryDistrictModel);
                }
            });
        }

        // 构造response
        if (page != null && page.size() > 0) {
            List<DistrictResponse> responseList = new ArrayList<>();
            for (DistrictGd district : page.getResult()) {
                DistrictResponse response = new DistrictResponse();
                response.setName(district.getName());
                response.setDistrictId(district.getId());
                response.setValue(district.getId());
                responseList.add(response);
            }

            return responseList;
        }

        return new ArrayList<>();
    }

    @Override
    public QueryGrowDirectionResponse growDirectionList() {

        QueryGrowDirectionResponse res = new QueryGrowDirectionResponse();

        GrowDirection queryModel = new GrowDirection();
        List<GrowDirection> resultList = growDirectionDAO.listRecord(queryModel);
        if (resultList != null && resultList.size() > 0) {
            List<GrowDirectionResponse> responseList = new ArrayList<>();
            for (GrowDirection direction : resultList) {
                GrowDirectionResponse response = new GrowDirectionResponse();
                response.setId(direction.getId());
                response.setValue(direction.getId());
                response.setName(direction.getTitle());
                responseList.add(response);
            }

            res.setList(responseList);
        }

        res.setContent("不同学校培养孩子的方向会有所不同");
        return res;
    }

    @Override
    public ArchiveDescResponse getAnswerDesc(final Long userId) {

        Map<String, Double> hzPeopleMap = new HashMap<>();
        // 萧山
        hzPeopleMap.put("330109", 0.175);
        // 余杭区
        hzPeopleMap.put("330110", 0.156);
        // 江干区
        hzPeopleMap.put("330104", 0.119);
        // 西湖区
        hzPeopleMap.put("330106", 0.090);
        // 富阳区
        hzPeopleMap.put("330111", 0.078);
        // 临安区
        hzPeopleMap.put("330185", 0.062);
        // 拱墅区
        hzPeopleMap.put("330105", 0.059);
        // 下城区
        hzPeopleMap.put("330103", 0.056);
        // 建德市
        hzPeopleMap.put("330182", 0.047);
        // 桐庐县
        hzPeopleMap.put("330122", 0.045);
        // 淳安县
        hzPeopleMap.put("330127", 0.037);
        // 上城区
        hzPeopleMap.put("330102", 0.036);
        // 滨江区
        hzPeopleMap.put("330108", 0.035);

        // 查询用户选择的
        StudentArchives queryModel = new StudentArchives();
        queryModel.setUserId(userId);
        queryModel.setDeleted(0);
        StudentArchives queryResult = archivesDAO.getRecord(queryModel);
        if (queryResult == null) {
            throw new BizException(ErrorCodeEnum.SYS_EXP, "学生基本信息不存在");
        }

        if (!hzPeopleMap.containsKey(queryResult.getAreaCode())) {
            throw new BizException(ErrorCodeEnum.SYS_EXP, "学生所在区信息有误");
        }

        // 竞争情况
        // 每年报读小学人口增长率1.1,
        // ----> 计算哪一年报读小学
        String schoolYear = "";
        // 2017年杭州总报考人数
        BigDecimal hzCurrentTotal = new BigDecimal(946.8);
        BigDecimal improveEvertYear = new BigDecimal(1.1);
        BigDecimal hzTotal = new BigDecimal(0);

        // ----> 环比去年增长
        String improveLastYear = "1.1";

        // ----> 该区的报考人数
        String bkCountString = "";

        if (queryResult.getGradeId() == 1) {
            // 小班
            schoolYear = "2022";
            // 计算出预估的杭州市报考总人数
            hzTotal = hzCurrentTotal.multiply(improveEvertYear.pow(5));


        } else if (queryResult.getGradeId() == 2) {
            // 中班
            schoolYear = "2021";
            hzTotal = hzCurrentTotal.multiply(improveEvertYear.pow(4));

        } else if (queryResult.getGradeId() == 3) {
            // 大班
            schoolYear = "2020";
            hzTotal = hzCurrentTotal.multiply(improveEvertYear.pow(3));
        }

        // 计算出用户所在区报考人数
        BigDecimal bkCount = hzTotal.multiply(new BigDecimal(hzPeopleMap.get(queryResult.getAreaCode())));
        bkCount = bkCount.setScale(1, BigDecimal.ROUND_HALF_UP);
        bkCountString = bkCount.toString();

        // ----> 杭州小学预估将达到
        hzTotal = hzTotal.setScale(1, BigDecimal.ROUND_HALF_UP);
        String hzTotalString =  hzTotal.toString();

        String districtDesc = schoolYear + "年4月份即将报读小学，从2011年开始，杭城小学迎来入学高峰，" + schoolYear + "年报考人数"
                + "预估将增长到" + hzTotalString + "万，环比去年增长了" + improveLastYear + "%，预估将有" + bkCountString + "人与您的孩子"
                + "一起报考" + queryResult.getAreaName() + "小学。";


        // 公办民办差异：判断是否为杭州展示不同的文案,
        // 培养方向
        String schoolPropertyDesc;
        String growDirectionDesc;
        if ("330100".equals(queryResult.getCityCode())) {
            schoolPropertyDesc = "选择公办，则需在户籍所在地学校就近入学，要重点注意的是每年学区划分有可能会有所变动，并且有些热门学校的一表生也可能出现被调剂，学军小学今年82"
                    + "人被调剂；选择民办，则需要进行考试，去年绿城育华报考报考比例2000：248；民办学费会相对较高一些，例如云谷小学每学期学费48000元";
            growDirectionDesc = "不同学校培养孩子的方向会有所不同，\n"
                    + "* 云谷强调以人为本，培养孩子多元化个性化\n"
                    + "* 杭州上海世界外国语偏向培养孩子走向世界\n"
                    + "* 大关小学偏向声乐培养，重视孩子多才多艺\n"
                    + "另外，如果选择了双语版，国际版在面试时会包含英语口语部分，比如娃哈哈双语学校、绿城育华双语班!";
        } else {
            schoolPropertyDesc = "选择公办，则需在户籍所在地学校就近入学，要重点注意的是每年学区划分有可能会有所变动，并且有些热门学校的一表生也可能出现被调剂；选择民办，则需要进行面试考试，竞争也是非常激烈；另外，民办学费会相对较高一些，每学期几万学费都属常态";
            growDirectionDesc = "不同学校培养孩子的方向会有所不同，有些学校会强调以人为本，培养孩子多元化个性化；有些外国语学校会偏向培养孩子走向世界的能力，学校内有开设了双语班；有些学校偏向声乐培养，重视孩子多才多艺；在报考这些学校时，面试题内容会有所倾向。";
        }

        ArchiveDescResponse response = new ArchiveDescResponse();
        response.setSchoolPropertyDesc(schoolPropertyDesc);
        response.setGrowDirectionDesc(growDirectionDesc);
        response.setDistrictDesc(districtDesc);

        return response;
    }

    @Override
    public UserAddDistrictResponse userAddDistrict(final UserAddDistrictRequest request) {

        UserAddDistrictResponse response = new UserAddDistrictResponse();

        DistrictGd saveDistrictModel = new DistrictGd();
        saveDistrictModel.setName(request.getName());

        AreaData queryAreaModel = new AreaData();
        queryAreaModel.setAreaCode(request.getAreaCode());
        AreaData areaData = areaDataDAO.getRecord(queryAreaModel);
        if (areaData == null || !AreaLevelEnum.AREA.getCode().equals(areaData.getAreaLevel())) {
            throw new BizException(ErrorCodeEnum.SYS_EXP, "省市区信息有误");
        }

        AreaData queryCityModel = new AreaData();
        queryCityModel.setAreaCode(areaData.getParentCode());
        AreaData cityData = areaDataDAO.getRecord(queryCityModel);
        if (cityData == null || !AreaLevelEnum.CITY.getCode().equals(cityData.getAreaLevel())) {
            throw new BizException(ErrorCodeEnum.SYS_EXP, "省市区信息有误");
        }

        AreaData queryProvinceModel = new AreaData();
        queryProvinceModel.setAreaCode(cityData.getParentCode());
        AreaData provinceData = areaDataDAO.getRecord(queryProvinceModel);
        if (provinceData == null || !AreaLevelEnum.PROVINCE.getCode().equals(provinceData.getAreaLevel())) {
            throw new BizException(ErrorCodeEnum.SYS_EXP, "省市区信息有误");
        }

        saveDistrictModel.setProvinceCode(provinceData.getAreaCode());
        saveDistrictModel.setProvince(provinceData.getAreaName());
        saveDistrictModel.setCityCode(cityData.getAreaCode());
        saveDistrictModel.setCity(cityData.getAreaName());
        saveDistrictModel.setAreaCode(areaData.getAreaCode());
        saveDistrictModel.setArea(areaData.getAreaName());


        saveDistrictModel.setAddress(request.getAddress());
        saveDistrictModel.setLat(request.getLat());
        saveDistrictModel.setLng(request.getLng());

        saveDistrictModel.setCreateTime(new Date());
        saveDistrictModel.setUpdateTime(new Date());
        saveDistrictModel.setDeleted(0);

        saveDistrictModel.setUserAdd(1);

        int result = districtGdDAO.saveRecord(saveDistrictModel);
        if (result == 0) {
            throw new BizException(ErrorCodeEnum.SYS_EXP, "保存小区信息失败");
        }

        response.setId(saveDistrictModel.getId());

        return response;
    }
}