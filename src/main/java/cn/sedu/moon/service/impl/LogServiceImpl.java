/*
 * Dian.com Inc.
 * Copyright (c) 2004-2019 All Rights Reserved.
 */
package cn.sedu.moon.service.impl;

import cn.sedu.moon.common.enums.ErrorCodeEnum;
import cn.sedu.moon.common.exception.BizException;
import cn.sedu.moon.dao.LogNewsDetailDAO;
import cn.sedu.moon.dao.model.LogNewsDetail;
import cn.sedu.moon.service.LogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;

/**
 * @author wukong
 * @version : LogServiceImpl.java, v 0.1 2019年08月24日 16:49 Exp $
 */
@Service
public class LogServiceImpl implements LogService {

    @Autowired
    private LogNewsDetailDAO logNewsDetailDAO;

    @Override
    public void logNewDetail(final Long newsId, final Long userId) {

        LogNewsDetail saveModel = new LogNewsDetail();
        saveModel.setNewsId(newsId);
        saveModel.setUserId(userId);
        saveModel.setCreateTime(new Date());
        saveModel.setUpdateTime(new Date());

        int result = logNewsDetailDAO.saveRecord(saveModel);

        if (result == 0) {
            throw new BizException(ErrorCodeEnum.SYS_EXP, "保存失败");
        }
    }
}