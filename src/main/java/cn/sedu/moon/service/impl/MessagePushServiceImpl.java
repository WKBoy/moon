package cn.sedu.moon.service.impl;

import cn.sedu.moon.dao.SubscriberBestWeekDAO;
import cn.sedu.moon.dao.model.SubscriberBestWeek;
import cn.sedu.moon.service.MessagePushService;
import cn.sedu.moon.service.MessageTaskService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

/**
 * 消息推送
 *
 * @author guanzhong
 * @version : MessagePushServiceImpl.java, v 0.1 2019年11月13日01:32:17
 */
@Service
@Slf4j
public class MessagePushServiceImpl implements MessagePushService {

    @Autowired
    private SubscriberBestWeekDAO subscriberBestWeekDAO;

    @Autowired
    private MessageTaskService messageTaskService;

    /**
     * 推送每周精选
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void pushBestNewsWeekMessage() {

        // 1、获取订阅精选文章用户列表
        // 待优化，用户量过大时候需要优化 TODO
        SubscriberBestWeek selectParam = new SubscriberBestWeek();
        List<SubscriberBestWeek> subscriberlist = subscriberBestWeekDAO.listRecord(selectParam);

        // 2、循环处理用户列表
        // 待优化，可使用消息队列 TODO
        for (SubscriberBestWeek item :subscriberlist){
            messageTaskService.bestWeekPushTemplateMessage(item.getUserId());
        }

    }

    /**
     * 用户想上的学校文章推送
     */
    @Override
    public boolean pushNewNewsNotice(Long id, String title, Long userId, String schoolName) {
        return messageTaskService.pushNewNewsNotice(id, title, userId, schoolName);
    }

    /**
     * 用户想上的学校评论推送
     */
    @Override
    public void pushNewCommentNotice(Long schoolId, String comment, Long userId, String schoolName) {
        messageTaskService.pushNewCommentNotice(schoolId, comment, userId, schoolName);
    }

    /**
     * 在校生家长评论通知
     * @param schoolId
     * @param schoolName
     * @param parentId
     * @param userName
     */
    @Override
    public void pushHelpToParentNotice(Long schoolId, String schoolName, Long parentId, String userName) {
        messageTaskService.pushHelpToParentNotice(schoolId, schoolName, parentId, userName);
    }

    /**
     * 学校问题有回应通知
     */
    @Override
    public void pushAskConcernUserNotice(Long askId, String title, Long userId, String schoolName) {
        messageTaskService.pushAskConcernUserNotice(askId, title, userId, schoolName);
    }

    @Override
    public void pushRegisterSuccessMsg(String msg, Long userId, String cityName, Date date) {
        messageTaskService.pushRegisterSuccessMsg(msg, userId, cityName, date);
    }

    @Override
    public void pushNotArchiveMsg(String userId) {
        messageTaskService.pushNotArchiveMsg(userId);
    }

    @Override
    public void pushArchiveNotReadNewsMsg(Long userId, Long newsId, String schoolName, String title) {
        messageTaskService.pushArchiveNotReadNewsMsg(userId, newsId, schoolName, title);
    }
}
