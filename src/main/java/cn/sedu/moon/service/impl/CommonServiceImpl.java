/*
 * Dian.com Inc.
 * Copyright (c) 2004-2018 All Rights Reserved.
 */
package cn.sedu.moon.service.impl;

import cn.sedu.moon.common.enums.ErrorCodeEnum;
import cn.sedu.moon.common.exception.BizException;
import cn.sedu.moon.common.util.OssHelper;
import cn.sedu.moon.controller.response.AreaNode;
import cn.sedu.moon.dao.AreaDataDAO;
import cn.sedu.moon.dao.model.AreaData;
import cn.sedu.moon.service.CommonService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 *
 * @author ${wukong}
 * @version $Id: CommonServiceImpl.java, v 0.1 2018年11月12日 5:05 PM Exp $
 */
@Service
@Slf4j
public class CommonServiceImpl implements CommonService {

    @Autowired
    private AreaDataDAO areaDataDAO;

    // level = 1 浙江省、北京市、上海市、广东省、四川省,
    // 黑龙江省，江苏省，福建省，山东省，河南省，湖北省，湖南

    private static final List<String> openProvinceCode = Arrays.asList("330000", "110000", "310000", "440000", "510000",
            "230000",
            "320000",
            "350000",
            "370000",
            "410000",
            "420000",
            "430000");
    // level = 2 杭州市、北京市、上海市、 衢州市、深圳市、广州市、成都市,
    // 哈尔滨 南京市 福州市 济南市 郑州市 武汉市 长沙市

    private static final List<String> openCityCode = Arrays.asList("330100", "110100", "310100", "330800", "440300", "440100", "510100",
            "230100",
            "320100",
            "350100",
            "370100",
            "410100",
            "420100",
            "430100");

    @Override
    public List<AreaNode> queryAreaInfo(final Integer areaLevel, final String parentCode) {

        AreaData areaData = new AreaData();
        areaData.setAreaLevel(areaLevel);
        areaData.setParentCode(parentCode);

        List<AreaData> resultList = areaDataDAO.listRecord(areaData);

        if (resultList != null && resultList.size() > 0) {

            List<AreaNode> resList = new ArrayList<>();
            for (AreaData data : resultList) {

                if (data.getAreaLevel() == 1 && openProvinceCode.contains(data.getAreaCode())) {
                    AreaNode node = new AreaNode();
                    node.setAreaCode(data.getAreaCode());
                    node.setAreaName(data.getAreaName());
                    node.setIsLeaf(false);
                    resList.add(node);

                } else if (data.getAreaLevel() == 2 && openCityCode.contains(data.getAreaCode())) {
                    AreaNode node = new AreaNode();
                    node.setAreaCode(data.getAreaCode());
                    node.setAreaName(data.getAreaName());
                    node.setIsLeaf(false);
                    resList.add(node);

                } else if (data.getAreaLevel() == 3) {
                    AreaNode node = new AreaNode();
                    node.setAreaCode(data.getAreaCode());
                    node.setAreaName(data.getAreaName());
                    node.setIsLeaf(data.getAreaLevel() == 3);
                    resList.add(node);
                }
            }

            return resList;
        }

        return null;
    }

    @Override
    public String uploadImage(final String originalFilename, final byte[] imageBytes, final String contentType, final long size) {
        try {
            BufferedImage image = ImageIO.read(new ByteArrayInputStream(imageBytes));
            Integer width = image.getWidth();
            Integer height = image.getHeight();
            String saveFileName = OssHelper.getSaveFileName(originalFilename, contentType, Long.toHexString(size), width, height);

            return OssHelper.uploadFile(saveFileName, new ByteArrayInputStream(imageBytes), contentType, size);
        } catch (Exception e) {
            log.error("[ImageUploadFailed]", e);
            throw new BizException(ErrorCodeEnum.UPLOAD_EXP);
        }
    }
}