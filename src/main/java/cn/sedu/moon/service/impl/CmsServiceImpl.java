/*
 * Dian.com Inc.
 * Copyright (c) 2004-2019 All Rights Reserved.
 */
package cn.sedu.moon.service.impl;

import java.io.UnsupportedEncodingException;
import java.util.List;

import cn.sedu.moon.common.constant.BizConstants;
import cn.sedu.moon.common.enums.ErrorCodeEnum;
import cn.sedu.moon.common.exception.BizException;
import cn.sedu.moon.common.util.AuthUtil;
import cn.sedu.moon.common.util.CacheKeyUtil;
import cn.sedu.moon.controller.request.cms.CmsLoginRequest;
import cn.sedu.moon.dao.CmsUserDAO;
import cn.sedu.moon.dao.model.CmsUser;
import cn.sedu.moon.service.CmsService;
import lombok.extern.slf4j.Slf4j;
import org.redisson.api.RBucket;
import org.redisson.api.RedissonClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author ${wukong}
 * @version $Id: CmsServiceImpl.java, v 0.1 2019年02月20日 7:44 PM Exp $
 */
@Slf4j
@Service
public class CmsServiceImpl implements CmsService {

    @Autowired
    private RedissonClient redissonClient;

    @Autowired
    private CmsUserDAO cmsUserDAO;

    @Override
    public String login(final CmsLoginRequest request) {

        CmsUser queryModel = new CmsUser();
        queryModel.setLoginName(request.getUsername());

        List<CmsUser> cmsUsers = cmsUserDAO.listRecord(queryModel);
        if (cmsUsers == null || cmsUsers.size() == 0) {
            throw new BizException(ErrorCodeEnum.SYS_EXP, "用户不存在");
        }

        CmsUser cmsUser = cmsUsers.get(0);
        if (!request.getPassword().equals(cmsUser.getPassword())) {
            throw new BizException(ErrorCodeEnum.SYS_EXP, "密码错误");
        }

        // 生成token
        // 根据用户 ID 生成token
        String token;
        Long userId = cmsUser.getId();

        try {
            token = AuthUtil.generateToken(userId + "");
        } catch (UnsupportedEncodingException e) {
            log.error("生成token 异常, userId is {}, e is {}", userId, e);
            throw new BizException(ErrorCodeEnum.SYS_EXP);
        }

        // Cache
        String cacheKey = CacheKeyUtil.genUserCmsTokenKey(userId);
        RBucket<String> bucket = redissonClient.getBucket(cacheKey);
        bucket.set(token, BizConstants.USER_TOKEN_TIME_TO_LIVE, BizConstants.USER_TOKEN_LIVE_TIME_UNIT);

        return token;
    }
}