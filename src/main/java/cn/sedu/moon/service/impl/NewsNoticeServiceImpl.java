/*
 * Dian.com Inc.
 * Copyright (c) 2004-2019 All Rights Reserved.
 */
package cn.sedu.moon.service.impl;

import cn.sedu.moon.common.enums.ErrorCodeEnum;
import cn.sedu.moon.common.exception.BizException;
import cn.sedu.moon.dao.NewsNoticeDAO;
import cn.sedu.moon.dao.NewsNoticeSubscribeDAO;
import cn.sedu.moon.dao.model.NewsNotice;
import cn.sedu.moon.dao.model.NewsNoticeSubscribe;
import cn.sedu.moon.service.NewsNoticeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

/**
 * @author wukong
 * @version : NewsNoticeServiceImpl.java, v 0.1 2019年10月27日 12:43 上午 Exp $
 */
@Service
public class NewsNoticeServiceImpl implements NewsNoticeService  {

    @Autowired
    private NewsNoticeDAO noticeDAO;

    @Autowired
    private NewsNoticeSubscribeDAO subscribeDAO;

    @Override
    public boolean checkUserSubscribe(final Long userId, final Long newsId) {

        NewsNotice queryNoticeModel = new NewsNotice();
        queryNoticeModel.setNewsId(newsId);
        queryNoticeModel.setDeleted(0);

        NewsNotice newsNotice = noticeDAO.getRecord(queryNoticeModel);
        if (newsNotice == null) {
            return false;
        }

        NewsNoticeSubscribe querySubscribeModel = new NewsNoticeSubscribe();
        querySubscribeModel.setUserId(userId);
        querySubscribeModel.setNoticeId(newsNotice.getId());
        querySubscribeModel.setDeleted(0);

        List<NewsNoticeSubscribe> newsNoticeSubscribeList = subscribeDAO.listRecord(querySubscribeModel);

        if (newsNoticeSubscribeList != null && newsNoticeSubscribeList.size() > 0) {
            return true;
        }

        return false;
    }

    @Override
    public NewsNotice getNoticeInfoByNewsId(final Long newsId) {

        NewsNotice queryNoticeModel = new NewsNotice();
        queryNoticeModel.setNewsId(newsId);
        queryNoticeModel.setDeleted(0);

        return noticeDAO.getRecord(queryNoticeModel);
    }

    @Override
    public void subscribeNotice(final Long userId, final Long noticeId) {

        NewsNoticeSubscribe querySubscribeModel = new NewsNoticeSubscribe();
        querySubscribeModel.setUserId(userId);
        querySubscribeModel.setNoticeId(noticeId);
        querySubscribeModel.setDeleted(0);

        List<NewsNoticeSubscribe> newsNoticeSubscribeList = subscribeDAO.listRecord(querySubscribeModel);

        if (newsNoticeSubscribeList != null && newsNoticeSubscribeList.size() > 0) {
            throw new BizException(ErrorCodeEnum.SYS_EXP, "已订阅");
        }

        NewsNoticeSubscribe saveModel = new NewsNoticeSubscribe();
        saveModel.setUserId(userId);
        saveModel.setNoticeId(noticeId);
        saveModel.setDeleted(0);
        saveModel.setCreateTime(new Date());
        saveModel.setUpdateTime(new Date());

        int result = subscribeDAO.saveRecord(saveModel);
        if (result == 0) {
            throw new BizException(ErrorCodeEnum.SYS_EXP, "订阅失败");
        }
    }

    @Override
    public void addNotice(final NewsNotice addModel) {

        addModel.setCreateTime(new Date());
        addModel.setUpdateTime(new Date());
        addModel.setDeleted(0);

        int result = noticeDAO.saveRecord(addModel);
        if (result == 0) {
            throw new BizException(ErrorCodeEnum.SYS_EXP, "添加失败");
        }
    }

    @Override
    public void deleteNotice(final Long noticeId) {

        NewsNotice deleteModel = new NewsNotice();
        deleteModel.setId(noticeId);
        deleteModel.setDeleted(1);
        deleteModel.setUpdateTime(new Date());

        noticeDAO.updateRecord(deleteModel);
    }

    @Override
    public void updateNotice(final NewsNotice notice) {
        notice.setUpdateTime(new Date());
        noticeDAO.updateRecord(notice);
    }
}