/*
 * Dian.com Inc.
 * Copyright (c) 2004-2019 All Rights Reserved.
 */
package cn.sedu.moon.service.impl;

import cn.hutool.core.util.StrUtil;
import cn.sedu.moon.common.cache.CacheKey;
import cn.sedu.moon.common.constant.BizConstants;
import cn.sedu.moon.common.enums.ErrorCodeEnum;
import cn.sedu.moon.common.enums.MedalTypeEnum;
import cn.sedu.moon.common.exception.BizException;
import cn.sedu.moon.common.util.CacheKeyUtil;
import cn.sedu.moon.controller.request.AskEditRequest;
import cn.sedu.moon.controller.request.AskPublishRequest;
import cn.sedu.moon.controller.request.AskReportAnswerRequest;
import cn.sedu.moon.controller.request.AskSearchRequest;
import cn.sedu.moon.controller.request.QueryAskDetailRequest;
import cn.sedu.moon.controller.request.cms.ask.CmsAskReplyRequest;
import cn.sedu.moon.controller.request.cms.ask.CmsSearchAskRequest;
import cn.sedu.moon.controller.response.AskAnswerResponse;
import cn.sedu.moon.controller.response.AskDetailResponse;
import cn.sedu.moon.controller.response.AskPublishResponse;
import cn.sedu.moon.controller.response.AskReportAnswerResponse;
import cn.sedu.moon.controller.response.AskResponse;
import cn.sedu.moon.controller.response.cms.ask.CmsAskDetailResponse;
import cn.sedu.moon.controller.response.cms.ask.CmsAskResponse;
import cn.sedu.moon.controller.response.cms.ask.CmsSearchAskResponse;
import cn.sedu.moon.dao.*;
import cn.sedu.moon.dao.model.*;
import cn.sedu.moon.service.AskService;
import cn.sedu.moon.service.MessagePushService;
import cn.sedu.moon.service.MessageTaskService;
import cn.sedu.moon.thirdparty.WechatApi;
import cn.sedu.moon.thirdparty.response.WxMsgSecCheckResponse;
import com.alibaba.fastjson.JSON;
import com.github.pagehelper.ISelect;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.redisson.api.RBucket;
import org.redisson.api.RLock;
import org.redisson.api.RedissonClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * 问答模块的实现类
 *
 * @author ${wukong}
 * @version $Id: AskServiceImpl.java, v 0.1 2019年01月16日 11:35 AM Exp $
 */
@Service
@Slf4j
public class AskServiceImpl implements AskService {

    @Autowired
    private AskDAO askDAO;

    @Autowired
    private AskAnswerDAO askAnswerDAO;

    @Autowired
    private AskAttentionDAO askAttentionDAO;

    @Autowired
    private UserDAO userDAO;

    @Autowired
    private AskUsefulDAO usefulDAO;

    @Autowired
    private SopAnswerDAO sopAnswerDAO;

    @Autowired
    private UserMedalDAO userMedalDAO;

    @Autowired
    private MessageTaskService messageTaskService;

    @Autowired
    private WechatApi wechatApi;

    @Resource
    private UserCollectAskDAO userCollectAskDAO;

    @Resource
    private NewsAskDAO newsAskDAO;

    @Autowired
    private RedissonClient redissonClient;

    @Autowired
    private SchoolDAO schoolDAO;

    @Autowired
    private StuSchDAO stuSchDAO;

    @Autowired
    private MessagePushService messagePushService;

    @Autowired
    private StudentArchivesDAO archivesDAO;

    // 多线程服务
    private static ExecutorService executorService = new ThreadPoolExecutor(40, 100, 60L, TimeUnit.SECONDS, new LinkedBlockingQueue<>(100));

    @Override
    public AskPublishResponse askPublish(final AskPublishRequest request) {

        RLock lock = redissonClient.getLock(CacheKey.PUBLISH_ASK_LOCK_KEY_PREFIX + request.getUserId());
        try {
            if (lock.tryLock(BizConstants.REDIS_LOCK_WAIT_SECONDS, BizConstants.REDIS_LOCK_RELEASE_SECONDS, TimeUnit.SECONDS)) {

                WxMsgSecCheckResponse checkResponse = wechatApi.contentCheck(request.getTitle());

                if (checkResponse.getErrorCode() != 0) {
                    throw new BizException(ErrorCodeEnum.SYS_EXP, "标题有不合法的内容，请修改后再试试哦");
                }

                if (StringUtils.isNotBlank(request.getContent())) {
                    checkResponse = wechatApi.contentCheck(request.getContent());
                    if (checkResponse.getErrorCode() != 0) {
                        throw new BizException(ErrorCodeEnum.SYS_EXP, "问题有不合法的内容，请修改后再试试哦");
                    }
                }

                Ask saveModel = new Ask();
                saveModel.setPublisher(request.getUserId());
                saveModel.setTitle(request.getTitle());
                saveModel.setContent(request.getContent());
                saveModel.setCreateTime(new Date());
                saveModel.setUpdateTime(new Date());
                saveModel.setAdmin(0);
                saveModel.setDeleted(0);
                int result = askDAO.saveRecord(saveModel);
                if (result == 0) {
                    throw new BizException(ErrorCodeEnum.SYS_EXP, "保存问题失败");
                }

                AskPublishResponse response = new AskPublishResponse();
                response.setId(saveModel.getId());

                // 添加新闻的绑定关系
                if (request.getNewsId() != null) {

                    NewsAsk saveNewsAskModel = new NewsAsk();
                    saveNewsAskModel.setNewsId(request.getNewsId());
                    saveNewsAskModel.setAskId(saveModel.getId());

                    saveNewsAskModel.setOperator("");
                    saveNewsAskModel.setDeleted(0);
                    saveNewsAskModel.setCreateTime(new Date());
                    saveNewsAskModel.setUpdateTime(new Date());

                    int saveNewsAskResult = newsAskDAO.saveRecord(saveNewsAskModel);
                    if (saveNewsAskResult == 0) {
                        log.error("保存新闻问单数据失败, save model: {}", JSON.toJSONString(saveNewsAskModel));
                    }
                }

                // 发送通知
                messageTaskService.askCreatedMessage(saveModel.getId());
                executorService.submit(() -> setAskPublishSchoolList(request.getUserId(), request.getTitle()));
                return response;

            }
        } catch (Exception e) {
            throw new BizException(ErrorCodeEnum.SYS_EXP, "发布问题出错");
        } finally {
            if (lock.isLocked()) {
                lock.unlock();
            }
        }

        return null;
    }

    private void setAskPublishSchoolList(Long userId, String title) {
        List<School> schoolList = schoolDAO.listAllSchoolNameAndId();
        List<Long> schoolIdList = new ArrayList<>();
        for (School school : schoolList){
            if (title.contains(school.getName())){
                schoolIdList.add(school.getId());
            }
        }
        String cacheKey = CacheKeyUtil.genUserAskPublishKey(userId);
        RBucket<List<Long>> bucket = redissonClient.getBucket(cacheKey);
        List<Long> nowIdList = bucket.get();
        if (nowIdList == null){
            bucket.set(schoolIdList);
        } else {
            for (Long schoolId : schoolIdList){
                if (!nowIdList.contains(schoolId)){
                    nowIdList.add(schoolId);
                }
            }
            bucket.set(nowIdList);
        }
    }

    @Override
    public void useful(final Long answerId, final Long userId) {

        AskUseful queryModel = new AskUseful();
        queryModel.setUserId(userId);
        queryModel.setAnswerId(answerId);
        queryModel.setType(1);

        List<AskUseful> askUsefulList = usefulDAO.listRecord(queryModel);

        if (askUsefulList != null && askUsefulList.size() > 0) {
            throw new BizException(ErrorCodeEnum.SYS_EXP, "您已经点过有用了");
        }

        AskUseful saveModel = new AskUseful();
        saveModel.setUserId(userId);
        saveModel.setAnswerId(answerId);
        saveModel.setDeleted(0);
        saveModel.setCreateTime(new Date());
        saveModel.setUpdateTime(new Date());
        saveModel.setType(1);
        int result = usefulDAO.saveRecord(saveModel);
        if (result == 0) {
            throw new BizException(ErrorCodeEnum.SYS_EXP, "保存失败");
        }
    }

    @Override
    public void unUseful(final Long answerId, final Long userId) {
        AskUseful queryModel = new AskUseful();
        queryModel.setUserId(userId);
        queryModel.setAnswerId(answerId);
        queryModel.setType(1);

        AskUseful askUseful = usefulDAO.getRecord(queryModel);

        if (askUseful == null) {
            throw new BizException(ErrorCodeEnum.SYS_EXP, "您没有点击过有用");
        }

        AskUseful updateModel = new AskUseful();
        updateModel.setId(askUseful.getId());
        updateModel.setUpdateTime(new Date());
        updateModel.setType(2);
        int result = usefulDAO.updateRecord(updateModel);
        if (result == 0) {
            throw new BizException(ErrorCodeEnum.SYS_EXP, "保存失败");
        }
    }

    @Override
    public void edit(final AskEditRequest request) {

        // 1 只有发布者才能对问题进行编辑的判断
        Ask queryAskModel = new Ask();
        queryAskModel.setId(request.getId());
        queryAskModel.setPublisher(request.getUserId());

        Ask queryAskResult = askDAO.getRecord(queryAskModel);
        if (queryAskResult == null) {
            throw new BizException(ErrorCodeEnum.SYS_EXP, "只有问题发布者才能编辑问题哦");
        }

        // 内容合法性校验
        WxMsgSecCheckResponse checkResponse = wechatApi.contentCheck(request.getTitle());
        if (checkResponse.getErrorCode() != 0) {
            throw new BizException(ErrorCodeEnum.SYS_EXP, "标题有不合法的内容，请修改后再试试哦");
        }

        if (StringUtils.isNotBlank(request.getContent())) {
            checkResponse = wechatApi.contentCheck(request.getContent());
            if (checkResponse.getErrorCode() != 0) {
                throw new BizException(ErrorCodeEnum.SYS_EXP, "问题有不合法的内容，请修改后再试试哦");
            }
        }

        // 2 更改数据
        Ask updateAskModel = new Ask();
        updateAskModel.setId(request.getId());
        updateAskModel.setTitle(request.getTitle());
        updateAskModel.setContent(request.getContent());

        int result = askDAO.updateRecord(updateAskModel);
        if (result == 0) {
            throw new BizException(ErrorCodeEnum.SYS_EXP, "更新失败");
        }
    }

    @Override
    public List<AskResponse> search(final AskSearchRequest request) {

        Ask queryModel = new Ask();
        queryModel.setTitle(request.getKeyword());
        queryModel.setDeleted(0);

        List<Ask> askList = askDAO.listRecord(queryModel);
        if (askList != null && askList.size() > 0) {

            List<AskResponse> responseList = new ArrayList<>();
            for (Ask ask : askList) {
                AskResponse response = new AskResponse();
                response.setId(ask.getId());
                response.setTitle(ask.getTitle());
                response.setAnswerCount(queryAskAnswerCount(ask.getId()));
                response.setFollowCount(queryAskAttentionCount(ask.getId()));
                responseList.add(response);
            }

            return responseList;
        }

        return new ArrayList<>();
    }

    @Override
    public List<AskResponse> myAskList(final Long userId) {

        List<AskResponse> askResponseList = new ArrayList<>();

        Ask queryAskModel = new Ask();
        queryAskModel.setPublisher(userId);
        queryAskModel.setAdmin(0);
        List<Ask> queryAskResult = askDAO.listRecord(queryAskModel);
        if (queryAskResult != null && queryAskResult.size() > 0) {
            for (Ask ask : queryAskResult) {
                askResponseList.add(convertAskToResponse(ask));
            }
        }

        return askResponseList;
    }

    private AskResponse convertAskToResponse(Ask ask) {
        AskResponse askResponse = new AskResponse();
        askResponse.setId(ask.getId());
        askResponse.setTitle(ask.getTitle());
        askResponse.setAnswerCount(queryAskAnswerCount(ask.getId()));
        askResponse.setFollowCount(queryAskAttentionCount(ask.getId()));
        return askResponse;
    }

    @Override
    public List<AskResponse> myAttention(final Long userId) {

        List<AskResponse> askResponseList = new ArrayList<>();

        AskAttention queryAttentionModel = new AskAttention();
        queryAttentionModel.setAttentionUserId(userId);

        List<AskAttention> queryAttentionResult = askAttentionDAO.listRecord(queryAttentionModel);
        if (queryAttentionResult != null && queryAttentionResult.size() > 0) {
            for (AskAttention attention : queryAttentionResult) {
                Ask queryAskModel = new Ask();
                queryAskModel.setId(attention.getAskId());
                queryAskModel.setDeleted(0);
                Ask askResult = askDAO.getRecord(queryAskModel);
                if (askResult != null) {
                    askResponseList.add(convertAskToResponse(askResult));
                }
            }
        }

        return askResponseList;
    }

    @Override
    public List<AskResponse> myCollect(final Long userId, final Integer offset, final Integer pageSize) {

        List<AskResponse> askResponseList = new ArrayList<>();

        UserCollectAsk queryModel = new UserCollectAsk();
        queryModel.setUserId(userId);
        queryModel.setDeleted(0);

        Page<UserCollectAsk> page = PageHelper.offsetPage(offset, pageSize).doSelectPage(new ISelect() {
            @Override
            public void doSelect() {
                userCollectAskDAO.listRecord(queryModel);
            }
        });

        if (page.size() > 0) {
            List<UserCollectAsk> userCollectAskList = page.getResult();

            if (userCollectAskList != null && userCollectAskList.size() > 0) {
                for (UserCollectAsk collectAsk : userCollectAskList) {
                    Ask queryAskModel = new Ask();
                    queryAskModel.setId(collectAsk.getAskId());
                    queryAskModel.setDeleted(0);
                    Ask askResult = askDAO.getRecord(queryAskModel);
                    if (askResult != null) {
                        askResponseList.add(convertAskToResponse(askResult));
                    }
                }
            }
        }

        return askResponseList;
    }

    @Override
    public List<AskResponse> myAnswer(final Long userId) {

        Long xiaoluUserId = 2548L;
        Long wukongUserId = 2562L;
        List<AskResponse> askResponseList = new ArrayList<>();

        if (xiaoluUserId.equals(userId) || wukongUserId.equals(userId)) {
            AskAnswer queryAskAnswerModel = new AskAnswer();
            queryAskAnswerModel.setReplyAdmin(1);
            queryAskAnswerModel.setDeleted(0);

            List<AskAnswer> queryAskAnswerResult = askAnswerDAO.listRecord(queryAskAnswerModel);
            if (queryAskAnswerResult != null && queryAskAnswerResult.size() > 0) {
                for (AskAnswer askAnswer : queryAskAnswerResult) {
                    Ask queryAskModel = new Ask();
                    queryAskModel.setId(askAnswer.getAskId());
                    queryAskModel.setDeleted(0);
                    Ask askResult = askDAO.getRecord(queryAskModel);
                    if (askResult != null) {
                        askResponseList.add(convertAskToResponse(askResult));
                    }
                }
            }

        } else {

            AskAnswer queryAskAnswerModel = new AskAnswer();
            queryAskAnswerModel.setAnswerUserId(userId);
            List<AskAnswer> queryAskAnswerResult = askAnswerDAO.selectMyAnsweredAskId(queryAskAnswerModel);
            if (queryAskAnswerResult != null && queryAskAnswerResult.size() > 0) {
                for (AskAnswer askAnswer : queryAskAnswerResult) {
                    Ask queryAskModel = new Ask();
                    queryAskModel.setId(askAnswer.getAskId());
                    queryAskModel.setDeleted(0);
                    Ask askResult = askDAO.getRecord(queryAskModel);
                    if (askResult != null) {
                        askResponseList.add(convertAskToResponse(askResult));
                    }
                }
            }
        }

        return askResponseList;
    }

    @Override
    public Integer queryAskAttentionCount(Long askId) {
        AskAttention queryModel = new AskAttention();
        queryModel.setAskId(askId);
        Integer count = askAttentionDAO.countRecord(queryModel);
        return count == null ? 0 : count;
    }

    @Override
    public Integer queryAskAnswerCount(Long askId) {
        AskAnswer queryModel = new AskAnswer();
        queryModel.setAskId(askId);
        Integer count = askAnswerDAO.countRecord(queryModel);
        return count == null ? 0 : count;
    }

    @Override
    public AskDetailResponse askDetail(final QueryAskDetailRequest request) {

        Ask queryAskModel = new Ask();
        queryAskModel.setId(request.getId());
        Ask ask = askDAO.getRecord(queryAskModel);
        if (ask == null) {
            throw new BizException(ErrorCodeEnum.SYS_EXP, "查不到问题");
        }

        AskDetailResponse response = new AskDetailResponse();
        response.setId(ask.getId());
        response.setTitle(ask.getTitle());
        response.setContent(ask.getContent());
        response.setAnswerCount(queryAskAnswerCount(ask.getId()));
        response.setFollowCount(queryAskAttentionCount(ask.getId()));

        if (request.getUserId().equals(ask.getPublisher())) {
            response.setIsOwner(true);
        } else {
            response.setIsOwner(false);
        }

        // todo tag标签
        // 用户是否关注
        AskAttention queryAskAttention = new AskAttention();
        queryAskAttention.setAskId(ask.getId());
        queryAskAttention.setAttentionUserId(request.getUserId());
        List<AskAttention> askAttentionList = askAttentionDAO.listRecord(queryAskAttention);
        if (askAttentionList != null && askAttentionList.size() > 0) {
            response.setIsFollow(true);
        } else {
            response.setIsFollow(false);
        }

        UserCollectAsk queryCollectModel = new UserCollectAsk();
        queryCollectModel.setUserId(request.getUserId());
        queryCollectModel.setAskId(ask.getId());
        queryCollectModel.setDeleted(0);

        List<UserCollectAsk> collectAskList = userCollectAskDAO.listRecord(queryCollectModel);
        if (collectAskList != null && collectAskList.size() > 0) {
            response.setCollect(true);
        } else {
            response.setCollect(false);
        }

        return response;
    }

    @Override
    public List<AskAnswerResponse> queryAnswerList(final QueryAskDetailRequest request) {

        AskAnswer queryAnswerModel = new AskAnswer();
        queryAnswerModel.setAskId(request.getId());
        queryAnswerModel.setDeleted(0);

        List<AskAnswer> answerList = askAnswerDAO.listRecord(queryAnswerModel);
        if (answerList == null || answerList.size() == 0) {
            return new ArrayList<>();
        }

        List<AskAnswerResponse> responseList = new ArrayList<>();
        for (AskAnswer answer : answerList) {
            AskAnswerResponse response = new AskAnswerResponse();
            response.setId(answer.getId());
            response.setAccepted(answer.getAccepted());
            response.setContent(answer.getContent());

            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
            String dateString = formatter.format(answer.getCreateTime());
            response.setCreateTime(dateString);
            responseList.add(response);

            // 回答者的用户信息
            if (answer.getReplyAdmin() == 1) {
                // 管理员回复
                response.setUserAvatar(BizConstants.ASK_DEFAULT_ANSWER_AVATAR);
                response.setUserName(BizConstants.ASK_DEFAULT_ANSWER_NAME);

            } else {
                // 如果答案表里没有用户头像和昵称，再去用户表中查询
                if (StringUtils.isBlank(answer.getAnswerUserNick()) || StringUtils.isBlank(answer.getAnswerUserAvatar())) {
                    User queryUserModel = new User();
                    queryUserModel.setId(answer.getAnswerUserId());
                    User user = userDAO.getRecord(queryUserModel);
                    if (user != null) {
                        response.setUserAvatar(user.getAvatar());
                        response.setUserName(user.getNickname());
                    }
                } else {
                    response.setUserAvatar(answer.getAnswerUserAvatar());
                    response.setUserName(answer.getAnswerUserNick());
                }

                // 如果还是没有娶到，则返回默认的头像
                if (StringUtils.isBlank(response.getUserAvatar())) {
                    response.setUserAvatar(BizConstants.ASK_DEFAULT_ANSWER_AVATAR);
                }

                if (StringUtils.isBlank(response.getUserName())) {
                    response.setUserAvatar(BizConstants.ASK_DEFAULT_ANSWER_NAME);
                }

            }

            // 多少人觉得有用
            AskUseful queryUsefulModel = new AskUseful();
            queryUsefulModel.setAnswerId(answer.getId());
            queryUsefulModel.setType(1);
            Integer usefulCount = usefulDAO.countRecord(queryUsefulModel);
            if (usefulCount == null) {
                usefulCount = 0;
            }
            response.setUsefulCount(usefulCount);

            // 当前用户觉得 有用没用
            queryUsefulModel = new AskUseful();
            queryUsefulModel.setUserId(request.getUserId());
            queryUsefulModel.setAnswerId(answer.getId());
            queryUsefulModel.setType(1);

            List<AskUseful> askUsefulList = usefulDAO.listRecord(queryUsefulModel);
            if (askUsefulList != null && askUsefulList.size() > 0) {
                response.setUseful(true);
            } else {
                response.setUseful(false);
            }
        }

        return responseList;
    }

    @Override
    public AskReportAnswerResponse reportAnswer(final AskReportAnswerRequest request) {

        WxMsgSecCheckResponse checkResponse = wechatApi.contentCheck(request.getContent());
        if (checkResponse.getErrorCode() != 0) {
            throw new BizException(ErrorCodeEnum.SYS_EXP, "有不合法的内容，请修改后再试试哦");
        }

        AskReportAnswerResponse response = new AskReportAnswerResponse();
        Long userId = request.getUserId();

        // 查询用户
        User queryUser = new User();
        queryUser.setId(request.getUserId());
        queryUser.setDeleted(0);

        User user = userDAO.getRecord(queryUser);
        if (user == null) {
            throw new BizException(ErrorCodeEnum.SYS_EXP, "用户不存在");
        }


        // 如果用户是第一次回答sop问题，或者 问答 ，颁发勋章
        SopAnswer queryAnswerModel = new SopAnswer();
        queryAnswerModel.setUserId(userId);
        queryAnswerModel.setDeleted(0);
        List<SopAnswer> answerList = sopAnswerDAO.listRecord(queryAnswerModel);
        if (answerList == null || answerList.size() == 0) {

            // 查询问答
            AskAnswer queryAskAnswerModel = new AskAnswer();
            queryAskAnswerModel.setAnswerUserId(userId);
            queryAskAnswerModel.setDeleted(0);
            List<AskAnswer> askAnswerList = askAnswerDAO.listRecord(queryAskAnswerModel);
            if (askAnswerList == null || askAnswerList.size() == 0) {
                response.setIsGetMedal(true);
                response.setMedalTitle(MedalTypeEnum.SOP_FIRST_ANSWER.getTitle());

                UserMedal medalSaveModel = new UserMedal();
                medalSaveModel.setUserId(userId);
                medalSaveModel.setType(MedalTypeEnum.SOP_FIRST_ANSWER.getCode());
                medalSaveModel.setTitle(MedalTypeEnum.SOP_FIRST_ANSWER.getTitle());
                medalSaveModel.setCreateTime(new Date());
                medalSaveModel.setUpdateTime(new Date());
                medalSaveModel.setDeleted(0);

                int result = userMedalDAO.saveRecord(medalSaveModel);
                if (result == 0) {
                    throw new BizException(ErrorCodeEnum.SYS_EXP, "保存勋章失败");
                }
            }
        } else {
            response.setIsGetMedal(false);
        }

        AskAnswer saveModel = new AskAnswer();
        saveModel.setAnswerUserId(user.getId());
        saveModel.setAnswerUserNick(user.getNickname());
        saveModel.setAnswerUserAvatar(user.getAvatar());
        saveModel.setAccepted(false);
        saveModel.setAskId(request.getId());
        saveModel.setContent(request.getContent());
        saveModel.setCreateTime(new Date());
        saveModel.setUpdateTime(new Date());
        saveModel.setDeleted(0);
        saveModel.setReplyAdmin(0);

        int result = askAnswerDAO.saveRecord(saveModel);
        if (result == 0) {
            throw new BizException(ErrorCodeEnum.SYS_EXP, "保存答案失败");
        }

        // 回答完成发送模版消息到提问者
        messageTaskService.askReceivedAnswerMessage(saveModel.getId());

        // 向关注该问题的学校关注者发送提醒
        executorService.submit(() -> noticeConcernUser(saveModel.getAskId()));

        return response;
    }

    /**
     * 向该问题的学校关注者发通知
     *
     * @param askId
     */
    private void noticeConcernUser(Long askId) {
        log.info("开始推送到想读人的");
        Ask model = new Ask();
        model.setId(askId);
        Ask ask = askDAO.getRecord(model);
        //获取所有该问题涉及的学校Id
        List<School> schoolList = schoolDAO.listNews(null);
        List<School> schoolNeedList = new ArrayList<>();
        for (School school : schoolList) {
            // 先看是否有关键词
            if (StrUtil.isNotBlank(school.getSearchKey())) {
                String[] searchKeyList = school.getSearchKey().split(",");
                for(String searchKey : searchKeyList){
                    if (ask.getTitle().contains(searchKey)) {
                        schoolNeedList.add(school);
                        break;
                    }
                }
            }else {
                // 没有关键词 就用学校真名
                String rightName = getSchoolRealName(school);
                if (ask.getTitle().contains(rightName)) {
                    schoolNeedList.add(school);
                }
            }
        }
        for (School school : schoolNeedList) {
            StuSch queryModel = new StuSch();
            queryModel.setSchoolId(school.getId());
            List<StuSch> stuSchList = stuSchDAO.listRecord(queryModel);
            for (StuSch stuSch : stuSchList) {
                log.info("推送userId："+stuSch.getUserId());
                //获取用户推送新文章等的天key（一天最多一次）
                String dayCacheKey = CacheKeyUtil.genUserNewNewsDayNoticeKey(stuSch.getUserId());
                RBucket<Integer> dayBucket = redissonClient.getBucket(dayCacheKey);
                if (dayBucket.get() != null) {
                    continue;
                }
                //获取用户推送新文章等的周key（七天最多三次）
                String weekCacheKey = CacheKeyUtil.genUserNewNewsWeekNoticeKey(stuSch.getUserId());
                RBucket<String> weekBucket = redissonClient.getBucket(weekCacheKey);
                String weekNoticeNumAndTime = weekBucket.get();
                Integer weekNoticeNum = null;
                if (StrUtil.isNotBlank(weekNoticeNumAndTime)) {
                    weekNoticeNum = Integer.valueOf(weekNoticeNumAndTime.split("~")[0]);
                    if (weekNoticeNum >= 3) {
                        continue;
                    }
                }
                messagePushService.pushAskConcernUserNotice(askId, ask.getTitle(), stuSch.getUserId(), school.getName());
                dayBucket.set(1, 1, TimeUnit.DAYS);
                Date now = new Date();
                if (StrUtil.isNotBlank(weekNoticeNumAndTime)) {
                    Date weekNoticeTime = cn.hutool.core.date.DateUtil.parse(weekNoticeNumAndTime.split("~")[1], "yyyy-MM-dd-HH-mm-ss");
                    //after避免redis没有及时删除而导致的key错加
                    if (cn.hutool.core.date.DateUtil.offsetWeek(weekNoticeTime, 1).isAfter(now)) {
                        //计算创建时间到现在还有多少毫秒
                        long outTimeMs = cn.hutool.core.date.DateUtil.betweenMs(cn.hutool.core.date.DateUtil.offsetWeek(weekNoticeTime, 1), new Date());
                        weekNoticeNum++;
                        //weekNoticeTime一直保持第一个key创建的时间
                        weekBucket.set(weekNoticeNum + "~" + weekNoticeTime, outTimeMs, TimeUnit.MILLISECONDS);
                    } else {
                        weekBucket.set(1 + "~" + cn.hutool.core.date.DateUtil.format(now, "yyyy-MM-dd-HH-mm-ss"), 7 * 24 * 60 * 60 * 1000, TimeUnit.MILLISECONDS);
                    }
                } else {
                    weekBucket.set(1 + "~" + cn.hutool.core.date.DateUtil.format(now, "yyyy-MM-dd-HH-mm-ss"), 7 * 24 * 60 * 60 * 1000, TimeUnit.MILLISECONDS);
                }

            }
        }
    }

    @Override
    public void follow(final Long askId, final Long userId) {

        // 先查询一下是否已经关注
        AskAttention queryModel = new AskAttention();
        queryModel.setAskId(askId);
        queryModel.setAttentionUserId(userId);
        queryModel.setDeleted(0);

        List<AskAttention> askAttentionList = askAttentionDAO.listRecord(queryModel);
        if (askAttentionList != null && askAttentionList.size() > 0) {
            throw new BizException(ErrorCodeEnum.SYS_EXP, "您已经关注了该问题");
        }

        AskAttention saveModel = new AskAttention();
        saveModel.setAskId(askId);
        saveModel.setAttentionUserId(userId);
        saveModel.setDeleted(0);
        saveModel.setCreateTime(new Date());
        saveModel.setUpdateTime(new Date());

        int result = askAttentionDAO.saveRecord(saveModel);
        if (result == 0) {
            throw new BizException(ErrorCodeEnum.SYS_EXP, "关注失败");
        }
        executorService.submit(() -> setAskFollowSchoolList(userId, askId));
    }

    private void setAskFollowSchoolList(Long userId, Long askId) {
        Ask ask = new Ask();
        ask.setId(askId);
        ask = askDAO.getRecord(ask);
        List<School> schoolList = schoolDAO.listAllSchoolNameAndId();
        List<Long> schoolIdList = new ArrayList<>();
        for (School school : schoolList){
            if (ask.getTitle().contains(school.getName())){
                schoolIdList.add(school.getId());
            }
        }
        String cacheKey = CacheKeyUtil.genUserAskFollowKey(userId);
        RBucket<List<Long>> bucket = redissonClient.getBucket(cacheKey);
        List<Long> nowIdList = bucket.get();
        if (nowIdList == null){
            bucket.set(schoolIdList);
        } else {
            for (Long schoolId : schoolIdList){
                if (!nowIdList.contains(schoolId)){
                    nowIdList.add(schoolId);
                }
            }
            bucket.set(nowIdList);
        }
    }

    @Override
    public void accept(final Long askId, final Long askAnswerId, final Long userId) {

        // 1 先判断问题是否为userId的用户发的
        Ask queryAskModel = new Ask();
        queryAskModel.setId(askId);
        Ask queryAskResult = askDAO.getRecord(queryAskModel);
        if (queryAskResult == null || !queryAskResult.getPublisher().equals(userId)) {
            throw new BizException(ErrorCodeEnum.SYS_EXP, "您不是该问题的发布者哦");
        }

        // 2 判断问题的答案是否存在
        AskAnswer queryAnswerModel = new AskAnswer();
        queryAnswerModel.setAskId(askId);
        queryAnswerModel.setId(askAnswerId);
        List<AskAnswer> askAnswerList = askAnswerDAO.listRecord(queryAnswerModel);
        if (askAnswerList == null || askAnswerList.size() == 0) {
            throw new BizException(ErrorCodeEnum.SYS_EXP, "该答案不存在哦");
        }

        AskAnswer updateAnswerModel = new AskAnswer();
        updateAnswerModel.setId(askAnswerId);
        updateAnswerModel.setAccepted(true);
        updateAnswerModel.setUpdateTime(new Date());
        int result = askAnswerDAO.updateRecord(updateAnswerModel);
        if (result == 0) {
            throw new BizException(ErrorCodeEnum.SYS_EXP, "更新失败");
        }
    }

    @Override
    public CmsAskDetailResponse cmsDetail(final Long id) {

        Ask queryAskModel = new Ask();
        queryAskModel.setId(id);
        queryAskModel.setDeleted(0);

        Ask ask = askDAO.getRecord(queryAskModel);
        if (ask == null) {
            throw new BizException(ErrorCodeEnum.SYS_EXP, "问题详情为空");
        }

        CmsAskDetailResponse response = new CmsAskDetailResponse();
        response.setTitle(ask.getTitle());

        // 问题发布者的信息
        if (ask.getPublisher() != null) {
            User queryUserModel = new User();
            queryUserModel.setId(ask.getPublisher());
            queryUserModel.setDeleted(0);
            User user = userDAO.getRecord(queryUserModel);
            if (user != null) {
                response.setAuthor(user.getNickname());
            }
        }

        response.setId(ask.getId());
        return response;
    }

    @Override
    public CmsSearchAskResponse cmsSearch(final CmsSearchAskRequest request) {
        CmsSearchAskResponse searchAskResponse = new CmsSearchAskResponse();

        Ask queryModel = new Ask();
        queryModel.setTitle(request.getKeyword());
        queryModel.setDeleted(0);

        Page<Ask> page = PageHelper.offsetPage(request.getOffset(), request.getPageSize()).doSelectPage(new ISelect() {
            @Override
            public void doSelect() {
                askDAO.search(queryModel);
            }
        });

        searchAskResponse.setTotal((int) page.getTotal());

        if (page.size() > 0) {

            List<CmsAskResponse> responseList = new ArrayList<>();
            for (Ask ask : page.getResult()) {


                CmsAskResponse response = new CmsAskResponse();
                response.setId(ask.getId());
                response.setTitle(ask.getTitle());
                response.setAnswerCount(queryAskAnswerCount(ask.getId()));
                response.setFollowCount(queryAskAttentionCount(ask.getId()));

                // 问题发布者的信息
                if (ask.getPublisher() != null) {
                    User queryUserModel = new User();
                    queryUserModel.setId(ask.getPublisher());
                    queryUserModel.setDeleted(0);
                    User user = userDAO.getRecord(queryUserModel);
                    if (user != null) {
                        response.setUserId(user.getId());
                        response.setUsername(user.getNickname());
                    }
                }

                // 发布时间
                if (ask.getCreateTime() != null) {
                    SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    String dateString = formatter.format(ask.getCreateTime());
                    response.setCreateTime(dateString);
                }

                responseList.add(response);
            }

            searchAskResponse.setList(responseList);
        }

        return searchAskResponse;
    }

    @Override
    public void cmsReply(final CmsAskReplyRequest request) {

        WxMsgSecCheckResponse checkResponse = wechatApi.contentCheck(request.getContent());
        if (checkResponse.getErrorCode() != 0) {
            throw new BizException(ErrorCodeEnum.SYS_EXP, "有不合法的内容，请修改后再试试哦");
        }

        Long userId = request.getUserId();

        AskAnswer saveModel = new AskAnswer();
        saveModel.setAnswerUserId(userId);
        saveModel.setAnswerUserNick(BizConstants.ASK_DEFAULT_ANSWER_NAME);
        saveModel.setAnswerUserAvatar(BizConstants.ASK_DEFAULT_ANSWER_AVATAR);
        saveModel.setAccepted(false);
        saveModel.setAskId(request.getId());
        saveModel.setContent(request.getContent());
        saveModel.setCreateTime(new Date());
        saveModel.setUpdateTime(new Date());
        saveModel.setDeleted(0);
        saveModel.setReplyAdmin(1);

        int result = askAnswerDAO.saveRecord(saveModel);
        if (result == 0) {
            throw new BizException(ErrorCodeEnum.SYS_EXP, "保存答案失败");
        }

        // 回答完成发送模版消息到提问者
        messageTaskService.askReceivedAnswerMessage(saveModel.getId());
    }

    @Override
    public void cmsDelete(final Long askId, final Long userId) {

        Ask deleteModel = new Ask();
        deleteModel.setId(askId);
        deleteModel.setDeleted(1);
        deleteModel.setUpdateTime(new Date());

        askDAO.updateRecord(deleteModel);
    }

    @Override
    public Long cmsPublish(final AskPublishRequest request, final Long userId) {

        WxMsgSecCheckResponse checkResponse = wechatApi.contentCheck(request.getTitle());

        if (checkResponse.getErrorCode() != 0) {
            throw new BizException(ErrorCodeEnum.SYS_EXP, "标题有不合法的内容，请修改后再试试哦");
        }

        if (StringUtils.isNotBlank(request.getContent())) {
            checkResponse = wechatApi.contentCheck(request.getContent());
            if (checkResponse.getErrorCode() != 0) {
                throw new BizException(ErrorCodeEnum.SYS_EXP, "问题有不合法的内容，请修改后再试试哦");
            }
        }

        Ask saveModel = new Ask();
        saveModel.setPublisher(request.getUserId());
        saveModel.setTitle(request.getTitle());
        saveModel.setContent(request.getContent());
        saveModel.setCreateTime(new Date());
        saveModel.setUpdateTime(new Date());
        saveModel.setPublisher(userId);
        saveModel.setAdmin(1);
        saveModel.setDeleted(0);
        int result = askDAO.saveRecord(saveModel);
        if (result == 0) {
            throw new BizException(ErrorCodeEnum.SYS_EXP, "保存问题失败");
        }

        return saveModel.getId();
    }

    @Override
    public void collect(final Long userId, final Long askId) {

        // 查询是否已经收藏
        UserCollectAsk queryModel = new UserCollectAsk();
        queryModel.setAskId(askId);
        queryModel.setUserId(userId);
        queryModel.setDeleted(0);

        List<UserCollectAsk> userCollectAskList = userCollectAskDAO.listRecord(queryModel);
        if (userCollectAskList != null && userCollectAskList.size() > 0) {
            throw new BizException(ErrorCodeEnum.SYS_EXP, "已收藏");
        }

        UserCollectAsk saveModel = new UserCollectAsk();
        saveModel.setUserId(userId);
        saveModel.setAskId(askId);
        saveModel.setUserId(userId);
        saveModel.setCreateTime(new Date());
        saveModel.setUpdateTime(new Date());
        saveModel.setDeleted(0);

        int result = userCollectAskDAO.saveRecord(saveModel);
        if (result == 0) {
            throw new BizException(ErrorCodeEnum.SYS_EXP, "收藏失败");
        }
    }

    @Override
    public void collectDelete(final Long userId, final Long askId) {


        UserCollectAsk deleteModel = new UserCollectAsk();
        deleteModel.setUserId(userId);
        deleteModel.setAskId(askId);

        userCollectAskDAO.logicDeleteByUserIdAndAskId(deleteModel);
    }

    /**
     * 获取学校除去省市区和括号的名字
     * @param school
     * @return
     */
    private String getSchoolRealName(School school) {
        String schoolName = school.getName();
        // 去掉括号数据
        String[] subSchoolNames = schoolName.split("（");
        String[] subs = subSchoolNames[0].split("\\(");
        schoolName = subs[0];

        //去掉省
        String province = school.getProvince();
        if (StrUtil.isNotBlank(province)){
            // 去掉省
            // 这一步去除的是浙江省
            if (schoolName.contains(province)) {
                subs = schoolName.split(province);

                if (subs.length > 1) {
                    schoolName = subs[1];
                }
            }
            // 这一步去掉的是浙江
//            province = province.substring(0, province.length()-1);
//            if (schoolName.contains(province)) {
//                subs = schoolName.split(province);
//
//                if (subs.length > 1) {
//                    schoolName = subs[1];
//                }
//            }
        }

        //去掉市
        String city = school.getCity();
        if (StrUtil.isNotBlank(city)){
            // 去掉省市区
            // 这一步去除的是杭州市
            if (schoolName.contains(city)) {
                subs = schoolName.split(city);

                if (subs.length > 1) {
                    schoolName = subs[1];
                }
            }
            // 这一步去掉的是杭州
//            city = city.substring(0, city.length()-1);
//            if (schoolName.contains(city)) {
//                subs = schoolName.split(city);
//
//                if (subs.length > 1) {
//                    schoolName = subs[1];
//                }
//            }
        }

        //去掉区
        String area = school.getArea();
        if (StrUtil.isNotBlank(area)){
            // 去掉省市区
            // 这一步去除的是滨江区
            if (schoolName.contains(area)) {
                subs = schoolName.split(area);

                if (subs.length > 1) {
                    schoolName = subs[1];
                }
            }
        }

        return schoolName;
    }
}