/*
 * Dian.com Inc.
 * Copyright (c) 2004-2019 All Rights Reserved.
 */
package cn.sedu.moon.service.impl;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.StrUtil;
import cn.sedu.moon.common.constant.BizConstants;
import cn.sedu.moon.common.enums.ErrorCodeEnum;
import cn.sedu.moon.common.enums.SchoolPropertyEnum;
import cn.sedu.moon.common.exception.BizException;
import cn.sedu.moon.common.util.CacheKeyUtil;
import cn.sedu.moon.controller.request.AskSearchRequest;
import cn.sedu.moon.controller.response.AskResponse;
import cn.sedu.moon.controller.response.SchoolNewsResponse;
import cn.sedu.moon.controller.response.SchoolTabAskResponse;
import cn.sedu.moon.controller.response.SopResponse;
import cn.sedu.moon.controller.response.school.SchoolCommentResponse;
import cn.sedu.moon.controller.response.school.SchoolTabResponse;
import cn.sedu.moon.dao.DistrictGdDAO;
import cn.sedu.moon.dao.SchoolCommentDAO;
import cn.sedu.moon.dao.SchoolCommentImgDAO;
import cn.sedu.moon.dao.SchoolDAO;
import cn.sedu.moon.dao.SchoolHotDAO;
import cn.sedu.moon.dao.SchoolImgSrcDAO;
import cn.sedu.moon.dao.SchoolSrcDAO;
import cn.sedu.moon.dao.StuSchDAO;
import cn.sedu.moon.dao.StudentArchivesDAO;
import cn.sedu.moon.dao.model.*;
import cn.sedu.moon.dao.mongo.SchoolLocation;
import cn.sedu.moon.dao.mongo.SchoolLocationRepository;
import cn.sedu.moon.service.*;
import cn.sedu.moon.service.context.AddSchoolCommentContext;
import cn.sedu.moon.service.context.QuerySchoolCommentContext;
import cn.sedu.moon.service.context.QuerySchoolNewsContext;
import cn.sedu.moon.service.context.SchoolTabSearchContext;
import cn.sedu.moon.service.context.SearchNewsContext;
import com.github.pagehelper.ISelect;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.gavaghan.geodesy.Ellipsoid;
import org.gavaghan.geodesy.GeodeticCalculator;
import org.gavaghan.geodesy.GlobalPosition;
import org.redisson.api.RBucket;
import org.redisson.api.RedissonClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.geo.Point;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import javax.annotation.Resource;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * @author wukong
 * @version : SchoolTabServiceImpl.java, v 0.1 2019年10月08日 2:36 PM Exp $
 */
@Service
@Slf4j
public class SchoolTabServiceImpl implements SchoolTabService {

    @Resource
    private SchoolCommentDAO schoolCommentDAO;

    @Resource
    private SchoolCommentImgDAO schoolCommentImgDAO;

    @Autowired
    private SchoolDAO schoolDAO;

    @Autowired
    private UserService userService;

    @Autowired
    private NewsService newsService;

    @Autowired
    private AskService askService;

    @Autowired
    private StuSchDAO stuSchDAO;

    @Autowired
    private StudentArchivesDAO archivesDAO;

    @Autowired
    private SchoolLocationRepository locationRepository;

    @Autowired
    private RedissonClient redissonClient;

    @Resource
    private DistrictGdDAO districtGdDAO;

    @Resource
    private SchoolHotDAO schoolHotDAO;

    @Autowired
    private SchoolSrcDAO schoolSrcDAO;

    @Autowired
    private SchoolImgSrcDAO schoolImgSrcDAO;

    @Autowired
    private MessagePushService messagePushService;

    @Autowired
    private SopService sopService;

    // 多线程服务
    private static ExecutorService executorService = new ThreadPoolExecutor(40, 100, 60L, TimeUnit.SECONDS, new LinkedBlockingQueue<>(100));

    @Override
    public List<SchoolTabResponse> search(final SchoolTabSearchContext context) {

        List<SchoolTabResponse> responseList = new ArrayList<>();

        context.setUserDistrictInfo(queryUserDistrictInfo(context.getUserId()));

        // 查询用户在读的学校
        Long readingSchoolId = archivesDAO.getReadingSchoolId(context.getUserId());

        // 查询用户想读的学校
        List<Long> stuSchSchoolIdList = stuSchDAO.getSchoolIdListByUserId(context.getUserId());

        if (StringUtils.isBlank(context.getKey())) {
            // 优先展示想读学校，
            // 再展示用户所在的学校，
            // 再由近及远 展示附近的学校
            List<Long> schoolIdList = schoolIdList(context);
            if (!CollectionUtils.isEmpty(schoolIdList)) {
                if (readingSchoolId != null){
                    for (Long id : schoolIdList) {
                        if (id.equals(readingSchoolId)){
                            SchoolTabResponse response = buildSchoolResponseBySchoolId(id, context, context.getUserId());
                            response.setReadTag("在读学校");
                            responseList.add(response);
                        }else {
                            SchoolTabResponse response = buildSchoolResponseBySchoolId(id, context, null);
                            if (stuSchSchoolIdList.contains(id)){
                                response.setReadTag("想读");
                            }
                            responseList.add(response);
                        }
                    }
                } else {
                    for (Long id : schoolIdList) {
                        SchoolTabResponse response = buildSchoolResponseBySchoolId(id, context, null);
                        if (stuSchSchoolIdList.contains(id)){
                            response.setReadTag("在读");
                        }
                        responseList.add(response);
                    }
                }

            }
        } else {

            // 按照关键字搜索
            School queryModel = new School();
            queryModel.setName(context.getKey());

            if (context.getUserDistrictInfo() != null && context.getUserDistrictInfo().getCityCode() != null) {
                queryModel.setCityCode(context.getUserDistrictInfo().getCityCode());
            }

            Page<School> page = PageHelper.offsetPage(context.getOffset(), context.getPageSize()).doSelectPage(new ISelect() {
                @Override
                public void doSelect() {
                    schoolDAO.listSchoolBySearchKey(queryModel.getName(), queryModel.getCityCode());
                }
            });

            if (page.size() > 0) {
                for (School school : page.getResult()) {
                    SchoolTabResponse response = buildSchoolResponseBySchoolId(school.getId(), context, null);
                    responseList.add(response);
                }
            }

        }

        return responseList;
    }

    private SchoolTabResponse buildSchoolResponseBySchoolId(Long schoolId, SchoolTabSearchContext context, Long userId) {

        SchoolTabResponse response = new SchoolTabResponse();

        // 查询学校基本信息
        School querySchoolModel = new School();
        querySchoolModel.setId(schoolId);
        School school = schoolDAO.getRecord(querySchoolModel);
        if (school != null) {
            response.setSchoolId(school.getId());
            response.setName(school.getName());
            response.setAddress(school.getAddress());
            response.setAreaName(school.getArea());

            // 学校离用户所在的小区的距离
            if (school.getLat() != null && school.getLng() != null) {
                double distance = calDistance(school.getLat(), school.getLng(),
                        context.getUserDistrictInfo().getLat(), context.getUserDistrictInfo().getLng());
                response.setDistance((int) distance + "米");
            }

            // 多少人想读
            response.setFollowCount(schoolFollowCount(schoolId));

            // 学校 tags
            List<String> tags = new ArrayList<>();
            if (school.getProperty() != null && school.getProperty().equals(SchoolPropertyEnum.GONGBAN.getCode())) {
                tags.add("公办");
            } else if (school.getProperty() != null && school.getProperty().equals(SchoolPropertyEnum.MINBAN.getCode())) {
                tags.add("民办");
            }

            // 根据缓存添加tag
            String cacheKey = CacheKeyUtil.genSchoolTagKey(schoolId);
            RBucket<List<Integer>> bucket = redissonClient.getBucket(cacheKey);
            List<Integer> subjectRefIdList = bucket.get();
            if (subjectRefIdList != null && subjectRefIdList.size() > 0){
                for (Integer subjectRefId : subjectRefIdList){
                    String tag = sopService.getSopTag(schoolId, subjectRefId);
                    tags.add(tag);
                }
            }

            // 查询热门学校信息
            SchoolHot querySchoolHotModel = new SchoolHot();
            querySchoolHotModel.setSchoolId(schoolId);
            querySchoolHotModel.setDeleted(0);
            List<SchoolHot> schoolHotList = schoolHotDAO.listRecord(querySchoolHotModel);
            if (schoolHotList != null && schoolHotList.size() > 0) {
                SchoolHot schoolHot = schoolHotList.get(0);
                if (schoolHot.getCityHot() == 1) {
                    tags.add("市热门");
                }

                if (schoolHot.getAreaHot() == 1) {
                    tags.add("区热门");
                }
            }

            response.setTags(tags);
        }

        // 最新一条评论
        QuerySchoolCommentContext querySchoolCommentContext = new QuerySchoolCommentContext();
        querySchoolCommentContext.setSchoolId(schoolId);
        querySchoolCommentContext.setOffset(0);
        querySchoolCommentContext.setPageSize(1);
        List<SchoolCommentResponse> schoolCommentResponses = commentList(querySchoolCommentContext);
        if (!CollectionUtils.isEmpty(schoolCommentResponses)) {
            response.setLatestComment(schoolCommentResponses.get(0).getContent());
        }

        // 评论数量
        SchoolComment countCommentModel = new SchoolComment();
        countCommentModel.setDeleted(0);
        countCommentModel.setSchoolId(schoolId);
        Integer count = schoolCommentDAO.countRecord(countCommentModel);
        response.setCommentCount(count);

        // 图片
        List<String> imgList = getSchoolImageList(school.getName());
        if (!CollectionUtils.isEmpty(imgList)) {
            response.setImg(imgList.get(0));
        } else {
            response.setImg(BizConstants.SCHOOL_ICON_DEFAULT_URL);
        }

        // 在读学校获取SOP
        if (userId != null){
            SopResponse sopResponse = sopService.getSchoolSopUnAnswer(userId, schoolId);
            response.setSopResponse(sopResponse);
        }

        return response;
    }

    private List<String> getSchoolImageList(String schoolName) {

        List<String> imageList = new ArrayList<>();
        SchoolSrc querySchoolSrc = new SchoolSrc();

        String[] subSchoolNames = schoolName.split("（");
        String[] subs = subSchoolNames[0].split("\\(");

        String keyTitle = subs[0];

        querySchoolSrc.setName(keyTitle);

        // 先去学校src 表中查询数据
        List<SchoolSrc> querySchoolSrcResult = schoolSrcDAO.listRecord(querySchoolSrc);
        if (querySchoolSrcResult != null && querySchoolSrcResult.size() > 0) {
            SchoolSrc schoolSrc = querySchoolSrcResult.get(0);

            if (StringUtils.isNotBlank(schoolSrc.getThumbnail())) {
                imageList.add(schoolSrc.getThumbnail());
            }

            // 再去img_src 中查询数据
            SchoolImgSrc queryImgSrcModel = new SchoolImgSrc();
            queryImgSrcModel.setSxId(schoolSrc.getSxId());
            List<SchoolImgSrc> schoolImgSrcList = schoolImgSrcDAO.listRecord(queryImgSrcModel);
            if (schoolImgSrcList != null && schoolImgSrcList.size() > 0) {
                for (SchoolImgSrc imgSrc : schoolImgSrcList) {
                    if (StringUtils.isNotBlank(imgSrc.getImages())) {
                        imageList.add(imgSrc.getImages());
                    }
                }
            }
        }

        return imageList;
    }

    private double calDistance(double latitude, double longitude, double userLat, double userLon) {
        GeodeticCalculator geoCalc = new GeodeticCalculator();
        Ellipsoid reference = Ellipsoid.WGS84;
        GlobalPosition pointA = new GlobalPosition(latitude, longitude, 0.0); // Point A
        GlobalPosition userPos = new GlobalPosition(userLat, userLon, 0.0); // Point B
        return geoCalc.calculateGeodeticCurve(reference, userPos, pointA).getEllipsoidalDistance(); // Distance between Point A and Point B
    }

    private Integer schoolFollowCount(Long schoolId) {
        StuSch queryStuschModel = new StuSch();
        queryStuschModel.setSchoolId(schoolId);
        queryStuschModel.setDeleted(0);

        List<StuSch> stuSchList = stuSchDAO.listRecord(queryStuschModel);
        return stuSchList.size();
    }

    private List<Long> schoolIdList(SchoolTabSearchContext context) {
        // 从redis中读取
        // 去 redis 中查询是否有数据
        String cacheKey = CacheKeyUtil.genSchoolTabUserSchoolKey(context.getUserId());
        RBucket<List<Long>> bucket = redissonClient.getBucket(cacheKey);
        if (bucket.get() == null) {

            List<Long> idList = rebuildSchoolIdList(context);
            bucket.set(idList, 1440, TimeUnit.MINUTES);

            Integer startIndex = context.getOffset();
            int endIndex = context.getOffset() + context.getPageSize() - 1;

            List<Long> resultList = new ArrayList<>();

            // 数据已经加载完了
            if (startIndex > idList.size() - 1) {
                return null;
            }

            for (int i = 0; i < idList.size(); i++) {
                if (i >= startIndex && i <= endIndex) {
                    resultList.add(idList.get(i));
                }
            }

            return resultList;
        } else {
            List<Long> idList = new ArrayList<>();

            Integer startIndex = context.getOffset();
            int endIndex = context.getOffset() + context.getPageSize() - 1;

            List<Long> cacheIdList = bucket.get();

            // 数据已经加载完了
            if (startIndex > cacheIdList.size() - 1) {
                return null;
            }

            for (int i = 0; i < cacheIdList.size(); i++) {
                if (i >= startIndex && i <= endIndex) {
                    idList.add(cacheIdList.get(i));
                }
            }

            return idList;
        }
    }


    private List<Long> rebuildSchoolIdList(SchoolTabSearchContext context) {
        DistrictGd userDistrict = context.getUserDistrictInfo();

        Map<Long, Integer> schoolIdMap = new HashMap<>();

        List<Long> schoolIdList = new ArrayList<>();

        // 用户所在的学校
        StudentArchives queryArchiveModel = new StudentArchives();
        queryArchiveModel.setUserId(context.getUserId());
        queryArchiveModel.setDeleted(0);

        List<StudentArchives> queryArchiveResult = archivesDAO.listRecord(queryArchiveModel);
        if (!CollectionUtils.isEmpty(queryArchiveResult)) {
            Long userSchoolId = queryArchiveResult.get(0).getSchoolId();

            // 去重添加
            if (userSchoolId != null) {
                schoolIdList.add(userSchoolId);
                schoolIdMap.put(userSchoolId, 1);
            }
        }

        // 想读的学校
        StuSch queryStuSchModel = new StuSch();
        queryStuSchModel.setUserId(context.getUserId());
        queryStuSchModel.setDeleted(0);
        List<StuSch> stuSchList = stuSchDAO.listRecord(queryStuSchModel);
        if (!CollectionUtils.isEmpty(stuSchList)) {
            for (StuSch stuSch : stuSchList) {

                // 去重添加
                if (!schoolIdMap.containsKey(stuSch.getSchoolId())) {
                    schoolIdList.add(stuSch.getSchoolId());
                    schoolIdMap.put(stuSch.getSchoolId(), 1);
                }
            }
        }


        int needCount = 200 - schoolIdList.size();
        Point queryPoint = new Point(userDistrict.getLng(), userDistrict.getLat());
        List<SchoolLocation> locations = locationRepository.findCircleNear(queryPoint, 20,
                null, null, 0, needCount);

        if (!CollectionUtils.isEmpty(locations)) {
            for (SchoolLocation location : locations) {

                Long locationSchoolId = location.getSchoolId();
                // 去重添加
                if (location.getSchoolId() != null && !schoolIdMap.containsKey(locationSchoolId)) {
                    schoolIdList.add(locationSchoolId);
                    schoolIdMap.put(locationSchoolId, 1);
                }
            }
        }

        return schoolIdList;
    }

    private DistrictGd queryUserDistrictInfo(Long userId) {
        // 获取用户家的地理位置
        StudentArchives queryArchiveModel = new StudentArchives();
        queryArchiveModel.setUserId(userId);
        queryArchiveModel.setDeleted(0);

        List<StudentArchives> queryArchiveResult = archivesDAO.listRecord(queryArchiveModel);
        if (queryArchiveResult == null || queryArchiveResult.size() == 0 || queryArchiveResult.get(0).getDistrictHouseId() == null) {
            return null;
        }

        DistrictGd queryDistrictModel = new DistrictGd();
        queryDistrictModel.setId(queryArchiveResult.get(0).getDistrictHouseId());
        queryDistrictModel.setDeleted(0);

        return districtGdDAO.getRecord(queryDistrictModel);
    }


    @Override
    public List<SchoolCommentResponse> commentList(final QuerySchoolCommentContext context) {

        List<SchoolCommentResponse> responseList = new ArrayList<>();

        // 按照发表时间搜索
        SchoolComment queryCommentModel = new SchoolComment();
        queryCommentModel.setSchoolId(context.getSchoolId());
        queryCommentModel.setDeleted(0);

        Page<SchoolComment> page = PageHelper.offsetPage(context.getOffset(), context.getPageSize()).doSelectPage(new ISelect() {
            @Override
            public void doSelect() {
                schoolCommentDAO.listRecord(queryCommentModel);
            }
        });

        if (page.size() == 0) {
            return responseList;
        }

        for (SchoolComment schoolComment : page.getResult()) {
            SchoolCommentResponse commentResponse = new SchoolCommentResponse();
            commentResponse.setAvatar(schoolComment.getAvatar());
            commentResponse.setNick(schoolComment.getNickname());

            if (schoolComment.getCreateTime() != null) {
                SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                String dateString = formatter.format(schoolComment.getCreateTime());
                commentResponse.setCreateTime(dateString);
            }

            commentResponse.setContent(schoolComment.getComment());
            commentResponse.setCommentId(schoolComment.getId());

            // 查询评论图片
            SchoolCommentImg queryImgModel = new SchoolCommentImg();
            queryImgModel.setCommentId(schoolComment.getId());
            queryImgModel.setDeleted(0);
            List<SchoolCommentImg> schoolCommentImgList = schoolCommentImgDAO.listRecord(queryImgModel);
            if (schoolCommentImgList != null && schoolCommentImgList.size() > 0) {
                List<String> imgUrlList = new ArrayList<>();
                for (SchoolCommentImg schoolCommentImg : schoolCommentImgList) {
                    imgUrlList.add(schoolCommentImg.getImg());
                }
                commentResponse.setImgList(imgUrlList);
            }

            responseList.add(commentResponse);
        }

        return responseList;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Long addSchoolComment(final AddSchoolCommentContext context) {

        // 查询用户信息
        User user = userService.getUserById(context.getUserId());
        if (user == null) {
            throw new BizException(ErrorCodeEnum.SYS_EXP, "用户不存在");
        }

        SchoolComment saveCommentModel = new SchoolComment();
        saveCommentModel.setSchoolId(context.getSchoolId());
        saveCommentModel.setUserId(context.getUserId());
        saveCommentModel.setAvatar(user.getAvatar());
        saveCommentModel.setNickname(user.getNickname());
        saveCommentModel.setComment(context.getContent());
        saveCommentModel.setDeleted(0);
        saveCommentModel.setCreateTime(new Date());
        saveCommentModel.setUpdateTime(new Date());

        int saveCommentResult = schoolCommentDAO.saveRecord(saveCommentModel);
        if (saveCommentResult == 0) {
            throw new BizException(ErrorCodeEnum.SYS_EXP, "保存评论失败");
        }

        if (context.getImgList() != null && context.getImgList().size() > 0) {
            for (String imgUrl : context.getImgList()) {
                SchoolCommentImg saveImgModel = new SchoolCommentImg();
                saveImgModel.setCommentId(saveCommentModel.getId());
                saveImgModel.setImg(imgUrl);
                saveImgModel.setCreateTime(new Date());
                saveImgModel.setUpdateTime(new Date());
                saveImgModel.setDeleted(0);

                int saveImgResult = schoolCommentImgDAO.saveRecord(saveImgModel);
                if (saveImgResult == 0) {
                    throw new BizException(ErrorCodeEnum.SYS_EXP, "保存评论图片失败");
                }
            }
        }
        // 通知想读该学校的用户
        executorService.submit(() -> noticeCommentToUser(saveCommentModel));

        return saveCommentModel.getId();
    }

    /**
     * 通知在校生家长来评价
     * @param saveCommentModel
     */
    private void noticeReadingSchoolUser(SchoolComment saveCommentModel) {
        List<Long> userIds = archivesDAO.listUserIdBySchoolId(saveCommentModel.getSchoolId());
        if (userIds != null && !userIds.isEmpty()){
            String schoolName = schoolDAO.getSchoolNameById(saveCommentModel.getSchoolId());
            for (Long userId : userIds){
                SchoolComment model = new SchoolComment();
                model.setSchoolId(saveCommentModel.getSchoolId());
                model.setUserId(userId);
                Integer userCommentNum = schoolCommentDAO.countRecord(model);
                if (userCommentNum == 0){
                    //获取用户推送新文章等的天key（一天最多一次）
                    String dayCacheKey = CacheKeyUtil.genUserNewNewsDayNoticeKey(userId);
                    RBucket<Integer> dayBucket = redissonClient.getBucket(dayCacheKey);
                    if (dayBucket.get() != null) {
                        continue;
                    }
                    //获取用户推送新文章等的周key（七天最多三次）
                    String weekCacheKey = CacheKeyUtil.genUserNewNewsWeekNoticeKey(userId);
                    RBucket<String> weekBucket = redissonClient.getBucket(weekCacheKey);
                    String weekNoticeNumAndTime = weekBucket.get();
                    Integer weekNoticeNum = null;
                    if (StrUtil.isNotBlank(weekNoticeNumAndTime)) {
                        weekNoticeNum = Integer.valueOf(weekNoticeNumAndTime.split("~")[0]);
                        if (weekNoticeNum >= 3) {
                            continue;
                        }
                    }
                    messagePushService.pushHelpToParentNotice(saveCommentModel.getSchoolId(), schoolName, userId, saveCommentModel.getNickname());
                    dayBucket.set(1, 1, TimeUnit.DAYS);
                    Date now = new Date();
                    if (StrUtil.isNotBlank(weekNoticeNumAndTime)) {
                        Date weekNoticeTime = DateUtil.parse(weekNoticeNumAndTime.split("~")[1], "yyyy-MM-dd-HH-mm-ss");
                        //after避免redis没有及时删除而导致的key错加
                        if (DateUtil.offsetWeek(weekNoticeTime, 1).isAfter(now)) {
                            //计算创建时间到现在还有多少毫秒
                            long outTimeMs = DateUtil.betweenMs(DateUtil.offsetWeek(weekNoticeTime, 1), new Date());
                            weekNoticeNum++;
                            //weekNoticeTime一直保持第一个key创建的时间
                            weekBucket.set(weekNoticeNum + "~" + weekNoticeTime, outTimeMs, TimeUnit.MILLISECONDS);
                        } else {
                            weekBucket.set(1 + "~" + DateUtil.format(now, "yyyy-MM-dd-HH-mm-ss"), 7 * 24 * 60 * 60 * 1000, TimeUnit.MILLISECONDS);
                        }
                    } else {
                        weekBucket.set(1 + "~" + DateUtil.format(now, "yyyy-MM-dd-HH-mm-ss"), 7 * 24 * 60 * 60 * 1000, TimeUnit.MILLISECONDS);
                    }
                }
            }
        }
    }

    /**
     * 通知想读该学校的用户有新评价
     * @param saveCommentModel
     */
    private void noticeCommentToUser(SchoolComment saveCommentModel) {
        StuSch queryModel = new StuSch();
        queryModel.setSchoolId(saveCommentModel.getSchoolId());
        List<StuSch> stuSchList = stuSchDAO.listRecord(queryModel);
        String schoolName = schoolDAO.getSchoolNameById(saveCommentModel.getSchoolId());
        for (StuSch stuSch : stuSchList) {
            //获取用户推送新文章等的天key（一天最多一次）
            String dayCacheKey = CacheKeyUtil.genUserNewNewsDayNoticeKey(stuSch.getUserId());
            RBucket<Integer> dayBucket = redissonClient.getBucket(dayCacheKey);
            if (dayBucket.get() != null) {
                continue;
            }
            //获取用户推送新文章等的周key（七天最多三次）
            String weekCacheKey = CacheKeyUtil.genUserNewNewsWeekNoticeKey(stuSch.getUserId());
            RBucket<String> weekBucket = redissonClient.getBucket(weekCacheKey);
            String weekNoticeNumAndTime = weekBucket.get();
            Integer weekNoticeNum = null;
            if (StrUtil.isNotBlank(weekNoticeNumAndTime)) {
                weekNoticeNum = Integer.valueOf(weekNoticeNumAndTime.split("~")[0]);
                if (weekNoticeNum >= 3) {
                    continue;
                }
            }
            messagePushService.pushNewCommentNotice(saveCommentModel.getSchoolId(), saveCommentModel.getComment(), stuSch.getUserId(), schoolName);
            dayBucket.set(1, 1, TimeUnit.DAYS);
            Date now = new Date();
            if (StrUtil.isNotBlank(weekNoticeNumAndTime)) {
                Date weekNoticeTime = DateUtil.parse(weekNoticeNumAndTime.split("~")[1], "yyyy-MM-dd-HH-mm-ss");
                //after避免redis没有及时删除而导致的key错加
                if (DateUtil.offsetWeek(weekNoticeTime, 1).isAfter(now)) {
                    //计算创建时间到现在还有多少毫秒
                    long outTimeMs = DateUtil.betweenMs(DateUtil.offsetWeek(weekNoticeTime, 1), new Date());
                    weekNoticeNum++;
                    //weekNoticeTime一直保持第一个key创建的时间
                    weekBucket.set(weekNoticeNum + "~" + weekNoticeTime, outTimeMs, TimeUnit.MILLISECONDS);
                } else {
                    weekBucket.set(1 + "~" + DateUtil.format(now, "yyyy-MM-dd-HH-mm-ss"), 7 * 24 * 60 * 60 * 1000, TimeUnit.MILLISECONDS);
                }
            } else {
                weekBucket.set(1 + "~" + DateUtil.format(now, "yyyy-MM-dd-HH-mm-ss"), 7 * 24 * 60 * 60 * 1000, TimeUnit.MILLISECONDS);
            }

        }
    }

    @Override
    public List<SchoolNewsResponse> newsList(final QuerySchoolNewsContext context) {

        School querySchoolModel = new School();
        querySchoolModel.setId(context.getSchoolId());

        School school = schoolDAO.getRecord(querySchoolModel);
        if (school == null) {
            throw new BizException(ErrorCodeEnum.SYS_EXP, "学校不存在");
        }

        String[] subSchoolNames = school.getName().split("（");
        String[] subs = subSchoolNames[0].split("\\(");

        String keyTitle = subs[0];

        // 按照学校名字匹配搜索
        SearchNewsContext searchNewsContext = new SearchNewsContext();
        searchNewsContext.setKey(keyTitle);
        searchNewsContext.setOffset(context.getOffset());
        searchNewsContext.setPageSize(context.getPageSize());
        searchNewsContext.setUserId(context.getUserId());
        return newsService.searchNews(searchNewsContext);
    }

    @Override
    public List<SchoolTabAskResponse> askList(final Long schoolId) {

        List<SchoolTabAskResponse> tabAskResponseList = new ArrayList<>();

        School querySchoolModel = new School();
        querySchoolModel.setId(schoolId);

        School school = schoolDAO.getRecord(querySchoolModel);
        if (school == null) {
            throw new BizException(ErrorCodeEnum.SYS_EXP, "学校不存在");
        }

        AskSearchRequest request = new AskSearchRequest();

        String[] subSchoolNames = school.getName().split("（");
        String[] subs = subSchoolNames[0].split("\\(");

        String keyTitle = subs[0];
        request.setKeyword(keyTitle);
        List<AskResponse> askResponses = askService.search(request);

        if (askResponses != null && askResponses.size() > 0) {
            for (AskResponse askResponse : askResponses) {
                SchoolTabAskResponse tabAskResponse = new SchoolTabAskResponse();
                tabAskResponse.setTitle(askResponse.getTitle());
                tabAskResponse.setAnswerCount(askResponse.getAnswerCount());
                tabAskResponse.setFollowCount(askResponse.getFollowCount());
                tabAskResponse.setAskId(askResponse.getId());
                tabAskResponseList.add(tabAskResponse);
            }
        }

        return tabAskResponseList;
    }

    @Override
    public SchoolTabResponse schoolDetail(final Long schoolId, final Long userId) {
        SchoolTabSearchContext context = new SchoolTabSearchContext();
        context.setUserDistrictInfo(queryUserDistrictInfo(userId));
        return buildSchoolResponseBySchoolId(schoolId, context, null);
    }
}