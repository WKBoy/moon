/*
 * Dian.com Inc.
 * Copyright (c) 2004-2019 All Rights Reserved.
 */
package cn.sedu.moon.service.impl;

import java.util.Date;

import cn.sedu.moon.common.enums.ErrorCodeEnum;
import cn.sedu.moon.common.exception.BizException;
import cn.sedu.moon.controller.request.DistrictUserReportRequest;
import cn.sedu.moon.controller.request.SchoolUserReportRequest;
import cn.sedu.moon.dao.DistrictUserReportDAO;
import cn.sedu.moon.dao.SchoolUserReportDAO;
import cn.sedu.moon.dao.model.DistrictUserReport;
import cn.sedu.moon.dao.model.SchoolUserReport;
import cn.sedu.moon.service.UserReportService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author ${wukong}
 * @version $Id: UserReportServiceImpl.java, v 0.1 2019年03月12日 2:15 AM Exp $
 */
@Service
public class UserReportServiceImpl implements UserReportService {

    @Autowired
    private SchoolUserReportDAO schoolUserReportDAO;

    @Autowired
    private DistrictUserReportDAO districtUserReportDAO;

    @Override
    public void reportSchool(final SchoolUserReportRequest request) {
        SchoolUserReport saveModel = new SchoolUserReport();
        saveModel.setName(request.getName());
        saveModel.setUserId(request.getUserId());
        saveModel.setAddress(request.getAddress());
        saveModel.setInfo(request.getInfo());

        saveModel.setProvinceCode(request.getProvinceCode());
        saveModel.setCityCode(request.getCityCode());
        saveModel.setAreaCode(request.getAreaCode());

        saveModel.setCreateTime(new Date());
        saveModel.setUpdateTime(new Date());

        int result = schoolUserReportDAO.saveRecord(saveModel);
        if (result == 0) {
            throw new BizException(ErrorCodeEnum.SYS_EXP, "保存数据失败");
        }
    }

    @Override
    public void reportDistrict(final DistrictUserReportRequest request) {
        DistrictUserReport saveModel = new DistrictUserReport();
        saveModel.setName(request.getName());
        saveModel.setUserId(request.getUserId());
        saveModel.setAddress(request.getAddress());
        saveModel.setInfo(request.getInfo());

        saveModel.setProvinceCode(request.getProvinceCode());
        saveModel.setCityCode(request.getCityCode());
        saveModel.setAreaCode(request.getAreaCode());

        saveModel.setCreateTime(new Date());
        saveModel.setUpdateTime(new Date());

        int result = districtUserReportDAO.saveRecord(saveModel);
        if (result == 0) {
            throw new BizException(ErrorCodeEnum.SYS_EXP, "保存数据失败");
        }
    }
}