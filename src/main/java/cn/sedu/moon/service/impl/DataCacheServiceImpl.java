/*
 * Dian.com Inc.
 * Copyright (c) 2004-2019 All Rights Reserved.
 */
package cn.sedu.moon.service.impl;

import cn.sedu.moon.common.util.CacheKeyUtil;
import cn.sedu.moon.service.DataCacheService;
import cn.sedu.moon.service.NewsService;
import cn.sedu.moon.service.SchoolService;
import cn.sedu.moon.service.context.QuerySchoolContext;
import org.redisson.api.RBucket;
import org.redisson.api.RedissonClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 *
 * @author ${wukong}
 * @version $Id: DataCacheServiceImpl.java, v 0.1 2019年03月13日 9:48 PM Exp $
 */
@Service
public class DataCacheServiceImpl implements DataCacheService {

    @Autowired
    private RedissonClient redissonClient;

    @Autowired
    private NewsService newsService;

    @Autowired
    @Lazy
    private SchoolService schoolService;

    @Override
    public void reloadNewsCache(final Long userId) {

        String cacheKey = CacheKeyUtil.genUserNewsKey(userId);
        RBucket<List<Long>> bucket = redissonClient.getBucket(cacheKey);
        bucket.delete();

        newsService.queryNews(userId, 1, 5);
    }

    @Override
    public void reloadRecommendSchoolCache(final Long userId) {
        String cacheKey = CacheKeyUtil.genUserRecommendSchoolKey(userId);
        RBucket<List<Long>> bucket = redissonClient.getBucket(cacheKey);
        bucket.delete();

        QuerySchoolContext context = new QuerySchoolContext();
        context.setUserId(userId);
        context.setOffset(0);
        context.setPageSize(5);

        schoolService.recommendSchoolList(context);
    }

    @Override
    public void clearSchoolTabData(final Long userId) {
        String cacheKey = CacheKeyUtil.genSchoolTabUserSchoolKey(userId);
        RBucket<List<Long>> bucket = redissonClient.getBucket(cacheKey);
        bucket.delete();
    }
}