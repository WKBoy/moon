/*
 * Dian.com Inc.
 * Copyright (c) 2004-2019 All Rights Reserved.
 */
package cn.sedu.moon.service.impl;

import cn.sedu.moon.common.enums.GdSourceTypeEnum;
import cn.sedu.moon.common.enums.SchoolTypeEnum;
import cn.sedu.moon.dao.GdDistrictSourceDAO;
import cn.sedu.moon.dao.GdSourceDAO;
import cn.sedu.moon.dao.GdSourcePhotoDAO;
import cn.sedu.moon.dao.model.DistrictGd;
import cn.sedu.moon.dao.model.GdDistrictSource;
import cn.sedu.moon.dao.model.GdSource;
import cn.sedu.moon.dao.model.GdSourcePhoto;
import cn.sedu.moon.dao.model.School;
import cn.sedu.moon.dao.model.TrackerPoint;
import cn.sedu.moon.service.DataService;
import cn.sedu.moon.service.DistrictService;
import cn.sedu.moon.service.SchoolService;
import cn.sedu.moon.service.TrackerService;
import cn.sedu.moon.service.context.data.QueryGdContext;
import cn.sedu.moon.thirdparty.GdApi;
import cn.sedu.moon.thirdparty.dto.GdPoiItem;
import cn.sedu.moon.thirdparty.dto.GdPoiPhotoItem;
import cn.sedu.moon.thirdparty.dto.GdPoiSearchResponse;
import com.alibaba.fastjson.JSON;
import com.github.pagehelper.ISelect;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * @author wukong
 * @version : DataServiceImpl.java, v 0.1 2019年10月31日 9:20 下午 Exp $
 */
@Service
@Slf4j(topic = "DATA-TASK")
public class DataServiceImpl implements DataService {

    @Value("${gd.key}")
    private String gdKey;

    @Autowired
    private GdApi gdApi;

    @Resource
    private GdSourceDAO gdSourceDAO;

    @Resource
    private GdDistrictSourceDAO gdDistrictSourceDAO;

    @Resource
    private GdSourcePhotoDAO gdSourcePhotoDAO;

    @Resource
    private TrackerService trackerService;

    @Autowired
    private DistrictService districtService;

    @Autowired
    private SchoolService schoolService;

    // 多线程服务
    private static ExecutorService executorService = new ThreadPoolExecutor(40, 100, 60L, TimeUnit.SECONDS, new LinkedBlockingQueue<>(100));

    @Override
    @Async
    public void fetchGdData(final String cityCode) {

        // 查询区信息
        GdDistrictSource queryDistrictModel = new GdDistrictSource();
        queryDistrictModel.setParentCode(cityCode);

        List<GdDistrictSource> districtSourceList = gdDistrictSourceDAO.listRecord(queryDistrictModel);
        if (!CollectionUtils.isEmpty(districtSourceList)) {

            List<Future<Integer>> futureList = new ArrayList<>();

            // 按照区获取
            for (GdDistrictSource districtSource : districtSourceList) {

                final String areaName = districtSource.getName();
                final String areaCode = districtSource.getAdcode();

                // 异步执行
                Future<Integer> future = executorService.submit(() -> fetchTask(areaCode, areaName));
                futureList.add(future);
            }

            int finishedCount = 0;
            for (Future<Integer> future : futureList) {
                while (true) {
                    if (future.isDone()) {
                        finishedCount += 1;
                        break;
                    }
                }
            }

            log.info("Task all finished, count: {}", finishedCount);
        }
    }

    private Integer fetchTask(String areaCode, String areaName) {

        log.info("start fetch:{}", areaName);

        fetchDataProcess(areaCode, 1, "幼儿园");
        fetchDataProcess(areaCode, 2, "小学");
        fetchDataProcess(areaCode, 3, "初中");
        fetchDataProcess(areaCode, 4, "高中");
        fetchDataProcess(areaCode, 5, "小区");

        log.info("start finish:{}", areaName);

        return 1;
    }

    private int fetchDataProcess(String areaCode, Integer type, String typeString) {

        QueryGdContext context = new QueryGdContext();
        context.setType(type);
        context.setPageNo(1);
        context.setPageSize(25);
        context.setAreaCode(areaCode);
        context.setKeywords(typeString);

        // 总量
        int total = queryGdTotal(context);
        log.info("fetchDataProcess, total:{}, context:{}", total, JSON.toJSONString(context));
        int pageNo = 1;
        int pageSize = 25;
        int totalPage;

        if (total > 0) {
            totalPage = total / pageSize + 1;

            while (pageNo <= totalPage) {
                context.setPageNo(pageNo);

                // 获取数据
                // 从高德获取数据
                List<GdSource> gdSourceList = queryGd(context);

                // 存入临时数据库
                if (!CollectionUtils.isEmpty(gdSourceList)) {
                    saveGdSourceBatch(gdSourceList);
                }

                pageNo ++;

//                // 延时
                try {
                    Thread.sleep(300);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }

        return 1;
    }

    private int saveGdSourceBatch(List<GdSource> gdSourceList) {
        int result = gdSourceDAO.saveBatch(gdSourceList);

        for (GdSource gdSource : gdSourceList) {
            if (!CollectionUtils.isEmpty(gdSource.getPhotos())) {

                for (GdPoiPhotoItem poiPhotoItem : gdSource.getPhotos()) {
                    if (StringUtils.isNotBlank(poiPhotoItem.getUrl())) {
                        GdSourcePhoto gdSourcePhoto = new GdSourcePhoto();
                        gdSourcePhoto.setGdId(gdSource.getGdId());
                        gdSourcePhoto.setUrl(poiPhotoItem.getUrl());
                        gdSourcePhoto.setCreateTime(new Date());
                        gdSourcePhoto.setUpdateTime(new Date());
                        gdSourcePhoto.setDeleted(0);
                        gdSourcePhotoDAO.saveRecord(gdSourcePhoto);
                    }
                }
            }
        }

        return result;
    }

    private int queryGdTotal(QueryGdContext context) {

        GdPoiSearchResponse gdResponse = gdApi.poiSearch(gdKey, context.getKeywords(),
                context.getAreaCode(), true, context.getPageSize(), context.getPageNo(), "all");

        if (gdResponse.getStatus() == 0) {
            log.info("queryGd status = 0, context: {}, response: {}", JSON.toJSONString(context), JSON.toJSONString(gdResponse));
            return 0;
        }

        return gdResponse.getCount();
    }

    private List<GdSource> queryGd(QueryGdContext context) {

        log.info("queryGd start context: {}", JSON.toJSONString(context));

        List<GdSource> gdSourceList = new ArrayList<>();

        GdPoiSearchResponse gdResponse = gdApi.poiSearch(gdKey, context.getKeywords(),
                context.getAreaCode(), true, context.getPageSize(), context.getPageNo(), "all");

        if (gdResponse.getStatus() == 0) {
            log.info("queryGd status = 0, context: {}, response: {}", JSON.toJSONString(context),
                    JSON.toJSONString(gdResponse));
            return null;
        }

        if (!CollectionUtils.isEmpty(gdResponse.getPoiList())) {
            for (GdPoiItem poiItem : gdResponse.getPoiList()) {
                GdSource gdSource = new GdSource();
                gdSource.setGdId(poiItem.getId());
                gdSource.setName(poiItem.getName());
                gdSource.setAddress(poiItem.getAddress());
                gdSource.setProvinceCode(poiItem.getProvinceCode());
                gdSource.setProvinceName(poiItem.getProvinceName());
                gdSource.setCityCode(poiItem.getCityCode());
                gdSource.setCityName(poiItem.getCityName());
                gdSource.setAreaCode(poiItem.getAreaCode());
                gdSource.setAreaName(poiItem.getAreaName());
                gdSource.setPhotos(poiItem.getPhotos());
                gdSource.setType(context.getType());

                // 经纬度
                if (StringUtils.isNotBlank(poiItem.getLocation())) {
                    try {
                        String[] subStrings = poiItem.getLocation().split(",");
                        if (subStrings.length > 1) {
                            gdSource.setLng(Double.parseDouble(subStrings[0]));
                            gdSource.setLat(Double.parseDouble(subStrings[1]));
                        }
                    } catch (Exception e) {
                        log.error("queryGd parse location info error:", e);
                    }
                }

                gdSource.setCreateTime(new Date());
                gdSource.setUpdateTime(new Date());
                gdSource.setDeleted(0);
                gdSourceList.add(gdSource);
            }
        }

        return gdSourceList;
    }

    @Override
    public void processHistoryPvUvData() {

        List<TrackerPoint> pointList = trackerService.allPoint();
        for (TrackerPoint point : pointList) {
        }
    }

    @Override
//    @Async
    public void processGdProvinceCode() {

        // 从高德读取数据
        GdSource queryTotalModel = new GdSource();
        queryTotalModel.setDeleted(0);
        Integer total = gdSourceDAO.count(queryTotalModel);

        log.info("processGdProvinceCode -- total: {}", total);


        int offset = 0;
        int pageSize = 1000;
        while (offset < total) {

            log.info("processGdProvinceCode -- offset: {}", offset);

            Page<GdSource> page = PageHelper.offsetPage(offset, pageSize).doSelectPage(new ISelect() {
                @Override
                public void doSelect() {
                    gdSourceDAO.listRecord(queryTotalModel);
                }
            });

            if (page.size() > 0) {
                for (GdSource gdSource : page.getResult()) {
                    // 从gd_district_source表中按照市名称查找code
                    GdDistrictSource queryDistrictModel = new GdDistrictSource();
                    queryDistrictModel.setName(gdSource.getCityName());
                    queryDistrictModel.setLevel("city");
                    List<GdDistrictSource> gdDistrictSourceList = gdDistrictSourceDAO.listRecord(queryDistrictModel);
                    if (!CollectionUtils.isEmpty(gdDistrictSourceList)) {
                        GdDistrictSource gdDistrictSource = gdDistrictSourceList.get(0);

                        // 更新gd_source表
                        GdSource updateGdSourceModel = new GdSource();
                        updateGdSourceModel.setId(gdSource.getId());
                        updateGdSourceModel.setCityCode(gdDistrictSource.getAdcode());
                        updateGdSourceModel.setUpdateTime(new Date());

                        int result = gdSourceDAO.updateRecord(updateGdSourceModel);
                        if (result == 0) {
                            log.error("processGdProvinceCode, 更新gd_source表的城市信息失败, gd_source: {}", JSON.toJSONString(gdDistrictSource));
                        }

                    } else {
                        log.info("processGdProvinceCode, 查找不到城市信息, gdSource: {}", JSON.toJSONString(gdSource));
                    }
                }
            }
            offset += pageSize;
        }

        log.info("processGdProvinceCode -- finish");
    }

    @Override
    @Async
    public void processGdDataAsync() {

        // 从高德读取数据
        GdSource queryTotalModel = new GdSource();
        queryTotalModel.setDeleted(0);
        Integer total = gdSourceDAO.count(queryTotalModel);

        log.info("processGdDataAsync -- total: {}", total);

        int offset = 0;
        int pageSize = 1000;
        while (offset < total) {

            log.info("processGdDataAsync -- offset: {}", offset);
            Page<GdSource> page = PageHelper.offsetPage(offset, pageSize).doSelectPage(new ISelect() {
                @Override
                public void doSelect() {
                    gdSourceDAO.listRecord(queryTotalModel);
                }
            });

            if (page.size() > 0) {
                for (GdSource gdSource : page.getResult()) {

                    GdSourceTypeEnum gdSourceTypeEnum = GdSourceTypeEnum.getByCode(gdSource.getType());
                    if (gdSourceTypeEnum == null) {
                        continue;
                    }

                    if (GdSourceTypeEnum.XIAOQU.equals(gdSourceTypeEnum)) {
                        // 存储小区
                        DistrictGd saveDistrictModel = new DistrictGd();
                        saveDistrictModel.setName(gdSource.getName());
                        saveDistrictModel.setAddress(gdSource.getAddress());
                        saveDistrictModel.setProvince(gdSource.getProvinceName());
                        saveDistrictModel.setProvinceCode(gdSource.getProvinceCode());
                        saveDistrictModel.setCity(gdSource.getCityName());
                        saveDistrictModel.setCityCode(gdSource.getCityCode());
                        saveDistrictModel.setArea(gdSource.getAreaName());
                        saveDistrictModel.setAreaCode(gdSource.getAreaCode());
                        saveDistrictModel.setGdId(gdSource.getGdId());
                        saveDistrictModel.setLat(gdSource.getLat());
                        saveDistrictModel.setLng(gdSource.getLng());
                        saveDistrictModel.setUserAdd(0);
                        saveDistrictModel.setCreateTime(new Date());
                        saveDistrictModel.setUpdateTime(new Date());
                        saveDistrictModel.setDeleted(0);

                        int result = districtService.saveDistrict(saveDistrictModel);
                        if (result == 0) {
                            log.error("保存小区数据失败, model: {}", JSON.toJSONString(saveDistrictModel));
                        }

                    } else {
                        // 存储学校
                        School saveSchoolModel = new School();
                        saveSchoolModel.setName(gdSource.getName());
                        saveSchoolModel.setAddress(gdSource.getAddress());
                        saveSchoolModel.setProvince(gdSource.getProvinceName());
                        saveSchoolModel.setProvinceCode(gdSource.getProvinceCode());
                        saveSchoolModel.setCity(gdSource.getCityName());
                        saveSchoolModel.setCityCode(gdSource.getCityCode());
                        saveSchoolModel.setArea(gdSource.getAreaName());
                        saveSchoolModel.setAreaCode(gdSource.getAreaCode());
                        saveSchoolModel.setUserAdd(0);
                        saveSchoolModel.setLat(gdSource.getLat());
                        saveSchoolModel.setLng(gdSource.getLng());

                        // 学校类型
                        SchoolTypeEnum type = null;
                        switch (gdSourceTypeEnum) {
                            case YOUERYUAN:
                                type = SchoolTypeEnum.YOUERYUAN;
                                break;
                            case XIAOXUE:
                                type = SchoolTypeEnum.XIAOXUE;
                                break;
                            case CHUZHONG:
                                type = SchoolTypeEnum.CHUZHONG;
                                break;
                            case GAOZHONG:
                                type = SchoolTypeEnum.GAOZHONG;
                                break;
                        }

                        if (type != null) {
                            saveSchoolModel.setType(type.getCode());
                        }

                        saveSchoolModel.setCreateTime(new Date());
                        saveSchoolModel.setUpdateTime(new Date());
                        saveSchoolModel.setDeleted(0);

                        int result = schoolService.saveSchool(saveSchoolModel);
                        if (result == 0) {
                            log.error("保存学校数据失败, model: {}", JSON.toJSONString(saveSchoolModel));
                        }
                    }
                }
            }

            offset += pageSize;
        }

        log.info("processGdDataAsync -- finish");
    }
}