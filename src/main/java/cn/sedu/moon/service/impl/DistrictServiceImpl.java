/*
 * Dian.com Inc.
 * Copyright (c) 2004-2019 All Rights Reserved.
 */
package cn.sedu.moon.service.impl;

import cn.sedu.moon.dao.DistrictGdDAO;
import cn.sedu.moon.dao.model.DistrictGd;
import cn.sedu.moon.service.DistrictService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * @author wukong
 * @version : DistrictServiceImpl.java, v 0.1 2019年11月19日 11:02 上午 Exp $
 */
@Service
public class DistrictServiceImpl implements DistrictService {

    @Resource
    private DistrictGdDAO districtGdDAO;

    @Override
    public int saveDistrict(final DistrictGd districtGd) {
        return districtGdDAO.saveRecord(districtGd);
    }
}