/*
 * Dian.com Inc.
 * Copyright (c) 2004-2018 All Rights Reserved.
 */
package cn.sedu.moon.service.impl;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.map.MapUtil;
import cn.hutool.core.util.StrUtil;
import cn.sedu.moon.common.enums.AreaLevelEnum;
import cn.sedu.moon.common.enums.ErrorCodeEnum;
import cn.sedu.moon.common.enums.IndexNewsTypeEnum;
import cn.sedu.moon.common.enums.SchoolPropertyEnum;
import cn.sedu.moon.common.enums.SchoolTypeEnum;
import cn.sedu.moon.common.exception.BizException;
import cn.sedu.moon.common.util.CacheKeyUtil;
import cn.sedu.moon.controller.request.cms.school.CmsSchoolSearchRequest;
import cn.sedu.moon.controller.request.cms.school.CmsSchoolSetHotRequest;
import cn.sedu.moon.controller.request.school.UserAddSchoolRequest;
import cn.sedu.moon.controller.response.AskNewsResponse;
import cn.sedu.moon.controller.response.SchoolDetailResponse;
import cn.sedu.moon.controller.response.SchoolRecommendResponse;
import cn.sedu.moon.controller.response.SchoolResponse;
import cn.sedu.moon.controller.response.cms.school.CmsSchoolResponse;
import cn.sedu.moon.controller.response.cms.school.CmsSearchSchoolResponse;
import cn.sedu.moon.controller.response.school.UserAddSchoolResponse;
import cn.sedu.moon.dao.*;
import cn.sedu.moon.dao.model.*;
import cn.sedu.moon.dao.mongo.SchoolLocation;
import cn.sedu.moon.dao.mongo.SchoolLocationRepository;
import cn.sedu.moon.service.*;
import cn.sedu.moon.service.context.QuerySchoolContext;
import cn.sedu.moon.service.context.SchoolAttentionContext;
import com.alibaba.fastjson.JSON;
import com.github.pagehelper.ISelect;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.redisson.api.RBucket;
import org.redisson.api.RedissonClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.geo.Point;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import javax.annotation.Resource;
import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * 学校相关服务的实现类
 *
 * @author ${wukong}
 * @version $Id: SchoolServiceImpl.java, v 0.1 2018年12月10日 11:47 AM Exp $
 */
@Service
@Slf4j
public class SchoolServiceImpl implements SchoolService {

    @Autowired
    private StuSchDAO stuSchDAO;

    @Autowired
    private SchoolDAO schoolDAO;

    @Autowired
    private AreaDataDAO areaDataDAO;

    @Autowired
    private StudentArchivesDAO archivesDAO;

    @Autowired
    private SopService sopService;

    @Autowired
    private SopSubjectDAO sopSubjectDAO;

    @Autowired
    private SchoolSrcDAO schoolSrcDAO;

    @Autowired
    private SchoolNewsSrcDAO schoolNewsSrcDAO;

    @Autowired
    private SchoolImgSrcDAO schoolImgSrcDAO;

    @Autowired
    private AskDAO askDAO;

    @Autowired
    private AskService askService;

    @Autowired
    private UserDAO userDAO;

    @Autowired
    private DistrictGdDAO districtGdDAO;

    @Autowired
    private SchoolDetailDAO schoolDetailDAO;

    @Autowired
    private DataCacheService cacheService;

    @Autowired
    private RedissonClient redissonClient;

    @Autowired
    private SchoolLocationRepository locationRepository;

    @Resource
    private SchoolHotDAO schoolHotDAO;

    @Autowired
    private MessagePushService messagePushService;

    @Resource
    private SchoolCommentDAO schoolCommentDAO;

    // 多线程服务
    private static ExecutorService executorService = new ThreadPoolExecutor(40, 100, 60L, TimeUnit.SECONDS, new LinkedBlockingQueue<>(100));

    // 钱塘外国语小学 49779、育才实验小学 49950、绿城育华小学 49771、新世纪外国语小学 54204 、
    // 江南实验小学 54301、人大附中小学 52121、云谷小学 53986， 崇文实验小学 49885L
    private static final List<Long> minBanSchoolIdList = Arrays.asList(49779L, 49950L, 49771L, 54204L, 54301L, 52121L, 53986L, 49885L);


    // 学军小学 49756L 、天长小学 53854L  、胜利小学 53850L、西湖小学  54182L 、求是小学 53852L 、
    // 文三街小学 54197L、安吉路小学 54174L、保俶塔小学 54180L、采荷二小 54188L
    private static final List<Long> gongBanSchoolIdList = Arrays.asList(49756L, 53854L, 53850L, 54182L, 53852L, 54197L, 54174L, 54180L,
            54188L);


    // 北京热门公办学校: 190 北京第二实验小学, 97 北京市师范大学实验小学（北师大附小）, 89 北京小学本部, 206 北京市中关村第三小学（中关村三小）, 145 北京市中关村第一小学（中关村一小）
    // 109 北京市府学小学（府学胡同小学）,   100 北京市史家小学,   1190 北京市汇文第一小学（汇文一小）（原丁香小学）, 3355 京第一师范学校附属小学（一师附小）
    // 523 北京市育民小学, 1405 北京育才学校小学部  , 963 北京市西城区黄城根小学, 716 中国人民大学附属小学（人大附小）（RDF）
    // 359 北京景山学校小学部, 1355 北京市中关村第二小学（中关村二小）
    private static final List<Long> bjGongBanSchoolIdList = Arrays.asList(190L, 97L, 89L, 206L, 145L, 109L, 100L, 1190L, 3355L,
            523L, 1405L, 963L, 716L, 359L, 1355L);


    // 16908 北京市建华实验学校小学部,  11073 北京拔萃双语学校, 2778 北京私立汇佳学校（小学部), 44319 北京市二十一世纪国际学校（实验学校),
    // 45046 北京市力迈学校，6778 北京市海淀外国语实验学校小学文体特长班， 25000 北大附属实验学校，37369 北京美国英语语言学院，
    // 1140 北京市昌平区天通苑小学，16524   北京市私立星星学校小学部
    private static final List<Long> bjMinBanSchoolIdList = Arrays.asList(16908L, 11073L, 2778L, 44319L, 45046L, 6778L, 25000L, 37369L, 1140L, 16524L);


    // 衢师二附小、衢州市实验学校 79884、新华小学79819、鹿鸣小学79819L、新星学校79885L、巨化第一小学79834L、浙师大附属衢州白云学校 80088
    private static final List<Long> quZhouGongBanSchoolIdList = Arrays.asList(79884L, 79819L, 79885L, 79834L, 80088L);

    // 衢州市衢江区第一小学、江山实验小学、城南小学(塘源口分校)，城南小学(北校区),    江山市中山小学、江山市解放路小学,
    private static final List<Long> quZhouMinBanSchoolIdList = Arrays.asList(79929L, 79847L, 79849L, 79932L, 79806L, 79835L);


    private AskNewsResponse queryAsk(Long userId) {

        Random random = new Random();
        Ask queryAskModel = new Ask();
        List<Ask> queryResult = askDAO.listRecord(queryAskModel);
        if (queryResult != null && queryResult.size() > 0) {
            int randomInt = random.nextInt(queryResult.size());
            Ask ask = queryResult.get(randomInt);

            AskNewsResponse response = new AskNewsResponse();
            response.setNewsType(IndexNewsTypeEnum.ASK.getCode());
            response.setAskId(ask.getId());
            response.setTitle(ask.getTitle());
            response.setFollowCount(askService.queryAskAttentionCount(ask.getId()));
            response.setAnswerCount(askService.queryAskAnswerCount(ask.getId()));

            User queryUserModel = new User();
            queryUserModel.setId(ask.getPublisher());
            User user = userDAO.getRecord(queryUserModel);
            if (user != null) {
                response.setAvatar(user.getAvatar());
                response.setNickName(user.getNickname());
            }

            return response;
        }

        return null;
    }

    @Override
    public List<SchoolResponse> nearBy(final QuerySchoolContext context) {

        SchoolExtendDO querySchoolModel = new SchoolExtendDO();


        if (context.getAreaCode() != null) {

            AreaData queryAreaModel = new AreaData();
            queryAreaModel.setAreaCode(context.getAreaCode());
            AreaData areaData = areaDataDAO.getRecord(queryAreaModel);
            if (areaData != null) {
                querySchoolModel.setAreaCode(areaData.getAreaCode());
                // 查询市
                queryAreaModel.setAreaCode(areaData.getParentCode());
                areaData = areaDataDAO.getRecord(queryAreaModel);
                if (areaData != null) {
                    querySchoolModel.setCityCode(areaData.getAreaCode());
                }
            }
        }

        Page<School> page;

        // 根据关键字搜索
        if (StringUtils.isNotBlank(context.getKeyName())) {

            querySchoolModel.setKeyName(context.getKeyName());

            // 学校类型检索
            if (context.getSchoolType() != null) {
                querySchoolModel.setType(context.getSchoolType().getCode());
            }

            page = PageHelper.offsetPage(context.getOffset(), context.getPageSize()).doSelectPage(new ISelect() {
                @Override
                public void doSelect() {
                    schoolDAO.queryByKeyName(querySchoolModel);
                }
            });

        } else {
            // 学校类型检索
            if (context.getSchoolType() != null) {
                querySchoolModel.setType(context.getSchoolType().getCode());
            }

            // 如果没有传如关键字，则全部返回
            page = PageHelper.offsetPage(context.getOffset(), context.getPageSize()).doSelectPage(
                    () -> schoolDAO.listRecord(querySchoolModel));
        }

        // 构造response
        if (page != null && page.size() > 0) {
            List<SchoolResponse> responseList = new ArrayList<>();
            for (School school : page.getResult()) {
                SchoolResponse response = new SchoolResponse();
                response.setName(school.getName());
                response.setSchoolId(school.getId());
                response.setValue(school.getId());

                SchoolTypeEnum schoolTypeEnum = SchoolTypeEnum.getByCode(school.getType());
                if (schoolTypeEnum != null) {
                    response.setType(schoolTypeEnum.getCode());
                }

                responseList.add(response);
            }

            return responseList;
        }

        return new ArrayList<>();
    }

    /**
     * 1、家对口的公办1所（离的最近的公办）--离家最近的公办
     * 2、车程15分钟以内的民办2-3所（6公里以内）--车程15分钟以内的民办
     * 3、家所在区热门学校（6公里内、人工给出）--家所在区热门学校
     * 4、杭州热门民办（人工给出10所）--杭州市热门民办
     * 5、杭州热门工办（人工给出10所）--杭州市热门公办, 有爆表被调剂风险
     */
    @Override
    public List<SchoolRecommendResponse> queryRecommendSchoolWithType(final QuerySchoolContext context) {

        List<SchoolRecommendResponse> list = new ArrayList<>();

        // 获取用户家的地理位置
        DistrictGd userDistrict = queryUserDistrictInfo(context.getUserId());
        if (userDistrict == null || userDistrict.getLat() == null || userDistrict.getLng() == null) {
            return list;
        }

        context.setCityCode(userDistrict.getCityCode());
        context.setCityName(userDistrict.getCity());

        if (context.getRecommendType() == 1) {

            //---- 离的最近的公办

            // 目前需求只推荐1所
            context.setOffset(0);
            context.setPageSize(3);

            // 根据经纬度查找
            Point queryPoint = new Point(userDistrict.getLng(), userDistrict.getLat());
            List<SchoolLocation> locations = locationRepository.findCircleNear(queryPoint, 10,
                    SchoolPropertyEnum.GONGBAN, SchoolTypeEnum.XIAOXUE, context.getOffset(), context.getPageSize());

            locations.forEach(location -> {
                SchoolRecommendResponse recommendResponse = recommendResponseBySchoolId(location.getSchoolId());
                if (recommendResponse != null) {
                    list.add(recommendResponse);
                }
            });

            return list;

        } else if (context.getRecommendType() == 2) {

            // ----- 6公里以内的民办

            // 目前需求只推荐3所
            context.setOffset(0);
            context.setPageSize(3);

            // 根据经纬度查找
            Point queryPoint = new Point(userDistrict.getLng(), userDistrict.getLat());
            List<SchoolLocation> locations = locationRepository.findCircleNear(queryPoint, 10,
                    SchoolPropertyEnum.MINBAN, SchoolTypeEnum.XIAOXUE, context.getOffset(), context.getPageSize());

            locations.forEach(location -> {
                SchoolRecommendResponse recommendResponse = recommendResponseBySchoolId(location.getSchoolId());
                if (recommendResponse != null) {
                    list.add(recommendResponse);
                }
            });

            return list;

        } else if (context.getRecommendType() == 3) {

            // 家所在区热门学校
            DistrictGd userDistrict2 = queryUserDistrictInfo(context.getUserId());
            if (userDistrict2 == null || StringUtils.isBlank(userDistrict2.getCityCode()) || StringUtils.isBlank(userDistrict2.getAreaCode())) {
                return list;
            }

            // 查询热门学校
            SchoolHot queryHotModel = new SchoolHot();
            queryHotModel.setCityCode(userDistrict2.getCityCode());
            queryHotModel.setAreaCode(userDistrict2.getAreaCode());
            queryHotModel.setAreaHot(1);
            queryHotModel.setDeleted(0);

            Page<SchoolHot> page = PageHelper.offsetPage(context.getOffset(), context.getPageSize()).doSelectPage(new ISelect() {
                @Override
                public void doSelect() {
                    schoolHotDAO.listRecord(queryHotModel);
                }
            });

            if (page != null && page.size() > 0) {
                page.getResult().forEach(schoolHot -> {
                    SchoolRecommendResponse recommendResponse = recommendResponseBySchoolId(schoolHot.getSchoolId());
                    if (recommendResponse != null) {
                        list.add(recommendResponse);
                    }
                });
            }

            return list;

        } else if (context.getRecommendType() == 4) {

            // 市热门民办学校
            SchoolHot queryHotModel = new SchoolHot();
            queryHotModel.setCityCode(context.getCityCode());
            queryHotModel.setProperty(SchoolPropertyEnum.MINBAN.getCode());
            queryHotModel.setCityHot(1);
            queryHotModel.setDeleted(0);

            Page<SchoolHot> page = PageHelper.offsetPage(context.getOffset(), context.getPageSize()).doSelectPage(new ISelect() {
                @Override
                public void doSelect() {
                    schoolHotDAO.listRecord(queryHotModel);
                }
            });

            if (page != null && page.size() > 0) {
                page.getResult().forEach(schoolHot -> {
                    SchoolRecommendResponse recommendResponse = recommendResponseBySchoolId(schoolHot.getSchoolId());
                    if (recommendResponse != null) {
                        list.add(recommendResponse);
                    }
                });
            }

            return list;

        } else if (context.getRecommendType() == 5) {

            // 市热门公办学校
            SchoolHot queryHotModel = new SchoolHot();
            queryHotModel.setCityCode(context.getCityCode());
            queryHotModel.setProperty(SchoolPropertyEnum.GONGBAN.getCode());
            queryHotModel.setCityHot(1);
            queryHotModel.setDeleted(0);

            Page<SchoolHot> page = PageHelper.offsetPage(context.getOffset(), context.getPageSize()).doSelectPage(new ISelect() {
                @Override
                public void doSelect() {
                    schoolHotDAO.listRecord(queryHotModel);
                }
            });

            if (page != null && page.size() > 0) {
                page.getResult().forEach(schoolHot -> {
                    SchoolRecommendResponse recommendResponse = recommendResponseBySchoolId(schoolHot.getSchoolId());
                    if (recommendResponse != null) {
                        list.add(recommendResponse);
                    }
                });
            }

            return list;
        }

        return list;
    }

    private DistrictGd queryUserDistrictInfo(Long userId) {
        // 获取用户家的地理位置
        StudentArchives queryArchiveModel = new StudentArchives();
        queryArchiveModel.setUserId(userId);
        queryArchiveModel.setDeleted(0);

        List<StudentArchives> queryArchiveResult = archivesDAO.listRecord(queryArchiveModel);
        if (queryArchiveResult == null || queryArchiveResult.size() == 0 || queryArchiveResult.get(0).getDistrictHouseId() == null) {
            return null;
        }

        DistrictGd queryDistrictModel = new DistrictGd();
        queryDistrictModel.setId(queryArchiveResult.get(0).getDistrictHouseId());
        queryDistrictModel.setDeleted(0);

        return districtGdDAO.getRecord(queryDistrictModel);
    }

    @Override
    public List<SchoolRecommendResponse> recommendSchoolList(QuerySchoolContext context) {

        // 1 获取用户提交的档案信息
        StudentArchives queryArchiveModel = new StudentArchives();
        queryArchiveModel.setUserId(context.getUserId());
        queryArchiveModel.setDeleted(0);
        StudentArchives queryArchiveResult = archivesDAO.getRecord(queryArchiveModel);
        if (queryArchiveResult == null) {
            throw new BizException(ErrorCodeEnum.SYS_EXP, "请先提交档案信息");
        }

        // todo 公办民办信息
        Integer schoolProperty = queryArchiveResult.getSchoolProperty();

        // 查询用户所在的小区
        if (queryArchiveResult.getDistrictHouseId() == null) {
            log.error("recommendSchoolList, 没有查找到小区信息，context is {}", JSON.toJSONString(context));
            throw new BizException(ErrorCodeEnum.SYS_EXP, "没有查找到小区信息");
        }

        DistrictGd queryDistrictModel = new DistrictGd();
        queryDistrictModel.setId(queryArchiveResult.getDistrictHouseId());
        DistrictGd queryDistrictResult = districtGdDAO.getRecord(queryDistrictModel);

        if (queryDistrictResult == null) {
            throw new BizException(ErrorCodeEnum.SYS_EXP, "查找不到小区信息");
        }

        // 小区所在城市和区
        String cityName = queryDistrictResult.getCity();
        String areaName = queryDistrictResult.getArea();
        log.info("推荐学校检索使用的区域信息--->" + cityName + ":" + areaName);

        context.setCityName(cityName);
        context.setAreaName(areaName);
        context.setSchoolType(SchoolTypeEnum.XIAOXUE);

        List<Long> idList = getSchoolIdList(context);

        // 构造response
        if (idList != null && idList.size() > 0) {
            List<SchoolRecommendResponse> responseList = new ArrayList<>();

            for (Long schoolId : idList) {

                SchoolRecommendResponse recommendResponse = recommendResponseBySchoolId(schoolId);
                if (recommendResponse != null) {
                    responseList.add(recommendResponse);
                }

            }

            return responseList;
        }

        return new ArrayList<>();
    }

    private SchoolRecommendResponse recommendResponseBySchoolId(Long schoolId) {

        // 查询school
        School querySchoolModel = new School();
        querySchoolModel.setId(schoolId);
        School school = schoolDAO.getRecord(querySchoolModel);
        if (school != null) {

            SchoolRecommendResponse response = new SchoolRecommendResponse();
            response.setSchoolName(school.getName());
            response.setSchoolId(school.getId());

            // 查询图片
            List<String> imageList = new ArrayList<>();
            SchoolSrc querySchoolSrc = new SchoolSrc();
            querySchoolSrc.setName(school.getName());

            // 先去学校src 表中查询数据
            List<SchoolSrc> querySchoolSrcResult = schoolSrcDAO.listRecord(querySchoolSrc);
            if (querySchoolSrcResult != null && querySchoolSrcResult.size() > 0) {
                SchoolSrc schoolSrc = querySchoolSrcResult.get(0);
                imageList.add(schoolSrc.getThumbnail());

                // 再去img_src 中查询数据
                SchoolImgSrc queryImgSrcModel = new SchoolImgSrc();
                queryImgSrcModel.setSxId(schoolSrc.getSxId());
                List<SchoolImgSrc> schoolImgSrcList = schoolImgSrcDAO.listRecord(queryImgSrcModel);
                if (schoolImgSrcList != null && schoolImgSrcList.size() > 0) {
                    for (SchoolImgSrc imgSrc : schoolImgSrcList) {
                        if (StringUtils.isNotBlank(imgSrc.getImages())) {
                            imageList.add(imgSrc.getImages());
                        }
                    }
                }
            }
            response.setImages(imageList);

            if (StringUtils.isNotBlank(school.getAttribute())) {
                List<String> tagList = new ArrayList<>();
                tagList.add(school.getAttribute());
                response.setTags(tagList);
            }

            // 是否有详情
            SchoolDetail querySchoolDetailModel = new SchoolDetail();
            querySchoolDetailModel.setSchoolId(school.getId());
            SchoolDetail schoolDetail = schoolDetailDAO.getRecord(querySchoolDetailModel);

            if (schoolDetail != null && schoolDetail.getIntroduction() != null) {
                response.setHasDetail(true);
            } else {
                response.setHasDetail(false);
            }

            return response;
        }

        return null;
    }

    private List<Long> getSchoolIdList(QuerySchoolContext context) {

        // 去 redis 中查询是否有数据
        String cacheKey = CacheKeyUtil.genUserRecommendSchoolKey(context.getUserId());
        RBucket<List<Long>> bucket = redissonClient.getBucket(cacheKey);

        if (bucket.get() == null) {
            Set<Long> idSet = new HashSet();
            List<Long> idList = new ArrayList<>();

            // logicA
            List<School> logicAList = recommendSchoolLogicA(context);
            if (logicAList != null && logicAList.size() > 0) {
                for (School school : logicAList) {
                    idSet.add(school.getId());
                    idList.add(school.getId());
                }
            }

            // logicB
            List<School> logicBList = recommendSchoolLogicB(context);
            if (logicBList != null && logicBList.size() > 0) {
                for (School school : logicBList) {
                    if (!idSet.contains(school.getId())) {
                        idSet.add(school.getId());
                        idList.add(school.getId());
                    }
                }
            }

            bucket.set(idList, 1440, TimeUnit.MINUTES);

            Integer startIndex = context.getOffset();
            Integer endIndex = context.getOffset() + context.getPageSize() - 1;

            List<Long> resultList = new ArrayList<>();

            // 数据已经加载完了
            if (startIndex > idList.size() - 1) {
                return null;
            }

            for (int i = 0; i < idList.size(); i++) {
                if (i >= startIndex && i <= endIndex) {
                    resultList.add(idList.get(i));
                }
            }

            return resultList;

        } else {
            List<Long> idList = new ArrayList<>();

            Integer startIndex = context.getOffset();
            Integer endIndex = context.getOffset() + context.getPageSize() - 1;

            // 数据已经加载完了
            if (startIndex > bucket.get().size() - 1) {
                return null;
            }

            for (int i = 0; i < bucket.get().size(); i++) {
                if (i >= startIndex && i <= endIndex) {
                    idList.add(bucket.get().get(i));
                }
            }

            return idList;
        }
    }

    private List<School> recommendSchoolLogicA(QuerySchoolContext context) {
        // 所在区的top10热门学校
        School querySchoolModel = new School();
        querySchoolModel.setCity(context.getCityName());
        querySchoolModel.setArea(context.getAreaName());
        querySchoolModel.setType(context.getSchoolType().getCode());

        return schoolDAO.listRecordByAreaInfoAndHighScore(querySchoolModel);
    }

    private List<School> recommendSchoolLogicB(QuerySchoolContext context) {
        // 杭州的热门学校
        School querySchoolModel = new School();
        querySchoolModel.setCity(context.getCityName());
        querySchoolModel.setAreaCode(context.getAreaCode());
        querySchoolModel.setType(context.getSchoolType().getCode());

        return schoolDAO.listHotInCity(querySchoolModel);
    }

    @Override
    public List<SchoolResponse> attentionSchoolList(final QuerySchoolContext context) {

        StuSch queryModel = new StuSch();
        queryModel.setUserId(context.getUserId());
        queryModel.setDeleted(0);

        List<StuSch> result = stuSchDAO.listRecord(queryModel);
        if (result != null && result.size() > 0) {
            List<SchoolResponse> responseList = new ArrayList<>();

            for (StuSch stuSch : result) {
                // 查询学校信息
                School querySchoolModel = new School();
                querySchoolModel.setId(stuSch.getSchoolId());
                School schoolResult = schoolDAO.getRecord(querySchoolModel);
                if (schoolResult != null) {
                    SchoolResponse schoolResponse = new SchoolResponse();

                    schoolResponse.setSchoolId(schoolResult.getId());
                    schoolResponse.setName(schoolResult.getName());
                    schoolResponse.setValue(schoolResponse.getSchoolId());
                    responseList.add(schoolResponse);
                }
            }

            return responseList;
        }

        return new ArrayList<>();
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void attentionSchoolReport(final SchoolAttentionContext context) {

        // 查看原来是否有想读学校
        Integer stuSchCount = stuSchDAO.countByStuId(context.getUserId());
        StuSch deleteModel = new StuSch();
        deleteModel.setUserId(context.getUserId());
        stuSchDAO.deleteByUserId(deleteModel);

        for (Long schoolId : context.getSchoolList()) {
            StuSch stuSch = new StuSch();
            stuSch.setSchoolId(schoolId);
            stuSch.setUserId(context.getUserId());

            stuSch.setDeleted(0);
            stuSch.setCreateTime(new Date());
            stuSch.setUpdateTime(new Date());

            int result = stuSchDAO.saveRecord(stuSch);
            if (result == 0) {
                throw new BizException(ErrorCodeEnum.SYS_EXP, "保存信息失败");
            }
        }

        // 更新缓存推荐数据
        reloadNewsData(context.getUserId());
        reloadRecommendSchoolCache(context.getUserId());
        cacheService.clearSchoolTabData(context.getUserId());

        // 通知该学校的在校生家长
        executorService.submit(() -> noticeHelpToParent(context.getSchoolList()));

        // 新用户注册成功通知
        if (stuSchCount == 0) {
            // 新用户第一次填写用户信息时，记录用户信息创建时间和用户ID
            Long userId = context.getUserId();
            String cacheKey = CacheKeyUtil.genNewUserArchiveMapKey();
            RBucket<Map<String, Date>> bucket = redissonClient.getBucket(cacheKey);
            if (bucket != null) {
                Map<String, Date> newUserMap = bucket.get();
                if (MapUtil.isEmpty(newUserMap)) {
                    newUserMap = new HashMap<>();
                }
                if (!newUserMap.containsKey(userId + "")) {
                    newUserMap.put(userId + "", new Date());
                    bucket.set(newUserMap);
                }
            }
            // 填写信息成功，将新用户登陆时的缓存删除
            cacheKey = CacheKeyUtil.genNewUserNotArchiveMapKey();
            bucket = redissonClient.getBucket(cacheKey);
            if (bucket != null) {
                Map<String, Date> newUserMap = bucket.get();
                if (MapUtil.isEmpty(newUserMap)) {
                    newUserMap = new HashMap<>();
                }
                if (newUserMap.containsKey(userId + "")) {
                    newUserMap.remove(userId + "");
                    bucket.set(newUserMap);
                }
            }
            executorService.submit(() -> noticeNewRegisterUser(context.getUserId()));
        }
    }

    /**
     * 新用户注册成功通知
     *
     * @param userId
     */
    private void noticeNewRegisterUser(Long userId) {
        User user = new User();
        user.setId(userId);
        user = userDAO.getRecord(user);
        if (user != null) {
            String msg = user.getNickname();
            if (StrUtil.isBlank(msg)) {
                msg = "快前往小鹿上学【我的】页面授权您的微信昵称获得荣誉值";
            }
            StudentArchives studentArchives = new StudentArchives();
            studentArchives.setUserId(userId);
            studentArchives.setDeleted(0);
            studentArchives = archivesDAO.getRecord(studentArchives);
            if (studentArchives != null && StrUtil.isNotBlank(studentArchives.getCityName())) {
                messagePushService.pushRegisterSuccessMsg(msg, userId, studentArchives.getCityName(), new Date());
            }

        }
    }

    /**
     * 求助在校生家长
     * 当有家长提交了想读学校，若该学校无评价，则在校生家长将接收到消息，展示“多位家长想邀请您，对XXX学校提出宝贵的评价。评价学校，将奖励荣誉值+2”
     * 若学校有评价，在校生家长未发布过该学校评价，将接收到消息，展示“多位家长想邀请您，对XXX学校提出宝贵的评价。评价学校，将奖励荣誉值+2”
     *
     * @param schoolList
     */
    private void noticeHelpToParent(List<Long> schoolList) {
        for (Long schoolId : schoolList) {
            String schoolName = schoolDAO.getSchoolNameById(schoolId);
            List<Long> parentIdList = archivesDAO.listUserIdBySchoolId(schoolId);
            for (Long parentId : parentIdList) {
                SchoolComment schoolComment = new SchoolComment();
                schoolComment.setSchoolId(schoolId);
                schoolComment.setUserId(parentId);
                List<SchoolComment> schoolCommentList = schoolCommentDAO.listRecord(schoolComment);
                if (!CollectionUtils.isEmpty(schoolCommentList)) {
                    continue;
                }
                String userName = userDAO.getUserNameById(parentId);
                //获取用户推送新文章等的天key（一天最多一次）
                String dayCacheKey = CacheKeyUtil.genUserNewNewsDayNoticeKey(parentId);
                RBucket<Integer> dayBucket = redissonClient.getBucket(dayCacheKey);
                if (dayBucket.get() != null) {
                    continue;
                }
                //获取用户推送新文章等的周key（七天最多三次）
                String weekCacheKey = CacheKeyUtil.genUserNewNewsWeekNoticeKey(parentId);
                RBucket<String> weekBucket = redissonClient.getBucket(weekCacheKey);
                String weekNoticeNumAndTime = weekBucket.get();
                Integer weekNoticeNum = null;
                if (StrUtil.isNotBlank(weekNoticeNumAndTime)) {
                    weekNoticeNum = Integer.valueOf(weekNoticeNumAndTime.split("~")[0]);
                    if (weekNoticeNum >= 3) {
                        continue;
                    }
                }
                messagePushService.pushHelpToParentNotice(schoolId, schoolName, parentId, userName);
                dayBucket.set(1, 1, TimeUnit.DAYS);
                Date now = new Date();
                if (StrUtil.isNotBlank(weekNoticeNumAndTime)) {
                    Date weekNoticeTime = cn.hutool.core.date.DateUtil.parse(weekNoticeNumAndTime.split("~")[1], "yyyy-MM-dd-HH-mm-ss");
                    //after避免redis没有及时删除而导致的key错加
                    if (cn.hutool.core.date.DateUtil.offsetWeek(weekNoticeTime, 1).isAfter(now)) {
                        //计算创建时间到现在还有多少毫秒
                        long outTimeMs = cn.hutool.core.date.DateUtil.betweenMs(cn.hutool.core.date.DateUtil.offsetWeek(weekNoticeTime, 1), new Date());
                        weekNoticeNum++;
                        //weekNoticeTime一直保持第一个key创建的时间
                        weekBucket.set(weekNoticeNum + "~" + weekNoticeTime, outTimeMs, TimeUnit.MILLISECONDS);
                    } else {
                        weekBucket.set(1 + "~" + cn.hutool.core.date.DateUtil.format(now, "yyyy-MM-dd-HH-mm-ss"), 7 * 24 * 60 * 60 * 1000, TimeUnit.MILLISECONDS);
                    }
                } else {
                    weekBucket.set(1 + "~" + cn.hutool.core.date.DateUtil.format(now, "yyyy-MM-dd-HH-mm-ss"), 7 * 24 * 60 * 60 * 1000, TimeUnit.MILLISECONDS);
                }
            }
        }
    }

    @Async
    void reloadNewsData(Long userId) {
        cacheService.reloadNewsCache(userId);
        log.info("刷新用户新闻缓存数据完成 userId is " + userId);

    }

    @Async
    void reloadRecommendSchoolCache(Long userId) {
        cacheService.reloadRecommendSchoolCache(userId);
        log.info("刷新用户推荐学校缓存数据完成 userId is " + userId);

    }

    @Override
    public SchoolDetailResponse detail(final Long schoolId) {

        // 查询学校信息
        School querySchoolModel = new School();
        querySchoolModel.setId(schoolId);

        School resultSchool = schoolDAO.getRecord(querySchoolModel);

        // 构造response
        if (resultSchool != null) {

            SchoolDetail querySchoolDetailModel = new SchoolDetail();
            querySchoolDetailModel.setSchoolId(schoolId);
            SchoolDetail schoolDetail = schoolDetailDAO.getRecord(querySchoolDetailModel);

            if (schoolDetail == null || schoolDetail.getIntroduction() == null) {
                throw new BizException(ErrorCodeEnum.SYS_EXP, "该学校详情正在收集中哦～");
            }

            SchoolDetailResponse response = new SchoolDetailResponse();

            String desc = schoolDetail.getIntroduction();
            desc = desc.replaceAll("\\s", "\r\n");
            response.setDesc(desc);
            return response;
        }

        return null;
    }

    @Override
    public UserAddSchoolResponse userAddSchool(final UserAddSchoolRequest request) {

        UserAddSchoolResponse response = new UserAddSchoolResponse();

        School addSchoolModel = new School();
        addSchoolModel.setName(request.getSchoolName());

        // 省市区信息处理
        AreaData queryAreaModel = new AreaData();
        queryAreaModel.setAreaCode(request.getAreaCode());
        AreaData areaData = areaDataDAO.getRecord(queryAreaModel);
        if (areaData == null || !AreaLevelEnum.AREA.getCode().equals(areaData.getAreaLevel())) {
            throw new BizException(ErrorCodeEnum.SYS_EXP, "省市区信息有误");
        }

        AreaData queryCityModel = new AreaData();
        queryCityModel.setAreaCode(areaData.getParentCode());
        AreaData cityData = areaDataDAO.getRecord(queryCityModel);
        if (cityData == null || !AreaLevelEnum.CITY.getCode().equals(cityData.getAreaLevel())) {
            throw new BizException(ErrorCodeEnum.SYS_EXP, "省市区信息有误");
        }

        AreaData queryProvinceModel = new AreaData();
        queryProvinceModel.setAreaCode(cityData.getParentCode());
        AreaData provinceData = areaDataDAO.getRecord(queryProvinceModel);
        if (provinceData == null || !AreaLevelEnum.PROVINCE.getCode().equals(provinceData.getAreaLevel())) {
            throw new BizException(ErrorCodeEnum.SYS_EXP, "省市区信息有误");
        }

        addSchoolModel.setProvinceCode(provinceData.getAreaCode());
        addSchoolModel.setProvince(provinceData.getAreaName());
        addSchoolModel.setCityCode(cityData.getAreaCode());
        addSchoolModel.setCity(cityData.getAreaName());
        addSchoolModel.setAreaCode(areaData.getAreaCode());
        addSchoolModel.setArea(areaData.getAreaName());


        addSchoolModel.setAddress(request.getAddress());
        addSchoolModel.setLat(request.getLat());
        addSchoolModel.setLng(request.getLng());

        addSchoolModel.setType(request.getType());

        addSchoolModel.setCreateTime(new Date());
        addSchoolModel.setUpdateTime(new Date());
        addSchoolModel.setDeleted(0);

        addSchoolModel.setUserAdd(1);

        int saveResult = schoolDAO.saveRecord(addSchoolModel);
        if (saveResult == 0) {
            throw new BizException(ErrorCodeEnum.SYS_EXP, "保存学校信息失败");
        }

        response.setId(addSchoolModel.getId());
        return response;
    }

    @Override
    public CmsSearchSchoolResponse cmsSearchSchool(final CmsSchoolSearchRequest request) {

        CmsSearchSchoolResponse response = new CmsSearchSchoolResponse();

        Page<School> page;

        // 根据关键字搜索
        if (StringUtils.isNotBlank(request.getSearchKey())) {

            SchoolExtendDO querySchoolModel = new SchoolExtendDO();
            querySchoolModel.setKeyName(request.getSearchKey());

            page = PageHelper.offsetPage(request.getOffset(), request.getPageSize()).doSelectPage(new ISelect() {
                @Override
                public void doSelect() {
                    schoolDAO.queryByKeyName(querySchoolModel);
                }
            });

        } else {
            // 如果没有传如关键字，则全部返回
            School querySchoolModel = new School();
            page = PageHelper.offsetPage(request.getOffset(), request.getPageSize()).doSelectPage(
                    () -> schoolDAO.listRecord(querySchoolModel));
        }

        // 构造response
        if (page != null && page.size() > 0) {

            response.setTotal((int) page.getTotal());

            List<CmsSchoolResponse> responseList = new ArrayList<>();
            for (School school : page.getResult()) {
                CmsSchoolResponse schoolResponse = new CmsSchoolResponse();
                schoolResponse.setSchoolName(school.getName());
                schoolResponse.setSchoolId(school.getId());

                SchoolTypeEnum schoolTypeEnum = SchoolTypeEnum.getByCode(school.getType());
                if (schoolTypeEnum != null) {
                    schoolResponse.setTypeString(schoolTypeEnum.getDescription());
                }

                schoolResponse.setAreaName(school.getArea());
                schoolResponse.setCityName(school.getCity());
                schoolResponse.setProvinceName(school.getProvince());

                // 热门学校查询
                SchoolHot querySchoolHotModel = new SchoolHot();
                querySchoolHotModel.setSchoolId(school.getId());
                querySchoolHotModel.setDeleted(0);
                List<SchoolHot> schoolHotList = schoolHotDAO.listRecord(querySchoolHotModel);
                if (schoolHotList != null && schoolHotList.size() > 0) {
                    for (SchoolHot schoolHot : schoolHotList) {
                        if (schoolHot.getAreaHot().equals(1)) {
                            schoolResponse.setAreaHot(true);
                        }

                        if (schoolHot.getCityHot().equals(1)) {
                            schoolResponse.setCityHot(true);
                        }
                    }
                }

                responseList.add(schoolResponse);
            }

            response.setList(responseList);
        }

        return response;
    }

    @Override
    public void cmsSetHotSchool(final CmsSchoolSetHotRequest request) {

        // 查询学校
        School querySchoolModel = new School();
        querySchoolModel.setId(request.getSchoolId());
        querySchoolModel.setDeleted(0);

        School school = schoolDAO.getRecord(querySchoolModel);
        if (school == null) {
            throw new BizException(ErrorCodeEnum.SYS_EXP, "学校不存在");
        }

        SchoolHot queryHotModel = new SchoolHot();
        queryHotModel.setSchoolId(school.getId());

        if (1 == request.getType()) {
            queryHotModel.setCityHot(1);
        } else if (2 == request.getType()) {
            queryHotModel.setAreaHot(1);
        }

        queryHotModel.setDeleted(0);

        List<SchoolHot> schoolHotList = schoolHotDAO.listRecord(queryHotModel);
        if (schoolHotList != null && schoolHotList.size() > 0) {
            if (!request.getDelete()) {
                return;
            }
        }

        if (!request.getDelete()) {
            SchoolHot saveModel = new SchoolHot();
            saveModel.setSchoolId(request.getSchoolId());
            saveModel.setCityCode(school.getCityCode());
            saveModel.setProvinceCode(school.getProvinceCode());
            saveModel.setAreaCode(school.getAreaCode());
            saveModel.setProperty(school.getProperty());
            saveModel.setType(school.getType());
            saveModel.setAreaHot(0);
            saveModel.setCityHot(0);
            saveModel.setCreateTime(new Date());
            saveModel.setUpdateTime(new Date());
            saveModel.setDeleted(0);

            if (1 == request.getType()) {
                saveModel.setCityHot(1);
            } else if (2 == request.getType()) {
                saveModel.setAreaHot(1);
            }
            int saveResult = schoolHotDAO.saveRecord(saveModel);
            if (saveResult == 0) {
                throw new BizException(ErrorCodeEnum.SYS_EXP, "设置失败");
            }

        } else {
            // 删除热门
            SchoolHot updateModel = new SchoolHot();
            updateModel.setSchoolId(request.getSchoolId());

            if (1 == request.getType()) {
                updateModel.setCityHot(1);
            } else if (2 == request.getType()) {
                updateModel.setAreaHot(1);
            }

            updateModel.setDeleted(1);
            updateModel.setUpdateTime(new Date());

            int updateResult = schoolHotDAO.updateRecordBySchoolId(updateModel);
            if (updateResult == 0) {
                throw new BizException(ErrorCodeEnum.SYS_EXP, "更新失败");
            }
        }
    }

    @Override
    public List<School> querySchoolByNameKey(final String nameKey, final Integer offset, final Integer pageSize) {

        CmsSearchSchoolResponse response = new CmsSearchSchoolResponse();

        List<School> schoolList = new ArrayList<>();

        Page<School> page;

        // 根据关键字搜索
        SchoolExtendDO querySchoolModel = new SchoolExtendDO();
        querySchoolModel.setKeyName(nameKey);

        page = PageHelper.offsetPage(offset, pageSize).doSelectPage(new ISelect() {
            @Override
            public void doSelect() {
                schoolDAO.queryByKeyName(querySchoolModel);
            }
        });

        // 构造response
        if (page != null && page.size() > 0) {

            response.setTotal((int) page.getTotal());

            List<CmsSchoolResponse> responseList = new ArrayList<>();
            for (School school : page.getResult()) {
                CmsSchoolResponse schoolResponse = new CmsSchoolResponse();
                schoolResponse.setSchoolName(school.getName());
                schoolResponse.setSchoolId(school.getId());

                SchoolTypeEnum schoolTypeEnum = SchoolTypeEnum.getByCode(school.getType());
                if (schoolTypeEnum != null) {
                    schoolResponse.setTypeString(schoolTypeEnum.getDescription());
                }

                schoolResponse.setAreaName(school.getArea());
                schoolResponse.setCityName(school.getCity());
                schoolResponse.setProvinceName(school.getProvince());

                // 热门学校查询
                SchoolHot querySchoolHotModel = new SchoolHot();
                querySchoolHotModel.setSchoolId(school.getId());
                querySchoolHotModel.setDeleted(0);
                List<SchoolHot> schoolHotList = schoolHotDAO.listRecord(querySchoolHotModel);
                if (schoolHotList != null && schoolHotList.size() > 0) {
                    for (SchoolHot schoolHot : schoolHotList) {
                        if (schoolHot.getAreaHot().equals(1)) {
                            schoolResponse.setAreaHot(true);
                        }

                        if (schoolHot.getCityHot().equals(1)) {
                            schoolResponse.setCityHot(true);
                        }
                    }
                }

                responseList.add(schoolResponse);
            }

            response.setList(responseList);
        }

        return schoolList;
    }

    @Override
    public int saveSchool(final School school) {
        return schoolDAO.saveRecord(school);
    }

    @Override
    public String getSchoolRealName(School school) {
        String schoolName = school.getName();
        // 去掉括号数据
        String[] subSchoolNames = schoolName.split("（");
        String[] subs = subSchoolNames[0].split("\\(");
        schoolName = subs[0];

        //去掉省
        String province = school.getProvince();
        if (StrUtil.isNotBlank(province)) {
            // 去掉省
            // 这一步去除的是浙江省
            if (schoolName.contains(province)) {
                subs = schoolName.split(province);

                if (subs.length > 1) {
                    schoolName = subs[1];
                }
            }
            // 这一步去掉的是浙江
            province = province.substring(0, province.length() - 1);
            if (schoolName.contains(province)) {
                subs = schoolName.split(province);

                if (subs.length > 1) {
                    schoolName = subs[1];
                }
            }
        }

        //去掉市
        String city = school.getCity();
        if (StrUtil.isNotBlank(city)) {
            // 去掉省市区
            // 这一步去除的是杭州市
            if (schoolName.contains(city)) {
                subs = schoolName.split(city);

                if (subs.length > 1) {
                    schoolName = subs[1];
                }
            }
            // 这一步去掉的是杭州
            city = province.substring(0, city.length() - 1);
            if (schoolName.contains(city)) {
                subs = schoolName.split(city);

                if (subs.length > 1) {
                    schoolName = subs[1];
                }
            }
        }

        //去掉区
        String area = school.getArea();
        if (StrUtil.isNotBlank(area)) {
            // 去掉省市区
            // 这一步去除的是滨江区
            if (schoolName.contains(area)) {
                subs = schoolName.split(area);

                if (subs.length > 1) {
                    schoolName = subs[1];
                }
            }
        }

        return schoolName;
    }

}