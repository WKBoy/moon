/*
 * Dian.com Inc.
 * Copyright (c) 2004-2019 All Rights Reserved.
 */
package cn.sedu.moon.service.impl;

import cn.sedu.moon.dao.TrackerDAO;
import cn.sedu.moon.dao.TrackerPointDAO;
import cn.sedu.moon.dao.TrackerPvUvDAO;
import cn.sedu.moon.dao.model.Tracker;
import cn.sedu.moon.dao.model.TrackerPoint;
import cn.sedu.moon.dao.model.TrackerPvUv;
import cn.sedu.moon.service.TrackerService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;

/**
 * @author wukong
 * @version : TrackerServiceImpl.java, v 0.1 2019年11月04日 11:43 上午 Exp $
 */
@Service
@Slf4j
public class TrackerServiceImpl implements TrackerService {

    @Resource
    private TrackerDAO trackerDAO;

    @Resource
    private TrackerPointDAO trackerPointDAO;

    @Resource
    private TrackerPvUvDAO trackerPvUvDAO;

    @Override
    @Async
    public void record(final String url, final Long userId) {

        Tracker saveModel = new Tracker();
        saveModel.setUrl(url);
        saveModel.setUserId(userId);
        saveModel.setCreateTime(new Date());
        saveModel.setUpdateTime(new Date());
        saveModel.setDeleted(0);

        int result = trackerDAO.saveRecord(saveModel);
        if (result == 0) {
            log.error("trackerDAO.saveRecord error. url:{}, userId:{}", url, userId);
        }
    }

    @Override
    public List<TrackerPoint> allPoint() {
        TrackerPoint queryModel = new TrackerPoint();
        return trackerPointDAO.listRecord(queryModel);
    }

    public int addPvUv(final TrackerPvUv trackerPvUv) {

        trackerPvUv.setCreateTime(new Date());
        trackerPvUv.setUpdateTime(new Date());
        trackerPvUv.setDeleted(0);

        return trackerPvUvDAO.saveRecord(trackerPvUv);
    }
}