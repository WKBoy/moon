package cn.sedu.moon.service.impl;

import cn.sedu.moon.common.enums.ErrorCodeEnum;
import cn.sedu.moon.common.enums.SubscribeStatusEnum;
import cn.sedu.moon.common.exception.BizException;
import cn.sedu.moon.common.util.DateTimeUtil;
import cn.sedu.moon.controller.request.subscriber.CheckSubscribeBestWeekRequest;
import cn.sedu.moon.controller.request.subscriber.ListBestWeekRequest;
import cn.sedu.moon.controller.request.subscriber.SubscribeBestWeekRequest;
import cn.sedu.moon.controller.request.subscriber.UnsubscribeBestWeekRequest;
import cn.sedu.moon.controller.response.subscriber.CheckSubscribeBestWeekResponse;
import cn.sedu.moon.controller.response.subscriber.ListBestWeekResponse;
import cn.sedu.moon.dao.NewsDAO;
import cn.sedu.moon.dao.NewsReadDAO;
import cn.sedu.moon.dao.SchoolDAO;
import cn.sedu.moon.dao.StuSchDAO;
import cn.sedu.moon.dao.SubscriberBestWeekDAO;
import cn.sedu.moon.dao.model.News;
import cn.sedu.moon.dao.model.School;
import cn.sedu.moon.dao.model.StuSch;
import cn.sedu.moon.dao.model.SubscriberBestWeek;
import cn.sedu.moon.service.SubscriberService;
import cn.sedu.moon.service.helper.NewsHelper;
import cn.sedu.moon.service.helper.UserHelper;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * 订阅者服务
 *
 * @author guanzhong
 * @version : SubscriberServiceImpl.java, v 0.1 2019年11月13日16:21:26
 */
@Service
@Slf4j
public class SubscriberServiceImpl implements SubscriberService {

    @Autowired
    private SubscriberBestWeekDAO subscriberBestWeekDAO;

    @Autowired
    private NewsDAO newsDAO;

    @Autowired
    private NewsReadDAO newsReadDAO;

    @Autowired
    private NewsHelper newsHelper;

    @Autowired
    private UserHelper userHelper;

    @Autowired
    private StuSchDAO stuSchDAO;

    @Autowired
    private SchoolDAO schoolDAO;

    /**
     * 订阅每周精选
     *
     * @param request
     */
    @Override
    public void subscribeBestWeek(SubscribeBestWeekRequest request) {

        // 参数
        Long userId = request.getUserId();

        // 检查用户是否已订阅
        SubscriberBestWeek selectParam = new SubscriberBestWeek();
        selectParam.setUserId(userId);
        SubscriberBestWeek selectData = subscriberBestWeekDAO.getRecord(selectParam);

        // 已经订阅
        if (selectData != null) {
            throw new BizException(ErrorCodeEnum.SUBSCRIBER_EXIST);
        }

        // 新增订阅记录
        SubscriberBestWeek insertParam = new SubscriberBestWeek();
        insertParam.setUserId(userId);
        insertParam.setCreateTime(new Date());
        insertParam.setUpdateTime(new Date());
        int insertResult = subscriberBestWeekDAO.saveRecord(insertParam);
        if (insertResult == 0) {
            throw new BizException(ErrorCodeEnum.SUBSCRIBER_SUBSCRIBE_ERROR, "订阅失败");
        }

        // 新增订阅相关记录 TODO
        // 表moon_subscriber_bestweek_log


    }

    /**
     * 取消订阅每周精选
     *
     * @param request
     */
    @Override
    public void unsubscribeBestWeek(UnsubscribeBestWeekRequest request) {

        // 参数
        Long userId = request.getUserId();

        // 检查用户是否订阅
        SubscriberBestWeek selectParam = new SubscriberBestWeek();
        selectParam.setUserId(userId);
        SubscriberBestWeek selectData = subscriberBestWeekDAO.getRecord(selectParam);

        // 未订阅
        if (selectData == null) {
            throw new BizException(ErrorCodeEnum.SUBSCRIBER_NOT_EXIST);
        }

        // 删除订阅记录
        int deleteResult = subscriberBestWeekDAO.deleteByUserId(userId);
        if (deleteResult == 0) {
            throw new BizException(ErrorCodeEnum.SUBSCRIBER_UNSUBSCRIBE_ERROR, "取消订阅失败");
        }

        // 新增订阅相关记录 TODO
        // 表moon_subscriber_bestweek_log


    }

    /**
     * 检查用户订阅每周精选状态
     *
     * @param request
     * @return
     */
    @Override
    public CheckSubscribeBestWeekResponse checkSubscribeBestWeek(CheckSubscribeBestWeekRequest request) {

        // 参数
        Long userId = request.getUserId();

        // 获取用户订阅信息
        SubscriberBestWeek selectParam = new SubscriberBestWeek();
        selectParam.setUserId(userId);
        SubscriberBestWeek selectData = subscriberBestWeekDAO.getRecord(selectParam);

        // 定义返回
        CheckSubscribeBestWeekResponse response = new CheckSubscribeBestWeekResponse();

        // 未订阅并返回
        if (selectData == null) {
            response.setSubscribeStatus(SubscribeStatusEnum.UNSUBSCRIBE.getCode());
            return response;
        }

        // 已订阅
        response.setSubscribeStatus(SubscribeStatusEnum.SUBSCRIBED.getCode());
        response.setSubscribeTime(selectData.getCreateTime());
        return response;
    }

    /**
     * 获取每周精选文章列表
     * <p>
     * 内容筛选规则：
     * ➢	文章筛选范围：筛选‘对应地区’‘未推荐过’的文章，例如用户所在杭州地区，筛选杭州地区文章
     * ➢	文章内容：共展示3篇
     * 1》	时间最新一篇精编文章（运营会在后台设置精编文章）
     * 2》	文章标题或者文章正文包含用户想读学校的文章，时间由新到旧
     * 3》	文章标题或者文章正文包含用户在读学校的文章，时间最近2个月内由新到旧
     * 4》	不足3篇，则按照最近一周新建文章中，阅读数由多到少筛选补足
     *
     * 需要搭建搜索引擎
     *
     * @param request
     * @return
     */
    @Override
    public List<ListBestWeekResponse> listBestWeek(ListBestWeekRequest request) {
        // 参数
        Long userId = request.getUserId();

        // 获取用户城市id
        String cityCode = userHelper.getUserCityCode(userId);

        return listLastNews(userId, cityCode);
/*



        // 从redis中获取当前用户推荐过文章ids TODO
        // testData
        List<Long> hadPushUserIds = new ArrayList<>();

        // 第一篇，获取最新时间最新一篇精编文章
        News selectBestNewsParam = new News();
        selectBestNewsParam.setType(NewsTypeEnum.SELECTED.getCode());
        selectBestNewsParam.setDeleted(0);
        News selectBestNewsResult = newsDAO.getTheTopRecord(selectBestNewsParam);

        // 不足3篇，则按照最近一周新建文章中，阅读数由多到少筛选补足
        if (selectBestNewsResult == null) {
            return listLastNews(userId, cityCode);
        }
        // 是否已经推荐过
        if (hadPushUserIds.contains(selectBestNewsResult.getId())) {
            return listLastNews(userId, cityCode);
        }

        // 第二篇，文章标题或者文章正文包含用户想读学校的文章，时间由新到旧
        News selectHopeSchoolNewsParam = new News();

        // 获取用户意向的学校列表
        // 使用缓存缓存意向学校和对应的学校信息
        StuSch selectParam = new StuSch();
        selectParam.setUserId(userId);
        List<StuSch> stuSchList = stuSchDAO.listRecord(selectParam);
        if (stuSchList.size() <= 0) {
            return listLastNews(userId, cityCode);
        }
        // 获取意向学校地区信息
        List<Long> schools =new ArrayList<>();
        String ids = StringUtils.join(schools,"m");
        for (StuSch item : stuSchList) {
            Long schoolId = item.getSchoolId();

            School school = schoolDAO.selectByPrimaryKey(schoolId);
            if (school == null || school.getDeleted() == 1) {
                continue;
            }
            // TODO

        }

        // 第三篇，文章标题或者文章正文包含用户在读学校的文章，时间最近2个月内由新到旧
        //



        return listLastNews(userId, cityCode);
        */
    }


    /**
     * 获取最新三篇文章
     *
     * @param userId
     * @return
     */
    private List<ListBestWeekResponse> listLastNews(Long userId, String cityCode) {

        News selectParam = new News();
        selectParam.setDeleted(0);
        selectParam.setCityCode(cityCode);
        Page<News> page = PageHelper.offsetPage(1, 3).doSelectPage(
                () -> newsDAO.listRecord(selectParam));

        List<ListBestWeekResponse> result = new ArrayList<>();
        if (page != null && page.size() > 0) {

            for (News news : page.getResult()) {
                Long newsId = news.getId();

                ListBestWeekResponse item = new ListBestWeekResponse();
                item.setId(news.getId());
                item.setTitle(news.getTitle());
                item.setAuthorName(news.getAuthor());
                item.setRead(newsHelper.checkUserReadNews(userId, newsId));
                item.setCreateTime(DateTimeUtil.dateToString(news.getCreateTime(), "yyyy-MM-dd HH:mm:ss"));
                item.setBrief(news.getBrief());
                item.setThumbnail(news.getThumbnail());
                item.setCollect(newsHelper.checkUserCollectNews(userId, newsId));
                item.setLight(news.getLight() == 1);
                result.add(item);
            }
        }
        return result;
    }


}
