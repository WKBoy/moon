/*
 * Dian.com Inc.
 * Copyright (c) 2004-2019 All Rights Reserved.
 */
package cn.sedu.moon.service.impl;

import cn.sedu.moon.common.cache.UserShareNewsCache;
import cn.sedu.moon.common.constant.BizConstants;
import cn.sedu.moon.common.enums.ErrorCodeEnum;
import cn.sedu.moon.common.enums.GloryChangeBizTypeEnum;
import cn.sedu.moon.common.enums.NewsTypeEnum;
import cn.sedu.moon.common.enums.ShareStatusEnum;
import cn.sedu.moon.common.exception.BizException;
import cn.sedu.moon.common.util.CacheKeyUtil;
import cn.sedu.moon.controller.response.share.CheckShareResponse;
import cn.sedu.moon.controller.response.share.DoShareResponse;
import cn.sedu.moon.controller.response.share.LightCheckResponse;
import cn.sedu.moon.controller.response.share.LightNewsResponse;
import cn.sedu.moon.controller.response.share.LightUserResponse;
import cn.sedu.moon.dao.*;
import cn.sedu.moon.dao.model.*;
import cn.sedu.moon.service.GloryService;
import cn.sedu.moon.service.ShareService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.redisson.api.RBucket;
import org.redisson.api.RedissonClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * 分享服务的实现类
 * @author ${wukong}
 * @version $Id: ShareServiceImpl.java, v 0.1 2019年04月14日 4:06 PM Exp $
 */
@Service
@Slf4j
public class ShareServiceImpl implements ShareService {

    @Autowired
    private ShareNewsDAO shareNewsDAO;

    @Autowired
    private RedissonClient redissonClient;

    @Autowired
    private ShareNewsLightDAO lightDAO;

    @Autowired
    private UserDAO userDAO;

    @Autowired
    private NewsDAO newsDAO;

    @Autowired
    private ShareNewsReadDAO shareNewsReadDAO;

    @Autowired
    private SchoolDAO schoolDAO;

    @Autowired
    private GloryService gloryService;
    // 多线程服务
    private static ExecutorService executorService = new ThreadPoolExecutor(40, 100, 60L, TimeUnit.SECONDS, new LinkedBlockingQueue<>(100));

    @Override
    public Long createShare(final Long userId, final Long newsId) {

        // 查询是否已经创建
        ShareNews queryShareModel = new ShareNews();
        queryShareModel.setDeleted(0);
        queryShareModel.setNewsId(newsId);
        queryShareModel.setUserId(userId);

//        ShareNews queryResult = shareNewsDAO.getRecord(queryShareModel);
//        if (queryResult != null) {
//            log.error("已经创建分享");
//            return queryResult.getId();
//        }

        ShareNews saveModel = new ShareNews();
        saveModel.setNewsId(newsId);
        saveModel.setUserId(userId);
        saveModel.setDeleted(0);
        saveModel.setStatus(ShareStatusEnum.CREATED.getCode());
        saveModel.setCreateTime(new Date());
        saveModel.setUpdateTime(new Date());

        int saveResult = shareNewsDAO.saveRecord(saveModel);
        if (saveResult == 0) {
            throw new BizException(ErrorCodeEnum.SYS_EXP, "创建分享失败");
        }
        executorService.submit(() -> setUserShareSchoolList(userId, newsId));

        return saveModel.getId();
    }

    private void setUserShareSchoolList(Long userId, Long newsId) {
        News news = new News();
        news.setId(newsId);
        news = newsDAO.getRecord(news);
        List<School> schoolList = schoolDAO.listAllSchoolNameAndId();
        List<Long> schoolIdList = new ArrayList<>();
        for (School school : schoolList){
            if (news.getTitle().contains(school.getName())){
                schoolIdList.add(school.getId());
            }
        }
        String cacheKey = CacheKeyUtil.genUserShareKey(userId);
        RBucket<List<Long>> bucket = redissonClient.getBucket(cacheKey);
        List<Long> nowIdList = bucket.get();
        if (nowIdList == null){
            bucket.set(schoolIdList);
        } else {
            for (Long schoolId : schoolIdList){
                if (!nowIdList.contains(schoolId)){
                    nowIdList.add(schoolId);
                }
            }
            bucket.set(nowIdList);
        }
    }

    @Override
    public DoShareResponse doShare(final Long userId, final Long shareId) {

        DoShareResponse response = new DoShareResponse();

        // 查找分享记录
        ShareNews queryShareModel = new ShareNews();
        queryShareModel.setDeleted(0);
        queryShareModel.setId(shareId);
        queryShareModel.setUserId(userId);

        ShareNews queryResult = shareNewsDAO.getRecord(queryShareModel);
        if (queryResult == null) {
            throw new BizException(ErrorCodeEnum.SYS_EXP, "查找得不到对应分享记录");
        }

        ShareNews updateModel = new ShareNews();
        updateModel.setId(queryResult.getId());
        updateModel.setUpdateTime(new Date());
        updateModel.setStatus(ShareStatusEnum.SHARED.getCode());

        int updateResult = shareNewsDAO.updateRecord(updateModel);

        if (updateResult == 0) {
            throw new BizException(ErrorCodeEnum.SYS_EXP, "更新分享记录失败");
        }

        // 记录分享行为到redis， 24小时候失效
        String shareCacheKey = CacheKeyUtil.genUserShareNewsKey(userId);
        RBucket<UserShareNewsCache> bucket = redissonClient.getBucket(shareCacheKey);
        if (bucket.get() == null) {
            UserShareNewsCache userShareNewsCache = new UserShareNewsCache();
            userShareNewsCache.setFreeReadCount(0);
            userShareNewsCache.setShared(1);
            bucket.set(userShareNewsCache,24 * 7, TimeUnit.HOURS);
        } else {
            UserShareNewsCache userShareNewsCache = bucket.get();
            userShareNewsCache.setShared(1);
            bucket.set(userShareNewsCache, 24 * 7, TimeUnit.HOURS);
        }

        CheckShareResponse checkShareResponse = checkShare(queryResult.getNewsId(), userId);
        response.setNeedShared(checkShareResponse.getIsNeedShare());
        response.setNeedShareTotal(checkShareResponse.getNeedShareTotal());
        response.setShareCount(checkShareResponse.getShareCount());
        response.setType(checkShareResponse.getNewsType().getCode());

        // 首次分享解锁文章奖励荣誉值
        if (checkShareResponse.getNewsType().getShareTotal().equals(checkShareResponse.getShareCount())) {
            gloryService.addGlory(userId, (long) checkShareResponse.getNewsType().getShareGloryReward(), GloryChangeBizTypeEnum.SHARE_UNLOCK_NEWS);
        }


        // shareId
        if (checkShareResponse.getIsNeedShare()) {
            response.setShareId(createShare(userId, queryResult.getNewsId()));
        }

        return response;
    }

    @Override
    public LightNewsResponse lightNews(final Long shareUserId, final Long lightUserId, final Long newsId) {

        LightNewsResponse response = new LightNewsResponse();
        response.setId(lightUserId);

        if (checkLight(shareUserId, lightUserId, newsId).getLight()) {
            throw new BizException(ErrorCodeEnum.SYS_EXP, "已经点亮过了");
        }

        ShareNewsLight saveModel = new ShareNewsLight();
        saveModel.setUserId(shareUserId);
        saveModel.setUserLightId(lightUserId);
        saveModel.setNewsId(newsId);
        saveModel.setCreateTime(new Date());
        saveModel.setUpdateTime(new Date());
        saveModel.setDeleted(0);

        int result = lightDAO.saveRecord(saveModel);
        if (result == 0) {
            throw new BizException(ErrorCodeEnum.SYS_EXP, "数据保存失败");
        }

        // 查询用户信息
        User queryUserModel = new User();
        queryUserModel.setId(lightUserId);
        queryUserModel.setDeleted(0);

        User user = userDAO.getRecord(queryUserModel);
        if (user != null) {
            response.setName(user.getNickname());
            response.setSrc(user.getAvatar());
        }

        return response;
    }

    @Override
    public LightCheckResponse checkLight(final Long shareUserId, final Long lightUserId, final Long newsId) {

        LightCheckResponse response = new LightCheckResponse();

        User queryUserModel = new User();
        queryUserModel.setId(shareUserId);

        User user = userDAO.getRecord(queryUserModel);
        if (user != null) {
            response.setShareUserName(user.getNickname());
        }


        ShareNewsLight queryLightUserListModel = new ShareNewsLight();

        queryLightUserListModel.setUserId(shareUserId);
        queryLightUserListModel.setNewsId(newsId);
        queryLightUserListModel.setDeleted(0);
        List<ShareNewsLight> shareNewsLightList = lightDAO.listRecord(queryLightUserListModel);

        if (shareNewsLightList != null && shareNewsLightList.size() > 0) {

            if (shareNewsLightList.size() >= BizConstants.NEWS_LIGHT_USER_COUNT) {
                response.setNeedLight(false);
            }

            List<LightUserResponse> avatarList = new ArrayList<>();

            for (ShareNewsLight shareNewsLight : shareNewsLightList) {
                // 查询用户信息
                User queryUser = new User();
                queryUser.setId(shareNewsLight.getUserLightId());
                User lightUser = userDAO.getRecord(queryUser);
                if (lightUser != null && StringUtils.isNotBlank(lightUser.getAvatar())) {

                    LightUserResponse userResponse = new LightUserResponse();
                    userResponse.setUserId(lightUser.getId());
                    userResponse.setAvatar(lightUser.getAvatar());

                    avatarList.add(userResponse);
                }
            }

            response.setUserAvatarList(avatarList);
        }

        ShareNewsLight queryModel = new ShareNewsLight();
        queryModel.setUserId(shareUserId);
        queryModel.setUserLightId(lightUserId);
        queryModel.setNewsId(newsId);
        queryModel.setDeleted(0);

        shareNewsLightList = lightDAO.listRecord(queryModel);
        if (shareNewsLightList != null && shareNewsLightList.size() > 0) {
            response.setLight(true);
            return response;
        }

        response.setLight(false);
        return response;
    }

    @Override
    public CheckShareResponse checkShare(final Long newsId, final Long userId) {

        // 查询新闻类型
        News queryNewsModel = new News();
        queryNewsModel.setId(newsId);

        News news = newsDAO.getRecord(queryNewsModel);
        if (news == null) {
            throw new BizException(ErrorCodeEnum.SYS_EXP, "新闻不存在");
        }

        CheckShareResponse checkShareResponse = new CheckShareResponse();

        NewsTypeEnum newsType = NewsTypeEnum.getByCode(news.getType());
        if (newsType == null) {
            throw new BizException(ErrorCodeEnum.SYS_EXP, "新闻类型错误");
        }

        checkShareResponse.setNewsType(newsType);

        if (NewsTypeEnum.NORMAL.equals(newsType)) {
            // 普通新闻

            // 判断是否需要分享
            // 需要分享才能看的条件:
            //  1 已浏览过3篇文章
            //  2 分享标志为空
            String shareCacheKey = CacheKeyUtil.genUserShareNewsKey(userId);
            RBucket<UserShareNewsCache> bucket = redissonClient.getBucket(shareCacheKey);

            if (bucket.get() == null) {
                checkShareResponse.setIsNeedShare(false);

                UserShareNewsCache shareNewsCache = new UserShareNewsCache();
                shareNewsCache.setFreeReadCount(1);
                shareNewsCache.setShared(0);
                bucket.set(shareNewsCache);

            } else {
                UserShareNewsCache shareNewsCache = bucket.get();

                if (shareNewsCache.getShared() == 1) {
                    checkShareResponse.setIsNeedShare(false);

                } else if (shareNewsCache.getFreeReadCount() < BizConstants.NEWS_FREE_READ_TIMES) {
                    checkShareResponse.setIsNeedShare(false);

                    shareNewsCache.setFreeReadCount(shareNewsCache.getFreeReadCount() + 1);
                    shareNewsCache.setShared(0);
                    bucket.set(shareNewsCache);

                } else {
                    checkShareResponse.setIsNeedShare(true);
                    checkShareResponse.setNeedShareTotal(1);
                    checkShareResponse.setShareCount(0);
                }
            }

            return checkShareResponse;
        }

        // 查询分享次数
        ShareNews queryShareNewsModel = new ShareNews();
        queryShareNewsModel.setNewsId(newsId);
        queryShareNewsModel.setUserId(userId);
        queryShareNewsModel.setStatus(ShareStatusEnum.SHARED.getCode());

        List<ShareNews> shareNewsList = shareNewsDAO.listRecord(queryShareNewsModel);

        Integer shareCount = 0;
        if (shareNewsList != null && shareNewsList.size() > 0) {
            checkShareResponse.setShared(true);
            shareCount = shareNewsList.size();
        }

        checkShareResponse.setShareCount(shareCount);
        checkShareResponse.setNeedShareTotal(newsType.getShareTotal());
        checkShareResponse.setIsNeedShare(shareCount < newsType.getShareTotal());

        return checkShareResponse;
    }


    @Override
    public void readShare(final Long userId, final Long shareId) {

        // 查询分享
        ShareNews queryShareModel = new ShareNews();
        queryShareModel.setId(shareId);

        ShareNews shareNews = shareNewsDAO.getRecord(queryShareModel);
        if (shareNews == null) {
            log.error("read share 分享内容不存在, userId: {}, shareId: {}", userId, shareId);
            throw new BizException(ErrorCodeEnum.SYS_EXP, "分享内容不存在");
        }

        // 查询是否已经read过了
        ShareNewsRead queryReadModel = new ShareNewsRead();
        queryReadModel.setShareId(shareId);
        queryReadModel.setUserId(userId);
        queryReadModel.setDeleted(0);

        ShareNewsRead shareNewsRead = shareNewsReadDAO.getRecord(queryReadModel);
        if (shareNewsRead != null) {
            throw new BizException(ErrorCodeEnum.SYS_EXP, "已经读过了");
        }

        // 查询此次阅读是不是该分享的第一次阅读
        ShareNewsRead queryReadFirstModel = new ShareNewsRead();
        queryReadFirstModel.setShareId(shareId);
        queryReadFirstModel.setDeleted(0);

        List<ShareNewsRead> shareNewsReadList = shareNewsReadDAO.listRecord(queryReadModel);
        if (shareNewsReadList == null || shareNewsReadList.size() == 0) {
            // 第一次阅读，更改原分享者荣誉值

            // 查询新闻类型
            News queryNewsModel = new News();
            queryNewsModel.setId(shareNews.getNewsId());
            News news = newsDAO.getRecord(queryNewsModel);

            NewsTypeEnum newsType = NewsTypeEnum.getByCode(news.getType());
            if (newsType != null && !newsType.equals(NewsTypeEnum.NORMAL)) {
                gloryService.addGlory(shareNews.getUserId(), (long)newsType.getReadShareGloryReward(), GloryChangeBizTypeEnum.READ_SHARE);
            }
        }

        // 记录
        ShareNewsRead saveNewsReadModel = new ShareNewsRead();
        saveNewsReadModel.setShareId(shareId);
        saveNewsReadModel.setShareUserId(shareNews.getUserId());
        saveNewsReadModel.setNewsId(shareNews.getNewsId());
        saveNewsReadModel.setUserId(userId);
        saveNewsReadModel.setCreateTime(new Date());
        saveNewsReadModel.setUpdateTime(new Date());
        saveNewsReadModel.setDeleted(0);

        int saveResult = shareNewsReadDAO.saveRecord(saveNewsReadModel);
        if (saveResult == 0) {
            log.error("read share -- shareNewsReadDAO.saveRecord error, save model: {}", saveNewsReadModel);
            throw new BizException(ErrorCodeEnum.SYS_EXP, "保存已读记录失败");
        }
    }
}