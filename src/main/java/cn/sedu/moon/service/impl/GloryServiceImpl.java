/*
 * Dian.com Inc.
 * Copyright (c) 2004-2019 All Rights Reserved.
 */
package cn.sedu.moon.service.impl;

import java.util.Date;
import java.util.concurrent.TimeUnit;

import cn.sedu.moon.common.cache.CacheKey;
import cn.sedu.moon.common.constant.BizConstants;
import cn.sedu.moon.common.enums.ErrorCodeEnum;
import cn.sedu.moon.common.enums.GloryChangeBizTypeEnum;
import cn.sedu.moon.common.enums.GloryChangeTypeEnum;
import cn.sedu.moon.common.exception.BizException;
import cn.sedu.moon.dao.UserGloryDAO;
import cn.sedu.moon.dao.UserGloryRecordDAO;
import cn.sedu.moon.dao.model.UserGlory;
import cn.sedu.moon.dao.model.UserGloryRecord;
import cn.sedu.moon.service.GloryService;
import org.redisson.api.RLock;
import org.redisson.api.RedissonClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author ${wukong}
 * @version $Id: GloryServiceImpl.java, v 0.1 2019年04月01日 3:11 PM Exp $
 */
@Service
public class GloryServiceImpl implements GloryService {

    @Autowired
    private RedissonClient redissonClient;

    @Autowired
    private UserGloryDAO gloryDAO;

    @Autowired
    private UserGloryRecordDAO gloryRecordDAO;

    @Override
    public void addGlory(final Long userId, final Long value, final GloryChangeBizTypeEnum bizType) {

        // redis lock
        RLock lock = redissonClient.getLock(CacheKey.GLORY_UPDATE_LOCK_KEY_PREFIX + userId);
        try {
            if (lock.tryLock(BizConstants.REDIS_LOCK_WAIT_SECONDS, BizConstants.REDIS_LOCK_RELEASE_SECONDS, TimeUnit.SECONDS)) {

                UserGlory queryModel = new UserGlory();
                queryModel.setUserId(userId);
                UserGlory userGlory = gloryDAO.getRecord(queryModel);
                if (userGlory != null) {
                    // 更改现有记录
                    Long newValue = userGlory.getGlory() + value;
                    if (newValue < 0) {
                        newValue = 0L;
                    }

                    UserGlory updateModel = new UserGlory();
                    updateModel.setId(userGlory.getId());
                    updateModel.setUpdateTime(new Date());
                    updateModel.setGlory(newValue);

                    int result = gloryDAO.updateRecord(updateModel);
                    if (result == 0) {
                       throw new BizException(ErrorCodeEnum.SYS_EXP, "更新荣誉值表失败");
                    }
                } else {
                    // 添加新的记录
                    UserGlory saveModel = new UserGlory();
                    saveModel.setUserId(userId);
                    Long newValue = value;
                    if (newValue < 0) {
                        newValue = 0L;
                    }

                    saveModel.setGlory(newValue);
                    saveModel.setUpdateTime(new Date());
                    saveModel.setCreateTime(new Date());
                    saveModel.setDeleted(0);

                    int result = gloryDAO.saveRecord(saveModel);
                    if (result == 0) {
                        throw new BizException(ErrorCodeEnum.SYS_EXP, "insert荣誉值表失败");
                    }
                }
;
                // 添加荣誉值变更记录
                GloryChangeTypeEnum changeTypeEnum = GloryChangeTypeEnum.ADD;
                if (value < 0) {
                    changeTypeEnum = GloryChangeTypeEnum.MINUS;
                }

                UserGloryRecord saveRecordModel = new UserGloryRecord();
                saveRecordModel.setUserId(userId);
                saveRecordModel.setValue(value + "");
                saveRecordModel.setDeleted(0);
                saveRecordModel.setCreateTime(new Date());
                saveRecordModel.setUpdateTime(new Date());
                saveRecordModel.setBizType(bizType.getCode());
                saveRecordModel.setChangeType(changeTypeEnum.getCode());

                int result = gloryRecordDAO.saveRecord(saveRecordModel);
                if (result == 0) {
                    throw new BizException(ErrorCodeEnum.SYS_EXP, "insert荣誉值记录表失败");
                }
            }

        } catch (Exception e) {
            throw new BizException(ErrorCodeEnum.SYS_EXP, "变更荣誉值出错");
        } finally {
            if (lock.isLocked()) {
                lock.unlock();
            }
        }
    }

    @Override
    public Long getUserGlory(final Long userId) {

        UserGlory queryModel = new UserGlory();
        queryModel.setUserId(userId);
        UserGlory userGlory = gloryDAO.getRecord(queryModel);
        if (userGlory != null) {
            return userGlory.getGlory();
        } else {
            return 0L;
        }
    }
}