/*
 * Dian.com Inc.
 * Copyright (c) 2004-2018 All Rights Reserved.
 */
package cn.sedu.moon.service.impl;

import java.util.Random;

import cn.sedu.moon.common.cache.SmsCodeCache;
import cn.sedu.moon.common.enums.ErrorCodeEnum;
import cn.sedu.moon.common.exception.BizException;
import cn.sedu.moon.common.util.CacheKeyUtil;
import cn.sedu.moon.service.SmsService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.redisson.api.RBucket;
import org.redisson.api.RedissonClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 短信息服务的实现类
 *
 * @author ${wukong}
 * @version $Id: SmsServiceImpl.java, v 0.1 2018年12月10日 3:09 PM Exp $
 */
@Service
@Slf4j
public class SmsServiceImpl implements SmsService {

    /**
     * 短信验证码相关缓存失效时间 (单位毫秒)
     */
    private final static long SMS_CACHE_EXP_TIME = 30 * 60 * 1000;

    /**
     * 在失效时间内获取短信验证码的最大次数
     */
    private final static int SMS_GET_MAX_TIMES_IN_EXP_TIME = 5;

    /**
     * 两次短信验证码发送的最小时间间隔 (单位毫秒)
     */
    private final static long SMS_SEND_MIN_DURATION = 30 * 1000;

    @Autowired
    private RedissonClient redissonClient;

    @Override
    public void sendToken(final String mobile) {

        // 从缓存中尝试获取Token
        String cacheKey = CacheKeyUtil.genSmsCodeCacheKey(mobile);
        RBucket<SmsCodeCache> bucket = redissonClient.getBucket(cacheKey);

        SmsCodeCache smsCodeCache = null;
        if (bucket != null && bucket.get() != null) {
            smsCodeCache = bucket.get();
        }

        if (smsCodeCache != null) {

            // 判断缓存的验证码是否已经过期
            if (System.currentTimeMillis() - smsCodeCache.getCreateTime() > SMS_CACHE_EXP_TIME) {
                bucket.delete();
                sendNewCode(mobile);

            } else if (System.currentTimeMillis() - smsCodeCache.getLastSendTime() <= SMS_SEND_MIN_DURATION) {
                // 两次获取短信验证码请求的时间间隔太短
                throw new BizException(ErrorCodeEnum.SMS_CODE_SEND_TOO_FREQUENT);

            } else if (smsCodeCache.getSendTimes() > SMS_GET_MAX_TIMES_IN_EXP_TIME) {
                // 在失效时间内获取验证码的请求次数超过了最大次数
                throw new BizException(ErrorCodeEnum.SMS_CODE_SEND_TOO_FREQUENT);

            } else {
                // 发送短信
                try {
                    // TODO 调用第三方接口获取短信验证码
                    //SmsUtil.sendSmsCode(mobile, smsCodeCache.getCode(), smsCodeCache.getChannelCode());
                } catch (Exception e) {
                    log.error("发送短信验证码失败 -- mobile is {}, code is {}, e is {}", mobile, smsCodeCache.getCode(), e);
                    throw new BizException(ErrorCodeEnum.SMS_CODE_SEND_ERROR);
                }

                // 发送次数+1
                smsCodeCache.setSendTimes(smsCodeCache.getSendTimes() + 1);
                // 更新上次发送时间
                smsCodeCache.setLastSendTime(System.currentTimeMillis());
                // 存入缓存
                bucket.set(smsCodeCache);
            }

        } else {
            // 缓存中没有，发送新的验证码
            sendNewCode(mobile);
        }
    }

    @Override
    public Boolean checkSmsCode(final String mobile, final String smsCode) {

        // 从缓存中获取验证码
        String cacheKey = CacheKeyUtil.genSmsCodeCacheKey(mobile);

        RBucket<SmsCodeCache> bucket = redissonClient.getBucket(cacheKey);

        SmsCodeCache smsCodeCache = null;
        if (bucket != null && bucket.get() != null) {
            smsCodeCache = bucket.get();
        }

        if (smsCodeCache == null) {
            return false;
        }

        if (StringUtils.equals(smsCode, smsCodeCache.getCode())) {
            return true;
        }

        return false;
    }

    /**
     * 发送新生成的验证码
     *
     * @param mobile 手机号码
     */
    private void sendNewCode(String mobile) {

        // key
        String key = CacheKeyUtil.genSmsCodeCacheKey(mobile);

        // code
        Random rand = new Random();
        String code = String.valueOf(rand.nextInt(8999) + 1000);

        // send
        try {
            //SmsUtil.sendSmsCode(mobile, code, SmsChannelEnum.SUDUN.getCode());
            // TODO 调用第三方接口获取短信验证码


        } catch (Exception e) {
            log.error("发送短信验证码失败 -- mobile is {}, code is {}, e is {}", mobile, code, e);
            throw new BizException(ErrorCodeEnum.SMS_CODE_SEND_ERROR, e);
        }

        // cache
        SmsCodeCache smsCodeCache = new SmsCodeCache();
        smsCodeCache.setCode(code);
        smsCodeCache.setSendTimes(1);
        smsCodeCache.setCreateTime(System.currentTimeMillis());
        smsCodeCache.setLastSendTime(System.currentTimeMillis());

        // 存入缓存
        redissonClient.getBucket(key).set(smsCodeCache);
    }
}