/*
 * Dian.com Inc.
 * Copyright (c) 2004-2019 All Rights Reserved.
 */
package cn.sedu.moon.service.impl;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

import cn.sedu.moon.dao.mongo.WxMessagePushContent;
import com.alibaba.fastjson.JSON;

import cn.sedu.moon.common.cache.CacheKey;
import cn.sedu.moon.common.cache.UserFormIdCache;
import cn.sedu.moon.common.util.DateUtil;
import cn.sedu.moon.dao.UserDAO;
import cn.sedu.moon.dao.model.User;
import cn.sedu.moon.service.MessageService;
import cn.sedu.moon.service.context.WxTemplateMessageContext;
import cn.sedu.moon.thirdparty.WechatApi;
import cn.sedu.moon.thirdparty.WechatPushApi;
import cn.sedu.moon.thirdparty.request.WxNotifyMessagePushRequest;
import cn.sedu.moon.thirdparty.response.WxGetAccessTokenResponse;
import cn.sedu.moon.thirdparty.response.WxNotifyMessagePushResponse;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.redisson.api.RBucket;
import org.redisson.api.RQueue;
import org.redisson.api.RedissonClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Service;

/**
 * 消息服务的实现类
 *
 * @author ${wukong}
 * @version $Id: MessageServiceImpl.java, v 0.1 2019年04月02日 11:20 AM Exp $
 */
@Service
@Slf4j
public class MessageServiceImpl implements MessageService {

    @Autowired
    private RedissonClient redissonClient;

    @Autowired
    private WechatApi wechatApi;

    @Autowired
    private WechatPushApi wechatPushApi;

    @Autowired
    private UserDAO userDAO;

    @Autowired
    private MongoTemplate mongoTemplate;

    @Override
    public void saveWxFormId(final Long userId, final String formId) {

        log.info("save wx form id, userId is {}, formId is {}", userId, formId);

        UserFormIdCache cache = new UserFormIdCache();
        cache.setFormId(formId);
        cache.setCreateTime(new Date());

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MMdd");
        RQueue<UserFormIdCache> rQueue = redissonClient.getQueue(
                String.format(CacheKey.PUSH_USER_DAY_QUEUE_PREFIX, simpleDateFormat.format(new Date()), userId));
        rQueue.add(cache);
        rQueue.expire(6, TimeUnit.DAYS);
    }

    @Override
    public String getFormIdByUserId(final Long userId) {
        return getFormId(userId, DateUtil.plusDay(new Date(), -6));
    }

    private String getFormId(Long userId, Date date) {
        if (DateUtil.natureDaysBetween(date, new Date()) < 0) {
            return null;
        }
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MMdd");
        RQueue<UserFormIdCache> rQueue = redissonClient.getQueue(
                String.format(CacheKey.PUSH_USER_DAY_QUEUE_PREFIX, simpleDateFormat.format(date), userId));
        UserFormIdCache result = rQueue.poll();
        if (result == null) {
            return getFormId(userId, DateUtil.plusDay(date, 1));
        }
        if (isExpire(result)) {
            return getFormId(userId, date);
        }
        return result.getFormId();
    }

    private boolean isExpire(UserFormIdCache request) {
        return request.getCreateTime().getTime() - System.currentTimeMillis() > 7 * 24 * 3600 * 1000;
    }

    @Override
    public boolean sendWechatAppTemplateMessage(final WxTemplateMessageContext context) {
        // 获取openId
        User queryUserModel = new User();
        queryUserModel.setId(context.getUserId());

        User user = userDAO.getRecord(queryUserModel);
        log.info("userId为："+queryUserModel.getId());
        if (user == null) {
            log.error("sendWechatAppTemplateMessage 获取用户信息失败 context is {}", context);
            return false;
        }

        WxMessagePushContent wxMessagePushContent = new WxMessagePushContent();
        wxMessagePushContent.setId(UUID.randomUUID().toString());
        wxMessagePushContent.setUserId(context.getUserId());
        wxMessagePushContent.setToUser(user.getWxOpenId());
        wxMessagePushContent.setTemplateId(context.getTemplateId());
        wxMessagePushContent.setPage(context.getPage());
        wxMessagePushContent.setData(context.getData());
        wxMessagePushContent.setCreateTime(cn.hutool.core.date.DateUtil.formatDateTime(new Date()));

        // 1 获取accessToken
        RBucket<String> bucket = redissonClient.getBucket(CacheKey.WX_ACCESS_TOKEN_KEY);

        String accessToken;
        if (bucket.get() == null) {
            WxGetAccessTokenResponse response = wechatApi.getAccessToken();
            if (response.getAccessToken() == null) {
                log.error("sendWechatAppTemplateMessage 获取微信accessToken失败 response is {}", response);
                wxMessagePushContent.setIsSuccess(0);
                mongoTemplate.save(wxMessagePushContent);
                return false;
                //throw new BizException(ErrorCodeEnum.SYS_EXP, "获取微信accessToken失败");
            }

            // 将accessToken存入redis，失效时间为微信返回的时间 减去100秒
            bucket.set(response.getAccessToken(), response.getExpiresIn() - 100, TimeUnit.SECONDS);
            accessToken = response.getAccessToken();

        } else {
            accessToken = bucket.get();
        }

        // 2 获取用户的formId
        String formId = getFormIdByUserId(context.getUserId());
        if (StringUtils.isBlank(formId)) {
            log.error("sendWechatAppTemplateMessage formId为空, context is {}", JSON.toJSONString(context));
            wxMessagePushContent.setIsSuccess(0);
            mongoTemplate.save(wxMessagePushContent);
            return false;
            //throw new BizException(ErrorCodeEnum.SYS_EXP, "formId为空");
        }else {
            log.info(context.getUserId() + "获取formId成功：" + formId);
        }

        // 3 组装微信消息对象
        WxNotifyMessagePushRequest wxPushMessageRequest = new WxNotifyMessagePushRequest();
        wxPushMessageRequest.setToUser(user.getWxOpenId());
        wxPushMessageRequest.setFormId(formId);
        wxPushMessageRequest.setTemplateId(context.getTemplateId());
        wxPushMessageRequest.setPage(context.getPage());
        wxPushMessageRequest.setData(context.getData());

        // 4 发送消息
        WxNotifyMessagePushResponse pushMessageResponse = wechatPushApi.sendNotifyPushMessage(accessToken, wxPushMessageRequest);

        // 5 处理结果
        // 6 记录发送结果
        if (pushMessageResponse == null || pushMessageResponse.getErrCode() != 0) {
            log.error("sendWechatAppTemplateMessage 发送小程序模版消息失败, context is {}, wx response is {}", context, pushMessageResponse);
            wxMessagePushContent.setIsSuccess(0);
            wxMessagePushContent.setResponse(pushMessageResponse);
            mongoTemplate.save(wxMessagePushContent);
            return false;
        } else {
            wxMessagePushContent.setIsSuccess(1);
            wxMessagePushContent.setResponse(pushMessageResponse);
            mongoTemplate.save(wxMessagePushContent);
            return true;
        }

    }


}