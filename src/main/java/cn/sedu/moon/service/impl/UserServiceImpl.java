/*
 * Dian.com Inc.
 * Copyright (c) 2004-2018 All Rights Reserved.
 */
package cn.sedu.moon.service.impl;

import cn.hutool.core.date.DateUnit;
import cn.hutool.core.map.MapUtil;
import cn.hutool.core.util.StrUtil;
import cn.sedu.moon.common.AES;
import cn.sedu.moon.common.WXCore;
import cn.sedu.moon.common.WxPKCS7Encoder;
import cn.sedu.moon.common.constant.BizConstants;
import cn.sedu.moon.common.constant.NewsConstants;
import cn.sedu.moon.common.enums.ErrorCodeEnum;
import cn.sedu.moon.common.exception.BizException;
import cn.sedu.moon.common.properties.WxAppProperties;
import cn.sedu.moon.common.util.AuthUtil;
import cn.sedu.moon.common.util.CacheKeyUtil;
import cn.sedu.moon.common.util.DateUtil;
import cn.sedu.moon.common.util.HttpUtil;
import cn.sedu.moon.controller.request.UserMobileReportRequest;
import cn.sedu.moon.controller.request.cms.user.CmsSearchUserRequest;
import cn.sedu.moon.controller.response.UserLoginResponse;
import cn.sedu.moon.controller.response.cms.user.CmsUserSearchItemResponse;
import cn.sedu.moon.controller.response.cms.user.CmsUserSearchResponse;
import cn.sedu.moon.controller.response.cms.user.CmsUserUvResponse;
import cn.sedu.moon.dao.*;
import cn.sedu.moon.dao.model.*;
import cn.sedu.moon.service.DataCacheService;
import cn.sedu.moon.service.MessagePushService;
import cn.sedu.moon.dao.model.WxCodeShareRecord;
import cn.sedu.moon.service.SmsService;
import cn.sedu.moon.service.UserService;
import cn.sedu.moon.service.context.UserBaseInfoContext;
import cn.sedu.moon.service.context.UserLoginContext;
import cn.sedu.moon.service.context.WxAuthContext;
import cn.sedu.moon.thirdparty.WechatClient;
import cn.sedu.moon.thirdparty.dto.WxAppSessionKey;
import cn.sedu.moon.thirdparty.dto.WxAppUserInfo;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.ISelect;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.redisson.api.RBucket;
import org.redisson.api.RedissonClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * 用户服务的实现类
 *
 * @author ${wukong}
 * @version $Id: UserServiceImpl.java, v 0.1 2018年12月10日 2:39 PM Exp $
 */
@Service
@Slf4j
public class UserServiceImpl implements UserService {

    @Autowired
    private SmsService smsService;

    @Autowired
    private NewsDAO newsDAO;

    @Autowired
    private UserDAO userDAO;

    @Autowired
    private RedissonClient redissonClient;

    @Autowired
    private StudentArchivesDAO studentArchivesDAO;

    @Autowired
    private WxAppProperties wxAppProperties;

    @Autowired
    private WechatClient wechatClient;

    @Autowired
    private WxCodeShareRecordDAO wxCodeShareRecordDAO;

    @Autowired
    private MessagePushService messagePushService;

    @Autowired
    private StuSchDAO stuSchDAO;

    @Autowired
    private SchoolDAO schoolDAO;

    // 多线程服务
    private static ExecutorService executorService = new ThreadPoolExecutor(40, 100, 60L, TimeUnit.SECONDS, new LinkedBlockingQueue<>(100));

    @Override
    @Transactional(rollbackFor = Exception.class)
    public UserLoginResponse login(UserLoginContext context) {

        boolean hasMobile = false;
        boolean newUser = false;

        WxAuthContext wxAuthContext = new WxAuthContext();
        wxAuthContext.setIv(context.getIv());
        wxAuthContext.setCode(context.getCode());
        wxAuthContext.setEncryptedData(context.getEncryptedData());

        WxAppUserInfo wxAppUserInfo = decryptedWxAppUserData(wxAuthContext);
        log.info("解密后的微信用户数据为:{}", JSON.toJSONString(wxAppUserInfo));

        if (wxAppUserInfo == null || StringUtils.isBlank(wxAppUserInfo.getOpenId())) {
            throw new BizException(ErrorCodeEnum.SYS_EXP, "登陆失败");
        }

        // 根据openid查询用户是否已经存在
        User queryMode = new User();
        queryMode.setWxOpenId(wxAppUserInfo.getOpenId());
        queryMode.setDeleted(0);
        User queryResult = userDAO.getRecord(queryMode);
        Long userId;
        if (queryResult == null) {
            // 将openId.unionId 存入数据库
            User saveModel = new User();
            saveModel.setWxOpenId(wxAppUserInfo.getOpenId());
            saveModel.setWxUnionid(wxAppUserInfo.getUnionId());
            saveModel.setNickname(wxAppUserInfo.getNickName());
            saveModel.setAvatar(wxAppUserInfo.getAvatarUrl());
            saveModel.setCity(wxAppUserInfo.getCity());
            saveModel.setProvince(wxAppUserInfo.getProvince());

            if (StringUtils.isNotBlank(wxAppUserInfo.getSex())) {
                saveModel.setGender(Integer.parseInt(wxAppUserInfo.getSex()));
            }

            saveModel.setCreateTime(new Date());
            saveModel.setUpdateTime(new Date());
            saveModel.setMock(0);
            saveModel.setTempUser(0);
            saveModel.setDeleted(0);
            int result = userDAO.saveRecord(saveModel);
            if (result == 0) {
                log.error("用户登陆---保存用户数据失败，{}", saveModel);
                throw new BizException(ErrorCodeEnum.SYS_EXP, "登陆失败");
            }

            userId = saveModel.getId();
            newUser = true;

            // 新用户第一次登陆保存用户信息时，记录用户创建时间和用户ID
            String cacheKey = CacheKeyUtil.genNewUserNotArchiveMapKey();
            RBucket<Map<String, Date>> bucket = redissonClient.getBucket(cacheKey);
            if (bucket != null) {
                Map<String, Date> newUserMap = bucket.get();
                if (MapUtil.isEmpty(newUserMap)) {
                    newUserMap = new HashMap<>();
                }
                if (!newUserMap.containsKey(userId + "")) {
                    newUserMap.put(userId + "", saveModel.getCreateTime());
                    bucket.set(newUserMap);
                }
            }
        } else {
            userId = queryResult.getId();
            newUser = false;

            if (StringUtils.isNotBlank(queryResult.getMobile())) {
                hasMobile = true;
            }

            // 记录登录时间, 更新头像昵称到最新
            User updateUserModel = new User();
            updateUserModel.setId(userId);
            updateUserModel.setNickname(wxAppUserInfo.getNickName());
            updateUserModel.setAvatar(wxAppUserInfo.getAvatarUrl());
            updateUserModel.setWxUnionid(wxAppUserInfo.getUnionId());
            updateUserModel.setWxOpenId(wxAppUserInfo.getOpenId());
            updateUserModel.setProvince(wxAppUserInfo.getProvince());
            updateUserModel.setCity(wxAppUserInfo.getCity());
            if (StringUtils.isNotBlank(wxAppUserInfo.getSex())) {
                updateUserModel.setGender(Integer.parseInt(wxAppUserInfo.getSex()));
            }
            updateUserModel.setUpdateTime(new Date());
            userDAO.updateRecord(updateUserModel);
        }

        // 二维码分享登录逻辑
        if (context.getShareUserId() != null) {
            WxCodeShareRecord wxCodeShareRecord = new WxCodeShareRecord();
            wxCodeShareRecord.setUserId(userId);
            wxCodeShareRecord.setShareUserId(context.getShareUserId());
            wxCodeShareRecord.setDeleted(0);
            List<WxCodeShareRecord> shareRecordList = wxCodeShareRecordDAO.listRecord(wxCodeShareRecord);
            if (CollectionUtils.isEmpty(shareRecordList)) {
                wxCodeShareRecord.setCreateTime(new Date());
                wxCodeShareRecord.setUpdateTime(new Date());
                wxCodeShareRecordDAO.saveRecord(wxCodeShareRecord);
            }
        }

        // 生成token
        String token;
        try {
            token = AuthUtil.generateToken(userId + "");
        } catch (UnsupportedEncodingException e) {
            log.error("生成token 异常, userId is {}, e is {}", userId, e);
            throw new BizException(ErrorCodeEnum.SYS_EXP);
        }

        // Cache
        String cacheKey = CacheKeyUtil.genUserTokenKey(userId);
        RBucket<String> bucket = redissonClient.getBucket(cacheKey);
        bucket.set(token, BizConstants.USER_TOKEN_TIME_TO_LIVE, BizConstants.USER_TOKEN_LIVE_TIME_UNIT);
        UserLoginResponse loginResponse = new UserLoginResponse();
        loginResponse.setToken(token);

        // 缓存sessionKey
        if (StringUtils.isNotBlank(wxAppUserInfo.getSessionKey())) {
            String sCacheKey = CacheKeyUtil.genUserSessionKeyCacheKey(userId);
            RBucket<String> sBucket = redissonClient.getBucket(sCacheKey);
            sBucket.set(wxAppUserInfo.getSessionKey());
        }

        // 是否上报过上学信息
        StudentArchives queryArchiveModel = new StudentArchives();
        queryArchiveModel.setUserId(userId);
        queryArchiveModel.setDeleted(0);
        StudentArchives queryArchiveResult = studentArchivesDAO.getRecord(queryArchiveModel);
        loginResponse.setReportArchive(queryArchiveResult != null);
        loginResponse.setUserId(userId);
        if (queryResult != null) {
            loginResponse.setNickName(queryResult.getNickname());
        }

        // 是否为新用户
        loginResponse.setNewUser(newUser);

        // 是否授权过手机号
        loginResponse.setHasMobile(hasMobile);

        // 是否进行过微信授权
        if (queryResult != null && StringUtils.isNotBlank(queryResult.getNickname())) {
            loginResponse.setHasWxAuth(true);
        }

        return loginResponse;
    }

    private WxAppSessionKey getWxSessionKey(WxAuthContext context) {

        // 1 获取sessionKey
        String appId = wxAppProperties.getAppId();
        String appSecret = wxAppProperties.getSecret();

        WxAppSessionKey wxAppSessionKey = wechatClient.code2Session(appId, appSecret, context.getCode(), BizConstants.WX_APP_GRANT_TYPE_SESSION_KEY);
        log.info("wechatClient.code2Session result: {}", JSON.toJSONString(wxAppSessionKey));
        if (wxAppSessionKey == null || wxAppSessionKey.getSessionKey() == null) {
            throw new BizException(ErrorCodeEnum.SYS_EXP, "获取sessionKey出错");
        }

        return wxAppSessionKey;
    }

    /**
     * 解密微信授权给用户的基本信息
     *
     * @param context 需要iv, encryptedData, code
     * @return 微信授权给用户的基本信息
     */
    private WxAppUserInfo decryptedWxAppUserData(WxAuthContext context) {

        WxAppSessionKey wxAppSessionKey = getWxSessionKey(context);

        if (StringUtils.isNotBlank(context.getIv()) && StringUtils.isNotBlank(context.getEncryptedData())) {
            // 2 解密
            String decryptedString = decryptedStringFromWxqEncryptedData(context.getEncryptedData(), wxAppSessionKey.getSessionKey(), context.getIv());
            WxAppUserInfo wxAppUserInfo = JSON.parseObject(decryptedString, WxAppUserInfo.class);
            if (wxAppUserInfo != null) {
                wxAppUserInfo.setSessionKey(wxAppSessionKey.getSessionKey());
            }

            return wxAppUserInfo;
        } else {
            WxAppUserInfo wxAppUserInfo = new WxAppUserInfo();
            wxAppUserInfo.setSessionKey(wxAppSessionKey.getSessionKey());
            wxAppUserInfo.setOpenId(wxAppSessionKey.getOpenId());
            wxAppUserInfo.setUnionId(wxAppSessionKey.getUnionId());
            return wxAppUserInfo;
        }
    }

    /**
     * 将微信的加密字符串解密
     *
     * @param encryptedData 加密字符串
     * @param sessionKey    sessionKey
     * @param iv            iv
     * @return 解密后的字符串
     */
    private String decryptedStringFromWxqEncryptedData(String encryptedData, String sessionKey, String iv) {
        AES wxAes = new AES();
        byte[] resultByte = new byte[0];
        try {
            resultByte = wxAes.decrypt(Base64.decodeBase64(encryptedData), Base64.decodeBase64(sessionKey),
                    Base64.decodeBase64(iv));
        } catch (InvalidAlgorithmParameterException e) {
            log.error("微信数据解密失败 InvalidAlgorithmParameterException  -  {}", e.getMessage());
            return null;
        }
        if (null == resultByte || resultByte.length == 0) {
            log.error("微信数据序解密失败 传参为 encryptedData|{}|iv|{}|sessionKey|{}|结果为|{}",
                    encryptedData, iv, sessionKey, resultByte);
            return null;
        }
        return new String(WxPKCS7Encoder.decode(resultByte));
    }

    /**
     * 微信授权接口，临时用户专用
     *
     * @param context
     */
    @Override
    public void wxAuth(final UserLoginContext context) {

        WxAuthContext wxAuthContext = new WxAuthContext();
        wxAuthContext.setIv(context.getIv());
        wxAuthContext.setCode(context.getCode());
        wxAuthContext.setEncryptedData(context.getEncryptedData());

        WxAppUserInfo wxAppUserInfo = decryptedWxAppUserData(wxAuthContext);
        log.info("解密后的微信用户数据为:{}", JSON.toJSONString(wxAppUserInfo));

        if (wxAppUserInfo == null || StringUtils.isBlank(wxAppUserInfo.getOpenId())) {
            throw new BizException(ErrorCodeEnum.SYS_EXP, "登陆失败");
        }

        // 缓存sessionKey
        if (StringUtils.isNotBlank(wxAppUserInfo.getSessionKey())) {
            String sCacheKey = CacheKeyUtil.genUserSessionKeyCacheKey(context.getUserId());
            RBucket<String> sBucket = redissonClient.getBucket(sCacheKey);
            sBucket.set(wxAppUserInfo.getSessionKey());
        }

        User queryMode = new User();
        queryMode.setWxOpenId(wxAppUserInfo.getOpenId());
        User queryResult = userDAO.getRecord(queryMode);
        Long userId;
        if (queryResult == null) {
            // 将openId.unionId 存入数据库
            User saveModel = new User();
            saveModel.setWxOpenId(wxAppUserInfo.getOpenId());
            saveModel.setWxUnionid(wxAppUserInfo.getUnionId());
            saveModel.setNickname(wxAppUserInfo.getNickName());
            saveModel.setAvatar(wxAppUserInfo.getAvatarUrl());
            saveModel.setCity(wxAppUserInfo.getCity());
            saveModel.setProvince(wxAppUserInfo.getProvince());

            if (StringUtils.isNotBlank(wxAppUserInfo.getSex())) {
                saveModel.setGender(Integer.parseInt(wxAppUserInfo.getSex()));
            }

            saveModel.setCreateTime(new Date());
            saveModel.setUpdateTime(new Date());
            saveModel.setMock(0);
            saveModel.setTempUser(0);
            saveModel.setDeleted(0);
            int result = userDAO.saveRecord(saveModel);
            if (result == 0) {
                log.error("用户登陆---保存用户数据失败，{}", saveModel);
                throw new BizException(ErrorCodeEnum.SYS_EXP, "登陆失败");
            }

            userId = saveModel.getId();
        } else {
            // 更新unionId
            User updateUserModel = new User();
            updateUserModel.setId(context.getUserId());
            updateUserModel.setNickname(wxAppUserInfo.getNickName());
            updateUserModel.setAvatar(wxAppUserInfo.getAvatarUrl());
            updateUserModel.setWxUnionid(wxAppUserInfo.getUnionId());
            updateUserModel.setWxOpenId(wxAppUserInfo.getOpenId());
            updateUserModel.setProvince(wxAppUserInfo.getProvince());
            updateUserModel.setCity(wxAppUserInfo.getCity());
            if (StringUtils.isNotBlank(wxAppUserInfo.getSex())) {
                updateUserModel.setGender(Integer.parseInt(wxAppUserInfo.getSex()));
            }
            updateUserModel.setUpdateTime(new Date());
            updateUserModel.setTempUser(0);
            userDAO.updateRecord(updateUserModel);
        }
    }

    @Override
    public UserLoginResponse tempLogin(final UserLoginContext context) {
        UserLoginResponse loginResponse = new UserLoginResponse();

        // 创建用户
        // 将openId.unionId 存入数据库
        User saveModel = new User();
        saveModel.setCreateTime(new Date());
        saveModel.setUpdateTime(new Date());
        saveModel.setMock(0);
        saveModel.setDeleted(0);
        saveModel.setTempUser(1);

        int result = userDAO.saveRecord(saveModel);
        if (result == 0) {
            log.error("用户登陆---保存用户数据失败，{}", saveModel);
            throw new BizException(ErrorCodeEnum.SYS_EXP, "登陆失败");
        }

        Long userId = saveModel.getId();
        loginResponse.setNewUser(true);
        loginResponse.setUserId(userId);
        loginResponse.setHasMobile(false);
        loginResponse.setReportArchive(false);

        // 生成token
        // 根据用户 ID 生成token
        String token;
        try {
            token = AuthUtil.generateToken(userId + "");
        } catch (UnsupportedEncodingException e) {
            log.error("生成token 异常, userId is {}, e is {}", userId, e);
            throw new BizException(ErrorCodeEnum.SYS_EXP);
        }

        // Cache
        String cacheKey = CacheKeyUtil.genUserTokenKey(userId);
        RBucket<String> bucket = redissonClient.getBucket(cacheKey);
        bucket.set(token, BizConstants.USER_TOKEN_TIME_TO_LIVE, BizConstants.USER_TOKEN_LIVE_TIME_UNIT);
        loginResponse.setToken(token);

        return loginResponse;
    }

    @Override
    public void reportMobile(UserMobileReportRequest request) {

        String appId = wxAppProperties.getAppId();
        String secret = wxAppProperties.getSecret();

        String code = request.getCode();

        String getSessionKeyUrl = "https://api.weixin.qq.com/sns/jscode2session?appid=" + appId + "&secret=" + secret + "&js_code=" + code
                + "&grant_type"
                + "=authorization_code";

        try {

            String sessionKey = null;

            // 先通过缓存获取sessionKey
            String sCacheKey = CacheKeyUtil.genUserSessionKeyCacheKey(request.getUserId());
            RBucket<String> sBucket = redissonClient.getBucket(sCacheKey);

            if (sBucket != null && StringUtils.isNotBlank(sBucket.get())) {
                sessionKey = sBucket.get();
            }

            if (sessionKey == null) {
                String response = HttpUtil.get(getSessionKeyUrl);
                JSONObject jsonObject = JSONObject.parseObject(response);

                if (!jsonObject.containsKey("session_key")) {
                    log.info("【reportMobile】获取sessionKey出错, json is {}", jsonObject);
                    throw new BizException(ErrorCodeEnum.SYS_EXP, "获取sessionKey出错");
                }

                sessionKey = jsonObject.getString("session_key");
            }

            if (StringUtils.isBlank(sessionKey)) {
                throw new BizException(ErrorCodeEnum.SYS_EXP, "最终没有拿到SessionKey");
            }

            // 解密
            String iv = request.getIv();
            String encryptedData = request.getEncryptedData();
            String decryptedString = WXCore.decrypt(appId, encryptedData, sessionKey, iv);

            log.info("解密手机号: iv: {}, encryptedData: {}, decryptedString: {}", iv, encryptedData, decryptedString);

            JSONObject jsonObject1 = JSONObject.parseObject(decryptedString);
            if (jsonObject1 == null || !jsonObject1.containsKey("purePhoneNumber")) {
                log.info("【reportMobile】解密出错, json is {}", jsonObject1);
                throw new BizException(ErrorCodeEnum.SYS_EXP, "解密数据出错");
            }

            String mobile = (String) jsonObject1.get("purePhoneNumber");
            User updateUserModel = new User();
            updateUserModel.setId(request.getUserId());
            updateUserModel.setMobile(mobile);

            int result = userDAO.updateRecord(updateUserModel);
            if (result == 0) {
                throw new BizException(ErrorCodeEnum.SYS_EXP, "保存用户手机号码失败");
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public User getUserById(final Long userId) {

        User queryUserModel = new User();
        queryUserModel.setId(userId);

        return userDAO.getRecord(queryUserModel);
    }

    @Override
    public void reportBaseInfo(final UserBaseInfoContext context) {

        // 1 首先检查用户是否存在
        User queryModel = new User();
        queryModel.setId(context.getUserId());
        queryModel.setDeleted(0);
        User queryResult = userDAO.getRecord(queryModel);
        if (queryResult == null) {
            throw new BizException(ErrorCodeEnum.USER_NOT_EXIST);
        }

        // 2 更新数据
        User updateModel = new User();
        updateModel.setId(context.getUserId());
        updateModel.setNickname(context.getNickName());
        updateModel.setAvatar(context.getAvatarUrl());
        updateModel.setProvince(context.getProvince());
        updateModel.setCity(context.getCity());
        updateModel.setGender(context.getGender());
        updateModel.setUpdateTime(new Date());
        int result = userDAO.updateRecord(updateModel);
        if (result == 0) {
            throw new BizException(ErrorCodeEnum.SYS_EXP, "保存用户信息失败");
        }
    }

    @Override
    public CmsUserSearchResponse cmsSearch(final CmsSearchUserRequest request) {

        CmsUserSearchResponse response = new CmsUserSearchResponse();

        // 昵称，根据user表搜索， 地区，阶段则 根据archive表搜索，(去掉mock用户)
        if (StringUtils.isNotBlank(request.getNickName())
                || (StringUtils.isBlank(request.getCityCode()) && request.getGrade() == null)) {

            User queryModel = new User();
            queryModel.setMock(0);

            Page<User> page = PageHelper.offsetPage(request.getOffset(), request.getPageSize()).doSelectPage(new ISelect() {
                @Override
                public void doSelect() {
                    if (StringUtils.isNotBlank(request.getNickName())) {
                        userDAO.searchByNick(request.getNickName());
                    } else {
                        userDAO.listRecord(queryModel);
                    }
                }
            });

            response.setTotal((int) page.getTotal());

            List<CmsUserSearchItemResponse> userList = new ArrayList<>();
            if (page.size() > 0) {


                for (User user : page.getResult()) {
                    CmsUserSearchItemResponse userResponse = new CmsUserSearchItemResponse();
                    userResponse.setUserId(user.getId());
                    userResponse.setNickName(user.getNickname());
                    userResponse.setMobile(user.getMobile());

                    if (user.getUpdateTime() != null) {
                        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                        String dateString = formatter.format(user.getUpdateTime());
                        userResponse.setLastLoginTime(dateString);
                    }

                    // 查询档案数据
                    StudentArchives queryArchivesModel = new StudentArchives();
                    queryArchivesModel.setUserId(user.getId());
                    queryArchivesModel.setDeleted(0);
                    StudentArchives archives = studentArchivesDAO.getRecord(queryArchivesModel);
                    if (archives != null) {
                        userResponse.setArea(archives.getAreaName());
                        userResponse.setProvince(archives.getProvinceName());
                        userResponse.setCity(archives.getCityName());
                        userResponse.setGrade(archives.getGrade());
                    }

                    userList.add(userResponse);
                }

                response.setList(userList);
            }
        } else {
            StudentArchives queryArchivesModel = new StudentArchives();
            queryArchivesModel.setGradeId(request.getGrade());
            queryArchivesModel.setCityCode(request.getCityCode());
            queryArchivesModel.setDeleted(0);

            Page<StudentArchives> page = PageHelper.offsetPage(request.getOffset(), request.getPageSize()).doSelectPage(new ISelect() {
                @Override
                public void doSelect() {
                    studentArchivesDAO.listRecord(queryArchivesModel);
                }
            });

            response.setTotal((int) page.getTotal());
            List<CmsUserSearchItemResponse> userList = new ArrayList<>();
            if (page.size() > 0) {

                for (StudentArchives archives : page.getResult()) {
                    CmsUserSearchItemResponse userResponse = new CmsUserSearchItemResponse();
                    userResponse.setUserId(archives.getUserId());
                    userResponse.setArea(archives.getAreaName());
                    userResponse.setProvince(archives.getProvinceName());
                    userResponse.setCity(archives.getCityName());
                    userResponse.setGrade(archives.getGrade());

                    User queryUserModel = new User();
                    queryUserModel.setId(archives.getUserId());

                    User user = userDAO.getRecord(queryUserModel);
                    if (user != null) {
                        userResponse.setNickName(user.getNickname());
                        userResponse.setMobile(user.getMobile());

                        if (user.getUpdateTime() != null) {
                            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                            String dateString = formatter.format(user.getUpdateTime());
                            userResponse.setLastLoginTime(dateString);
                            userResponse.setLastLoginTimeLongValue(user.getUpdateTime().getTime());
                        }
                    }

                    userList.add(userResponse);
                }

                userList.sort((o1, o2) -> {
                    long diff = o1.getLastLoginTimeLongValue() - o2.getLastLoginTimeLongValue();
                    if (diff > 0) {
                        return -1;
                    } else if (diff < 0) {
                        return 1;
                    }
                    return 0;
                });

                response.setList(userList);
            }
        }

        return response;
    }

    @Override
    public CmsUserUvResponse uv() {

        CmsUserUvResponse response = new CmsUserUvResponse();

        User queryModel = new User();
        Date startTime = DateUtil.plusDay(new Date(), -7);
        queryModel.setStartTime(startTime);
        queryModel.setEndTime(new Date());
        Integer uv = userDAO.uv(queryModel);

        response.setWeekUv(uv);
        return response;
    }

    @Override
    public void pushOneHourNotArchiveUser() {
        String cacheKey = CacheKeyUtil.genNewUserNotArchiveMapKey();
        RBucket<Map<String, Date>> bucket = redissonClient.getBucket(cacheKey);
        Map<String, Date> map = bucket.get();
        if (MapUtil.isNotEmpty(map)) {
            for (Iterator<Map.Entry<String, Date>> it = map.entrySet().iterator(); it.hasNext(); ) {
                Map.Entry<String, Date> entry = it.next();
                if (StrUtil.isNotBlank(entry.getKey()) && entry.getValue() != null) {
                    Date outTime = cn.hutool.core.date.DateUtil.offsetHour(entry.getValue(), 1);
                    if (outTime.before(new Date())) {
                        messagePushService.pushNotArchiveMsg(entry.getKey());
                        it.remove();
                    }
                }
            }
        }
    }

    @Override
    public void pushOneHourArchiveUserNotReadNews() {
        String cacheKey = CacheKeyUtil.genNewUserArchiveMapKey();
        RBucket<Map<String, Date>> bucket = redissonClient.getBucket(cacheKey);
        Map<String, Date> map = bucket.get();
        if (MapUtil.isNotEmpty(map)) {
            for (Iterator<Map.Entry<String, Date>> it = map.entrySet().iterator(); it.hasNext(); ) {
                Map.Entry<String, Date> entry = it.next();
                if (StrUtil.isNotBlank(entry.getKey()) && entry.getValue() != null) {
                    Date outTime = cn.hutool.core.date.DateUtil.offsetHour(entry.getValue(), 1);
                    // 如果创建时间+1小时，比现在早，那么推送
                    if (outTime.before(new Date())) {
                        Long userId = Long.parseLong(entry.getKey());
                        List<Long> schoolIdList = stuSchDAO.getSchoolIdListByUserId(userId);
                        Long sendSchoolId = -1L;
                        // 如果想读学校不为空，则取想读该学校人最多的去找文章
                        // 否则，拿在读学校去找文章
                        if (CollectionUtils.isNotEmpty(schoolIdList)) {
                            Integer max = 0;
                            for (Long schoolId : schoolIdList) {
                                Integer count = stuSchDAO.countBySchoolId(schoolId);
                                if (count > max) {
                                    max = count;
                                    sendSchoolId = schoolId;
                                }
                            }
                        } else {
                            StudentArchives archives = new StudentArchives();
                            archives.setUserId(userId);
                            archives = studentArchivesDAO.getRecord(archives);
                            if (archives != null && archives.getSchoolId() != null) {
                                sendSchoolId = archives.getSchoolId();
                            }
                        }
                        // 有想读或在读学校
                        if (sendSchoolId != -1L) {
                            School school = schoolDAO.getSchoolSimpleById(sendSchoolId);
                            List<String> schoolTitleList = new ArrayList<>();
                            // 优先根据searchKey进行查询
                            String searchKey = school.getSearchKey();
                            if (StringUtils.isNotBlank(searchKey)) {
                                String[] searchKeyList = searchKey.split(",");
                                schoolTitleList.addAll(Arrays.asList(searchKeyList));
                            } else {
                                // 没有searchKey，再根据名称搜索
                                String schoolName = getSchoolRealName(school);
                                log.info("keyTitle --- is " + schoolName);
                                schoolTitleList.add(schoolName);
                            }
                            // 查找文章
                            News news = newsDAO.getNewNewsBySchoolName(schoolTitleList, school.getCityCode());
                            if (news != null && StrUtil.isNotBlank(news.getTitle())) {
                                // 如果在读想读有文章  取最新的一篇
                                messagePushService.pushArchiveNotReadNewsMsg(userId, news.getId(), school.getName(), news.getTitle());
                            } else {
                                // 如果没有 取新人必读
                                news = new News();
                                news.setId(NewsConstants.NEW_USER_NEWS_ID);
                                news = newsDAO.getRecord(news);
                                if (news != null && StrUtil.isNotBlank(news.getTitle())) {
                                    messagePushService.pushArchiveNotReadNewsMsg(userId, news.getId(), school.getName(), news.getTitle());
                                }
                            }
                        }
                        // 删除这个key
                        it.remove();
                    }
                }
            }
        }
    }

    /**
     * 获取学校除去省市区和括号的名字
     *
     * @param school
     * @return
     */
    private String getSchoolRealName(School school) {
        String schoolName = school.getName();
        // 去掉括号数据
        String[] subSchoolNames = schoolName.split("（");
        String[] subs = subSchoolNames[0].split("\\(");
        schoolName = subs[0];

        //去掉省
        String province = school.getProvince();
        if (StrUtil.isNotBlank(province)) {
            // 去掉省
            // 这一步去除的是浙江省
            if (schoolName.contains(province)) {
                subs = schoolName.split(province);

                if (subs.length > 1) {
                    schoolName = subs[1];
                }
            }
            // 这一步去掉的是浙江
//            province = province.substring(0, province.length()-1);
//            if (schoolName.contains(province)) {
//                subs = schoolName.split(province);
//
//                if (subs.length > 1) {
//                    schoolName = subs[1];
//                }
//            }
        }

        //去掉市
        String city = school.getCity();
        if (StrUtil.isNotBlank(city)) {
            // 去掉省市区
            // 这一步去除的是杭州市
            if (schoolName.contains(city)) {
                subs = schoolName.split(city);

                if (subs.length > 1) {
                    schoolName = subs[1];
                }
            }
            // 这一步去掉的是杭州
//            city = city.substring(0, city.length()-1);
//            if (schoolName.contains(city)) {
//                subs = schoolName.split(city);
//
//                if (subs.length > 1) {
//                    schoolName = subs[1];
//                }
//            }
        }

        //去掉区
        String area = school.getArea();
        if (StrUtil.isNotBlank(area)) {
            // 去掉省市区
            // 这一步去除的是滨江区
            if (schoolName.contains(area)) {
                subs = schoolName.split(area);

                if (subs.length > 1) {
                    schoolName = subs[1];
                }
            }
        }

        return schoolName;
    }
}