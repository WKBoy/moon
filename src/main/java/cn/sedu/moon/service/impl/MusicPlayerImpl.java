/*
 * Dian.com Inc.
 * Copyright (c) 2004-2019 All Rights Reserved.
 */
package cn.sedu.moon.service.impl;

import cn.sedu.moon.common.enums.ErrorCodeEnum;
import cn.sedu.moon.common.exception.BizException;
import cn.sedu.moon.dao.UserGloryDAO;
import cn.sedu.moon.dao.model.UserGlory;
import cn.sedu.moon.service.MusicPlayer;
import lombok.extern.slf4j.Slf4j;
import org.springframework.aop.framework.AopContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;

/**
 * @author wukong
 * @version : MusicPlayerImpl.java, v 0.1 2019年10月22日 3:09 PM Exp $
 */
@Service
@Slf4j
public class MusicPlayerImpl implements MusicPlayer {

    @Autowired
    private UserGloryDAO userGloryDAO;

    @Override
    public void play() {
        log.info("music player async play start");
        ((MusicPlayer)AopContext.currentProxy()).asyncPlay();

//        asyncPlay();
        log.info("music player async play finish");
    }

    @Override
    @Async
    public void asyncPlay() {
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        log.info("music player async playing");
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void saveSong() {
//        saveSongTransaction();
        ((MusicPlayer) AopContext.currentProxy()).saveSongTransaction();
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    @Cacheable
    @Async
    public void saveSongTransaction() {

        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        UserGlory saveModel = new UserGlory();
        saveModel.setUserId(2L);
        saveModel.setGlory(0L);
        saveModel.setCreateTime(new Date());
        saveModel.setUpdateTime(new Date());
        saveModel.setDeleted(0);

        userGloryDAO.saveRecord(saveModel);

        throw new BizException(ErrorCodeEnum.SYS_EXP, "测试异常");
    }
}

