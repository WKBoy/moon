/*
 * Dian.com Inc.
 * Copyright (c) 2004-2019 All Rights Reserved.
 */
package cn.sedu.moon.service.impl;

import cn.sedu.moon.common.enums.ErrorCodeEnum;
import cn.sedu.moon.common.enums.IndexNewsTypeEnum;
import cn.sedu.moon.common.exception.BizException;
import cn.sedu.moon.common.util.CacheKeyUtil;
import cn.sedu.moon.controller.response.AskNewsResponse;
import cn.sedu.moon.controller.response.NewsResponse;
import cn.sedu.moon.controller.response.SchoolNewsResponse;
import cn.sedu.moon.controller.response.SopResponse;
import cn.sedu.moon.dao.AskAttentionDAO;
import cn.sedu.moon.dao.AskDAO;
import cn.sedu.moon.dao.IntroReadDAO;
import cn.sedu.moon.dao.SchoolDAO;
import cn.sedu.moon.dao.SopSubjectDAO;
import cn.sedu.moon.dao.StuSchDAO;
import cn.sedu.moon.dao.StudentArchivesDAO;
import cn.sedu.moon.dao.UserDAO;
import cn.sedu.moon.dao.model.Ask;
import cn.sedu.moon.dao.model.AskAttention;
import cn.sedu.moon.dao.model.IntroRead;
import cn.sedu.moon.dao.model.News;
import cn.sedu.moon.dao.model.School;
import cn.sedu.moon.dao.model.SopSubject;
import cn.sedu.moon.dao.model.StuSch;
import cn.sedu.moon.dao.model.StudentArchives;
import cn.sedu.moon.dao.model.User;
import cn.sedu.moon.service.AskService;
import cn.sedu.moon.service.HomeService;
import cn.sedu.moon.service.NewsService;
import cn.sedu.moon.service.SopService;
import cn.sedu.moon.service.context.IndexAskContext;
import cn.sedu.moon.service.context.QueryNewsContext;
import com.alibaba.fastjson.JSON;
import com.github.pagehelper.ISelect;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.redisson.api.RBucket;
import org.redisson.api.RedissonClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

/**
 * 首页相关服务
 *
 * @author ${wukong}
 * @version $Id: HomeServiceImpl.java, v 0.1 2019年03月27日 7:36 PM Exp $
 */
@Service
@Slf4j
public class HomeServiceImpl implements HomeService {

    @Autowired
    private NewsService newsService;

    @Autowired
    private StudentArchivesDAO archivesDAO;

    @Autowired
    private AskDAO askDAO;

    @Autowired
    private AskService askService;

    @Autowired
    private AskAttentionDAO askAttentionDAO;

    @Autowired
    private UserDAO userDAO;

    @Autowired
    private SopSubjectDAO sopSubjectDAO;

    @Autowired
    private SopService sopService;

    @Autowired
    private RedissonClient redissonClient;

    @Autowired
    private StuSchDAO stuSchDAO;

    @Autowired
    private SchoolDAO schoolDAO;

    @Autowired
    private IntroReadDAO introReadDAO;

    @Override
    public List<NewsResponse> indexNews(final QueryNewsContext context) {

        /**
         * 推荐规则：
         * 1 优先推荐孩子想读的学校的内容(标题里包含想读学校)
         * 2 在推荐对应阶段的内容的内容（幼儿园推荐幼升小，小学推荐小升初）
         * 3 孩子的家对应区域的优先
         *
         * 展示规则:
         * 每页10条，四条内容一条sop，再四条内容一条问答
         */

        List<NewsResponse> responseList = new ArrayList<>();

        // 如果用户是小学的话，直接返回小学对应的新闻
        // 幼儿园用户，返回意向学校的新闻
        getUserChooseSchool(context);

        News queryNewsListModel = new News();
        queryNewsListModel.setDeleted(0);

        // 获取学校新闻

        long startTime = System.currentTimeMillis();
        long endTime = 0L;

        // 明明已经获取了schoolList  为什么又去拿userId去搜新闻？？
        List<SchoolNewsResponse> schoolNewsResponseList = newsService.queryNews(context.getUserId(), context.getOffset(),
                context.getPageSize());
        if (schoolNewsResponseList != null && schoolNewsResponseList.size() > 0) {
            responseList.addAll(schoolNewsResponseList);
        }

        endTime = System.currentTimeMillis();
        log.info("获取学校新闻，耗时 -->" + (endTime - startTime));

        int newsCount = responseList.size();

        if (context.getOffset() != 0){
            // 获取sop问题
            startTime = System.currentTimeMillis();
            if (context.getStuSchList() != null && context.getStuSchList().size() > 0) {
                SopResponse sopResponse = getSop(context);
                if (sopResponse != null) {
                    if (newsCount > 4) {
                        responseList.add(4, sopResponse);
                    } else {
                        responseList.add(sopResponse);
                    }
                }
            }

            endTime = System.currentTimeMillis();
            log.info("获取sop问题，耗时 -->" + (endTime - startTime));

            startTime = System.currentTimeMillis();
            if (context.getUserArchiveCityCode() != null && "330100".equals(context.getUserArchiveCityCode())) {
                // 获取问答
                AskNewsResponse askNewsResponse = getAsk(context);
                if (askNewsResponse != null) {
                    if (newsCount > 8) {
                        responseList.add(8, askNewsResponse);
                    } else {
                        responseList.add(askNewsResponse);
                    }
                }
            }

            endTime = System.currentTimeMillis();
            log.info("获取问答，耗时 -->" + (endTime - startTime));
        }

        return responseList;
    }

    private List<StuSch> getUserChooseSchool(QueryNewsContext context) {

        StudentArchives queryArchive = new StudentArchives();
        queryArchive.setUserId(context.getUserId());
        queryArchive.setDeleted(0);
        //获取学生档案
        StudentArchives queryArchiveResult = archivesDAO.getRecord(queryArchive);
        if (queryArchiveResult == null) {
            throw new BizException(ErrorCodeEnum.USER_NOT_ARCHIVE);
        }

        context.setUserArchiveCityCode(queryArchiveResult.getCityCode());

        Long gradeId = queryArchiveResult.getGradeId();

        List<StuSch> stuSchList = new ArrayList<>();

        // 匹配sop问题使用用户目前所在的学校.
        StuSch stuSch = new StuSch();
        stuSch.setSchoolId(queryArchiveResult.getSchoolId());
        stuSchList.add(stuSch);
        context.setStuSchList(stuSchList);

        List<StuSch> likeStuSchList = new ArrayList<>();
        // 学生意向的学校
        // 原：幼儿园获取想读  小学获取在读
        // 现：想读在读都要
        // 2019/12/1 20:12
        if (gradeId >= 4 && gradeId <= 9) {

            // 非幼儿园用户, 设置为目前所在的学校
            StuSch addstu = new StuSch();
            stuSch.setSchoolId(queryArchiveResult.getSchoolId());
            likeStuSchList.add(addstu);

        } else {
            // 幼儿园用户, 获取学生意向学校列表
            StuSch queryModel = new StuSch();
            queryModel.setUserId(context.getUserId());

            likeStuSchList = stuSchDAO.listRecord(queryModel);
        }

        context.setLikeSchoolList(likeStuSchList);

        return stuSchList;
    }

    private SopResponse getSop(QueryNewsContext context) {

        Integer selectIndex = context.getOffset() / context.getPageSize();

        List<SopResponse> sopResponseList = getSopResponse(context.getUserId(), context.getStuSchList(), selectIndex + 1);

        if (sopResponseList != null && sopResponseList.size() > 0) {
            return sopResponseList.get(sopResponseList.size() - 1);
        }

        return null;
    }

    /**
     * 获取问答列表
     *
     * @param context 上下文
     * @return response
     */
    private AskNewsResponse getAsk(QueryNewsContext context) {

        // 获取问答也采用redis缓存20条的方案
        // 去 redis 中查询是否有数据
        String cacheKey = CacheKeyUtil.genUserAskKey(context.getUserId());
        RBucket<List<Long>> bucket = redissonClient.getBucket(cacheKey);

        Long askId = null;

        if (bucket.get() == null) {
            // 没有数据，走查询逻辑
            Set<Long> idSet = new HashSet();
            List<Long> idList = new ArrayList<>();

            // logicA
            List<Ask> logicAList = askLogicA(context);
            if (logicAList != null && logicAList.size() > 0) {
                for (Ask ask: logicAList) {
                    idSet.add(ask.getId());
                    idList.add(ask.getId());

                    if (idList.size() >= 200) {
                        break;
                    }
                }
            }

            if (idList.size() < 200) {
                // logicB
                List<Ask> logicBList = askLogicB(context);
                if (logicBList != null && logicBList.size() > 0) {
                    for (Ask ask : logicBList) {
                        if (!idSet.contains(ask.getId())) {
                            idSet.add(ask.getId());
                            idList.add(ask.getId());

                            if (idList.size() >= 200) {
                                break;
                            }

                        }
                    }
                }
            }

            bucket.set(idList, 1440, TimeUnit.MINUTES);

            Integer startIndex = context.getOffset();

            if (startIndex > idList.size() - 1) {
                return null;
            }

            askId = idList.get(startIndex);
        } else {


            Integer startIndex = context.getOffset();

            // 数据已经加载完了
            if (startIndex > bucket.get().size() - 1) {
                return null;
            }

            askId = bucket.get().get(startIndex);
        }

        if (askId != null ) {

            Ask queryAskModel = new Ask();
            queryAskModel.setId(askId);
            queryAskModel.setDeleted(0);

            Ask askResult = askDAO.getRecord(queryAskModel);
            if (askResult == null) {
                return null;
            }

            AskNewsResponse response = new AskNewsResponse();
            response.setNewsType(IndexNewsTypeEnum.ASK.getCode());
            response.setAskId(askResult.getId());
            response.setTitle(askResult.getTitle());
            response.setFollowCount(askService.queryAskAttentionCount(askResult.getId()));
            response.setAnswerCount(askService.queryAskAnswerCount(askResult.getId()));

            AskAttention queryModel = new AskAttention();
            queryModel.setAskId(askResult.getId());
            queryModel.setAttentionUserId(context.getUserId());
            queryModel.setDeleted(0);

            List<AskAttention> askAttentionList = askAttentionDAO.listRecord(queryModel);
            if (askAttentionList != null && askAttentionList.size() > 0) {
                response.setIsFollow(true);
            } else {
                response.setIsFollow(false);
            }

            User queryUserModel = new User();
            queryUserModel.setId(askResult.getPublisher());
            User user = userDAO.getRecord(queryUserModel);
            if (user != null) {
                response.setAvatar(user.getAvatar());
                response.setNickName(user.getNickname());
            }

            return response;

        }

        return null;
    }


    private List<AskNewsResponse> getAskList(QueryNewsContext context) {

        List<AskNewsResponse> responseList = new ArrayList<>();

        // 获取问答也采用redis缓存20条的方案
        // 去 redis 中查询是否有数据
        String cacheKey = CacheKeyUtil.genUserAskKey(context.getUserId());
        RBucket<List<Long>> bucket = redissonClient.getBucket(cacheKey);

        List<Long> askIdList = new ArrayList<>();

        log.info("get ask list bucket is :" + JSON.toJSONString(bucket.get()));

        if (bucket.get() == null || bucket.get().size() == 0) {
            // 没有数据，走查询逻辑
            Set<Long> idSet = new HashSet();
            List<Long> idList = new ArrayList<>();

            // logicA
            List<Ask> logicAList = askLogicA(context);
            if (logicAList != null && logicAList.size() > 0) {
                for (Ask ask : logicAList) {
                    idSet.add(ask.getId());
                    idList.add(ask.getId());

                    if (idList.size() >= 200) {
                        break;
                    }
                }
            }

            if (idList.size() < 200) {
                // logicB
                List<Ask> logicBList = askLogicB(context);
                if (logicBList != null && logicBList.size() > 0) {
                    for (Ask ask : logicBList) {
                        if (!idSet.contains(ask.getId())) {
                            idSet.add(ask.getId());
                            idList.add(ask.getId());

                            if (idList.size() >= 200) {
                                break;
                            }

                        }
                    }
                }
            }

            bucket.set(idList, 1440, TimeUnit.MINUTES);

            Integer startIndex = context.getOffset();
            Integer endIndex = context.getOffset() + context.getPageSize() - 1;

            List<Long> resultList = new ArrayList<>();

            // 数据已经加载完了
            if (startIndex > idList.size() - 1) {
                return null;
            }

            for (int i = 0; i < idList.size(); i++) {
                if (i >= startIndex && i <= endIndex) {
                    resultList.add(idList.get(i));
                }
            }

            askIdList = resultList;
        } else {

            Integer startIndex = context.getOffset();
            Integer endIndex = context.getOffset() + context.getPageSize() - 1;

            List<Long> resultList = new ArrayList<>();

            // 数据已经加载完了
            if (startIndex > bucket.get().size() - 1) {
                return null;
            }

            for (int i = 0; i < bucket.get().size(); i++) {
                if (i >= startIndex && i <= endIndex) {
                    resultList.add(bucket.get().get(i));
                }
            }

            askIdList = resultList;
        }

        if (askIdList != null && askIdList.size() > 0) {

            for (Long askId : askIdList) {

                Ask queryAskModel = new Ask();
                queryAskModel.setId(askId);
                queryAskModel.setDeleted(0);

                Ask askResult = askDAO.getRecord(queryAskModel);
                if (askResult == null) {
                    continue;
                }

                AskNewsResponse response = new AskNewsResponse();
                response.setNewsType(IndexNewsTypeEnum.ASK.getCode());
                response.setAskId(askResult.getId());
                response.setTitle(askResult.getTitle());
                response.setFollowCount(askService.queryAskAttentionCount(askResult.getId()));
                response.setAnswerCount(askService.queryAskAnswerCount(askResult.getId()));

                AskAttention queryModel = new AskAttention();
                queryModel.setAskId(askResult.getId());
                queryModel.setAttentionUserId(context.getUserId());
                queryModel.setDeleted(0);

                List<AskAttention> askAttentionList = askAttentionDAO.listRecord(queryModel);
                if (askAttentionList != null && askAttentionList.size() > 0) {
                    response.setIsFollow(true);
                } else {
                    response.setIsFollow(false);
                }

                User queryUserModel = new User();
                queryUserModel.setId(askResult.getPublisher());
                User user = userDAO.getRecord(queryUserModel);
                if (user != null) {
                    response.setAvatar(user.getAvatar());
                    response.setNickName(user.getNickname());
                }

                responseList.add(response);
            }

            return responseList;
        }

        return null;
    }

    List<Ask> askLogicA(QueryNewsContext context) {
        // 推荐想读的学校内容
        if (context.getLikeSchoolList() != null && context.getLikeSchoolList().size() > 0) {

            List<String> schoolTitleList = new ArrayList<>();
            for (StuSch stuSch : context.getLikeSchoolList()) {

                // 查询学校名称
                School querySchoolModel = new School();
                querySchoolModel.setId(stuSch.getSchoolId());
                List<School> schoolList = schoolDAO.listRecord(querySchoolModel);
                if (schoolList != null && schoolList.size() > 0) {

                    // 优先根据searchKey进行查询
                    String searchKey = schoolList.get(0).getSearchKey();
                    if (StringUtils.isNotBlank(searchKey)) {
                        String[] searchKeyList = searchKey.split(",");
                        schoolTitleList.addAll(Arrays.asList(searchKeyList));

                    } else {
                        // 没有searchKey，再根据名称搜索

                        String schoolName = schoolList.get(0).getName();

                        // 去掉括号数据
                        String[] subSchoolNames = schoolName.split("（");
                        String[] subs = subSchoolNames[0].split("\\(");

                        String keyTitle = subs[0];
                        // 去掉杭州，或者杭州市
                        if (keyTitle.contains("杭州市")) {
                            subs = keyTitle.split("杭州市");

                            if (subs.length > 1) {
                                keyTitle = subs[1];
                            }
                        }

                        if (keyTitle.contains("杭州")) {
                            subs = keyTitle.split("杭州");

                            if (subs.length > 1) {
                                keyTitle = subs[1];
                            }
                        }

                        log.info("keyTitle --- is " + keyTitle);
                        schoolTitleList.add(keyTitle);
                    }
                }
            }

            return askDAO.listRecordBySchoolNameList(schoolTitleList);
        }

        return null;
    }

    List<Ask> askLogicB(QueryNewsContext context) {

        Ask queryAskModel = new Ask();
        queryAskModel.setDeleted(0);

        // 推荐 剩下的
        Page<Ask> page = PageHelper.offsetPage(0, 200).doSelectPage(new ISelect() {
            @Override
            public void doSelect() {
                askDAO.listRecord(queryAskModel);
            }
        });

        if (page != null && page.getResult().size() > 0) {
            return page.getResult();
        }

        return null;
    }


    /**
     * 根据用户择校信息获取 sop 问题
     *
     * @param stuSchList 用户择校信息
     * @param sopNeedTotal 一共所需的sop问题的数量
     * @return sop问题列表, 返回<= sopNeedTotal
     */
    private List<SopResponse> getSopResponse(Long userId, List<StuSch> stuSchList, Integer sopNeedTotal) {

        List<SopResponse> responsesList = new ArrayList<>();
        Integer sopCount = 0;

        // 查询所有的sop问题列表
        SopSubject queryAllSubjectModel = new SopSubject();
        List<SopSubject> sopSubjectList = sopSubjectDAO.listRecord(queryAllSubjectModel);
        List<Integer> sopAllSubjectRefIdList = new ArrayList<>();

        for (SopSubject subject : sopSubjectList) {
            sopAllSubjectRefIdList.add(subject.getRefId());
        }

        for (StuSch sch : stuSchList) {

            Long schoolId = sch.getSchoolId();

            // 查询用户回答过的问题列表，然后将剩下的问题构造sop
            List<Integer> subjectRefIdList = sopService.getSubjectIdListByUserIdAndSchoolId(userId, sch.getSchoolId());
            if (subjectRefIdList != null && subjectRefIdList.size() > 0) {
                sopAllSubjectRefIdList.removeAll(subjectRefIdList);
            }

            if (sopAllSubjectRefIdList.size() > 0) {

                for (Integer targetId : sopAllSubjectRefIdList) {

                    SopResponse sopResponse = sopService.getSop(schoolId, targetId, userId);
                    if (sopResponse != null) {
                        responsesList.add(sopResponse);
                        sopCount++;
                        if (sopCount.equals(sopNeedTotal)) {
                            return responsesList;
                        }
                    }
                }
            }
        }

        return responsesList;
    }

    @Override
    public List<NewsResponse> indexAsk(final IndexAskContext context) {

        // 获取意向的学校信息，如果没有意向的学校信息，则SOP不返回
        QueryNewsContext queryNewsContext = new QueryNewsContext();
        queryNewsContext.setOffset(context.getOffset());
        queryNewsContext.setPageSize(8);
        queryNewsContext.setUserId(context.getUserId());

        getUserChooseSchool(context);

        // 每页加载2个sop
        List<NewsResponse> responseList = new ArrayList<>();

        if (context.getUserArchiveCityCode() != null) {
            // 获取问答
            List<AskNewsResponse> askNewsList = getAskList(context);
            if (askNewsList != null && askNewsList.size() > 0) {
                responseList.addAll(askNewsList);
            }
        }

        // 获取sop 问题
        if (responseList.size() > 0 && context.getStuSchList() != null && context.getStuSchList().size() > 0) {
            SopResponse sopResponse = getSop(context);
            responseList.add(sopResponse);
        }

        return responseList;
    }

    @Override
    public Boolean checkIntroRead(final Long userId) {

        IntroRead queryIntroReadModel = new IntroRead();
        queryIntroReadModel.setUserId(userId);
        List<IntroRead> introReadList = introReadDAO.listRecord(queryIntroReadModel);
        if (introReadList != null && introReadList.size() > 0) {
            return true;
        }

        return false;
    }

    @Override
    public void introRead(final Long userId) {
        IntroRead saveModel = new IntroRead();
        saveModel.setUserId(userId);
        saveModel.setCreateTime(new Date());
        saveModel.setUpdateTime(new Date());

        int result = introReadDAO.saveRecord(saveModel);

        if (result == 0) {
            throw new BizException(ErrorCodeEnum.SYS_EXP, "保存失败");
        }
    }
}