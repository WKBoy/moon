/*
 * Dian.com Inc.
 * Copyright (c) 2004-2019 All Rights Reserved.
 */
package cn.sedu.moon.service.impl;

import cn.sedu.moon.common.enums.ErrorCodeEnum;
import cn.sedu.moon.common.enums.GloryChangeBizTypeEnum;
import cn.sedu.moon.common.enums.IndexNewsTypeEnum;
import cn.sedu.moon.common.enums.MedalTypeEnum;
import cn.sedu.moon.common.enums.SopSubjectTypeEnum;
import cn.sedu.moon.common.exception.BizException;
import cn.sedu.moon.common.util.CacheKeyUtil;
import cn.sedu.moon.controller.request.SopOptionAnswerRequest;
import cn.sedu.moon.controller.response.SopOption;
import cn.sedu.moon.controller.response.SopReportAnswerResponse;
import cn.sedu.moon.controller.response.SopResponse;
import cn.sedu.moon.dao.AskAnswerDAO;
import cn.sedu.moon.dao.SchoolDAO;
import cn.sedu.moon.dao.SopAnswerDAO;
import cn.sedu.moon.dao.SopAttentionDAO;
import cn.sedu.moon.dao.SopOptionDAO;
import cn.sedu.moon.dao.SopSubjectDAO;
import cn.sedu.moon.dao.UserMedalDAO;
import cn.sedu.moon.dao.model.AskAnswer;
import cn.sedu.moon.dao.model.School;
import cn.sedu.moon.dao.model.SopAnswer;
import cn.sedu.moon.dao.model.SopAttention;
import cn.sedu.moon.dao.model.SopOptionDO;
import cn.sedu.moon.dao.model.SopSubject;
import cn.sedu.moon.dao.model.UserMedal;
import cn.sedu.moon.service.GloryService;
import cn.sedu.moon.service.SopService;
import cn.sedu.moon.service.context.SopAnswerContext;
import cn.sedu.moon.thirdparty.WechatApi;
import cn.sedu.moon.thirdparty.response.WxMsgSecCheckResponse;
import com.alibaba.fastjson.JSON;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.redisson.api.RBucket;
import org.redisson.api.RedissonClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 *
 * @author ${wukong}
 * @version $Id: SopServiceImpl.java, v 0.1 2019年01月09日 11:55 AM Exp $
 */
@Service
@Slf4j
public class SopServiceImpl implements SopService {

    @Autowired
    private SopSubjectDAO sopSubjectDAO;

    @Autowired
    private SopOptionDAO sopOptionDAO;

    @Autowired
    private SopAnswerDAO sopAnswerDAO;

    @Autowired
    private SchoolDAO schoolDAO;

    @Autowired
    private SopAttentionDAO attentionDAO;

    @Autowired
    private UserMedalDAO userMedalDAO;

    @Autowired
    private AskAnswerDAO askAnswerDAO;

    @Autowired
    private GloryService gloryService;

    @Autowired
    private WechatApi wechatApi;

    @Autowired
    private RedissonClient redissonClient;

    @Override
    public SopResponse getSop(final Long schoolId, final Integer refId, Long userId) {

        // 根据refId得到问题
        SopSubject sopDO = sopSubjectDAO.getRecordByRefId(refId);
        if (sopDO == null) {
            return null;
        }

        // 查询学校，获取学校名称
        School querySchoolModel = new School();
        querySchoolModel.setId(schoolId);
        School queryResultSchool = schoolDAO.getRecord(querySchoolModel);
        if (queryResultSchool == null) {
            log.error("【获取sop问题】, 查询学校为空, schoolId is {}", schoolId);
            return null;
        }

        SopResponse response = new SopResponse();
        response.setId(sopDO.getRefId());
        String title = MessageFormat.format(sopDO.getTitle(), queryResultSchool.getName());
        response.setTitle(title);
        response.setSopType(sopDO.getType());
        response.setNewsType(IndexNewsTypeEnum.SOP.getCode());
        response.setSchoolId(schoolId);

        // 获取关注人数
        SopAttention queryAttentionModel = new SopAttention();
        queryAttentionModel.setSopId(sopDO.getRefId());
        Integer attentionCount = attentionDAO.countRecord(queryAttentionModel);
        if (attentionCount == null) {
            attentionCount = 0;
        }

        response.setFollowCount(attentionCount);

        // 判断当前用户有没有关注
        queryAttentionModel = new SopAttention();
        queryAttentionModel.setAttentionUserId(userId);
        queryAttentionModel.setSopId(sopDO.getRefId());
        List<SopAttention> queryResult = attentionDAO.listRecord(queryAttentionModel);
        if (queryResult != null && queryResult.size() > 0) {
            response.setIsFollow(true);
        } else {
            response.setIsFollow(false);
        }

        if (SopSubjectTypeEnum.INPUT_TEXT.getCode().equals(sopDO.getType())) {
            return response;
        }

        // 查询所有的选项
        SopOptionDO queryOptionsModel = new SopOptionDO();
        queryOptionsModel.setSubjectRefId(refId);
        queryOptionsModel.setDeleted(0);
        List<SopOptionDO> sopOptionDOList = sopOptionDAO.listRecord(queryOptionsModel);
        if (sopOptionDOList == null || sopOptionDOList.size() == 0) {
            return null;
        }

        // 遍历选项, 构造response
        List<SopOption> sopOptionListRes = new ArrayList<>();
        for (SopOptionDO sopOptionDO : sopOptionDOList) {
            SopOption sopOption = new SopOption();
            sopOption.setValue(sopOptionDO.getRefId());
            sopOption.setTitle(sopOptionDO.getContent());
            sopOption.setChoiceShowInput(sopOptionDO.getChooseShowInput() == 1);
            sopOption.setInputInfo(sopOptionDO.getInputInfo());
            sopOptionListRes.add(sopOption);
        }

        response.setOptionList(sopOptionListRes);



        return response;
    }

    @Override
    public void attention(final Integer sopRefId, final Long userId) {

        // 先查询一下是否已经关注
        SopAttention queryModel = new SopAttention();
        queryModel.setSopId(sopRefId);
        queryModel.setAttentionUserId(userId);
        List<SopAttention> queryResult = attentionDAO.listRecord(queryModel);
        if (queryResult != null && queryResult.size() > 0) {
            throw new BizException(ErrorCodeEnum.SYS_EXP, "您已经关注了这个问题");
        }

        // 添加关注
        SopAttention saveModel = new SopAttention();
        saveModel.setSopId(sopRefId);
        saveModel.setAttentionUserId(userId);
        saveModel.setDeleted(0);
        saveModel.setCreateTime(new Date());
        saveModel.setUpdateTime(new Date());
        int result = attentionDAO.saveRecord(saveModel);

        if (result == 0) {
            throw new BizException(ErrorCodeEnum.SYS_EXP, "关注失败");
        }
    }

    @Override
    public List<Integer> getSubjectIdListByUserIdAndSchoolId(final Long userId, final Long schoolId) {

        SopAnswer queryAnswerModel = new SopAnswer();
        queryAnswerModel.setUserId(userId);
        queryAnswerModel.setSchoolId(schoolId);

        List<SopAnswer> sopAnswerList = sopAnswerDAO.selectSubjectIdList(queryAnswerModel);
        if (sopAnswerList == null || sopAnswerList.size() == 0) {
            return null;
        }

        List<Integer> subjectRefIdList = new ArrayList<>();
        for (SopAnswer sopAnswer : sopAnswerList) {
             subjectRefIdList.add(sopAnswer.getSubjectRefId());
        }

        return subjectRefIdList;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public SopReportAnswerResponse reportSopAnswer(final SopAnswerContext context) {
        if (StringUtils.isNotBlank(context.getInputString())) {
            WxMsgSecCheckResponse checkResponse = wechatApi.contentCheck(context.getInputString());
            if (checkResponse.getErrorCode() != 0) {
                throw new BizException(ErrorCodeEnum.SYS_EXP, "有不合法的内容，请修改后再试试哦");
            }
        }

        SopReportAnswerResponse response = new SopReportAnswerResponse();

        // 查询问题的基本信息
        SopSubject sopSubject = sopSubjectDAO.getRecordByRefId(context.getSubjectRefId());
        if (sopSubject == null) {
            throw new BizException(ErrorCodeEnum.SOP_SUBJECT_NOT_EXIST);
        }
        context.setSopSubject(sopSubject);

        // 如果用户是第一次回答sop问题，或者 问答 ，颁发勋章
        SopAnswer queryAnswerModel = new SopAnswer();
        queryAnswerModel.setUserId(context.getUserId());
        queryAnswerModel.setDeleted(0);

        List<SopAnswer> answerList = sopAnswerDAO.listRecord(queryAnswerModel);
        if (answerList == null || answerList.size() == 0) {

            // 查询问答
            AskAnswer queryAskAnswerModel = new AskAnswer();
            queryAskAnswerModel.setAnswerUserId(context.getUserId());
            queryAskAnswerModel.setDeleted(0);
            List<AskAnswer> askAnswerList = askAnswerDAO.listRecord(queryAskAnswerModel);
            if (askAnswerList == null || askAnswerList.size() == 0) {
                response.setIsGetMedal(true);
                response.setMedalTitle(MedalTypeEnum.SOP_FIRST_ANSWER.getTitle());

                UserMedal medalSaveModel = new UserMedal();
                medalSaveModel.setUserId(context.getUserId());
                medalSaveModel.setType(MedalTypeEnum.SOP_FIRST_ANSWER.getCode());
                medalSaveModel.setTitle(MedalTypeEnum.SOP_FIRST_ANSWER.getTitle());
                medalSaveModel.setCreateTime(new Date());
                medalSaveModel.setUpdateTime(new Date());
                medalSaveModel.setDeleted(0);

                int result = userMedalDAO.saveRecord(medalSaveModel);
                if (result == 0) {
                    throw new BizException(ErrorCodeEnum.SYS_EXP, "保存勋章失败");
                }
            }
        } else {
            response.setIsGetMedal(false);
        }

        // 对问题的答案进行检查
        checkSopAnswer(context);

        if (SopSubjectTypeEnum.INPUT_TEXT.getCode().equals(sopSubject.getType())) {
            // 如果是输入型问题, 直接插入数据即可
            SopAnswer answerSaveModel = new SopAnswer();
            answerSaveModel.setUserId(context.getUserId());
            answerSaveModel.setSubjectRefId(context.getSubjectRefId());
            answerSaveModel.setSchoolId(context.getSchoolId());

            answerSaveModel.setOptionInputText(context.getInputString());

            answerSaveModel.setCreateTime(new Date());
            answerSaveModel.setUpdateTime(new Date());
            answerSaveModel.setDeleted(0);

            int result = sopAnswerDAO.saveRecord(answerSaveModel);
            if (result == 0) {
                log.error("【保存sop答案数据失败】save model is {}", JSON.toJSONString(answerSaveModel));
                throw new BizException(ErrorCodeEnum.SYS_EXP, "保存sop答案数据失败");
            }
        } else {
            // 遍历options，逐条插入
            if (context.getOptionList() != null && context.getOptionList().size() > 0) {
                for (SopOptionAnswerRequest optionAnswer : context.getOptionList()) {
                    SopAnswer answerSaveModel = new SopAnswer();
                    answerSaveModel.setUserId(context.getUserId());
                    answerSaveModel.setSubjectRefId(context.getSubjectRefId());
                    answerSaveModel.setSchoolId(context.getSchoolId());

                    answerSaveModel.setOptionInputText(optionAnswer.getInputString());
                    answerSaveModel.setOptionRefId(optionAnswer.getId());

                    answerSaveModel.setCreateTime(new Date());
                    answerSaveModel.setUpdateTime(new Date());
                    answerSaveModel.setDeleted(0);

                    int result = sopAnswerDAO.saveRecord(answerSaveModel);

                    if (result == 0) {
                        log.error("【保存sop答案数据失败】save model is {}", JSON.toJSONString(answerSaveModel));
                        throw new BizException(ErrorCodeEnum.SYS_EXP, "保存sop答案数据失败");
                    }
                }
            }
        }

//        // 荣誉值 + 1, redis锁
//        gloryService.addGlory(context.getUserId(), 1L, GloryChangeBizTypeEnum.SOP);

        // 根据情况给荣誉值
        // 用户第一次回答sop问题，奖励2分，其他情况回答奖励1分
        response.setGloryNum(0);

        // 获取该SOP的答题
        SopAnswer model = new SopAnswer();
        model.setUserId(context.getUserId());
        model.setDeleted(0);

        List<SopAnswer> sopAnswerList = sopAnswerDAO.listRecord(model);
        if (CollectionUtils.isEmpty(sopAnswerList)) {
            gloryService.addGlory(context.getUserId(), 2L, GloryChangeBizTypeEnum.SOP);
            response.setGloryNum(2);
        } else {
            gloryService.addGlory(context.getUserId(), 1L, GloryChangeBizTypeEnum.SOP);
            response.setGloryNum(1);
        }

        // 当sop问题答案大于10个，且所选的答案80%相同时，生成标签，不再投放
        Integer sopAnswerCount = sopAnswerDAO.selectCountBySubjectRefId(context.getSubjectRefId());
        Integer maxAnswerCount = 0;
        if (sopSubject.getType() == 1){
            maxAnswerCount = sopAnswerDAO.selectCountBySubjectRefIdMaxOpt(context.getSchoolId(), context.getSubjectRefId());
        } else {
            maxAnswerCount = sopAnswerDAO.selectCountBySubjectRefIdMaxText(context.getSchoolId(),context.getSubjectRefId());
        }
        if (maxAnswerCount > 8 && sopAnswerCount * 0.8 <= maxAnswerCount){
            String cacheKey = CacheKeyUtil.genSchoolTagKey(context.getSchoolId());
            RBucket<List<Integer>> bucket = redissonClient.getBucket(cacheKey);
            List<Integer> sopIdList = bucket.get();
            if (sopIdList == null || sopAnswerList.size() == 0){
                sopIdList = new ArrayList<>();
                sopIdList.add(context.getSubjectRefId());
            } else {
                if (!sopIdList.contains(context.getSubjectRefId())){
                    sopIdList.add(context.getSubjectRefId());
                }
            }
            bucket.set(sopIdList);
        }


        SopAttention queryAttentionModel = new SopAttention();
        queryAttentionModel.setSopId(context.getSubjectRefId());
        Integer attentionCount = attentionDAO.countRecord(queryAttentionModel);
        if (attentionCount == null) {
            attentionCount = 0;
        }

        response.setFollowCount(attentionCount);
        return response;

        // TODO 更新 题目的 viewCount, answerCount

    }

    private void checkSopAnswer(SopAnswerContext context) {
        // 单选不能不选多选
        // 输入的问题类型，输入字段不可为空
        // 多选的不能不选
        Integer subjectType = context.getSopSubject().getType();

        if (SopSubjectTypeEnum.INPUT_TEXT.getCode().equals(subjectType)) {

            if (StringUtils.isBlank(context.getInputString())) {
                throw new BizException(ErrorCodeEnum.PARAM_CHECK_EXP, "你还未输入回答哦，输入回答可获得荣誉值");
            }

        } else if (SopSubjectTypeEnum.SINGLE_CHOICE.getCode().equals(subjectType)
                || SopSubjectTypeEnum.MULTI_CHOICE.getCode().equals(subjectType)
                || SopSubjectTypeEnum.SINGLE_CHOICE_AND_INPUT_TEXT.getCode().equals(subjectType)
                || SopSubjectTypeEnum.MULTI_CHOICE_AND_INPUT_TEXT.getCode().equals(subjectType)) {

            if (context.getOptionList() == null || context.getOptionList().size() == 0) {
                throw new BizException(ErrorCodeEnum.PARAM_CHECK_EXP, "至少需要选择一个答案选项");
            }
        }
    }

    @Override
    public SopResponse getSchoolSopUnAnswer(Long userId, Long schoolId) {
        String cacheKey = CacheKeyUtil.genSchoolTagKey(schoolId);
        RBucket<List<Integer>> bucket = redissonClient.getBucket(cacheKey);
        List<Integer> sopIdList = bucket.get();
        if (sopIdList == null) {
            sopIdList = new ArrayList<>();
        }

        SopAnswer queryAnswerModel = new SopAnswer();
        queryAnswerModel.setUserId(userId);
        queryAnswerModel.setSchoolId(schoolId);
        // 获取已答过的sop问题列表
        List<SopAnswer> sopAnswerList = sopAnswerDAO.selectSubjectIdList(queryAnswerModel);
        if (sopAnswerList != null && sopAnswerList.size() != 0) {
            for (SopAnswer sopAnswer : sopAnswerList){
                if (sopAnswer.getSubjectRefId() != null) {
                    sopIdList.add(sopAnswer.getSubjectRefId());
                }
            }
        }
        SopSubject sop = sopSubjectDAO.getFirstSop(sopIdList);
        // 查询学校，获取学校名称
        School querySchoolModel = new School();
        querySchoolModel.setId(schoolId);
        School queryResultSchool = schoolDAO.getRecord(querySchoolModel);
        if (queryResultSchool == null) {
            log.error("【获取sop问题】, 查询学校为空, schoolId is {}", schoolId);
            return null;
        }

        SopResponse response = new SopResponse();
        response.setId(sop.getRefId());
        String title = MessageFormat.format(sop.getTitle(), queryResultSchool.getName());
        response.setTitle(title);
        response.setSopType(sop.getType());
        response.setNewsType(IndexNewsTypeEnum.SOP.getCode());
        response.setSchoolId(schoolId);

        // 获取关注人数
        SopAttention queryAttentionModel = new SopAttention();
        queryAttentionModel.setSopId(sop.getRefId());
        Integer attentionCount = attentionDAO.countRecord(queryAttentionModel);
        if (attentionCount == null) {
            attentionCount = 0;
        }

        response.setFollowCount(attentionCount);

        // 判断当前用户有没有关注
        queryAttentionModel = new SopAttention();
        queryAttentionModel.setAttentionUserId(userId);
        queryAttentionModel.setSopId(sop.getRefId());
        List<SopAttention> queryResult = attentionDAO.listRecord(queryAttentionModel);
        if (queryResult != null && queryResult.size() > 0) {
            response.setIsFollow(true);
        } else {
            response.setIsFollow(false);
        }

        if (SopSubjectTypeEnum.INPUT_TEXT.getCode().equals(sop.getType())) {
            return response;
        }

        // 查询所有的选项
        SopOptionDO queryOptionsModel = new SopOptionDO();
        queryOptionsModel.setSubjectRefId(sop.getRefId());
        queryOptionsModel.setDeleted(0);
        List<SopOptionDO> sopOptionDOList = sopOptionDAO.listRecord(queryOptionsModel);
        if (sopOptionDOList == null || sopOptionDOList.size() == 0) {
            return null;
        }

        // 遍历选项, 构造response
        List<SopOption> sopOptionListRes = new ArrayList<>();
        for (SopOptionDO sopOptionDO : sopOptionDOList) {
            SopOption sopOption = new SopOption();
            sopOption.setValue(sopOptionDO.getRefId());
            sopOption.setTitle(sopOptionDO.getContent());
            sopOption.setChoiceShowInput(sopOptionDO.getChooseShowInput() == 1);
            sopOption.setInputInfo(sopOptionDO.getInputInfo());
            sopOptionListRes.add(sopOption);
        }

        response.setOptionList(sopOptionListRes);
        return response;
    }

    @Override
    public String getSopTag(Long schoolId, Integer subjectRefId) {
        SopSubject sopSubject = sopSubjectDAO.getRecordByRefId(subjectRefId);
        if (sopSubject.getType() == 3){
            Integer optionId = sopAnswerDAO.selectBySubjectRefIdMaxOpt(schoolId, subjectRefId);
            return sopOptionDAO.selectContentByOptionId(optionId, subjectRefId);
        } else {
            return sopAnswerDAO.selectBySubjectRefIdMaxText(schoolId, subjectRefId);

        }
    }
}