/*
 * Dian.com Inc.
 * Copyright (c) 2004-2019 All Rights Reserved.
 */
package cn.sedu.moon.service.impl;

import cn.hutool.core.date.TimeInterval;
import cn.hutool.core.map.MapUtil;
import cn.hutool.core.util.StrUtil;
import cn.sedu.moon.common.constant.NewsConstants;
import cn.sedu.moon.common.enums.ErrorCodeEnum;
import cn.sedu.moon.common.enums.IndexNewsTypeEnum;
import cn.sedu.moon.common.enums.NewsTypeEnum;
import cn.sedu.moon.common.exception.BizException;
import cn.sedu.moon.common.util.CacheKeyUtil;
import cn.sedu.moon.common.util.DateUtil;
import cn.sedu.moon.controller.response.census.EditCensusResponse;
import cn.sedu.moon.controller.request.CmsNewsQueryRequest;
import cn.sedu.moon.controller.request.news.AddNewsRequest;
import cn.sedu.moon.controller.request.news.NewsCheckRequest;
import cn.sedu.moon.controller.response.AskResponse;
import cn.sedu.moon.controller.response.NewsNoticeResponse;
import cn.sedu.moon.controller.response.SchoolNewsCheckDetailResponse;
import cn.sedu.moon.controller.response.SchoolNewsCheckResponse;
import cn.sedu.moon.controller.response.SchoolNewsDetailResponse;
import cn.sedu.moon.controller.response.SchoolNewsResponse;
import cn.sedu.moon.controller.response.SearchCollectNewsResponse;
import cn.sedu.moon.controller.response.share.CheckShareResponse;
import cn.sedu.moon.dao.AskDAO;
import cn.sedu.moon.dao.CmsUserDAO;
import cn.sedu.moon.dao.LogNewsDetailDAO;
import cn.sedu.moon.dao.NewsAskDAO;
import cn.sedu.moon.dao.NewsDAO;
import cn.sedu.moon.dao.NewsDetailDAO;
import cn.sedu.moon.dao.NewsReadDAO;
import cn.sedu.moon.dao.SchoolDAO;
import cn.sedu.moon.dao.ShareNewsDAO;
import cn.sedu.moon.dao.ShareNewsLightDAO;
import cn.sedu.moon.dao.StuSchDAO;
import cn.sedu.moon.dao.StudentArchivesDAO;
import cn.sedu.moon.dao.UserCollectNewsDAO;
import cn.sedu.moon.dao.model.*;
import cn.sedu.moon.service.*;
import cn.sedu.moon.service.context.QueryNewsRecommendContext;
import cn.sedu.moon.service.context.QueryStickNewsContext;
import cn.sedu.moon.service.context.SearchNewsContext;
import cn.sedu.moon.service.context.StickNewsContext;
import com.alibaba.fastjson.JSON;
import com.github.pagehelper.ISelect;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.redisson.api.RBucket;
import org.redisson.api.RedissonClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * @author ${wukong}
 * @version $Id: NewsServiceImpl.java, v 0.1 2019年02月15日 12:05 PM Exp $
 */
@Service
@Slf4j
public class NewsServiceImpl implements NewsService {

    @Autowired
    private NewsDAO newsDAO;

    @Autowired
    private NewsDetailDAO newsDetailDAO;

    @Autowired
    private CmsUserDAO cmsUserDAO;

    @Autowired
    private StudentArchivesDAO archivesDAO;

    @Autowired
    private StuSchDAO stuSchDAO;

    @Autowired
    private SchoolDAO schoolDAO;

    @Autowired
    private RedissonClient redissonClient;

    @Autowired
    private NewsReadDAO newsReadDAO;

    @Autowired
    private ShareNewsDAO shareNewsDAO;

    @Autowired
    private DataCacheService cacheService;

    @Autowired
    private UserCollectNewsDAO userCollectNewsDAO;

    @Autowired
    private ShareNewsLightDAO lightDAO;

    @Autowired
    private ShareService shareService;

    @Autowired
    private LogService logService;

    @Resource
    private LogNewsDetailDAO logNewsDetailDAO;

    @Resource
    private NewsAskDAO newsAskDAO;

    @Resource
    private AskDAO askDAO;

    @Autowired
    private AskService askService;

    @Autowired
    private NewsNoticeService newsNoticeService;

    @Autowired
    private MessagePushService messagePushService;

    // 多线程服务
    private static ExecutorService executorService = new ThreadPoolExecutor(40, 100, 60L, TimeUnit.SECONDS, new LinkedBlockingQueue<>(100));

    @Override
    public SchoolNewsCheckDetailResponse checkDetail(final Long newsId) {

        News queryNewsModel = new News();
        queryNewsModel.setId(newsId);
        queryNewsModel.setDeleted(0);

        News news = newsDAO.getRecord(queryNewsModel);
        SchoolNewsCheckDetailResponse response = new SchoolNewsCheckDetailResponse();

        if (news == null) {
            throw new BizException(ErrorCodeEnum.SYS_EXP, "新闻不存在");
        }

        response.setCityCode(news.getCityCode());
        response.setThumbnail(news.getThumbnail());
        response.setBrief(news.getBrief());
        response.setType(news.getType());

        NewsDetail queryNewsDetailModel = new NewsDetail();
        queryNewsDetailModel.setNewsId(newsId);

        List<NewsDetail> newsDetailList = newsDetailDAO.listRecord(queryNewsDetailModel);
        if (newsDetailList != null && newsDetailList.size() > 0) {
            NewsDetail detail = newsDetailList.get(0);
            response.setTitle(detail.getTitle());
            response.setContent(detail.getContent());
            response.setId(detail.getId());

            // 查询news ask
            NewsAsk queryNewsAskModel = new NewsAsk();
            queryNewsAskModel.setNewsId(newsId);
            queryNewsAskModel.setDeleted(0);
            List<NewsAsk> newsAskList = newsAskDAO.listRecord(queryNewsAskModel);
            if (newsAskList != null && newsAskList.size() > 0) {
                StringBuilder stringBuilder = new StringBuilder();
                for (NewsAsk newsAsk : newsAskList) {
                    stringBuilder.append("#");
                    stringBuilder.append(newsAsk.getAskId());
                }
                response.setAskIdListString(stringBuilder.toString().substring(1));
            }

            // 新闻通知信息
            NewsNotice newsNotice = newsNoticeService.getNoticeInfoByNewsId(newsId);
            if (newsNotice != null) {
                response.setHasNotice(true);

                NewsNoticeResponse noticeResponse = new NewsNoticeResponse();
                noticeResponse.setNoticeId(newsNotice.getId());
                noticeResponse.setNewsId(newsNotice.getNewsId());
                noticeResponse.setTitle(newsNotice.getTitle());
                noticeResponse.setNoticeHour(newsNotice.getNoticeHour());
                noticeResponse.setNoticeMin(newsNotice.getNoticeMin());
                noticeResponse.setNoticeDay(newsNotice.getNoticeDay());
                response.setNotice(noticeResponse);
            }

            return response;
        }

        return null;
    }

    @Override
    public SchoolNewsDetailResponse newsDetail(final Long newsId, final Long userId) {

        /// 埋点
        try {
            logService.logNewDetail(newsId, userId);
        } catch (Exception e) {
            log.error("logNewDetail -- error, userId: {}, newsId: {}", userId, newsId);
        }

        NewsDetail queryNewsDetailModel = new NewsDetail();
        queryNewsDetailModel.setNewsId(newsId);

        List<NewsDetail> newsDetailList = newsDetailDAO.listRecord(queryNewsDetailModel);

        if (newsDetailList == null || newsDetailList.size() == 0) {
            throw new BizException(ErrorCodeEnum.SYS_EXP, "新闻数据不存在");
        }

        NewsDetail detail = newsDetailList.get(0);
        SchoolNewsDetailResponse newsResponse = new SchoolNewsDetailResponse();

        newsResponse.setId(detail.getNewsId());
        newsResponse.setTitle(detail.getTitle());
        newsResponse.setContent(detail.getContent());

        // 通过news表查询
        News queryNewsModel = new News();
        queryNewsModel.setId(detail.getNewsId());
        News newsItem = newsDAO.getRecord(queryNewsModel);

        newsResponse.setAuthorName(newsItem.getAuthor());

        // 是否为需要点亮的文章
        if (newsItem.getLight() == 1) {
            newsResponse.setLight(true);
        }

        if (newsItem.getNewsCreateTime() != null) {
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
            String dateString = formatter.format(newsItem.getNewsCreateTime());
            newsResponse.setCreateTime(dateString);
        }

        // 新闻类型
        newsResponse.setType(newsItem.getType());

        // 判断是否需要分享
        CheckShareResponse checkShareResponse = shareService.checkShare(newsId, userId);
        newsResponse.setNeedShared(checkShareResponse.getIsNeedShare());
        newsResponse.setShareCount(checkShareResponse.getShareCount());
        newsResponse.setNeedShareTotal(checkShareResponse.getNeedShareTotal());

        // 是否已经收藏
        UserCollectNews queryCollectModel = new UserCollectNews();
        queryCollectModel.setUserId(userId);
        queryCollectModel.setNewsId(newsId);
        queryCollectModel.setDeleted(0);

        List<UserCollectNews> collectNewsList = userCollectNewsDAO.listRecord(queryCollectModel);
        if (collectNewsList != null && collectNewsList.size() > 0) {
            newsResponse.setCollect(true);
        } else {
            newsResponse.setCollect(false);
        }

        // 标记已读
        read(userId, newsId);

        // 创建分享id
        Long shareId = shareService.createShare(userId, newsId);
        newsResponse.setShareId(shareId);

        // 查询文章相关联的问题列表
        NewsAsk queryNewsAskModel = new NewsAsk();
        queryNewsAskModel.setNewsId(newsId);
        queryNewsAskModel.setDeleted(0);

        List<NewsAsk> newsAskList = newsAskDAO.listRecord(queryNewsAskModel);
        if (newsAskList != null && newsAskList.size() > 0) {

            List<AskResponse> askResponseList = new ArrayList<>();

            for (NewsAsk newsAsk : newsAskList) {
                Ask queryAskModel = new Ask();
                queryAskModel.setId(newsAsk.getAskId());
                queryAskModel.setDeleted(0);
                Ask ask = askDAO.getRecord(queryAskModel);
                if (ask != null) {
                    AskResponse askResponse = new AskResponse();
                    askResponse.setAnswerCount(askService.queryAskAnswerCount(ask.getId()));
                    askResponse.setFollowCount(askService.queryAskAttentionCount(ask.getId()));
                    askResponse.setTitle(ask.getTitle());
                    askResponse.setId(ask.getId());
                    askResponseList.add(askResponse);
                    newsResponse.setAskList(askResponseList);
                }
            }
        }

        // 新闻通知信息
        NewsNotice newsNotice = newsNoticeService.getNoticeInfoByNewsId(newsId);
        if (newsNotice != null) {
            newsResponse.setHasNotice(true);

            NewsNoticeResponse noticeResponse = new NewsNoticeResponse();
            noticeResponse.setNoticeId(newsNotice.getId());
            noticeResponse.setNewsId(newsNotice.getNewsId());
            noticeResponse.setTitle(newsNotice.getTitle());
            noticeResponse.setNoticeHour(newsNotice.getNoticeHour());
            noticeResponse.setNoticeMin(newsNotice.getNoticeMin());
            noticeResponse.setNoticeDay(newsNotice.getNoticeDay());
            newsResponse.setNewsNotice(noticeResponse);

            // 查询用户的订阅状态
            boolean checkResult = newsNoticeService.checkUserSubscribe(userId, newsId);
            newsResponse.setSubscribedNotice(checkResult);
        }

        delNewUserKey(userId);

        newsResponse.setShared(checkShareResponse.isShared());
        return newsResponse;
    }

    @Async
    private void delNewUserKey(Long userId) {
        // 阅读某一篇文章，将新用户注册完成的缓存删除
        String cacheKey = CacheKeyUtil.genNewUserArchiveMapKey();
        RBucket<Map<String,Date>> bucket = redissonClient.getBucket(cacheKey);
        if (bucket != null) {
            Map<String, Date> newUserMap = bucket.get();
            if (MapUtil.isEmpty(newUserMap)) {
                newUserMap = new HashMap<>();
            }
            if (newUserMap.containsKey(userId + "")) {
                newUserMap.remove(userId + "");
                bucket.set(newUserMap);
            }
        }
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void updateDetail(final NewsCheckRequest request) {

        // 先查询新闻详情是否存在
        NewsDetail queryModel = new NewsDetail();
        queryModel.setNewsId(request.getNewsId());

        List<NewsDetail> currentDetailList = newsDetailDAO.listRecord(queryModel);
        if (currentDetailList == null || currentDetailList.size() == 0) {
            // 插入
            AddNewsRequest addNewsRequest = new AddNewsRequest();
            addNewsRequest.setTitle(request.getTitle());
            addNewsRequest.setContent(processContent(request.getContent()));
            addNewsRequest.setUserId(request.getUserId());
            addNewsRequest.setLight(request.getLight());
            addNewsRequest.setType(request.getType());
            addNews(addNewsRequest);
            return;
        }

        // 如果存在，则更新
        NewsDetail updateModel = new NewsDetail();
        updateModel.setNewsId(request.getNewsId());
        updateModel.setTitle(request.getTitle());
        updateModel.setContent(processContent(request.getContent()));
        updateModel.setUpdateTime(new Date());

        int result = newsDetailDAO.updateByNewsId(updateModel);
        if (result == 0) {
            throw new BizException(ErrorCodeEnum.SYS_EXP, "更新新闻详情数据失败");
        }

        News updateNewsModel = new News();
        updateNewsModel.setId(request.getNewsId());
        updateNewsModel.setTitle(request.getTitle());
        updateNewsModel.setUpdateTime(new Date());
        updateNewsModel.setCityCode(request.getCityCode());
        updateNewsModel.setLight(request.getLight());

        if (request.getType() != null) {
            updateNewsModel.setType(request.getType());
        }

        if (StringUtils.isNotBlank(request.getBrief())) {
            updateNewsModel.setBrief(request.getBrief());
        } else {
            // 设置 第一段内容 作为简介
            updateNewsModel.setBrief(getBriefFromNewsContext(request.getContent()));
        }

        if (StringUtils.isNotBlank(request.getThumbnail())) {
            updateNewsModel.setThumbnail(request.getThumbnail());
        } else {
            // 设置第一张图片做为 首图
            updateNewsModel.setThumbnail(getThumnailFromNewsContext(request.getContent()));
        }

        CmsUser queryCmsUserModel = new CmsUser();
        queryCmsUserModel.setId(request.getUserId());
        CmsUser currentUser = cmsUserDAO.getRecord(queryCmsUserModel);
        updateNewsModel.setEditor(currentUser.getNickname());

        result = newsDAO.updateRecord(updateNewsModel);
        if (result == 0) {
            throw new BizException(ErrorCodeEnum.SYS_EXP, "更新新闻列表数据失败");
        }

        // 更新新闻问答关系表
        NewsAsk deleteNewsAskModel = new NewsAsk();
        deleteNewsAskModel.setNewsId(request.getNewsId());
        newsAskDAO.removeRecordByNewsId(deleteNewsAskModel);

        // 关联的新闻
        if (StringUtils.isNotBlank(request.getAskIdListString())) {
            String[] askIdList = request.getAskIdListString().split("#");
            if (askIdList.length > 0) {
                for (int i = 0; i < askIdList.length; i++) {
                    Long askId = Long.parseLong(askIdList[i]);
                    NewsAsk saveNewsAskModel = new NewsAsk();
                    saveNewsAskModel.setNewsId(request.getNewsId());
                    saveNewsAskModel.setAskId(askId);
                    saveNewsAskModel.setOperator(currentUser.getNickname());
                    saveNewsAskModel.setDeleted(0);
                    saveNewsAskModel.setCreateTime(new Date());
                    saveNewsAskModel.setUpdateTime(new Date());

                    int saveNewsAskResult = newsAskDAO.saveRecord(saveNewsAskModel);
                    if (saveNewsAskResult == 0) {
                        throw new BizException(ErrorCodeEnum.SYS_EXP, "保存新闻问单数据失败");
                    }
                }
            }
        }

        // 通知
        // 首先判断是否是要删除通知
        NewsNotice newsNotice = newsNoticeService.getNoticeInfoByNewsId(request.getNewsId());
        if (newsNotice != null && !request.isHasNotice()) {
            // 删除通知
            newsNoticeService.deleteNotice(newsNotice.getId());
        } else if (newsNotice != null) {
            // 编辑通知
            NewsNotice updateNoticeModel = new NewsNotice();
            updateNoticeModel.setId(newsNotice.getId());
            updateNoticeModel.setTitle(request.getNoticeTitle());
            updateNoticeModel.setNoticeHour(request.getNoticeHour());
            updateNoticeModel.setNoticeMin(request.getNoticeMin());
            updateNoticeModel.setNoticeDay(request.getNoticeDay());
            newsNoticeService.updateNotice(updateNoticeModel);
        } else if (newsNotice == null && request.isHasNotice()) {
            // 新增
            NewsNotice saveNoticeModel = new NewsNotice();
            saveNoticeModel.setTitle(request.getNoticeTitle());
            saveNoticeModel.setNoticeHour(request.getNoticeHour());
            saveNoticeModel.setNoticeMin(request.getNoticeMin());
            saveNoticeModel.setNewsId(request.getNewsId());
            saveNoticeModel.setNoticeDay(request.getNoticeDay());
            newsNoticeService.addNotice(saveNoticeModel);
        }
    }

    @Override
    public List<SchoolNewsResponse> searchNews(final SearchNewsContext context) {

        List<SchoolNewsResponse> responseList = new ArrayList<>();

        News queryModel = new News();
        queryModel.setDeleted(0);
        if (StringUtils.isNotBlank(context.getKey())) {
            queryModel.setSearchKey(context.getKey());
        }

        // 查询所在学校
        StudentArchives queryArchive = new StudentArchives();
        queryArchive.setUserId(context.getUserId());
        queryArchive.setDeleted(0);

        StudentArchives queryArchiveResult = archivesDAO.getRecord(queryArchive);
        if (queryArchiveResult == null) {
            throw new BizException(ErrorCodeEnum.USER_NOT_ARCHIVE);
        }

        queryModel.setCityCode(queryArchiveResult.getCityCode());

        Page<News> page = PageHelper.offsetPage(context.getOffset(), context.getPageSize()).doSelectPage(
                () -> newsDAO.search(queryModel));

        // 构造response
        if (page != null && page.size() > 0) {

            for (News news : page.getResult()) {
                SchoolNewsResponse schoolNewsResponse = new SchoolNewsResponse();
                schoolNewsResponse.setId(news.getId());
                schoolNewsResponse.setTitle(news.getTitle());
                schoolNewsResponse.setNewsType(IndexNewsTypeEnum.NEWS.getCode());

                if (news.getUpdateTime() != null) {
                    SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    String dateString = formatter.format(news.getUpdateTime());
                    schoolNewsResponse.setCreateTime(dateString);
                }

                schoolNewsResponse.setAuthorName(news.getAuthor());

                // 查询是否已读
                NewsRead queryNewsRead = new NewsRead();
                queryNewsRead.setNewsId(news.getId());
                queryNewsRead.setUserId(context.getUserId());
                queryNewsRead.setDeleted(0);
                NewsRead newsRead = newsReadDAO.getRecord(queryNewsRead);
                if (newsRead != null) {
                    schoolNewsResponse.setRead(true);
                } else {
                    schoolNewsResponse.setRead(false);
                }

                schoolNewsResponse.setThumbnail(news.getThumbnail());
                schoolNewsResponse.setBrief(news.getBrief());

                // 查询是否收藏
                UserCollectNews queryCollectModel = new UserCollectNews();
                queryCollectModel.setNewsId(news.getId());
                queryCollectModel.setUserId(context.getUserId());
                queryCollectModel.setDeleted(0);

                List<UserCollectNews> collectNewsList = userCollectNewsDAO.listRecord(queryCollectModel);
                if (collectNewsList != null && collectNewsList.size() > 0) {
                    schoolNewsResponse.setCollect(true);
                } else {
                    schoolNewsResponse.setCollect(false);
                }

                schoolNewsResponse.setLight(news.getLight() == 1);
                responseList.add(schoolNewsResponse);
            }
        }

        return responseList;
    }

    @Override
    public List<SchoolNewsCheckResponse> checkList(final CmsNewsQueryRequest request) {

        News queryModel = new News();
        queryModel.setDeleted(0);

        if (StringUtils.isNotBlank(request.getEditor()) && !request.getEditor().equals("all")) {
            queryModel.setEditor(request.getEditor());
        }

        if (StringUtils.isNotBlank(request.getSrc()) && !request.getSrc().equals("all")) {
            queryModel.setSrc(request.getSrc());
        }

        if (StringUtils.isNotBlank(request.getCityCode())) {
            queryModel.setCityCode(request.getCityCode());
        }

        if (StringUtils.isNotBlank(request.getSearchKey())) {
            queryModel.setSearchKey(request.getSearchKey());
        }

        List<SchoolNewsCheckResponse> responseList = new ArrayList<>();

        Page<News> page = PageHelper.offsetPage(request.getOffset(), request.getPageSize()).doSelectPage(
                () -> newsDAO.listRecord(queryModel));

        // 构造response
        if (page != null && page.size() > 0) {

            for (News news : page.getResult()) {
                SchoolNewsCheckResponse response = new SchoolNewsCheckResponse();
                response.setTitle(news.getTitle());
                response.setAuthorName(news.getAuthor());

                if (news.getNewsCreateTime() != null) {
                    SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
                    String dateString = formatter.format(news.getNewsCreateTime());
                    response.setCreateTime(dateString);
                }

                if (news.getUpdateTime() != null) {
                    SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    String dateString = formatter.format(news.getUpdateTime());
                    response.setUpdateTime(dateString);
                }

                if (news.getThumbnail() != null) {
                    List<String> imgList = new ArrayList<>();
                    imgList.add(news.getThumbnail());
                    response.setImgList(imgList);
                }
                response.setNewsType(IndexNewsTypeEnum.NEWS.getCode());
                response.setId(news.getId());

                response.setDetailUrl(news.getDetailUrl());
                response.setSrc(news.getSrc());
                response.setThumbnail(news.getThumbnail());
                response.setEditor(news.getEditor());
                response.setStick(news.getStick());

                // 每一个新闻查询阅读量
                LogNewsDetail queryReadModel = new LogNewsDetail();
                queryReadModel.setNewsId(news.getId());
                Integer readCount = logNewsDetailDAO.countRecord(queryReadModel);
                response.setReadCount(readCount == null ? 0 : readCount);

                // 每一个新闻的推送数量
                String newsNoticeCountKey = CacheKeyUtil.genNewsNoticeCountKey(news.getId());
                RBucket<Integer> newsNoticeCountBucket = redissonClient.getBucket(newsNoticeCountKey);
                Integer newsNoticeCount = newsNoticeCountBucket.get();
                if (newsNoticeCount == null){
                    newsNoticeCount = 0;
                }
                response.setNoticeCount(newsNoticeCount);

                // 每一条新闻的推送成功量
                String newsNoticeSuccessCountKey = CacheKeyUtil.genNewsNoticeSuccessCountKey(news.getId());
                RBucket<Integer> newsNoticeSuccessCountBucket = redissonClient.getBucket(newsNoticeSuccessCountKey);
                Integer newsNoticeSuccessCount = newsNoticeSuccessCountBucket.get();
                if (newsNoticeSuccessCount == null){
                    newsNoticeSuccessCount = 0;
                }
                response.setNoticeSuccessCount(newsNoticeSuccessCount);
                responseList.add(response);
            }
        }

        return responseList;
    }

    @Override
    public Integer checkListTotal(CmsNewsQueryRequest request) {

        News queryModel = new News();
        queryModel.setDeleted(0);

        if (StringUtils.isNotBlank(request.getEditor()) && !request.getEditor().equals("all")) {
            queryModel.setEditor(request.getEditor());
        }

        if (StringUtils.isNotBlank(request.getSrc()) && !request.getSrc().equals("all")) {
            queryModel.setSrc(request.getSrc());
        }

        if (StringUtils.isNotBlank(request.getCityCode())) {
            queryModel.setCityCode(request.getCityCode());
        }

        if (StringUtils.isNotBlank(request.getSearchKey())) {
            queryModel.setSearchKey(request.getSearchKey());
        }

        Integer count = newsDAO.countAll(queryModel);
        if (count == null) {
            return 0;
        }

        return count;
    }

    @Override
    public void deleteNews(final Long newsId) {

        News updateModel = new News();
        updateModel.setId(newsId);
        updateModel.setDeleted(1);

        int result = newsDAO.updateRecord(updateModel);
        if (result == 0) {
            throw new BizException(ErrorCodeEnum.SYS_EXP, "删除新闻失败");
        }
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void batchDeleteNews(final List<Long> idList) {
        if (idList != null && idList.size() > 0) {
            for (Long id : idList) {
                deleteNews(id);
            }
        }
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void addNews(final AddNewsRequest request) {

        // add to news table
        News saveModel = new News();
        saveModel.setTitle(request.getTitle());
        saveModel.setCreateTime(new Date());
        saveModel.setUpdateTime(new Date());
        saveModel.setDeleted(0);
        saveModel.setAuthor("小鹿上学");
        saveModel.setNewsCreateTime(new Date());
        saveModel.setThirdParty(0);
        saveModel.setSrc("sxvip");
        saveModel.setCityCode(request.getCityCode());
        saveModel.setStick(0);
        saveModel.setLight(request.getLight());
        saveModel.setType(request.getType());

        if (StringUtils.isNotBlank(request.getBrief())) {
            saveModel.setBrief(request.getBrief());
        } else {
            // 设置 第一段内容 作为简介
            saveModel.setBrief(getBriefFromNewsContext(request.getContent()));
        }

        if (StringUtils.isNotBlank(request.getThumbnail())) {
            saveModel.setThumbnail(request.getThumbnail());
        } else {
            // 设置第一张图片做为 首图
            saveModel.setThumbnail(getThumnailFromNewsContext(request.getContent()));
        }

        CmsUser queryCmsUserModel = new CmsUser();
        queryCmsUserModel.setId(request.getUserId());
        CmsUser currentUser = cmsUserDAO.getRecord(queryCmsUserModel);
        saveModel.setEditor(currentUser.getNickname());

        int result = newsDAO.saveRecord(saveModel);
        if (result == 0) {
            throw new BizException(ErrorCodeEnum.SYS_EXP, "保存新闻失败");
        }

        Long newsId = saveModel.getId();

        // add to news detail table
        NewsDetail newsDetail = new NewsDetail();
        newsDetail.setNewsId(newsId);
        newsDetail.setTitle(request.getTitle());
        newsDetail.setContent(processContent(request.getContent()));
        newsDetail.setUpdateTime(new Date());
        newsDetail.setCreateTime(new Date());
        newsDetail.setDeleted(0);
        newsDetail.setAuthor("小鹿上学");
        newsDetail.setNewsCreateTime(new Date());
        newsDetail.setSrc("sxvip");

        result = newsDetailDAO.saveRecord(newsDetail);
        if (result == 0) {
            throw new BizException(ErrorCodeEnum.SYS_EXP, "保存新闻详情失败");
        }

        // 关联的新闻
        if (StringUtils.isNotBlank(request.getAskIdListString())) {
            String[] askIdList = request.getAskIdListString().split("#");
            if (askIdList.length > 0) {
                for (int i = 0; i < askIdList.length; i++) {
                    Long askId = Long.parseLong(askIdList[i]);
                    NewsAsk saveNewsAskModel = new NewsAsk();
                    saveNewsAskModel.setNewsId(newsId);
                    saveNewsAskModel.setAskId(askId);
                    saveNewsAskModel.setOperator(currentUser.getNickname());
                    saveNewsAskModel.setDeleted(0);
                    saveNewsAskModel.setCreateTime(new Date());
                    saveNewsAskModel.setUpdateTime(new Date());

                    int saveNewsAskResult = newsAskDAO.saveRecord(saveNewsAskModel);
                    if (saveNewsAskResult == 0) {
                        throw new BizException(ErrorCodeEnum.SYS_EXP, "保存新闻问单数据失败");
                    }
                }
            }
        }

        // 添加通知
        if (request.isHasNotice()) {

            NewsNotice saveNoticeModel = new NewsNotice();
            saveNoticeModel.setTitle(request.getNoticeTitle());
            saveNoticeModel.setNoticeHour(request.getNoticeHour());
            saveNoticeModel.setNoticeMin(request.getNoticeMin());
            saveNoticeModel.setNoticeDay(request.getNoticeDay());
            saveNoticeModel.setPush(0);
            saveNoticeModel.setCreateTime(new Date());
            saveNoticeModel.setUpdateTime(new Date());

            saveNoticeModel.setNewsId(newsId);

            // 计算推送时间
            String hour = String.format("%03d", request.getNoticeHour());
            String min = String.format("%02d", request.getNoticeMin());
            String pushTimeString = request.getNoticeDay() + " " + hour + ":" +  min + ":00";

            SimpleDateFormat smt = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date pushTime = null;
            try {
                pushTime = smt.parse(pushTimeString);
            } catch (ParseException e) {
                log.error("pushTime 时间转换错误");
                throw new BizException(ErrorCodeEnum.SYS_EXP);
            }

            saveNoticeModel.setPushTime(pushTime);
            newsNoticeService.addNotice(saveNoticeModel);
        }

        // 通知想读该学校的用户
        executorService.submit(() -> noticeUser(saveModel));

    }

    /**
     * @param saveModel 通知想读该学校的用户有新文章
     */
    private void noticeUser(News saveModel) {
        //获取所有该标题设计的学校Id
        List<School> schoolList = schoolDAO.listNews(saveModel.getCityCode());
        List<School> schoolNeedList = new ArrayList<>();
        for (School school : schoolList) {
            // 先看是否有关键词
            if (StrUtil.isNotBlank(school.getSearchKey())) {
                String[] searchKeyList = school.getSearchKey().split(",");
                for(String searchKey : searchKeyList){
                    if (saveModel.getTitle().contains(searchKey)) {
                        schoolNeedList.add(school);
                        break;
                    }
                }
            }else {
                // 没有关键词 就用学校真名
                String rightName = getSchoolRealName(school);
                if (saveModel.getTitle().contains(rightName)) {
                    schoolNeedList.add(school);
                }
            }
        }
        //获取该news的推送数
        String newsNoticeCountKey = CacheKeyUtil.genNewsNoticeCountKey(saveModel.getId());
        RBucket<Integer> newsNoticeCountBucket = redissonClient.getBucket(newsNoticeCountKey);
        Integer newsNoticeCount = newsNoticeCountBucket.get();
        if (newsNoticeCount == null){
            newsNoticeCount = 0;
        }

        String newsNoticeSuccessCountKey = CacheKeyUtil.genNewsNoticeSuccessCountKey(saveModel.getId());
        RBucket<Integer> newsNoticeSuccessCountBucket = redissonClient.getBucket(newsNoticeSuccessCountKey);
        Integer newsNoticeSuccessCount = newsNoticeSuccessCountBucket.get();
        if (newsNoticeSuccessCount == null){
            newsNoticeSuccessCount = 0;
        }

        for (School school : schoolNeedList) {
            StuSch queryModel = new StuSch();
            queryModel.setSchoolId(school.getId());
            List<StuSch> stuSchList = stuSchDAO.listRecord(queryModel);
            for (StuSch stuSch : stuSchList) {
                //获取用户推送新文章等的天key（一天最多一次）
                String dayCacheKey = CacheKeyUtil.genUserNewNewsDayNoticeKey(stuSch.getUserId());
                RBucket<Integer> dayBucket = redissonClient.getBucket(dayCacheKey);
                if (dayBucket.get() != null) {
                    continue;
                }
                //获取用户推送新文章等的周key（七天最多三次）
                String weekCacheKey = CacheKeyUtil.genUserNewNewsWeekNoticeKey(stuSch.getUserId());
                RBucket<String> weekBucket = redissonClient.getBucket(weekCacheKey);
                String weekNoticeNumAndTime = weekBucket.get();
                Integer weekNoticeNum = null;
                if (StrUtil.isNotBlank(weekNoticeNumAndTime)) {
                    weekNoticeNum = Integer.valueOf(weekNoticeNumAndTime.split("~")[0]);
                    if (weekNoticeNum >= 3) {
                        continue;
                    }
                }
                if (messagePushService.pushNewNewsNotice(saveModel.getId(), saveModel.getTitle(), stuSch.getUserId(), school.getName())){
                    newsNoticeSuccessCount++;
                }
                newsNoticeCount++;
                dayBucket.set(1, 1, TimeUnit.DAYS);
                Date now = new Date();
                if (StrUtil.isNotBlank(weekNoticeNumAndTime)) {
                    Date weekNoticeTime = cn.hutool.core.date.DateUtil.parse(weekNoticeNumAndTime.split("~")[1], "yyyy-MM-dd-HH-mm-ss");
                    //after避免redis没有及时删除而导致的key错加
                    if (cn.hutool.core.date.DateUtil.offsetWeek(weekNoticeTime, 1).isAfter(now)) {
                        //计算创建时间到现在还有多少毫秒
                        long outTimeMs = cn.hutool.core.date.DateUtil.betweenMs(cn.hutool.core.date.DateUtil.offsetWeek(weekNoticeTime, 1), new Date());
                        weekNoticeNum++;
                        //weekNoticeTime一直保持第一个key创建的时间
                        weekBucket.set(weekNoticeNum + "~" + weekNoticeTime, outTimeMs, TimeUnit.MILLISECONDS);
                    } else {
                        weekBucket.set(1 + "~" + cn.hutool.core.date.DateUtil.format(now, "yyyy-MM-dd-HH-mm-ss"), 7 * 24 * 60 * 60 * 1000, TimeUnit.MILLISECONDS);
                    }
                } else {
                    weekBucket.set(1 + "~" + cn.hutool.core.date.DateUtil.format(now, "yyyy-MM-dd-HH-mm-ss"), 7 * 24 * 60 * 60 * 1000, TimeUnit.MILLISECONDS);
                }

            }
        }
        newsNoticeCountBucket.set(newsNoticeCount);
        newsNoticeSuccessCountBucket.set(newsNoticeSuccessCount);

    }

    private String processContent(String content) {
        String pattern = "width.*?px";
        content = content.replaceAll(pattern, " ");
        pattern = "width=\".*?\"";
        content = content.replaceAll(pattern, " ");
        pattern = "width:.*?;";
        content = content.replaceAll(pattern, " ");

        return content;
    }

    private String getThumnailFromNewsContext(String content) {

        Document doc = Jsoup.parse(content);

        // 缩略图
        Elements elements = doc.getElementsByTag("img");
        if (elements != null && elements.size() > 0) {
            Element element = elements.get(0);
            return element.attr("src");
        }

        return null;
    }

    private String getBriefFromNewsContext(String content) {

        Document doc = Jsoup.parse(content);

        // 简介
        String text = doc.text();
        if (StringUtils.isNotBlank(text) && text.length() > 60) {
            text = text.substring(0, 60);
        }

        return text;
    }

    @Override
    public List<SchoolNewsResponse> queryNews(final Long userId, final Integer offset, final Integer pageSize) {

        QueryNewsRecommendContext context = new QueryNewsRecommendContext();
        context.setUserId(userId);
        context.setOffset(offset);
        context.setPageSize(pageSize);

        // 因为传参没有传context，所以又获取了一遍学校list
        getUserChooseSchool(context);

        long startTime = System.currentTimeMillis();
        List<Long> idList = new ArrayList<>();
        long endTime = System.currentTimeMillis();
        log.info("getNewsIdList -- 耗费时间:" + (endTime - startTime));
        log.info("offset:"+offset);
        // 置顶
        // offset为0 说明是首页
        if (offset == 0) {
            // 首页先看看是否需要新人文章
            String oldUserListCacheKey = CacheKeyUtil.genOldUserListKey(context.getUserId());
            RBucket<List<Long>> oldUserListBucket = redissonClient.getBucket(oldUserListCacheKey);
            List<Long> oldUserIdList = oldUserListBucket.get();
            String newUserNewsReadKey = CacheKeyUtil.genNewUserNewsReadKey(context.getUserId());
            RBucket<Integer> newUserNewsReadBucket = redissonClient.getBucket(newUserNewsReadKey);
            if (oldUserIdList == null){
                oldUserIdList = new ArrayList<>();
                oldUserListBucket.set(oldUserIdList);
            }
            if (oldUserIdList.size() == 0 && newUserNewsReadBucket.get() == null) {
                idList.add(NewsConstants.NEW_USER_NEWS_ID);
                // 设置阅读次数
                newUserNewsReadBucket.set(1);
            } else {
                // oldUserIdList中不包含userId  说明是新用户 进行新用户文章推荐
                if (!oldUserIdList.contains(userId)) {
                    Integer newUserNewsReadNum = newUserNewsReadBucket.get();
                    idList.add(NewsConstants.NEW_USER_NEWS_ID);
                    // 如果newUserNewsReadNum不等于null 那么是第二次看首页  看完需要将用户设置到老用户list中  并将用户阅读数的key删除
                    if (newUserNewsReadNum != null) {
                        newUserNewsReadBucket.delete();
                        oldUserIdList.add(userId);
                        oldUserListBucket.set(oldUserIdList);
                    } else {
                        // 设置阅读次数
                        newUserNewsReadBucket.set(1);
                    }
                }
            }


            startTime = System.currentTimeMillis();

            QueryStickNewsContext queryStickNewsContext = new QueryStickNewsContext();
            queryStickNewsContext.setCityCode(context.getCityCode());
            // 没必要获取SchoolNewsCheckResponse  只用到置顶文章的id
//                List<SchoolNewsCheckResponse> stickList = queryStickNewsList(queryStickNewsContext);
            List<Long> stickIdList = queryStickNewsIdList(queryStickNewsContext);
            if (stickIdList.size() > 0) {
                // 第一页文章不需要包含其他文章  只要置顶的  且去除去重逻辑
                idList.addAll(stickIdList);
            }

            endTime = System.currentTimeMillis();
            log.info("获取置顶数据 -- 耗费时间:" + (endTime - startTime));
            TimeInterval timer = cn.hutool.core.date.DateUtil.timer();
            String cacheKey = CacheKeyUtil.genUserNewsKey(context.getUserId());
            RBucket<List<Long>> bucket = redissonClient.getBucket(cacheKey);
            if (bucket != null){
                bucket.delete();
            }
            executorService.submit(() -> getNewsIdList(context));
            log.info("获取用户推荐文章用时："+timer.interval());
        } else {

            idList = getNewsIdList(context);
        }

        // 查询新闻，构造response
        List<SchoolNewsResponse> schoolNewsResponseList = new ArrayList<>();

        for (Long newsId : idList) {
            News queryModel = new News();
            queryModel.setId(newsId);
            News news = newsDAO.getRecord(queryModel);
            if (news != null) {

                SchoolNewsResponse schoolNewsResponse = new SchoolNewsResponse();
                schoolNewsResponse.setId(news.getId());
                schoolNewsResponse.setTitle(news.getTitle());
                schoolNewsResponse.setNewsType(IndexNewsTypeEnum.NEWS.getCode());

                if (news.getUpdateTime() != null) {
                    SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    String dateString = formatter.format(news.getUpdateTime());
                    schoolNewsResponse.setCreateTime(dateString);
                }

                schoolNewsResponse.setAuthorName(news.getAuthor());

                // 查询是否已读
                NewsRead queryNewsRead = new NewsRead();
                queryNewsRead.setNewsId(news.getId());
                queryNewsRead.setUserId(userId);
                queryNewsRead.setDeleted(0);
                NewsRead newsRead = newsReadDAO.getRecord(queryNewsRead);
                if (newsRead != null) {
                    schoolNewsResponse.setRead(true);
                } else {
                    schoolNewsResponse.setRead(false);
                }

                schoolNewsResponse.setThumbnail(news.getThumbnail());
                schoolNewsResponse.setBrief(news.getBrief());


                // 查询是否收藏
                UserCollectNews queryCollectModel = new UserCollectNews();
                queryCollectModel.setNewsId(news.getId());
                queryCollectModel.setUserId(userId);
                queryCollectModel.setDeleted(0);

                List<UserCollectNews> collectNewsList = userCollectNewsDAO.listRecord(queryCollectModel);
                if (collectNewsList != null && collectNewsList.size() > 0) {
                    schoolNewsResponse.setCollect(true);
                } else {
                    schoolNewsResponse.setCollect(false);
                }

                schoolNewsResponse.setLight(news.getLight() == 1);
                // 设置tag
                List<String> tagList = new ArrayList<>();
                if (schoolNewsResponse.getId().equals(NewsConstants.NEW_USER_NEWS_ID)) {
                    tagList.add("新人必读");
                }
                if (news.getType().equals(NewsTypeEnum.SELECTED.getCode())) {
                    // 小编精选
                    tagList.add("小编精选");
                }
                if (news.getStick() == 1 && "system auto task".equals(news.getSticker())) {
                    tagList.add("今日最新");
                }

                //设置想读，在读标签
                List<Long> schoolIdList = context.getLikeSchoolIdList();
                if (!CollectionUtils.isEmpty(schoolIdList)){
                    List<School> schoolNameList = schoolDAO.getSchoolSimpleByIds(schoolIdList);
                    for (School school : schoolNameList) {
                        if (StrUtil.isNotBlank(school.getSearchKey())){
                            String[] searchKeyList = school.getSearchKey().split(",");
                            for(String searchKey : searchKeyList){
                                if (news.getTitle().contains(searchKey) && !tagList.contains("想读学校")) {
                                    tagList.add("想读学校");
                                    break;
                                }
                            }
                        } else {
                            if (StrUtil.isNotBlank(school.getName())) {
                                if (news.getTitle().contains(getSchoolRealName(school)) && !tagList.contains("想读学校")) {
                                    tagList.add("想读学校");
                                }
                            }
                        }
                    }
                }
                Long studentArchivesId = context.getCurrentSchoolId();
                if (studentArchivesId != null){
                    School school = schoolDAO.getSchoolSimpleById(studentArchivesId);
                    if (school != null){
                        if (StrUtil.isNotBlank(school.getSearchKey())){
                            String[] searchKeyList = school.getSearchKey().split(",");
                            for(String searchKey : searchKeyList){
                                if (news.getTitle().contains(searchKey) && !tagList.contains("在读学校")) {
                                    tagList.add("在读学校");
                                    break;
                                }
                            }
                        } else {
                            if (StrUtil.isNotBlank(school.getName())){
                                if (news.getTitle().contains(getSchoolRealName(school))) {
                                    tagList.add("在读学校");
                                }
                            }
                        }
                    }
                }
                schoolNewsResponse.setTagList(tagList);
                schoolNewsResponseList.add(schoolNewsResponse);
            }
        }

        return schoolNewsResponseList;

    }

    private List<Long> getNewsIdList(QueryNewsRecommendContext context) {

        // 去 redis 中查询是否有数据

        long startTime = System.currentTimeMillis();
        String cacheKey = CacheKeyUtil.genUserNewsKey(context.getUserId());
        RBucket<List<Long>> bucket = redissonClient.getBucket(cacheKey);
        long endTime = System.currentTimeMillis();
        log.info("getNewsIdList redisson 耗时:" + (endTime - startTime));


        if (bucket.get() == null) {
            List<Long> idList = new ArrayList<>();

//            // logicA
//            List<News> logicAList = newsLogicA(context);
//            if (logicAList.size() > 0) {
//                for (News news : logicAList) {
//                    idSet.add(news.getId());
//                    idList.add(news.getId());
//
//                    if (idList.size() >= 200) {
//                        break;
//                    }
//                }
//            }

            // 想读获取换取新逻辑
            List<List<Long>> logicAIdList = newsLogicAV2(context);


//            if (idList.size() < 200) {
//                // logicB
//                List<News> logicBList = newsLogicB(context);
//                if (logicBList != null && logicBList.size() > 0) {
//                    for (News news : logicBList) {
//                        if (!idSet.contains(news.getId())) {
//                            idSet.add(news.getId());
//                            idList.add(news.getId());
//
//                            if (idList.size() >= 200) {
//                                break;
//                            }
//
//                        }
//                    }
//                }
//            }
            List<Long> logicBList = newsLogicBV2(context);

            // logicC
//            if (idList.size() < 200) {
//                List<News> logicCList = newsLogicC(context);
//                if (logicCList != null && logicCList.size() > 0) {
//                    for (News news : logicCList) {
//                        if (!idSet.contains(news.getId())) {
//                            idSet.add(news.getId());
//                            idList.add(news.getId());
//
//                            if (idList.size() >= 200) {
//                                break;
//                            }
//
//                        }
//                    }
//                }
//            }
            List<Long> logicCList = newsLogicCV2(context);
            // 获取所有用户已读  将这些数据删除
            List<Long> newsIdReadList = newsReadDAO.listRecordIdByUserId(context.getUserId());
            // 去重
            for (List<Long> likeSchoolNewsList : logicAIdList){
                logicCList.removeAll(likeSchoolNewsList);
                likeSchoolNewsList.removeAll(newsIdReadList);
            }
            logicBList.removeAll(newsIdReadList);
            logicCList.removeAll(logicBList);
            logicCList.removeAll(newsIdReadList);

            // 填充200条缓存数据
            while (idList.size() <= 200){
                boolean currentSchoolNewsFlag = false;
                boolean otherNewsFlag = false;
                for (List<Long> likeSchoolNewsList : logicAIdList){
                    Iterator<Long> iter = likeSchoolNewsList.iterator();
                    if (iter.hasNext()){
                        idList.add(iter.next());
                        iter.remove();
                    }
                    if (iter.hasNext()){
                        idList.add(iter.next());
                        iter.remove();
                    }
                }
                Iterator<Long> currentSchoolIter = logicBList.iterator();
                if (currentSchoolIter.hasNext()){
                    idList.add(currentSchoolIter.next());
                    currentSchoolIter.remove();
                } else {
                    currentSchoolNewsFlag = true;
                }
                Iterator<Long> otherIter = logicCList.iterator();
                if (otherIter.hasNext()){
                    idList.add(otherIter.next());
                    otherIter.remove();
                }
                if (otherIter.hasNext()){
                    idList.add(otherIter.next());
                    otherIter.remove();
                } else {
                    otherNewsFlag = true;
                }
                if (currentSchoolNewsFlag && otherNewsFlag){
                    break;
                }

            }

            idList.remove(NewsConstants.NEW_USER_NEWS_ID);
            bucket.set(idList, 1440, TimeUnit.MINUTES);

            int bucketSize = idList.size();

            int startIndex = context.getOffset()-1;
            int endIndex = startIndex + context.getPageSize();

            // 临界值判断
            if (startIndex > bucketSize - 1) {
                return null;
            }

            if (endIndex > bucketSize) {
                endIndex = bucketSize - 1;
            }

            return new ArrayList<>(idList.subList(startIndex, endIndex));

        } else {

            int bucketSize = bucket.get().size();

            startTime = System.currentTimeMillis();
            List<Long> idList = new ArrayList<>(context.getPageSize());

            int startIndex = context.getOffset();
            int endIndex = context.getOffset() + context.getPageSize();

            // 临界值判断
            if (startIndex > bucketSize - 1) {
                return null;
            }

            if (endIndex > bucketSize - 1) {
                endIndex = bucketSize - 1;
            }

            idList.addAll(bucket.get().subList(startIndex, endIndex));
            endTime = System.currentTimeMillis();
            log.info("getNewsIdList get id list 耗时:" + (endTime - startTime));

            return idList;
        }
    }

    private List<List<Long>> newsLogicAV2(QueryNewsRecommendContext context) {
        // 1 优先公办再民办
        // 2 再按照距离由近及远  有待商榷
        // 3 时间按照创建时间由新到旧

        // 推荐想读的学校内容
        List<List<Long>> finalNewsList = new ArrayList<>();
        if (context.getLikeSchoolIdList() != null && context.getLikeSchoolIdList().size() > 0) {

            List<School> schoolList = schoolDAO.getSchoolByIds(context.getLikeSchoolIdList());
            List<School> schoolListOrderBy = new ArrayList<>();
            if (schoolList != null){
                Iterator<School> iterator = schoolList.iterator();
                while (iterator.hasNext()){
                    School school = iterator.next();
                    if (school.getProperty() != null && school.getProperty() == 1) {
                        schoolListOrderBy.add(school);
                        iterator.remove();
                    }
                }
                // 2、当用户提问（3）、想问（2）、分享（1）操作中涉及到学校名字，则将对该学校进行分权重加权，在后续文章推荐中，增加该学校曝光
                if (schoolListOrderBy.size() != 0){
                    sortSchoolByCache(context.getUserId(), schoolListOrderBy);
                }
                if (schoolList.size() != 0){
                    sortSchoolByCache(context.getUserId(), schoolList);
                }
                schoolListOrderBy.addAll(schoolList);
            }

            List<String> schoolTitleList = new ArrayList<>();
            if (schoolListOrderBy.size() != 0) {
                for (School school : schoolListOrderBy) {

                    // 优先根据searchKey进行查询
                    String searchKey = school.getSearchKey();
                    if (StringUtils.isNotBlank(searchKey)) {
                        String[] searchKeyList = searchKey.split(",");
                        schoolTitleList.addAll(Arrays.asList(searchKeyList));
                    } else {
                        // 没有searchKey，再根据名称搜索
                        String schoolName = getSchoolRealName(school);
                        log.info("keyTitle --- is " + schoolName);
                        schoolTitleList.add(schoolName);
                    }
                    List<Long> newsIdList = newsDAO.listRecordIdBySchoolNameList(schoolTitleList, context.getCityCode());
                    if (newsIdList.size() != 0){
                        finalNewsList.add(newsIdList);
                    }
                }
            }
        }
        return finalNewsList;
    }

    private void sortSchoolByCache(Long userId, List<School> schoolListOrderBy) {
        //公办学校按照用户提问（3）、想问（2）、分享（1）排序
        //提问优先
        String askPublishCacheKey = CacheKeyUtil.genUserAskPublishKey(userId);
        RBucket<List<Long>> askPublishCacheKeyBucket = redissonClient.getBucket(askPublishCacheKey);
        List<Long> askPublishList = askPublishCacheKeyBucket.get();
        if (askPublishList == null){
            askPublishList = new ArrayList<>();
        }
        List<School> schoolListOrderByPublish = new ArrayList<>();
        Iterator<School> publishIterator = schoolListOrderBy.iterator();
        while (publishIterator.hasNext()){
            School school = publishIterator.next();
            if (askPublishList.contains(school.getId())){
                schoolListOrderByPublish.add(school);
                publishIterator.remove();
            }
        }
        //想问
        String askFollowCacheKey = CacheKeyUtil.genUserAskFollowKey(userId);
        RBucket<List<Long>> askFollowBucket = redissonClient.getBucket(askFollowCacheKey);
        List<Long> askFollowList = askFollowBucket.get();
        if (askFollowList == null){
            askFollowList = new ArrayList<>();
        }
        List<School> schoolListOrderByFollow = new ArrayList<>();
        Iterator<School> followIterator = schoolListOrderBy.iterator();
        while (followIterator.hasNext()){
            School school = followIterator.next();
            if (askFollowList.contains(school.getId()) && !askPublishList.contains(school.getId())){
                schoolListOrderByFollow.add(school);
                followIterator.remove();
            }
        }
        //分享
        String shareCacheKey = CacheKeyUtil.genUserShareKey(userId);
        RBucket<List<Long>> shareBucket = redissonClient.getBucket(shareCacheKey);
        List<Long> shareList = shareBucket.get();
        if (shareList == null){
            shareList = new ArrayList<>();
        }
        List<School> schoolListOrderByShare = new ArrayList<>();
        Iterator<School> shareIterator = schoolListOrderBy.iterator();
        while (shareIterator.hasNext()){
            School school = shareIterator.next();
            if (shareList.contains(school.getId()) && !askPublishList.contains(school.getId()) && !askFollowList.contains(school.getId())){
                schoolListOrderByShare.add(school);
                shareIterator.remove();
            }
        }
        schoolListOrderByPublish.addAll(schoolListOrderByFollow);
        schoolListOrderByPublish.addAll(schoolListOrderByShare);
        schoolListOrderByPublish.addAll(schoolListOrderBy);
        schoolListOrderBy.clear();
        schoolListOrderBy.addAll(schoolListOrderByPublish);
    }

    private List<News> newsLogicA(QueryNewsRecommendContext context) {

        // todo 根据【想读】的学校，推荐文章标题或内容里包含该学校的文章，每个学校3篇 + 1篇标题里面不包含学校的【小鹿说】文章，然后间隔展示
        // 如（A3 + 1, B3 + 1, C3 + 1）

        // 1 优先公办再民办
        // 2 再按照距离由近及远
        // 3 时间按照创建时间由新到旧
        // 4 作者优先"小鹿上学"  新需求取消了这一点

        // 推荐想读的学校内容
        List<News> finalNewsList = new ArrayList<>();
        if (context.getLikeSchoolIdList() != null && context.getLikeSchoolIdList().size() > 0) {

            List<School> schoolList = schoolDAO.getSchoolByIds(context.getLikeSchoolIdList());
            List<String> schoolTitleList = new ArrayList<>();
            if (schoolList.size() != 0) {
                for (School school : schoolList) {

                    // 优先根据searchKey进行查询
                    String searchKey = school.getSearchKey();
                    if (StringUtils.isNotBlank(searchKey)) {
                        String[] searchKeyList = searchKey.split(",");
                        schoolTitleList.addAll(Arrays.asList(searchKeyList));

                    } else {
                        // 没有searchKey，再根据名称搜索

                        String schoolName = school.getName();

                        // 去掉括号数据
                        String[] subSchoolNames = schoolName.split("（");
                        String[] subs = subSchoolNames[0].split("\\(");

                        String keyTitle = subs[0];
                        // 去掉杭州，或者杭州市
                        if (keyTitle.contains("杭州市")) {
                            subs = keyTitle.split("杭州市");

                            if (subs.length > 1) {
                                keyTitle = subs[1];
                            }
                        }

                        if (keyTitle.contains("杭州")) {
                            subs = keyTitle.split("杭州");

                            if (subs.length > 1) {
                                keyTitle = subs[1];
                            }
                        }

                        // 去掉北京，或者北京市
                        if (keyTitle.contains("北京市")) {
                            subs = keyTitle.split("北京市");

                            if (subs.length > 1) {
                                keyTitle = subs[1];
                            }
                        }

                        if (keyTitle.contains("北京")) {
                            subs = keyTitle.split("北京");

                            if (subs.length > 1) {
                                keyTitle = subs[1];
                            }
                        }

                        log.info("keyTitle --- is " + keyTitle);
                        schoolTitleList.add(keyTitle);
                    }

                    // 查询新闻列表 (按照更新时间倒叙返回)
                    // 既然是获取page的result，为什么还要用PageHelper  黑人问号脸。。
                    Page<News> page = PageHelper.offsetPage(0, 3).doSelectPage(new ISelect() {
                        @Override
                        public void doSelect() {
                            newsDAO.listRecordBySchoolNameList(schoolTitleList, context.getCityCode());
                        }
                    });
                    if (page.size() > 0) {
                        finalNewsList.addAll(page.getResult());
                    }

                    // 查找小鹿说的文章 新需求取消了这一点
                    News queryXlsNewsModel = new News();
                    queryXlsNewsModel.setTitle("小鹿说");
                    queryXlsNewsModel.setCityCode(context.getCityCode());

                    queryXlsNewsModel.setDeleted(0);
                    Page<News> xlsNewsPage = PageHelper.offsetPage(0, 1).doSelectPage(new ISelect() {
                        @Override
                        public void doSelect() {
                            newsDAO.listRecord(queryXlsNewsModel);
                        }
                    });
                    if (xlsNewsPage.size() > 0) {
                        finalNewsList.addAll(xlsNewsPage.getResult());
                    }

                }
            }
        }


        return finalNewsList;
    }


    private List<News> newsLogicB(QueryNewsRecommendContext context) {

        // 推荐 文章标题 或者 正文中包含 目前正在读的学校名字的文章

        // 作者优先"小鹿上学"
        // 时间按照创建时间由新到旧

        List<News> finalNewsList = new ArrayList<>();

        Long currentSchoolId = context.getCurrentSchoolId();
        if (currentSchoolId == null) {
            log.info("目前正在读的学校信息为空---context is {}", JSON.toJSON(context));
            return finalNewsList;
        }

        // 查询学校名称
        School querySchoolModel = new School();
        querySchoolModel.setId(context.getCurrentSchoolId());
        List<School> schoolList = schoolDAO.listRecord(querySchoolModel);

        List<String> schoolTitleList = new ArrayList<>();
        if (schoolList != null && schoolList.size() > 0) {

            // 优先根据searchKey进行查询
            String searchKey = schoolList.get(0).getSearchKey();
            if (StringUtils.isNotBlank(searchKey)) {
                String[] searchKeyList = searchKey.split(",");
                schoolTitleList.addAll(Arrays.asList(searchKeyList));

            } else {
                // 没有searchKey，再根据名称搜索

                String schoolName = schoolList.get(0).getName();

                // 去掉括号数据
                String[] subSchoolNames = schoolName.split("（");
                String[] subs = subSchoolNames[0].split("\\(");

                String keyTitle = subs[0];
                // 去掉杭州，或者杭州市
                if (keyTitle.contains("杭州市")) {
                    subs = keyTitle.split("杭州市");

                    if (subs.length > 1) {
                        keyTitle = subs[1];
                    }
                }

                if (keyTitle.contains("杭州")) {
                    subs = keyTitle.split("杭州");

                    if (subs.length > 1) {
                        keyTitle = subs[1];
                    }
                }

                // 去掉北京，或者北京市
                if (keyTitle.contains("北京市")) {
                    subs = keyTitle.split("北京市");

                    if (subs.length > 1) {
                        keyTitle = subs[1];
                    }
                }

                if (keyTitle.contains("北京")) {
                    subs = keyTitle.split("北京");

                    if (subs.length > 1) {
                        keyTitle = subs[1];
                    }
                }

                log.info("keyTitle --- is " + keyTitle);
                schoolTitleList.add(keyTitle);
            }

            // 查询新闻列表 (按照更新时间倒叙返回)
            Page<News> page = PageHelper.offsetPage(0, 1000).doSelectPage(new ISelect() {
                @Override
                public void doSelect() {
                    newsDAO.listRecordBySchoolNameList(schoolTitleList, context.getCityCode());
                }
            });

            if (page.size() > 0) {
                finalNewsList.addAll(page.getResult());
            }
        }

        return finalNewsList;
    }

    private List<Long> newsLogicBV2(QueryNewsRecommendContext context) {

        // 推荐 文章标题 或者 正文中包含 目前正在读的学校名字的文章

        // 作者优先"小鹿上学"
        // 时间按照创建时间由新到旧

        List<Long> finalNewsList = new ArrayList<>();

        Long currentSchoolId = context.getCurrentSchoolId();
        if (currentSchoolId == null) {
            log.info("目前正在读的学校信息为空---context is {}", JSON.toJSON(context));
            return finalNewsList;
        }

        // 查询学校名称
        School querySchoolModel = new School();
        querySchoolModel.setId(context.getCurrentSchoolId());
        List<School> schoolList = schoolDAO.listRecord(querySchoolModel);

        List<String> schoolTitleList = new ArrayList<>();
        if (schoolList != null && schoolList.size() > 0) {

            // 优先根据searchKey进行查询
            String searchKey = schoolList.get(0).getSearchKey();
            if (StringUtils.isNotBlank(searchKey)) {
                String[] searchKeyList = searchKey.split(",");
                schoolTitleList.addAll(Arrays.asList(searchKeyList));

            } else {
                // 没有searchKey，再根据名称搜索
                String schoolName = getSchoolRealName(schoolList.get(0));
                log.info("keyTitle --- is " + schoolName);
                schoolTitleList.add(schoolName);
            }

            // 查询新闻列表 (按照更新时间倒叙返回)
            return newsDAO.listRecordIdBySchoolNameList(schoolTitleList, context.getCityCode());
        }

        return finalNewsList;
    }

    private List<News> newsLogicC(QueryNewsRecommendContext context) {

        // 置顶的文章, 不参与排序规则，直接由后台运营操作


        // 推荐所在区 对应阶段 的学校内容 各三篇
        String areaName = context.getArchives().getAreaName();
        Long gradeId = context.getArchives().getGradeId();

        if (areaName != null && gradeId != null) {

            List<String> keywordList = new ArrayList<>();
            keywordList.add(areaName);

            if (gradeId < 4) {
                keywordList.add("小学");
            } else if (gradeId < 10) {
                keywordList.add("中学");
            }

            return newsDAO.listRecordByKeywordsList(keywordList, context.getCityCode());
        }

        return new ArrayList<>();

    }

    private List<Long> newsLogicCV2(QueryNewsRecommendContext context) {

        ArrayList<Long> idList = new ArrayList<>();
        //公办学校按照用户提问（3）、想问（2）、分享（1）排序
        //提问优先
        Long userId = context.getUserId();
        String askPublishCacheKey = CacheKeyUtil.genUserAskPublishKey(userId);
        RBucket<List<Long>> askPublishCacheKeyBucket = redissonClient.getBucket(askPublishCacheKey);
        List<Long> askPublishList = askPublishCacheKeyBucket.get();
        if (askPublishList!= null && askPublishList.size() != 0){
            List<String> schoolNames = schoolDAO.getSchoolNameByIds(askPublishList);
            idList.addAll(newsDAO.listRecordIdByKeywordsList(schoolNames, context.getCityCode()));
        }
        //想问
        String askFollowCacheKey = CacheKeyUtil.genUserAskFollowKey(userId);
        RBucket<List<Long>> askFollowBucket = redissonClient.getBucket(askFollowCacheKey);
        List<Long> askFollowList = askFollowBucket.get();
        if (askFollowList != null && askFollowList.size() != 0){
            if (!CollectionUtils.isEmpty(askPublishList)){
                askFollowList.removeAll(askPublishList);
            }
            List<String> schoolNames = schoolDAO.getSchoolNameByIds(askFollowList);
            idList.addAll(newsDAO.listRecordIdByKeywordsList(schoolNames, context.getCityCode()));
        }
        //分享
        String shareCacheKey = CacheKeyUtil.genUserShareKey(userId);
        RBucket<List<Long>> shareBucket = redissonClient.getBucket(shareCacheKey);
        List<Long> shareList = shareBucket.get();
        if (shareList != null && shareList.size() != 0){
            if (!CollectionUtils.isEmpty(askFollowList)){
                shareList.removeAll(askFollowList);
            }
            if (!CollectionUtils.isEmpty(askPublishList)){
                shareList.removeAll(askPublishList);
            }
            List<String> schoolNames = schoolDAO.getSchoolNameByIds(shareList);
            idList.addAll(newsDAO.listRecordIdByKeywordsList(schoolNames, context.getCityCode()));
        }
        // 推荐所在区 对应阶段 的学校内容 各三篇
        String areaName = context.getArchives().getAreaName();
        Long gradeId = context.getArchives().getGradeId();


        if (areaName != null && gradeId != null) {

            List<String> keywordList = new ArrayList<>();
            keywordList.add(areaName);

            if (gradeId < 4) {
                keywordList.add("小学");
            } else if (gradeId < 10) {
                keywordList.add("中学");
            }

            List<Long> otherIdList = newsDAO.listRecordIdByKeywordsList(keywordList, context.getCityCode());
            otherIdList.removeAll(idList);
            idList.addAll(otherIdList);
        }

        return idList;

    }

    /**
     * 获取用户选择的学校列表
     * 幼儿园用户 -> 意向的学校
     * 小学, 初中，高中 -> 所在的学校
     *
     * @param context 上下文
     * @return 学校列表
     */
    private List<StuSch> getUserChooseSchool(QueryNewsRecommendContext context) {

        StudentArchives queryArchive = new StudentArchives();
        queryArchive.setUserId(context.getUserId());
        queryArchive.setDeleted(0);

        StudentArchives queryArchiveResult = archivesDAO.getRecord(queryArchive);
        if (queryArchiveResult == null) {
            throw new BizException(ErrorCodeEnum.USER_NOT_ARCHIVE);
        }

        context.setArchives(queryArchiveResult);

        // 设置城市编码
        context.setCityCode(queryArchiveResult.getCityCode());

//        Long gradeId = queryArchiveResult.getGradeId();

        List<StuSch> stuSchList = new ArrayList<>();

        // 匹配sop问题使用用户目前所在的学校.
        context.setCurrentSchoolId(queryArchiveResult.getSchoolId());

        // 学生意向的学校
        // 由于首页文章想读 在读不再区分  所以去掉这块逻辑
//        if (gradeId >= 4 && gradeId <= 9) {
//
//            // 非幼儿园用户, 设置为目前所在的学校
//            StuSch stuSch = new StuSch();
//            stuSch.setSchoolId(queryArchiveResult.getSchoolId());
//            stuSchList.add(stuSch);
//
//        } else {
//      // 幼儿园用户, 获取学生意向学校列表
        StuSch queryModel = new StuSch();
        queryModel.setUserId(context.getUserId());
        stuSchList = stuSchDAO.listRecord(queryModel);
        ArrayList<Long> likeSchoolIdList = new ArrayList<>();
        for (StuSch stuSch : stuSchList) {
            likeSchoolIdList.add(stuSch.getSchoolId());
        }
        context.setLikeSchoolIdList(likeSchoolIdList);
//        }

        if (stuSchList == null || stuSchList.size() == 0) {
            log.info("用户上报的学校信息为空 context is {}", JSON.toJSONString(context));
            //throw new BizException(ErrorCodeEnum.SYS_EXP, "用户上报的学校信息为空");
        }

        context.setLikeSchoolList(stuSchList);

        return stuSchList;
    }

    @Override
    public void read(final Long userId, final Long newsId) {
        // 判断是否为新用户文章推荐
        if (NewsConstants.NEW_USER_NEWS_ID.equals(newsId)) {
            String oldUserListCacheKey = CacheKeyUtil.genOldUserListKey(userId);
            RBucket<List<Long>> oldUserListBucket = redissonClient.getBucket(oldUserListCacheKey);
            List<Long> oldUserIdList = oldUserListBucket.get();
            if (oldUserIdList == null) {
                oldUserIdList = new ArrayList<>();
                oldUserIdList.add(userId);
                oldUserListBucket.set(oldUserIdList);
            } else if (!oldUserIdList.contains(userId)) {
                oldUserIdList.add(userId);
                oldUserListBucket.set(oldUserIdList);
            }

            String newUserNewsReadKey = CacheKeyUtil.genNewUserNewsReadKey(userId);
            RBucket<Integer> newUserNewsReadBucket = redissonClient.getBucket(newUserNewsReadKey);
            if (newUserNewsReadBucket != null) {
                newUserNewsReadBucket.delete();
            }
        }

        // 先查询新闻是否存在
        News queryNewsModel = new News();
        queryNewsModel.setId(newsId);
        queryNewsModel.setDeleted(0);
        News news = newsDAO.getRecord(queryNewsModel);
        if (news == null) {
            log.error("新闻已读逻辑--新闻不存在: newsId:" + newsId);
            return;
//            throw new BizException(ErrorCodeEnum.SYS_EXP, "新闻不存在");
        }

        // 查询用户是否已读
        NewsRead queryNewsReadModel = new NewsRead();
        queryNewsReadModel.setUserId(userId);
        queryNewsReadModel.setNewsId(newsId);
        queryNewsReadModel.setDeleted(0);
        NewsRead newsRead = newsReadDAO.getRecord(queryNewsReadModel);
        if (newsRead == null) {
            // 保存数据
            NewsRead saveModel = new NewsRead();
            saveModel.setUserId(userId);
            saveModel.setNewsId(newsId);
            saveModel.setCreateTime(new Date());
            saveModel.setUpdateTime(new Date());
            saveModel.setDeleted(0);
            int result = newsReadDAO.saveRecord(saveModel);
            if (result == 0) {
                throw new BizException(ErrorCodeEnum.SYS_EXP, "保存失败");
            }
        }


    }

    @Override
    public void stickNews(StickNewsContext context) {

        News updateModel = new News();
        updateModel.setId(context.getNewsId());
        updateModel.setUpdateTime(new Date());
        updateModel.setStick(1);

        CmsUser queryCmsUserModel = new CmsUser();
        queryCmsUserModel.setId(context.getUserId());
        CmsUser currentUser = cmsUserDAO.getRecord(queryCmsUserModel);
        updateModel.setSticker(currentUser.getNickname());

        int result = newsDAO.updateRecord(updateModel);
        if (result == 0) {
            throw new BizException(ErrorCodeEnum.SYS_EXP, "置顶失败");
        }

    }

    @Async
    void reloadNewsData(Long userId) {
        cacheService.reloadNewsCache(userId);
        log.info("刷新用户新闻缓存数据完成 userId is " + userId);

    }


    /**
     * 获取学生意向学校所在城市的所有置顶文章
     * 包含详细信息
     *
     * @param context 上下文 其中只有意向城市的cityCode
     * @return
     */
    @Override
    public List<SchoolNewsCheckResponse> queryStickNewsList(final QueryStickNewsContext context) {

        News queryModel = new News();
        queryModel.setStick(1);
        queryModel.setCityCode(context.getCityCode());
        queryModel.setDeleted(0);

        List<SchoolNewsCheckResponse> responseList = new ArrayList<>();
        // 获取所在城市的所有置顶新闻
        List<News> list = newsDAO.listRecord(queryModel);
        if (list != null && list.size() > 0) {
            for (News news : list) {
                SchoolNewsCheckResponse response = new SchoolNewsCheckResponse();
                response.setTitle(news.getTitle());
                response.setAuthorName(news.getAuthor());

                if (news.getNewsCreateTime() != null) {
                    SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
                    String dateString = formatter.format(news.getNewsCreateTime());
                    response.setCreateTime(dateString);
                }

                if (news.getUpdateTime() != null) {
                    SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    String dateString = formatter.format(news.getUpdateTime());
                    response.setUpdateTime(dateString);
                }

                if (news.getThumbnail() != null) {
                    List<String> imgList = new ArrayList<>();
                    imgList.add(news.getThumbnail());
                    response.setImgList(imgList);
                }
                response.setNewsType(IndexNewsTypeEnum.NEWS.getCode());
                response.setId(news.getId());

                response.setDetailUrl(news.getDetailUrl());
                response.setSrc(news.getSrc());
                response.setThumbnail(news.getThumbnail());
                response.setEditor(news.getEditor());
                response.setSticker(news.getSticker());
                response.setStick(news.getStick());

                responseList.add(response);
            }
        }

        return responseList;
    }

    /**
     * 获取学生意向学校所在城市的所有置顶文章Id
     * 包含详细信息
     *
     * @param context 上下文 其中只有意向城市的cityCode
     * @return todo 我们展示人工置顶和系统置顶的时候  要区分城市吗？？
     */
    private List<Long> queryStickNewsIdList(final QueryStickNewsContext context) {
        // 获取当前城市的人工置顶新闻
        List<Long> manStick = newsDAO.listNewsIdManStick(context.getCityCode());
        // 获取当前城市的系统置顶新闻
        List<Long> systemStick = newsDAO.listNewsIdSystemStick(context.getCityCode());

        //将manStick放在前面 system放后面
        manStick.addAll(systemStick);

        return manStick;
    }

    @Override
    public void deleteStick(final StickNewsContext context) {

        News updateModel = new News();
        updateModel.setId(context.getNewsId());

        CmsUser queryCmsUserModel = new CmsUser();
        queryCmsUserModel.setId(context.getUserId());
        CmsUser currentUser = cmsUserDAO.getRecord(queryCmsUserModel);
        updateModel.setSticker(currentUser.getNickname());

        updateModel.setUpdateTime(new Date());
        updateModel.setStick(0);

        int result = newsDAO.updateRecord(updateModel);
        if (result == 0) {
            throw new BizException(ErrorCodeEnum.SYS_EXP, "取消置顶失败");
        }
    }

    @Override
    public void collect(final Long userId, final Long newsId) {

        // 查询是否已经收藏
        UserCollectNews queryModel = new UserCollectNews();
        queryModel.setNewsId(newsId);
        queryModel.setUserId(userId);
        queryModel.setDeleted(0);

        List<UserCollectNews> userCollectNewsList = userCollectNewsDAO.listRecord(queryModel);
        if (userCollectNewsList != null && userCollectNewsList.size() > 0) {
            throw new BizException(ErrorCodeEnum.SYS_EXP, "已收藏");
        }

        UserCollectNews saveModel = new UserCollectNews();
        saveModel.setUserId(userId);
        saveModel.setNewsId(newsId);
        saveModel.setCreateTime(new Date());
        saveModel.setUpdateTime(new Date());
        saveModel.setDeleted(0);

        int result = userCollectNewsDAO.saveRecord(saveModel);
        if (result == 0) {
            throw new BizException(ErrorCodeEnum.SYS_EXP, "收藏失败");
        }
    }

    @Override
    public SearchCollectNewsResponse searchCollect(final Long userId, final Integer offset, final Integer pageSize) {

        SearchCollectNewsResponse response = new SearchCollectNewsResponse();

        UserCollectNews queryModel = new UserCollectNews();
        queryModel.setUserId(userId);
        queryModel.setDeleted(0);

        Page<UserCollectNews> page = PageHelper.offsetPage(offset, pageSize).doSelectPage(new ISelect() {
            @Override
            public void doSelect() {
                userCollectNewsDAO.listRecord(queryModel);
            }
        });

        response.setTotal((int) page.getTotal());

        if (page.size() > 0) {
            List<SchoolNewsResponse> newsResponseList = new ArrayList<>();

            for (UserCollectNews collectNews : page.getResult()) {
                // 查询新闻详情
                News queryNewsModel = new News();
                queryNewsModel.setId(collectNews.getNewsId());
                queryNewsModel.setDeleted(0);
                News news = newsDAO.getRecord(queryNewsModel);
                if (news != null) {
                    SchoolNewsResponse newsResponse = new SchoolNewsResponse();
                    newsResponse.setTitle(news.getTitle());

                    SchoolNewsResponse schoolNewsResponse = new SchoolNewsResponse();
                    schoolNewsResponse.setId(news.getId());
                    schoolNewsResponse.setTitle(news.getTitle());
                    schoolNewsResponse.setNewsType(IndexNewsTypeEnum.NEWS.getCode());

                    if (news.getUpdateTime() != null) {
                        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                        String dateString = formatter.format(news.getUpdateTime());
                        schoolNewsResponse.setCreateTime(dateString);
                    }

                    schoolNewsResponse.setAuthorName(news.getAuthor());

                    // 查询是否已读
                    NewsRead queryNewsRead = new NewsRead();
                    queryNewsRead.setNewsId(news.getId());
                    queryNewsRead.setUserId(userId);
                    queryNewsRead.setDeleted(0);
                    NewsRead newsRead = newsReadDAO.getRecord(queryNewsRead);
                    if (newsRead != null) {
                        schoolNewsResponse.setRead(true);
                    } else {
                        schoolNewsResponse.setRead(false);
                    }

                    schoolNewsResponse.setThumbnail(news.getThumbnail());
                    schoolNewsResponse.setBrief(news.getBrief());

                    newsResponseList.add(schoolNewsResponse);
                }
            }

            response.setList(newsResponseList);
        }

        return response;
    }

    @Override
    public void deleteCollect(final Long userId, final Long newsId) {

        UserCollectNews deleteModel = new UserCollectNews();
        deleteModel.setUserId(userId);
        deleteModel.setNewsId(newsId);

        userCollectNewsDAO.logicDeleteByUserIdAndNewsId(deleteModel);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void stickLastDayCreatedNews() {

        newsDAO.removeSystemStick();

        // 计算昨日的时间字符串
        Date lastDay = DateUtil.plusDay(new Date(), -1);
        Date startTime = DateUtil.beginOfDay(lastDay);

        Calendar dateEnd = Calendar.getInstance();
        dateEnd.setTime(new Date());
        dateEnd.set(Calendar.HOUR_OF_DAY, 2);
        dateEnd.set(Calendar.MINUTE, 0);
        dateEnd.set(Calendar.SECOND, 0);
        Date endTime = dateEnd.getTime();

        log.info("stickLastDayCreatedNews, start");
        int result = newsDAO.stickNewsByDateDuration(startTime, endTime);
        log.info("stickLastDayCreatedNews, operation total:{}", result);
    }

    @Override
    public EditCensusResponse newsEditCensus(String cityCode) {
        Date end = new Date();
        Date start = DateUtil.plusDay(end, -30);
        // 新建文章
        Integer newNewsCount = newsDAO.getCreateNewsNumByDateDuration(start, end, cityCode);
        //暂时没法判断 原创文章
//        Integer originalNewsCount = newsDAO.getOriginalNewsNumByDateDuration(now,DateUtil.plusDay(now,-30));
        // 精编文章
        Integer editWellNewsCount = newsDAO.getEditWellNewsNumByDateDuration(start, end, cityCode);

        EditCensusResponse response = new EditCensusResponse();
        response.setNewNewsCount(newNewsCount);
        response.setEditWellNewsCount(editWellNewsCount);
        return response;
    }

    @Override
    public List<SchoolNewsResponse> weeklySelectedList(final Long userId) {

        // 从redis中读取
        String cachekey = CacheKeyUtil.genWeeklySelectedNewsKey(userId);
        RBucket<List<Long>> bucket = redissonClient.getBucket(cachekey);

        List<Long> newsIdList = new ArrayList<>();
        if (bucket.get() == null) {
            // build data
            List<Long> newsList = buildWeelySelectedNewsList(userId);
            bucket.set(newsList, 1440, TimeUnit.MINUTES);

        } else {
            //
            newsIdList = bucket.get();
        }

        // 查询新闻，构造response
        List<SchoolNewsResponse> schoolNewsResponseList = new ArrayList<>();

        for (Long newsId : newsIdList) {
            News queryModel = new News();
            queryModel.setId(newsId);
            News news = newsDAO.getRecord(queryModel);
            if (news != null) {

                SchoolNewsResponse schoolNewsResponse = new SchoolNewsResponse();
                schoolNewsResponse.setId(news.getId());
                schoolNewsResponse.setTitle(news.getTitle());
                schoolNewsResponse.setNewsType(IndexNewsTypeEnum.NEWS.getCode());

                if (news.getUpdateTime() != null) {
                    SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    String dateString = formatter.format(news.getUpdateTime());
                    schoolNewsResponse.setCreateTime(dateString);
                }

                schoolNewsResponse.setAuthorName(news.getAuthor());

                // 查询是否已读
                NewsRead queryNewsRead = new NewsRead();
                queryNewsRead.setNewsId(news.getId());
                queryNewsRead.setUserId(userId);
                queryNewsRead.setDeleted(0);
                NewsRead newsRead = newsReadDAO.getRecord(queryNewsRead);
                if (newsRead != null) {
                    schoolNewsResponse.setRead(true);
                } else {
                    schoolNewsResponse.setRead(false);
                }

                schoolNewsResponse.setThumbnail(news.getThumbnail());
                schoolNewsResponse.setBrief(news.getBrief());


                // 查询是否收藏
                UserCollectNews queryCollectModel = new UserCollectNews();
                queryCollectModel.setNewsId(news.getId());
                queryCollectModel.setUserId(userId);
                queryCollectModel.setDeleted(0);

                List<UserCollectNews> collectNewsList = userCollectNewsDAO.listRecord(queryCollectModel);
                if (collectNewsList != null && collectNewsList.size() > 0) {
                    schoolNewsResponse.setCollect(true);
                } else {
                    schoolNewsResponse.setCollect(false);
                }

                schoolNewsResponse.setLight(news.getLight() == 1);
                schoolNewsResponseList.add(schoolNewsResponse);
            }
        }

        return schoolNewsResponseList;
    }

    private List<Long> buildWeelySelectedNewsList(Long userId) {

        /**
         * 1》	时间最新一篇精编文章（运营会在后台设置精编文章）
         * 2》	文章标题或者文章正文包含用户想读学校的文章，时间由新到旧
         * 3》	文章标题或者文章正文包含用户在读学校的文章，时间最近2个月内由新到旧
         * 4》	不足3篇，则按照最近7天新建文章中，阅读数由多到少筛选补足
         */

        List<Long> newsIdList = new ArrayList<>();

        // 查询所在学校
        StudentArchives queryArchive = new StudentArchives();
        queryArchive.setUserId(userId);
        queryArchive.setDeleted(0);

        StudentArchives queryArchiveResult = archivesDAO.getRecord(queryArchive);
        if (queryArchiveResult == null || queryArchiveResult.getCityCode() == null) {
            throw new BizException(ErrorCodeEnum.USER_NOT_ARCHIVE);
        }

        String cityCode = queryArchiveResult.getCityCode();
        News queryNewsModel = new News();
        queryNewsModel.setCityCode(cityCode);
        queryNewsModel.setType(NewsTypeEnum.SELECTED.getCode());
        queryNewsModel.setDeleted(0);
        List<News> newsList = newsDAO.listRecord(queryNewsModel);
        if (CollectionUtils.isNotEmpty(newsList)) {
            newsIdList.add(newsList.get(0).getId());
        }

        return newsIdList;
    }

    /**
     * 获取学校除去省市区和括号的名字
     * @param school
     * @return
     */
    private String getSchoolRealName(School school) {
        String schoolName = school.getName();
        // 去掉括号数据
        String[] subSchoolNames = schoolName.split("（");
        String[] subs = subSchoolNames[0].split("\\(");
        schoolName = subs[0];

        //去掉省
        String province = school.getProvince();
        if (StrUtil.isNotBlank(province)){
            // 去掉省
            // 这一步去除的是浙江省
            if (schoolName.contains(province)) {
                subs = schoolName.split(province);

                if (subs.length > 1) {
                    schoolName = subs[1];
                }
            }
            // 这一步去掉的是浙江
//            province = province.substring(0, province.length()-1);
//            if (schoolName.contains(province)) {
//                subs = schoolName.split(province);
//
//                if (subs.length > 1) {
//                    schoolName = subs[1];
//                }
//            }
        }

        //去掉市
        String city = school.getCity();
        if (StrUtil.isNotBlank(city)){
            // 去掉省市区
            // 这一步去除的是杭州市
            if (schoolName.contains(city)) {
                subs = schoolName.split(city);

                if (subs.length > 1) {
                    schoolName = subs[1];
                }
            }
            // 这一步去掉的是杭州
//            city = city.substring(0, city.length()-1);
//            if (schoolName.contains(city)) {
//                subs = schoolName.split(city);
//
//                if (subs.length > 1) {
//                    schoolName = subs[1];
//                }
//            }
        }

        //去掉区
        String area = school.getArea();
        if (StrUtil.isNotBlank(area)){
            // 去掉省市区
            // 这一步去除的是滨江区
            if (schoolName.contains(area)) {
                subs = schoolName.split(area);

                if (subs.length > 1) {
                    schoolName = subs[1];
                }
            }
        }

        return schoolName;
    }
}