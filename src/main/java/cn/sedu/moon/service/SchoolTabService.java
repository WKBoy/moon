/*
 * Dian.com Inc.
 * Copyright (c) 2004-2019 All Rights Reserved.
 */
package cn.sedu.moon.service;

import cn.sedu.moon.controller.response.SchoolNewsResponse;
import cn.sedu.moon.controller.response.SchoolTabAskResponse;
import cn.sedu.moon.controller.response.school.SchoolCommentResponse;
import cn.sedu.moon.controller.response.school.SchoolTabResponse;
import cn.sedu.moon.service.context.AddSchoolCommentContext;
import cn.sedu.moon.service.context.QuerySchoolCommentContext;
import cn.sedu.moon.service.context.QuerySchoolNewsContext;
import cn.sedu.moon.service.context.SchoolTabSearchContext;

import java.util.List;

/**
 * 学校tab所提供的所有服务接口
 *
 * @author wukong
 * @version : SchoolTabService.java, v 0.1 2019年10月08日 2:08 PM Exp $
 */
public interface SchoolTabService {

    /**
     * 学校列表接口
     *
     * @param context 上下文
     * @return 学校信息列表
     */
    List<SchoolTabResponse> search(SchoolTabSearchContext context);

    /**
     * 查询学校的评论列表
     *
     * @param context 上下文
     * @return 评论列表
     */
    List<SchoolCommentResponse> commentList(QuerySchoolCommentContext context);

    /**
     * 添加学校的评论
     * @param context 上下文
     * @return 添加成功，返回评论id
     */
    Long addSchoolComment(AddSchoolCommentContext context);

    /**
     * 查询学校资讯列表
     *
     * @param context 上下文
     * @return 资讯列表
     */
    List<SchoolNewsResponse> newsList(QuerySchoolNewsContext context);

    List<SchoolTabAskResponse> askList(Long schoolId);

    /**
     * 查询学校详情
     *
     * @param schoolId 学校id
     * @return 学校数据
     */
    SchoolTabResponse schoolDetail(Long schoolId, Long userId);
}