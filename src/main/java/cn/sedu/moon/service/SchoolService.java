/*
 * Dian.com Inc.
 * Copyright (c) 2004-2018 All Rights Reserved.
 */
package cn.sedu.moon.service;

import cn.sedu.moon.controller.request.cms.school.CmsSchoolSearchRequest;
import cn.sedu.moon.controller.request.cms.school.CmsSchoolSetHotRequest;
import cn.sedu.moon.controller.request.school.UserAddSchoolRequest;
import cn.sedu.moon.controller.response.SchoolDetailResponse;
import cn.sedu.moon.controller.response.SchoolRecommendResponse;
import cn.sedu.moon.controller.response.SchoolResponse;
import cn.sedu.moon.controller.response.cms.school.CmsSearchSchoolResponse;
import cn.sedu.moon.controller.response.school.UserAddSchoolResponse;
import cn.sedu.moon.dao.model.School;
import cn.sedu.moon.service.context.QuerySchoolContext;
import cn.sedu.moon.service.context.SchoolAttentionContext;

import java.util.List;

/**
 * 学校相关的服务接口
 *
 * @author ${wukong}
 * @version $Id: SchoolService.java, v 0.1 2018年12月10日 11:42 AM Exp $
 */
public interface SchoolService {

    /**
     * 查找附近的学校
     *
     * @return 学校列表
     */
    List<SchoolResponse> nearBy(QuerySchoolContext context);

    /**
     * 根据用户ID获取推荐的学校列表
     *
     * @param context 上下文
     * @return 学校列表
     */
    List<SchoolRecommendResponse> recommendSchoolList(QuerySchoolContext context);

    /**
     * 查询不同类型推荐逻辑的学校列表
     *
     * @param context
     * @return
     */
    List<SchoolRecommendResponse> queryRecommendSchoolWithType(QuerySchoolContext context);

    /**
     * 获取用户关注的学校列表
     *
     * @param context 上下文
     * @return 学校列表
     */
    List<SchoolResponse> attentionSchoolList(QuerySchoolContext context);

    /**
     * 上报用户关注的学校
     *
     * @param context 上下文
     */
    void attentionSchoolReport(SchoolAttentionContext context);

    /**
     * 根据学校ID获取学校详情
     *
     * @param schoolId 学校ID
     * @return 学校详情
     */
    SchoolDetailResponse detail(Long schoolId);

    /**
     * 用户主动添加学校
     *
     * @param request request
     */
    UserAddSchoolResponse userAddSchool(UserAddSchoolRequest request);

    /**
     * 搜索学校
     *
     * @param request request
     * @return response
     */
    CmsSearchSchoolResponse cmsSearchSchool(CmsSchoolSearchRequest request);


    /**
     * 设置热门学校
     *
     * @param request request
     * @return response
     */
    void cmsSetHotSchool(CmsSchoolSetHotRequest request);


    List<School> querySchoolByNameKey(String nameKey, Integer offset, Integer pageSize);

    /**
     * 保存学校
     *
     * @param school 学校
     * @return 保存结果
     */
    int saveSchool(School school);

    /**
     * 获取学校除去省市区和括号的名字
     * @param school
     * @return
     */
    String getSchoolRealName(School school);
}