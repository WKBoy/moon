/*
 * Dian.com Inc.
 * Copyright (c) 2004-2019 All Rights Reserved.
 */
package cn.sedu.moon.service;

/**
 * 数据服务
 *
 * @author wukong
 * @version : DataService.java, v 0.1 2019年10月31日 9:19 下午 Exp $
 */
public interface DataService {

    void fetchGdData(String cityCode);

    void processHistoryPvUvData();

    void processGdProvinceCode();

    /**
     * 高德数据入主库
     */
    void processGdDataAsync();
}