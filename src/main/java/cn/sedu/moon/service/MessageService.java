/*
 * Dian.com Inc.
 * Copyright (c) 2004-2019 All Rights Reserved.
 */
package cn.sedu.moon.service;

import cn.sedu.moon.service.context.WxTemplateMessageContext;

/**
 *
 * @author ${wukong}
 * @version $Id: MessageService.java, v 0.1 2019年04月02日 11:17 AM Exp $
 */
public interface MessageService {

    /**
     * 保存微信formId
     *
     * @param userId 用户id
     * @param formId formId
     */
    void saveWxFormId(Long userId, String formId);

    /**
     * 获取用户的formId
     *
     * @param userId 用户id
     * @return formId
     */
    String getFormIdByUserId(Long userId);

    /**
     * 发送微信小程序模版消息
     */

    /**
     * 发送微信小程序模版消息
     *
     * @param context 上下文对象
     */
    boolean sendWechatAppTemplateMessage(WxTemplateMessageContext context);

}