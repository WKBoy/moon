/*
 * Dian.com Inc.
 * Copyright (c) 2004-2019 All Rights Reserved.
 */
package cn.sedu.moon.service;

import java.util.List;

import cn.sedu.moon.controller.response.SopReportAnswerResponse;
import cn.sedu.moon.controller.response.SopResponse;
import cn.sedu.moon.service.context.SopAnswerContext;

/**
 * sop 问题
 *
 * @author ${wukong}
 * @version $Id: SopService.java, v 0.1 2019年01月09日 11:55 AM Exp $
 */
public interface SopService {

    SopResponse getSop(Long schoolId, Integer refId, Long userId);

    void attention(final Integer sopRefId, final Long userId);

    /**
     * 查询用户回答过的sop 问题列表
     *
     * @param userId 用户id
     * @param schoolId 学校id
     * @return 问题 refId 列表
     */
    List<Integer> getSubjectIdListByUserIdAndSchoolId(Long userId, Long schoolId);

    /**
     * 用户作答SOP问题
     *
     * @param context 用户提交的答案
     */
    SopReportAnswerResponse reportSopAnswer(SopAnswerContext context);

    /**
     * 获取未回答的SOP问题
     * @param userId
     * @param schoolId
     * @return
     */
    SopResponse getSchoolSopUnAnswer(Long userId, Long schoolId);

    /**
     * 获取学校Tag
     * @param schoolId
     * @param subjectRefId
     * @return
     */
    String getSopTag(Long schoolId, Integer subjectRefId);
}