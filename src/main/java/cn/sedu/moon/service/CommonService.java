/*
 * Dian.com Inc.
 * Copyright (c) 2004-2018 All Rights Reserved.
 */
package cn.sedu.moon.service;

import java.util.List;

import cn.sedu.moon.controller.response.AreaNode;

/**
 *
 * @author ${wukong}
 * @version $Id: CommonService.java, v 0.1 2018年11月12日 5:05 PM Exp $
 */
public interface CommonService {

    List<AreaNode> queryAreaInfo(Integer areaLevel, String parentCode);

    String uploadImage(String originalFilename, byte[] imageBytes, String contentType, long size);

}