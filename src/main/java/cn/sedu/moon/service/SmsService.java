/*
 * Dian.com Inc.
 * Copyright (c) 2004-2018 All Rights Reserved.
 */
package cn.sedu.moon.service;

/**
 * 短信息服务
 *
 * @author ${wukong}
 * @version $Id: SmsService.java, v 0.1 2018年12月10日 3:04 PM Exp $
 */
public interface SmsService {

    /**
     * 发送短信验证码
     *
     * @param mobile 手机号码
     */
    void sendToken(String mobile);

    /**
     * 验证短信验证码
     *
     * @param mobile 手机号码
     * @param smsCode 短信验证码
     * @return 验证结果
     */
    Boolean checkSmsCode(String mobile, String smsCode);

}