/*
 * Dian.com Inc.
 * Copyright (c) 2004-2018 All Rights Reserved.
 */
package cn.sedu.moon.service;

import cn.sedu.moon.controller.request.UserMobileReportRequest;
import cn.sedu.moon.controller.request.cms.user.CmsSearchUserRequest;
import cn.sedu.moon.controller.response.UserLoginResponse;
import cn.sedu.moon.controller.response.cms.user.CmsUserSearchResponse;
import cn.sedu.moon.controller.response.cms.user.CmsUserUvResponse;
import cn.sedu.moon.dao.model.User;
import cn.sedu.moon.service.context.UserBaseInfoContext;
import cn.sedu.moon.service.context.UserLoginContext;

/**
 * 用户服务
 *
 * @author ${wukong}
 * @version $Id: UserService.java, v 0.1 2018年12月10日 2:38 PM Exp $
 */
public interface UserService {

    /**
     * 用户登陆
     *
     * @param context 上下文
     * @return 登陆成功后返回token，否则抛出异常
     */
    UserLoginResponse login(UserLoginContext context);

    void wxAuth(UserLoginContext context);

    UserLoginResponse tempLogin(UserLoginContext context);

    void reportMobile(UserMobileReportRequest request);

    User getUserById(Long userId);

    void reportBaseInfo(UserBaseInfoContext context);

    /**
     * cms 后台搜索用户
     *
     * @param request request
     * @return response
     */
    CmsUserSearchResponse cmsSearch(CmsSearchUserRequest request);

    /**
     * 统计uv信息
     * @return
     */
    CmsUserUvResponse uv();

    /**
     * 推送一小时还未注册完成的用户
     */
    void pushOneHourNotArchiveUser();

    /**
     * 推送注册完成一小时还未阅读过新闻的用户
     */
    void pushOneHourArchiveUserNotReadNews();
}