/*
 * Dian.com Inc.
 * Copyright (c) 2004-2019 All Rights Reserved.
 */
package cn.sedu.moon.service;

import cn.sedu.moon.dao.model.DistrictGd;

/**
 * @author wukong
 * @version : DistrictService.java, v 0.1 2019年11月19日 11:00 上午 Exp $
 */
public interface DistrictService {

    /**
     * 保存小区数据
     *
     * @return 保存成功的条数
     */
    int saveDistrict(DistrictGd districtGd);
}