/*
 * Dian.com Inc.
 * Copyright (c) 2004-2019 All Rights Reserved.
 */
package cn.sedu.moon.service;

/**
 *
 * @author ${wukong}
 * @version $Id: DataCacheService.java, v 0.1 2019年03月13日 9:47 PM Exp $
 */
public interface DataCacheService {

    void reloadNewsCache(Long userId);

    void reloadRecommendSchoolCache(Long userId);

    void clearSchoolTabData(Long userId);

}