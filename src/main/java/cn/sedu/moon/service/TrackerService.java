/*
 * Dian.com Inc.
 * Copyright (c) 2004-2019 All Rights Reserved.
 */
package cn.sedu.moon.service;

import cn.sedu.moon.dao.model.TrackerPoint;

import java.util.List;

/**
 * @author wukong
 * @version : TrackerService.java, v 0.1 2019年11月04日 11:41 上午 Exp $
 */
public interface TrackerService {

    /**
     * 记录埋点
     *
     * @param url  url
     * @param userId 用户id
     */
    void record(String url, Long userId);

    List<TrackerPoint> allPoint();

}