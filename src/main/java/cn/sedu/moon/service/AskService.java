/*
 * Dian.com Inc.
 * Copyright (c) 2004-2019 All Rights Reserved.
 */
package cn.sedu.moon.service;

import cn.sedu.moon.controller.request.AskEditRequest;
import cn.sedu.moon.controller.request.AskPublishRequest;
import cn.sedu.moon.controller.request.AskReportAnswerRequest;
import cn.sedu.moon.controller.request.AskSearchRequest;
import cn.sedu.moon.controller.request.QueryAskDetailRequest;
import cn.sedu.moon.controller.request.cms.ask.CmsAskReplyRequest;
import cn.sedu.moon.controller.request.cms.ask.CmsSearchAskRequest;
import cn.sedu.moon.controller.response.AskAnswerResponse;
import cn.sedu.moon.controller.response.AskDetailResponse;
import cn.sedu.moon.controller.response.AskPublishResponse;
import cn.sedu.moon.controller.response.AskReportAnswerResponse;
import cn.sedu.moon.controller.response.AskResponse;
import cn.sedu.moon.controller.response.cms.ask.CmsAskDetailResponse;
import cn.sedu.moon.controller.response.cms.ask.CmsSearchAskResponse;

import java.util.List;

/**
 *
 * @author ${wukong}
 * @version $Id: AskService.java, v 0.1 2019年01月16日 11:35 AM Exp $
 */
public interface AskService {

    AskPublishResponse askPublish(AskPublishRequest request);

    void collect(Long userId, Long askId);

    void collectDelete(Long userId, Long askId);

    void edit(AskEditRequest request);

    List<AskResponse> search(AskSearchRequest request);

    List<AskResponse> myAskList(Long userId);

    List<AskResponse> myCollect(Long userId, Integer offset, Integer pageSize);

    List<AskResponse> myAttention(Long userId);
    List<AskResponse> myAnswer(Long userId);

    AskDetailResponse askDetail(QueryAskDetailRequest request);

    List<AskAnswerResponse> queryAnswerList(QueryAskDetailRequest request);

    AskReportAnswerResponse reportAnswer(AskReportAnswerRequest request);

    void follow(Long askId, Long userId);

    /**
     * 点击了页面的"有用"
     *
     * @param answerId
     * @param userId
     */
    void useful(Long answerId, Long userId);
    void unUseful(Long answerId, Long userId);

    void accept(Long askId, Long askAnswerId, Long userId);

    Integer queryAskAttentionCount(Long askId);

    Integer queryAskAnswerCount(Long askId);

    CmsSearchAskResponse cmsSearch(CmsSearchAskRequest request);

    CmsAskDetailResponse cmsDetail(Long id);

    Long cmsPublish(AskPublishRequest request, Long userId);

    void cmsReply(CmsAskReplyRequest request);

    void cmsDelete(Long askId, Long userId);

}