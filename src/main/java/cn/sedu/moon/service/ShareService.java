/*
 * Dian.com Inc.
 * Copyright (c) 2004-2019 All Rights Reserved.
 */
package cn.sedu.moon.service;

import cn.sedu.moon.controller.response.share.CheckShareResponse;
import cn.sedu.moon.controller.response.share.DoShareResponse;
import cn.sedu.moon.controller.response.share.LightCheckResponse;
import cn.sedu.moon.controller.response.share.LightNewsResponse;

/**
 *
 * @author ${wukong}
 * @version $Id: ShareService.java, v 0.1 2019年04月14日 4:01 PM Exp $
 */
public interface ShareService {

    /**
     * 创建分享
     *
     * @param userId  用户id
     * @param newsId  新闻id
     * @return 分享id
     */
    Long createShare(Long userId, Long newsId);

    /**
     * 分享完成
     *
     * @param userId 用户id
     * @param shareId 分享id
     */
    DoShareResponse doShare(Long userId, Long shareId);

    /**
     * 点亮新闻
     * @param shareUserId 原分享者的用户id
     * @param lightUserId 点亮者的用户id
     * @param newsId 新闻id
     */
    LightNewsResponse lightNews(Long shareUserId, Long lightUserId, Long newsId);

    /**
     * 查询是否已经点亮过了
     *
     * @param shareUserId 原分享者的用户id
     * @param lightUserId 点亮者的用户id
     * @param newsId 新闻id
     * @return true: 点亮过了 false: 没有点亮过
     */
    LightCheckResponse checkLight(Long shareUserId, Long lightUserId, Long newsId);


    /**
     * 检查新闻的分享信息
     *
     * @param newsId 新闻id
     * @param userId 用户id
     *
     * @return response
     */
    CheckShareResponse checkShare(Long newsId, Long userId);

    /**
     * 阅读分享
     *
     * @param userId 用户id
     * @param shareId 分享id
     */
    void readShare(Long userId, Long shareId);

}