/*
 * Dian.com Inc.
 * Copyright (c) 2004-2019 All Rights Reserved.
 */
package cn.sedu.moon.service;

/**
 * @author wukong
 * @version : MusicPlayer.java, v 0.1 2019年10月22日 3:08 PM Exp $
 */
public interface MusicPlayer {

    void play();

    void asyncPlay();

    void saveSong();

    void saveSongTransaction();
}