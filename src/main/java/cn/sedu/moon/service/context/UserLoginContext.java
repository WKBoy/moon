/*
 * Dian.com Inc.
 * Copyright (c) 2004-2018 All Rights Reserved.
 */
package cn.sedu.moon.service.context;

import lombok.Data;

/**
 * 用户登陆上下文
 *
 * @author ${wukong}
 * @version $Id: UserLoginContext.java, v 0.1 2018年12月16日 5:08 PM Exp $
 */
@Data
public class UserLoginContext {

    private Long userId;

    /**
     * 加密的数据
     */
    private String encryptedData;

    /**
     * 微信分配的iv
     */
    private String iv;

    /**
     * 微信分配的code
     */
    private String code;

    private Long shareUserId;

}