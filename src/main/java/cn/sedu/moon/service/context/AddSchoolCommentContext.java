/*
 * Dian.com Inc.
 * Copyright (c) 2004-2019 All Rights Reserved.
 */
package cn.sedu.moon.service.context;

import lombok.Data;

import java.util.List;

/**
 * @author wukong
 * @version : AddSchoolCommentContext.java, v 0.1 2019年10月08日 6:45 PM Exp $
 */
@Data
public class AddSchoolCommentContext {

    private Long userId;

    private Long schoolId;

    private String content;

    private List<String> imgList;
}