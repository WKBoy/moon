/*
 * Dian.com Inc.
 * Copyright (c) 2004-2018 All Rights Reserved.
 */
package cn.sedu.moon.service.context;

import cn.sedu.moon.common.enums.SchoolTypeEnum;
import lombok.Data;

/**
 *
 * @author ${wukong}
 * @version $Id: QuerySchoolContext.java, v 0.1 2018年12月11日 1:08 PM Exp $
 */
@Data
public class QuerySchoolContext {

    /**
     * userId
     */
    private Long userId;

    /**
     * 翻页参数：偏移量
     */
    private Integer offset;

    /**
     * 翻页参数：页大小
     */
    private Integer pageSize;

    /**
     * 纬度
     */
    private Double lat;

    /**
     * 经度
     */
    private Double lnt;

    /**
     * 搜索关键字
     */
    private String keyName;

    /**
     * 学校类型
     */
    private SchoolTypeEnum schoolType;

    /**
     * 区编码
     */
    private String areaCode;


    private String areaName;

    private String cityName;

    private String cityCode;

    /**
     * 推荐逻辑的类型
     */
    private Integer recommendType;

}