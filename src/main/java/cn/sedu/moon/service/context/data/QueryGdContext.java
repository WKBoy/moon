/*
 * Dian.com Inc.
 * Copyright (c) 2004-2019 All Rights Reserved.
 */
package cn.sedu.moon.service.context.data;

import lombok.Data;

/**
 * @author wukong
 * @version : QueryGdContext.java, v 0.1 2019年11月01日 2:46 下午 Exp $
 */
@Data
public class QueryGdContext {

    private String keywords;

    private String areaCode;

    private Integer pageNo;

    private Integer pageSize;

    private Integer type;
}