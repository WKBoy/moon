/*
 * Dian.com Inc.
 * Copyright (c) 2004-2019 All Rights Reserved.
 */
package cn.sedu.moon.service.context;

import lombok.Data;

/**
 * @author wukong
 * @version : QuerySchoolNewsContext.java, v 0.1 2019年10月08日 2:34 PM Exp $
 */
@Data
public class QuerySchoolNewsContext {

    /**
     * 学校id
     */
    private Long schoolId;

    /**
     * 偏移量
     */
    private Integer offset;

    /**
     * 页大小
     */
    private Integer pageSize;

    private Long userId;

}