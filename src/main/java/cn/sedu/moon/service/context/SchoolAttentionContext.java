/*
 * Dian.com Inc.
 * Copyright (c) 2004-2018 All Rights Reserved.
 */
package cn.sedu.moon.service.context;

import java.util.List;

import lombok.Data;

/**
 *
 * @author ${wukong}
 * @version $Id: SchoolAttentionContext.java, v 0.1 2018年12月16日 11:26 AM Exp $
 */
@Data
public class SchoolAttentionContext {

    private List<Long> schoolList;

    private Long userId;
}