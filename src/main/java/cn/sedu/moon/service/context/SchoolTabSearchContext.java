/*
 * Dian.com Inc.
 * Copyright (c) 2004-2019 All Rights Reserved.
 */
package cn.sedu.moon.service.context;

import cn.sedu.moon.dao.model.DistrictGd;
import lombok.Data;

/**
 * @author wukong
 * @version : SchoolTabSearchContext.java, v 0.1 2019年10月08日 2:09 PM Exp $
 */
@Data
public class SchoolTabSearchContext {

    /**
     * 搜索关键字
     */
    private String key;

    private Long userId;

    /**
     * 翻页参数：偏移量
     */
    private Integer offset = 0;

    /**
     * 翻页参数：页大小
     */
    private Integer pageSize = 10;

    /**
     * 用户小区信息
     */
    private DistrictGd userDistrictInfo;
}