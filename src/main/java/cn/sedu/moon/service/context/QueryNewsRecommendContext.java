/*
 * Dian.com Inc.
 * Copyright (c) 2004-2019 All Rights Reserved.
 */
package cn.sedu.moon.service.context;

import java.util.List;

import cn.sedu.moon.dao.model.StuSch;
import cn.sedu.moon.dao.model.StudentArchives;
import lombok.Data;

/**
 *
 * @author ${wukong}
 * @version $Id: QueryNewsRecommendContext.java, v 0.1 2019年03月13日 11:24 AM Exp $
 */
@Data
public class QueryNewsRecommendContext {

    private Integer offset;

    private Integer pageSize;

    private Long userId;

    private List<StuSch> likeSchoolList;

    /**
     * 学生上报的档案
     */
    private StudentArchives archives;

    /**
     * 学生目前所在的学校
     */
    private Long currentSchoolId;

    /**
     * 城市编码
     */
    private String cityCode;

    /**
     * 想读学校IdList
     */
    private List<Long> likeSchoolIdList;
}