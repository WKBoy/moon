/*
 * Dian.com Inc.
 * Copyright (c) 2004-2019 All Rights Reserved.
 */
package cn.sedu.moon.service.context;

import java.util.List;

import cn.sedu.moon.dao.model.StuSch;
import lombok.Data;

/**
 * 查询新闻上下文
 *
 * @author ${wukong}
 * @version $Id: QueryNewsContext.java, v 0.1 2019年02月17日 2:04 PM Exp $
 */
@Data
public class QueryNewsContext {

    /**
     * 用户id
     */
    private Long userId;

    /**
     * 页偏移量
     */
    private Integer offset;

    private String userArchiveCityCode;

    /**
     * 页大小
     */
    private Integer pageSize;

    private List<StuSch> stuSchList;

    private List<StuSch> likeSchoolList;
}