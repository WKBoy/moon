/*
 * Dian.com Inc.
 * Copyright (c) 2004-2019 All Rights Reserved.
 */
package cn.sedu.moon.service.context;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author wukong
 * @version : IndexAskContext.java, v 0.1 2019年09月01日 17:36 Exp $
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class IndexAskContext extends QueryNewsContext {

    private Long userId;

    /**
     * 翻页参数：偏移量
     */
    private Integer offset = 0;

    /**
     * 翻页参数：页大小
     */
    private Integer pageSize = 10;


}