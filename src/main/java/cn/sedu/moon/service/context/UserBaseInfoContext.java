/*
 * Dian.com Inc.
 * Copyright (c) 2004-2019 All Rights Reserved.
 */
package cn.sedu.moon.service.context;

import lombok.Data;

/**
 *
 * @author ${wukong}
 * @version $Id: UserBaseInfoContext.java, v 0.1 2019年01月06日 2:50 PM Exp $
 */
@Data
public class UserBaseInfoContext {

    private Long userId;

    /**
     * 昵称
     */
    private String nickName;

    private Integer gender;

    private String language;

    private String city;

    private String province;

    private String country;

    private String avatarUrl;
}