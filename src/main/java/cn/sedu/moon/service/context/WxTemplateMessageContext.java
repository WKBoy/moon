/*
 * Dian.com Inc.
 * Copyright (c) 2004-2019 All Rights Reserved.
 */
package cn.sedu.moon.service.context;

import java.util.Map;

import lombok.Data;

/**
 * 微信模版消息上线
 *
 * @author ${wukong}
 * @version $Id: WxTemplateMessageContext.java, v 0.1 2019年04月22日 8:25 PM Exp $
 */
@Data
public class WxTemplateMessageContext {

    /**
     * 发送对象的userId
     */
    private Long userId;

    /**
     * 微信模版id
     */
    private String templateId;

    /**
     * 点击模版消息后的跳转路径
     */
    private String page;

    /**
     * 模版消息内容
     */
    private Map data;
}