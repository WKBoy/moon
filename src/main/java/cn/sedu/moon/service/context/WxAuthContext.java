/*
 * Dian.com Inc.
 * Copyright (c) 2004-2019 All Rights Reserved.
 */
package cn.sedu.moon.service.context;

import cn.sedu.moon.thirdparty.dto.WxAppUserInfo;
import lombok.Data;

/**
 * @author wukong
 * @version : WxAuthContext.java, v 0.1 2019年10月09日 11:49 AM Exp $
 */
@Data
public class WxAuthContext {

    /**
     * 小程序提供的参数
     */
    private String code;

    /**
     * 小程序授权的用户信息的加密数据
     */
    private String encryptedData;

    /**
     * 小程序提供的参数
     */
    private String iv;

    /**
     * 用户角色
     */
    private Integer role;

    /**
     * 解密后的微信授权的用户信息
     */
    private WxAppUserInfo wxAppUserInfo;
}