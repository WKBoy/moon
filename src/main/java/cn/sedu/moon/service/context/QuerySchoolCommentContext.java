/*
 * Dian.com Inc.
 * Copyright (c) 2004-2019 All Rights Reserved.
 */
package cn.sedu.moon.service.context;

import lombok.Data;

/**
 * @author wukong
 * @version : QuerySchoolCommentContext.java, v 0.1 2019年10月08日 2:32 PM Exp $
 */
@Data
public class QuerySchoolCommentContext {

    /**
     * 学校id
     */
    private Long schoolId;

    /**
     * 偏移量
     */
    private Integer offset = 0;

    /**
     * 页大小
     */
    private Integer pageSize = 10;
}