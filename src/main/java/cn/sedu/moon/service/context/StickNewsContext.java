/*
 * Dian.com Inc.
 * Copyright (c) 2004-2019 All Rights Reserved.
 */
package cn.sedu.moon.service.context;

import lombok.Data;

/**
 *
 * @author ${wukong}
 * @version $Id: StickNewsContext.java, v 0.1 2019年05月27日 11:21 AM Exp $
 */
@Data
public class StickNewsContext {

    /**
     * 新闻id
     */
    private Long newsId;

    /**
     * 操作置顶的人
     */
    private Long userId;
}