/*
 * Dian.com Inc.
 * Copyright (c) 2004-2019 All Rights Reserved.
 */
package cn.sedu.moon.service.context;

import lombok.Data;

/**
 *
 * @author ${wukong}
 * @version $Id: SearchNewsContext.java, v 0.1 2019年07月07日 11:23 AM Exp $
 */
@Data
public class SearchNewsContext {

    private Integer offset;

    private Integer pageSize;

    private String key;

    private Long userId;
}