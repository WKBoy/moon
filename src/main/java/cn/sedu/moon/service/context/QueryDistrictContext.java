/*
 * Dian.com Inc.
 * Copyright (c) 2004-2018 All Rights Reserved.
 */
package cn.sedu.moon.service.context;

import lombok.Data;

/**
 *
 * @author ${wukong}
 * @version $Id: QueryDistrictContext.java, v 0.1 2018年12月16日 12:04 AM Exp $
 */
@Data
public class QueryDistrictContext {

    /**
     * 用户ID
     */
    private Long userId;

    /**
     * 翻页参数：偏移量
     */
    private Integer offset;

    /**
     * 翻页参数：页大小
     */
    private Integer pageSize;

    /**
     * 纬度
     */
    private Double lat;

    /**
     * 经度
     */
    private Double lnt;

    /**
     * 搜索关键字
     */
    private String keyName;

    private String areaCode;

}