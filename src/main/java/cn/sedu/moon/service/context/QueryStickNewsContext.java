/*
 * Dian.com Inc.
 * Copyright (c) 2004-2019 All Rights Reserved.
 */
package cn.sedu.moon.service.context;

import lombok.Data;

/**
 *
 * @author ${wukong}
 * @version $Id: QueryStickNewsContext.java, v 0.1 2019年04月19日 2:05 PM Exp $
 */
@Data
public class QueryStickNewsContext {

    private String cityCode;
}