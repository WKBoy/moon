/*
 * Dian.com Inc.
 * Copyright (c) 2004-2018 All Rights Reserved.
 */
package cn.sedu.moon.service.context;

import java.util.List;

import lombok.Data;

/**
 * 上报学生档案的上下文
 *
 * @author ${wukong}
 * @version $Id: ArchiveReportContext.java, v 0.1 2018年12月14日 3:33 PM Exp $
 */
@Data
public class ArchiveReportContext {

    private static final long serialVersionUID = 8362572958013930277L;

    /**
     * 用户ID
     */
    private Long userId;

    /**
     * 学生姓名
     */
    private String name;

    /**
     * 年级
     */
    private Long grade;

    /**
     * 学校ID
     */
    private Long schoolId;

    /**
     * 性别，1 男，2 女
     */
    private Integer gender;

    /**
     * 学校属性，公办，民办，全选
     */
    private Integer schoolProperty;

    /**
     * 培养方向
     */
    private List<Long> growDirectionList;

    /**
     * 小区ID
     */
    private Long districtId;

    /**
     * 省code
     */
    private String provinceCode;

    /**
     * 市code
     */
    private String cityCode;

    /**
     * 区code
     */
    private String areaCode;
}