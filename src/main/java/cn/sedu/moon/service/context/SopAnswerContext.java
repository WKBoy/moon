/*
 * Dian.com Inc.
 * Copyright (c) 2004-2019 All Rights Reserved.
 */
package cn.sedu.moon.service.context;

import java.util.List;

import cn.sedu.moon.controller.request.SopOptionAnswerRequest;
import cn.sedu.moon.dao.model.SopSubject;
import lombok.Data;

/**
 * 用户回答SOP问题的上下文
 *
 * @author ${wukong}
 * @version $Id: SopAnswerContext.java, v 0.1 2019年01月09日 5:11 PM Exp $
 */
@Data
public class SopAnswerContext {

    /**
     * 问题id
     */
    private Integer subjectRefId;

    /**
     * 学校id
     */
    private Long schoolId;

    /**
     * 用户id
     */
    private Long userId;

    /**
     * 用户输入的文本
     */
    private String inputString;

    /**
     * 选项列表
     */
    private List<SopOptionAnswerRequest> optionList;

    /**
     * 查询到的题目对象
     */
    private SopSubject sopSubject;

}