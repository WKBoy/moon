/*
 * Dian.com Inc.
 * Copyright (c) 2004-2019 All Rights Reserved.
 */
package cn.sedu.moon.controller;

import cn.sedu.moon.common.Result;
import cn.sedu.moon.common.auth.Auth;
import cn.sedu.moon.common.enums.ErrorCodeEnum;
import cn.sedu.moon.common.exception.BizException;
import cn.sedu.moon.common.template.ControllerCallback;
import cn.sedu.moon.controller.request.share.CreateShareRequest;
import cn.sedu.moon.controller.request.share.LightNewsRequest;
import cn.sedu.moon.controller.request.share.ReadShareRequest;
import cn.sedu.moon.controller.request.share.ShareNewsRequest;
import cn.sedu.moon.controller.response.share.CheckShareResponse;
import cn.sedu.moon.controller.response.share.CreateShareResponse;
import cn.sedu.moon.controller.response.share.DoShareResponse;
import cn.sedu.moon.controller.response.share.LightCheckResponse;
import cn.sedu.moon.controller.response.share.LightNewsResponse;
import cn.sedu.moon.service.ShareService;
import com.alibaba.fastjson.JSON;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * 分享控制器
 * @author ${wukong}
 * @version $Id: ShareController.java, v 0.1 2019年04月14日 3:43 PM Exp $
 */
@RestController
@Slf4j
public class ShareController extends BaseController {

    @Autowired
    private ShareService shareService;

    @Auth
    @RequestMapping(value = "/moon/share/createShare", method = RequestMethod.GET)
    public Result createShare(CreateShareRequest request) {

        return template.execute(new ControllerCallback<CreateShareResponse>() {

            @Override
            public void checkParam() {
                if (request.getNewsId() == null) {
                    throw new BizException(ErrorCodeEnum.PARAM_CHECK_EXP, "新闻id不能为空");
                }
            }

            @Override
            public CreateShareResponse execute() {
                Long shareId = shareService.createShare(getUserId(), request.getNewsId());
                CreateShareResponse shareResponse = new CreateShareResponse();
                shareResponse.setShareId(shareId);

                CheckShareResponse checkShareResponse = shareService.checkShare(request.getNewsId(), getUserId());
                shareResponse.setNeedShared(checkShareResponse.getIsNeedShare());
                shareResponse.setNeedShareTotal(checkShareResponse.getNeedShareTotal());
                shareResponse.setShareCount(checkShareResponse.getShareCount());
                shareResponse.setType(checkShareResponse.getNewsType().getCode());

                return shareResponse;
            }
        });
    }

    @Auth
    @RequestMapping(value = "/moon/share/doShare", method = RequestMethod.POST)
    public Result doShare(@RequestBody ShareNewsRequest request) {

        return template.execute(new ControllerCallback<DoShareResponse>() {

            @Override
            public void checkParam() {
                if (request.getShareId() == null) {
                    throw new BizException(ErrorCodeEnum.PARAM_CHECK_EXP, "分享ID不能为空");
                }
            }

            @Override
            public DoShareResponse execute() {
                return shareService.doShare(getUserId(), request.getShareId());
            }
        });
    }

    @Auth
    @RequestMapping(value = "/moon/share/lightCheck", method = RequestMethod.POST)
    public Result lightCheck(@RequestBody LightNewsRequest request) {

        return template.execute(new ControllerCallback<LightCheckResponse>() {

            @Override
            public void checkParam() {
                //if (request.getShareUserId() == null) {
                //    throw new BizException(ErrorCodeEnum.PARAM_CHECK_EXP, "原分享者的用户id不能为空");
                //}

                if (request.getNewsId() == null) {
                    throw new BizException(ErrorCodeEnum.PARAM_CHECK_EXP, "新闻id不能为空");
                }

                if (request.getShareUserId() == null) {
                    request.setShareUserId(getUserId());
                }

            }

            @Override
            public LightCheckResponse execute() {
                return shareService.checkLight(request.getShareUserId(), getUserId(), request.getNewsId());
            }
        });
    }

    @Auth
    @RequestMapping(value = "/moon/share/light", method = RequestMethod.POST)
    public Result light(@RequestBody LightNewsRequest request) {

        return template.execute(new ControllerCallback<LightNewsResponse>() {

            @Override
            public void checkParam() {
                if (request.getShareUserId() == null) {
                    throw new BizException(ErrorCodeEnum.PARAM_CHECK_EXP, "原分享者的用户id不能为空");
                }

                if (request.getNewsId() == null) {
                    throw new BizException(ErrorCodeEnum.PARAM_CHECK_EXP, "新闻id不能为空");
                }
            }

            @Override
            public LightNewsResponse execute() {
                return shareService.lightNews(request.getShareUserId(), getUserId(), request.getNewsId());
            }
        });
    }

    @Auth
    @RequestMapping(value = "/moon/share/readShare", method = RequestMethod.POST)
    public Result readShare(@RequestBody ReadShareRequest request) {

        return template.execute(new ControllerCallback<Boolean>() {

            @Override
            public void checkParam() {
                if (request.getShareId() == null) {
                    throw new BizException(ErrorCodeEnum.PARAM_CHECK_EXP, "分享id不能为空");
                }
            }

            @Override
            public Boolean execute() {
                log.info("readShare -- :" + JSON.toJSONString(request) + ", userId:" + getUserId());
                shareService.readShare(getUserId(), request.getShareId());
                return true;
            }
        });
    }
}