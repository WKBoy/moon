/*
 * Dian.com Inc.
 * Copyright (c) 2004-2019 All Rights Reserved.
 */
package cn.sedu.moon.controller;

import cn.sedu.moon.common.Result;
import cn.sedu.moon.common.auth.Auth;
import cn.sedu.moon.common.enums.ErrorCodeEnum;
import cn.sedu.moon.common.exception.BizException;
import cn.sedu.moon.common.template.ControllerCallback;
import cn.sedu.moon.controller.request.ReportFormIdRequest;
import cn.sedu.moon.service.MessageService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 消息控制器
 *
 * @author ${wukong}
 * @version $Id: MessageController.java, v 0.1 2019年04月02日 10:35 AM Exp $
 */
@RestController
public class MessageController extends BaseController {

    @Autowired
    private MessageService messageService;

    @RequestMapping(value = "/moon/wx/formId/report")
    @Auth
    public Result saveFormId(@RequestBody ReportFormIdRequest request) {

        return template.execute(new ControllerCallback<Boolean>() {
            @Override
            public void checkParam() {
                if (StringUtils.isBlank(request.getFormId())) {
                    throw new BizException(ErrorCodeEnum.PARAM_CHECK_EXP, "formId不能为空");
                }
            }

            @Override
            public Boolean execute() {
                messageService.saveWxFormId(getUserId(), request.getFormId());
                return true;
            }
        });
    }




}