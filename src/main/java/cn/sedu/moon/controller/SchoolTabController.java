/*
 * Dian.com Inc.
 * Copyright (c) 2004-2019 All Rights Reserved.
 */
package cn.sedu.moon.controller;

import cn.sedu.moon.common.Result;
import cn.sedu.moon.common.auth.Auth;
import cn.sedu.moon.common.enums.ErrorCodeEnum;
import cn.sedu.moon.common.exception.BizException;
import cn.sedu.moon.common.template.ControllerCallback;
import cn.sedu.moon.controller.request.school.AddSchoolCommentRequest;
import cn.sedu.moon.controller.request.school.QuerySchoolCommentRequest;
import cn.sedu.moon.controller.request.school.QuerySchoolTabNewsRequest;
import cn.sedu.moon.controller.request.school.SchoolTabListRequest;
import cn.sedu.moon.controller.response.SchoolNewsResponse;
import cn.sedu.moon.controller.response.SchoolTabAskResponse;
import cn.sedu.moon.controller.response.school.AddSchoolCommentResponse;
import cn.sedu.moon.controller.response.school.SchoolCommentResponse;
import cn.sedu.moon.controller.response.school.SchoolTabResponse;
import cn.sedu.moon.service.SchoolTabService;
import cn.sedu.moon.service.context.AddSchoolCommentContext;
import cn.sedu.moon.service.context.QuerySchoolCommentContext;
import cn.sedu.moon.service.context.QuerySchoolNewsContext;
import cn.sedu.moon.service.context.SchoolTabSearchContext;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author wukong
 * @version : SchoolTabController.java, v 0.1 2019年10月07日 3:40 PM Exp $
 */
@RestController
public class SchoolTabController extends BaseController {

    @Autowired
    private SchoolTabService schoolTabService;

    @Auth
    @RequestMapping(value = "/moon/school/search", method = RequestMethod.GET)
    public Result search(SchoolTabListRequest request) {

        return template.execute(new ControllerCallback<List<SchoolTabResponse>>() {

            SchoolTabSearchContext context = new SchoolTabSearchContext();

            @Override
            public void checkParam() {
                if (request.getOffset() == null || request.getPageSize() == null) {
                    throw new BizException(ErrorCodeEnum.PARAM_CHECK_EXP, "翻页参数不能为空");
                }
            }

            @Override
            public void buildContext() {
                context.setKey(request.getKey());
                context.setUserId(getUserId());
                context.setOffset(request.getOffset());
                context.setPageSize(request.getPageSize());
            }

            @Override
            public List<SchoolTabResponse> execute() {

                // 埋点
                tracker();
                return schoolTabService.search(context);
            }
        });
    }

    @Auth
    @RequestMapping(value = "/moon/school/tab/detail", method = RequestMethod.GET)
    public Result schoolDetail(QuerySchoolTabNewsRequest request) {

        return template.execute(new ControllerCallback<SchoolTabResponse>() {

            @Override
            public void checkParam() {
                if (request.getSchoolId() == null) {
                    throw new BizException(ErrorCodeEnum.PARAM_CHECK_EXP, "学校id不能为空");
                }
            }

            @Override
            public void buildContext() {
            }

            @Override
            public SchoolTabResponse execute() {

                // 埋点
                tracker();
                return schoolTabService.schoolDetail(request.getSchoolId(), getUserId());
            }
        });
    }

    @Auth
    @RequestMapping(value = "/moon/school/news/query", method = RequestMethod.GET)
    public Result querySchoolNews(QuerySchoolTabNewsRequest request) {

        return template.execute(new ControllerCallback<List<SchoolNewsResponse>>() {

            QuerySchoolNewsContext context = new QuerySchoolNewsContext();

            @Override
            public void checkParam() {
                if (request.getSchoolId() == null) {
                    throw new BizException(ErrorCodeEnum.PARAM_CHECK_EXP, "学校id不能为空");
                }
            }

            @Override
            public void buildContext() {
                context.setOffset(request.getOffset());
                context.setPageSize(request.getPageSize());
                context.setSchoolId(request.getSchoolId());
                context.setUserId(getUserId());
//                                context.setUserId(3544L);

            }

            @Override
            public List<SchoolNewsResponse> execute() {
                return schoolTabService.newsList(context);
            }
        });
    }

    @Auth
    @RequestMapping(value = "/moon/school/ask/query", method = RequestMethod.GET)
    public Result querySchoolAsk(QuerySchoolTabNewsRequest request) {

        return template.execute(new ControllerCallback<List<SchoolTabAskResponse>>() {

            @Override
            public void checkParam() {
                if (request.getSchoolId() == null) {
                    throw new BizException(ErrorCodeEnum.PARAM_CHECK_EXP, "学校id不能为空");
                }
            }

            @Override
            public List<SchoolTabAskResponse> execute() {
                return schoolTabService.askList(request.getSchoolId());
            }
        });
    }

    @Auth
    @RequestMapping(value = "/moon/school/comment/query", method = RequestMethod.GET)
    public Result querySchoolComment(QuerySchoolCommentRequest request) {

        return template.execute(new ControllerCallback<List<SchoolCommentResponse>>() {

            QuerySchoolCommentContext context = new QuerySchoolCommentContext();

            @Override
            public void checkParam() {
                if (request.getSchoolId() == null) {
                    throw new BizException(ErrorCodeEnum.PARAM_CHECK_EXP, "学校id不能为空");
                }
            }

            @Override
            public void buildContext() {
                context.setOffset(request.getOffset());
                context.setPageSize(request.getPageSize());
                context.setSchoolId(request.getSchoolId());
            }

            @Override
            public List<SchoolCommentResponse> execute() {
                return schoolTabService.commentList(context);
            }
        });
    }

    @Auth
    @RequestMapping(value = "/moon/school/comment/add", method = RequestMethod.POST)
    public Result addSchoolComment(@RequestBody AddSchoolCommentRequest request) {

        return template.execute(new ControllerCallback<AddSchoolCommentResponse>() {

            AddSchoolCommentContext context = new AddSchoolCommentContext();

            @Override
            public void checkParam() {
                if (request.getSchoolId() == null) {
                    throw new BizException(ErrorCodeEnum.PARAM_CHECK_EXP, "学校id不能为空");
                }

                if (StringUtils.isBlank(request.getContent())) {
                    throw new BizException(ErrorCodeEnum.PARAM_CHECK_EXP, "评论内容不能为空");
                }
            }

            @Override
            public void buildContext() {
                context.setSchoolId(request.getSchoolId());
                context.setContent(request.getContent());
                context.setImgList(request.getImgList());
                context.setUserId(getUserId());
            }

            @Override
            public AddSchoolCommentResponse execute() {

                // 埋点
                tracker();

                Long commentId = schoolTabService.addSchoolComment(context);
                AddSchoolCommentResponse response = new AddSchoolCommentResponse();
                response.setCommentId(commentId);
                return response;
            }
        });
    }



}