/*
 * Dian.com Inc.
 * Copyright (c) 2004-2019 All Rights Reserved.
 */
package cn.sedu.moon.controller;

import cn.hutool.core.util.StrUtil;
import cn.sedu.moon.common.Result;
import cn.sedu.moon.common.auth.Auth;
import cn.sedu.moon.common.enums.ErrorCodeEnum;
import cn.sedu.moon.common.exception.BizException;
import cn.sedu.moon.common.template.ControllerCallback;
import cn.sedu.moon.controller.request.BaseRequest;
import cn.sedu.moon.controller.request.census.CityCodeNewsRequest;
import cn.sedu.moon.controller.request.NewsReadRequest;
import cn.sedu.moon.controller.request.news.CollectNewsRequest;
import cn.sedu.moon.controller.request.news.DeleteCollectNewsRequest;
import cn.sedu.moon.controller.response.SchoolNewsResponse;
import cn.sedu.moon.controller.response.SearchCollectNewsResponse;
import cn.sedu.moon.controller.response.census.EditCensusResponse;
import cn.sedu.moon.service.NewsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * 新闻内容相关控制器
 *
 * @author ${wukong}
 * @version $Id: NewsController.java, v 0.1 2019年02月15日 11:59 AM Exp $
 */
@RestController
public class NewsController extends BaseController {

    @Autowired
    private NewsService newsService;

    @RequestMapping(value = "/moon/news/read", method = RequestMethod.POST)
    @Auth
    public Result read(@RequestBody NewsReadRequest request) {

        return template.execute(new ControllerCallback<Boolean>() {

            @Override
            public void checkParam() {
                if (request.getNewsId() == null) {
                    throw new BizException(ErrorCodeEnum.PARAM_CHECK_EXP, "新闻id不能为空");
                }
            }

            @Override
            public Boolean execute() {
                newsService.read(getUserId(), request.getNewsId());
                return true;
            }
        });
    }

    /**
     * 收藏
     *
     * @param request
     * @return
     */
    @RequestMapping(value = "/moon/news/collect", method = RequestMethod.POST)
    @Auth
    public Result collect(@RequestBody CollectNewsRequest request) {

        return template.execute(new ControllerCallback<Boolean>() {

            @Override
            public void checkParam() {
                if (request.getNewsId() == null) {
                    throw new BizException(ErrorCodeEnum.PARAM_CHECK_EXP, "新闻id不能为空");
                }
            }

            @Override
            public Boolean execute() {
                newsService.collect(getUserId(), request.getNewsId());
                return true;
            }
        });
    }

    @RequestMapping(value = "/moon/news/collectSearch", method = RequestMethod.GET)
    @Auth
    public Result collectSearch(BaseRequest request) {

        return template.execute(new ControllerCallback<SearchCollectNewsResponse>() {

            @Override
            public void checkParam() {
            }

            @Override
            public SearchCollectNewsResponse execute() {
                return newsService.searchCollect(getUserId(), request.getOffset(), request.getPageSize());
            }
        });
    }

    @RequestMapping(value = "/moon/news/collectDelete", method = RequestMethod.POST)
    @Auth
    public Result collectDelete(@RequestBody DeleteCollectNewsRequest request) {

        return template.execute(new ControllerCallback<Boolean>() {

            @Override
            public void checkParam() {
                if (request.getNewsId() == null) {
                    throw new BizException(ErrorCodeEnum.PARAM_CHECK_EXP, "新闻id不能为空");
                }}

            @Override
            public Boolean execute() {
                newsService.deleteCollect(getUserId(), request.getNewsId());
                return true;
            }
        });
    }

    /**
     * 文章按地区编辑情况：（近30天）
     * @return
     */
    @RequestMapping(value = "/moon/news/editCensus", method = RequestMethod.POST)
    @Auth
    public Result newsEditCensus(@RequestBody CityCodeNewsRequest request) {

        return template.execute(new ControllerCallback<EditCensusResponse>() {

            @Override
            public void checkParam() {
                if (StrUtil.isBlank(request.getCityCode())) {
                    throw new BizException(ErrorCodeEnum.PARAM_CHECK_EXP, "城市编码不能为空");
                }
            }

            @Override
            public EditCensusResponse execute() {
                return newsService.newsEditCensus(request.getCityCode());
            }
        });
    }

    @RequestMapping(value = "/moon/news/weekly/selected/list", method = RequestMethod.GET)
    @Auth
    public Result weeklySelectList(@RequestBody DeleteCollectNewsRequest request) {

        return template.execute(new ControllerCallback<List<SchoolNewsResponse>>() {

            @Override
            public void checkParam() {
            }

            @Override
            public List<SchoolNewsResponse> execute() {
                return newsService.weeklySelectedList(getUserId());
            }
        });
    }
}