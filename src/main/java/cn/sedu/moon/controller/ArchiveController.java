/*
 * Dian.com Inc.
 * Copyright (c) 2004-2018 All Rights Reserved.
 */
package cn.sedu.moon.controller;

import cn.sedu.moon.common.Result;
import cn.sedu.moon.common.auth.Auth;
import cn.sedu.moon.common.enums.ErrorCodeEnum;
import cn.sedu.moon.common.enums.SchoolTypeEnum;
import cn.sedu.moon.common.exception.BizException;
import cn.sedu.moon.common.template.ControllerCallback;
import cn.sedu.moon.controller.request.ArchiveReportRequest;
import cn.sedu.moon.controller.request.DistrictNearbyRequest;
import cn.sedu.moon.controller.request.SchoolNearbyRequest;
import cn.sedu.moon.controller.request.district.UserAddDistrictRequest;
import cn.sedu.moon.controller.response.ArchiveChooseSchoolInfoResponse;
import cn.sedu.moon.controller.response.ArchiveDescResponse;
import cn.sedu.moon.controller.response.DistrictResponse;
import cn.sedu.moon.controller.response.GradeResponse;
import cn.sedu.moon.controller.response.QueryGrowDirectionResponse;
import cn.sedu.moon.controller.response.SchoolResponse;
import cn.sedu.moon.controller.response.StudentArchiveResponse;
import cn.sedu.moon.controller.response.district.UserAddDistrictResponse;
import cn.sedu.moon.service.ArchiveService;
import cn.sedu.moon.service.SchoolService;
import cn.sedu.moon.service.context.ArchiveReportContext;
import cn.sedu.moon.service.context.QueryDistrictContext;
import cn.sedu.moon.service.context.QuerySchoolContext;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * 学生档案相关接口
 *
 * @author ${wukong}
 * @version $Id: ArchiveController.java, v 0.1 2018年12月10日 9:58 PM Exp $
 */
@RestController
public class ArchiveController extends BaseController {

    @Autowired
    private ArchiveService archiveService;

    @Autowired
    private SchoolService schoolService;

    /**
     * 查询档案信息
     *
     * @return
     */
    @RequestMapping(value = "/moon/archive/query", method = RequestMethod.GET)
    @Auth
    public Result queryArchive() {

        return template.execute(new ControllerCallback<StudentArchiveResponse>() {
            @Override
            public void checkParam() {
            }

            @Override
            public StudentArchiveResponse execute() {
                return archiveService.queryArchive(getUserId());
            }
        });
    }

    /**
     * 查询择校信息
     *
     * @return
     */
    @RequestMapping(value = "/moon/archive/chooseSchoolInfo", method = RequestMethod.GET)
    @Auth
    public Result queryChooseSchoolInfo() {

        return template.execute(new ControllerCallback<ArchiveChooseSchoolInfoResponse>() {
            @Override
            public void checkParam() {
            }

            @Override
            public ArchiveChooseSchoolInfoResponse execute() {
                return archiveService.queryChooseSchoolInfo(getUserId());
            }
        });
    }


    /**
     * 获取年级列表
     *
     * @return 年级列表
     */
    @RequestMapping(value = "/moon/grade/list", method = RequestMethod.GET)
    @Auth
    public Result gradeList() {

        return template.execute(new ControllerCallback<List<GradeResponse>>() {
            @Override
            public void checkParam() {
            }

            @Override
            public List<GradeResponse> execute() {
                return archiveService.queryGradeList();
            }
        });
    }

    /**
     * 按照关键字获取附近的学校列表
     * 关键字可为空
     *
     * @param request 请求参数
     * @return 包含学校ID和名称的学校列表
     */
    @Auth
    @RequestMapping(value = "/moon/school/nearby", method = RequestMethod.GET)
    public Result schoolNearby(SchoolNearbyRequest request) {

        return template.execute(new ControllerCallback<List<SchoolResponse>>() {

            QuerySchoolContext context = new QuerySchoolContext();

            @Override
            public void checkParam() {
                if (request.getOffset() == null || request.getPageSize() == null) {
                    throw new BizException(ErrorCodeEnum.PARAM_CHECK_EXP, "翻页参数不能为空");
                }
            }

            @Override
            public void buildContext() {
                context.setOffset(request.getOffset());
                context.setPageSize(request.getPageSize());
                context.setKeyName(request.getKeyName());
                context.setLat(request.getLat());
                context.setLnt(request.getLnt());
                context.setUserId(getUserId());
                context.setAreaCode(request.getAreaCode());

                if (request.getSchoolType() != null) {
                    SchoolTypeEnum schoolTypeEnum = SchoolTypeEnum.getByCode(request.getSchoolType());
                    if (schoolTypeEnum != null) {
                        context.setSchoolType(schoolTypeEnum);
                    }
                }
            }

            @Override
            public List<SchoolResponse> execute() {
                return schoolService.nearBy(context);
            }
        });
    }

    /**
     * 按照关键字获取附近的小区列表
     * 根据用户之前上报的地理位置区域信息获取小区列表信息
     *
     * 关键字可为空
     *
     * @param request 请求参数
     * @return 包含学校ID和名称的学校列表
     */
    @Auth
    @RequestMapping(value = "/moon/district/nearby", method = RequestMethod.GET)
    public Result districtNearby(DistrictNearbyRequest request) {

        return template.execute(new ControllerCallback<List<DistrictResponse>>() {

            QueryDistrictContext context = new QueryDistrictContext();

            @Override
            public void checkParam() {
                if (request.getOffset() == null || request.getPageSize() == null) {
                    throw new BizException(ErrorCodeEnum.PARAM_CHECK_EXP, "翻页参数不能为空");
                }
            }

            @Override
            public void buildContext() {
                context.setOffset(request.getOffset());
                context.setPageSize(request.getPageSize());
                context.setKeyName(request.getKeyName());
                context.setLat(request.getLat());
                context.setLnt(request.getLnt());
                context.setUserId(getUserId());
            }

            @Override
            public List<DistrictResponse> execute() {
                return archiveService.districtNearby(context);
            }
        });
    }

    /**
     * 按照关键字获取附近的小区列表
     * 根据用户之前上报的地理位置区域信息获取小区列表信息
     *
     * 关键字可为空
     *
     * @param request 请求参数
     * @return 包含学校ID和名称的学校列表
     */
    @Auth
    @RequestMapping(value = "/moon/district/nearby/area", method = RequestMethod.GET)
    public Result districtNearbyArea(DistrictNearbyRequest request) {

        return template.execute(new ControllerCallback<List<DistrictResponse>>() {

            QueryDistrictContext context = new QueryDistrictContext();

            @Override
            public void checkParam() {
                if (request.getOffset() == null || request.getPageSize() == null) {
                    throw new BizException(ErrorCodeEnum.PARAM_CHECK_EXP, "翻页参数不能为空");
                }

                if (request.getAreaCode() == null) {
                    throw new BizException(ErrorCodeEnum.PARAM_CHECK_EXP, "区编码不能为空");
                }
            }

            @Override
            public void buildContext() {
                context.setOffset(request.getOffset());
                context.setPageSize(request.getPageSize());
                context.setKeyName(request.getKeyName());
                context.setLat(request.getLat());
                context.setLnt(request.getLnt());
                context.setUserId(getUserId());
                context.setAreaCode(request.getAreaCode());
            }

            @Override
            public List<DistrictResponse> execute() {
                return archiveService.districtNearbyArea(context);
            }
        });
    }

    /**
     * 获取培养方向列表
     *
     * @return 培养方向列表
     */
    @Auth
    @RequestMapping(value = "/moon/intro/growDirection/list", method = RequestMethod.GET)
    public Result growDirectionList() {

        return template.execute(new ControllerCallback<QueryGrowDirectionResponse>() {

            @Override
            public void checkParam() {
            }

            @Override
            public QueryGrowDirectionResponse execute() {
                return archiveService.growDirectionList();
            }
        });
    }

    /**
     * 上报学生基本信息接口
     */
    @Auth
    @RequestMapping(value = "/moon/intro/reportBaseInfo", method = RequestMethod.POST)
    public Result reportBaseInfo(@RequestBody ArchiveReportRequest request) {

        return template.execute(new ControllerCallback<Boolean>() {

            ArchiveReportContext context = new ArchiveReportContext();

            @Override
            public void checkParam() {
                if (request.getGrade() == null) {
                    throw new BizException(ErrorCodeEnum.PARAM_CHECK_EXP, "年级信息不可为空");
                }

                if (request.getSchoolId() == null) {
                    throw new BizException(ErrorCodeEnum.PARAM_CHECK_EXP, "学校信息不能为空");
                }

                if (request.getGender() == null) {
                    throw new BizException(ErrorCodeEnum.PARAM_CHECK_EXP, "性别信息不能为空");
                }

                if (request.getDistrictId() == null) {
                    throw new BizException(ErrorCodeEnum.PARAM_CHECK_EXP, "小区信息不能为空");
                }
            }

            @Override
            public void buildContext() {
                context.setUserId(getUserId());
                context.setGrade(request.getGrade());
                context.setProvinceCode(request.getProvinceCode());
                context.setCityCode(request.getCityCode());
                context.setAreaCode(request.getAreaCode());
                context.setSchoolId(request.getSchoolId());
                context.setGender(request.getGender());
                context.setDistrictId(request.getDistrictId());
            }

            @Override
            public Boolean execute() {
                archiveService.reportBaseInfo(context);
                return true;
            }
        });
    }

    /**
     * 上报学生档案的接口
     *
     * @param request
     * @return
     */
    @Auth
    @RequestMapping(value = "/moon/intro/report", method = RequestMethod.POST)
    public Result report(@RequestBody ArchiveReportRequest request) {

        return template.execute(new ControllerCallback<Boolean>() {

            ArchiveReportContext context = new ArchiveReportContext();

            @Override
            public void checkParam() {

                //if (StringUtils.isBlank(request.getName())) {
                //    throw new BizException(ErrorCodeEnum.PARAM_CHECK_EXP, "学生姓名不能为空");
                //}

                //if (request.getGrade() == null) {
                //    throw new BizException(ErrorCodeEnum.PARAM_CHECK_EXP, "年级不能为空");
                //}
                //
                //if (request.getGender() == null) {
                //    throw new BizException(ErrorCodeEnum.PARAM_CHECK_EXP, "性别不能为空");
                //}

                if (request.getSchoolProperty() == null ) {
                    throw new BizException(ErrorCodeEnum.PARAM_CHECK_EXP, "学校公办民办选项不能为空");
                }

                if (request.getGrowDirectionList() == null || request.getGrowDirectionList().size() == 0) {
                   throw new BizException(ErrorCodeEnum.PARAM_CHECK_EXP, "培养方向不能为空");
                }

                //if (request.getDistrictId() == null) {
                //    throw new BizException(ErrorCodeEnum.PARAM_CHECK_EXP, "小区不能为空");
                //}

                //if (request.getSchoolId() == null) {
                //    throw new BizException(ErrorCodeEnum.PARAM_CHECK_EXP, "学校不能为空");
                //}
            }

            @Override
            public void buildContext() {
                context.setUserId(getUserId());
                context.setName(request.getName());
                context.setGrade(request.getGrade());
                context.setSchoolId(request.getSchoolId());
                context.setGender(request.getGender());
                context.setSchoolProperty(request.getSchoolProperty());
                context.setGrowDirectionList(request.getGrowDirectionList());
                context.setDistrictId(request.getDistrictId());
            }

            @Override
            public Boolean execute() {
                archiveService.report(context);
                return true;
            }
        });
    }

    /**
     * 获取档案问题的描述信息
     *
     * @return
     */
    @Auth
    @RequestMapping(value = "/moon/intro/getAnswerDesc", method = RequestMethod.GET)
    public Result getAnswerDesc() {

        return template.execute(new ControllerCallback<ArchiveDescResponse>() {


            @Override
            public void checkParam() {
            }

            @Override
            public void buildContext() {
            }

            @Override
            public ArchiveDescResponse execute() {
                //ArchiveDescResponse response = new ArchiveDescResponse();
                //response.setDistrictDesc("2018年4月份即将报读小学，从2011年开始，杭城小学迎来入学高峰，2018年报考人数预估将增长到5.6万，环比去年增长了3.4%，预估将有300人与您的孩子一起报考西湖区小学。");
                //response.setSchoolPropertyDesc("选择公办，则需在户籍所在地学校就近入学，要重点注意的是每年学区划分有可能会有所变动，并且有些热门学校的一表生也可能出现被调剂，学军小学今年82"
                //        + "人被调剂；选择民办，则需要进行考试，去年绿城育华报考报考比例2000：248；民办学费会相对较高一些，例如云谷小学每学期学费48000元");
                //response.setGrowDirectionDesc("不同学校培养孩子的方向会有所不同，\n"
                //        + "* 云谷强调以人为本，培养孩子多元化个性化\n"
                //        + "* 杭州上海世界外国语偏向培养孩子走向世界\n"
                //        + "* 大关小学偏向声乐培养，重视孩子多才多艺\n"
                //        + "另外，如果选择了双语版，国际版在面试时会包含英语口语部分，比如娃哈哈双语学校、绿城育华双语班！");
                return archiveService.getAnswerDesc(getUserId());
            }
        });
    }

    @RequestMapping(value = "/moon/district/userAdd", method = RequestMethod.POST)
    public Result districtUserAdd(@RequestBody UserAddDistrictRequest request) {

        return template.execute(new ControllerCallback<UserAddDistrictResponse>() {
            @Override
            public void checkParam() {

                if (StringUtils.isBlank(request.getName())) {
                    throw new BizException(ErrorCodeEnum.PARAM_CHECK_EXP, "小区名称不能为空");
                }

                if (StringUtils.isBlank(request.getAddress())) {
                    throw new BizException(ErrorCodeEnum.PARAM_CHECK_EXP, "小区地址不能为空");
                }

                if (StringUtils.isBlank(request.getAreaCode())) {
                    throw new BizException(ErrorCodeEnum.PARAM_CHECK_EXP, "小区所在区编码不能为空");
                }

                if (request.getLat() == null || request.getLng() == null) {
                    throw new BizException(ErrorCodeEnum.PARAM_CHECK_EXP, "小区经纬度不能为空");
                }
            }

            @Override
            public UserAddDistrictResponse execute() {
                return archiveService.userAddDistrict(request);
            }
        });
    }





}