package cn.sedu.moon.controller;

import cn.sedu.moon.common.Result;
import cn.sedu.moon.common.auth.Auth;
import cn.sedu.moon.common.template.ControllerCallback;
import cn.sedu.moon.controller.request.subscriber.CheckSubscribeBestWeekRequest;
import cn.sedu.moon.controller.request.subscriber.ListBestWeekRequest;
import cn.sedu.moon.controller.request.subscriber.SubscribeBestWeekRequest;
import cn.sedu.moon.controller.request.subscriber.UnsubscribeBestWeekRequest;
import cn.sedu.moon.controller.response.subscriber.CheckSubscribeBestWeekResponse;
import cn.sedu.moon.controller.response.subscriber.ListBestWeekResponse;
import cn.sedu.moon.service.SubscriberService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class SubscriberController extends BaseController {


    @Autowired
    private SubscriberService subscriberService;

    /**
     * 订阅每周精选
     *
     * @return
     */
    @RequestMapping(value = "/moon/subscriber/subscribeBestWeek", method = RequestMethod.POST)
    @Auth
    public Result subscribeBestWeek() {


        return template.execute(new ControllerCallback<Boolean>() {

            SubscribeBestWeekRequest request = new SubscribeBestWeekRequest();

            @Override
            public void checkParam() {
                request.setUserId(getUserId());
            }

            @Override
            public Boolean execute() {
                subscriberService.subscribeBestWeek(request);
                return true;
            }
        });
    }

    /**
     * 取消订阅每周精选
     *
     * @return
     */
    @RequestMapping(value = "/moon/subscriber/unsubscribeBestWeek", method = RequestMethod.POST)
    @Auth
    public Result unsubscribeBestWeek() {

        return template.execute(new ControllerCallback<Boolean>() {

            UnsubscribeBestWeekRequest request = new UnsubscribeBestWeekRequest();

            @Override
            public void checkParam() {
                request.setUserId(getUserId());
            }

            @Override
            public Boolean execute() {
                subscriberService.unsubscribeBestWeek(request);
                return true;
            }
        });
    }


    /**
     * 检查用户订阅每周精选状态
     *
     * @return CheckSubscribeBestWeekResponse
     */
    @RequestMapping(value = "/moon/subscriber/checkSubscribeBestWeek", method = RequestMethod.GET)
    @Auth
    public Result checkSubscribeBestWeek() {

        return template.execute(new ControllerCallback<CheckSubscribeBestWeekResponse>() {

            CheckSubscribeBestWeekRequest request = new CheckSubscribeBestWeekRequest();

            @Override
            public void checkParam() {
                request.setUserId(getUserId());
            }

            @Override
            public CheckSubscribeBestWeekResponse execute() {
                CheckSubscribeBestWeekResponse response = subscriberService.checkSubscribeBestWeek(request);
                return response;
            }
        });
    }


    /**
     * 获取每周精选文章列表
     *
     * @return
     */
    @RequestMapping(value = "/moon/subscriber/listBestWeek", method = RequestMethod.GET)
    @Auth
    public Result listBestWeek() {

        return template.execute(new ControllerCallback<List<ListBestWeekResponse>>() {

            ListBestWeekRequest request = new ListBestWeekRequest();

            @Override
            public void checkParam() {
                request.setUserId(getUserId());
            }

            @Override
            public List<ListBestWeekResponse> execute() {
                List<ListBestWeekResponse> response = subscriberService.listBestWeek(request);
                return response;
            }
        });
    }
}
