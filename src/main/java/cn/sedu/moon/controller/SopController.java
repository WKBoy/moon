/*
 * Dian.com Inc.
 * Copyright (c) 2004-2019 All Rights Reserved.
 */
package cn.sedu.moon.controller;

import cn.sedu.moon.common.Result;
import cn.sedu.moon.common.auth.Auth;
import cn.sedu.moon.common.enums.ErrorCodeEnum;
import cn.sedu.moon.common.exception.BizException;
import cn.sedu.moon.common.template.ControllerCallback;
import cn.sedu.moon.controller.request.SopAttentionRequest;
import cn.sedu.moon.service.SopService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 *
 * @author ${wukong}
 * @version $Id: SopController.java, v 0.1 2019年01月26日 3:57 PM Exp $
 */
@RestController
public class SopController extends BaseController {

    @Autowired
    private SopService sopService;

    @RequestMapping(value = "/moon/sop/attention", method = RequestMethod.POST)
    @Auth
    public Result attention(@RequestBody SopAttentionRequest request) {

        return template.execute(new ControllerCallback<Boolean>() {

            @Override
            public void checkParam() {
                if (request.getSopId() == null) {
                    throw new BizException(ErrorCodeEnum.PARAM_CHECK_EXP, "SopId不能为空");
                }
            }

            @Override
            public Boolean execute() {

                tracker();

                sopService.attention(request.getSopId(), getUserId());
                return true;
            }
        });
    }


}