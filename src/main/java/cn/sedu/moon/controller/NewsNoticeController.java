/*
 * Dian.com Inc.
 * Copyright (c) 2004-2019 All Rights Reserved.
 */
package cn.sedu.moon.controller;

import cn.sedu.moon.common.Result;
import cn.sedu.moon.common.auth.Auth;
import cn.sedu.moon.common.enums.ErrorCodeEnum;
import cn.sedu.moon.common.exception.BizException;
import cn.sedu.moon.common.template.ControllerCallback;
import cn.sedu.moon.controller.request.notice.NoticeSubscribeRequest;
import cn.sedu.moon.service.NewsNoticeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author wukong
 * @version : NewsNoticeController.java, v 0.1 2019年10月27日 12:52 上午 Exp $
 */
@RestController
public class NewsNoticeController extends BaseController {

    @Autowired
    private NewsNoticeService newsNoticeService;

    @RequestMapping(value = "/moon/news/notice/subscribe", method = RequestMethod.POST)
    @Auth
    public Result read(@RequestBody NoticeSubscribeRequest request) {

        return template.execute(new ControllerCallback<Boolean>() {

            @Override
            public void checkParam() {
                if (request.getNoticeId() == null) {
                    throw new BizException(ErrorCodeEnum.PARAM_CHECK_EXP, "noticeId不能为空");
                }
            }

            @Override
            public Boolean execute() {
                newsNoticeService.subscribeNotice(getUserId(), request.getNoticeId());
                return true;
            }
        });
    }
}