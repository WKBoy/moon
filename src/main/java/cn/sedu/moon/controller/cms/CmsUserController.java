/*
 * Dian.com Inc.
 * Copyright (c) 2004-2019 All Rights Reserved.
 */
package cn.sedu.moon.controller.cms;

import cn.sedu.moon.common.Result;
import cn.sedu.moon.common.auth.CmsAuth;
import cn.sedu.moon.common.template.ControllerCallback;
import cn.sedu.moon.controller.BaseController;
import cn.sedu.moon.controller.request.cms.user.CmsSearchUserRequest;
import cn.sedu.moon.controller.response.cms.user.CmsUserSearchResponse;
import cn.sedu.moon.controller.response.cms.user.CmsUserUvResponse;
import cn.sedu.moon.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author wukong
 * @version : CmsUserController.java, v 0.1 2019年09月22日 18:59 Exp $
 */
@RestController
public class CmsUserController extends BaseController {

    @Autowired
    private UserService userService;

    /**
     * 按条件搜索用户
     *
     * @param request
     * @return
     */
    @RequestMapping(value = "/moon/cms/user/search", method = RequestMethod.GET)
//    @CmsAuth
    public Result search(CmsSearchUserRequest request) {

        return template.execute(new ControllerCallback<CmsUserSearchResponse>() {
            @Override
            public void checkParam() {
                if (request.getGrade() == null || request.getGrade() == 0) {
                    request.setGrade(null);
                }
            }

            @Override
            public CmsUserSearchResponse execute() {
                request.setPageSize(4000);
                return userService.cmsSearch(request);
            }
        });
    }

    /**
     * 用户访问uv统计
     *
     * @return
     */
    @RequestMapping(value = "/moon/cms/user/uv", method = RequestMethod.GET)
    @CmsAuth
    public Result uv() {

        return template.execute(new ControllerCallback<CmsUserUvResponse>() {
            @Override
            public void checkParam() {
            }

            @Override
            public CmsUserUvResponse execute() {
                return userService.uv();
            }
        });
    }




}