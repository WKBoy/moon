/*
 * Dian.com Inc.
 * Copyright (c) 2004-2019 All Rights Reserved.
 */
package cn.sedu.moon.controller.cms;

import cn.sedu.moon.common.Result;
import cn.sedu.moon.common.auth.CmsAuth;
import cn.sedu.moon.common.enums.ErrorCodeEnum;
import cn.sedu.moon.common.exception.BizException;
import cn.sedu.moon.common.template.ControllerCallback;
import cn.sedu.moon.controller.BaseController;
import cn.sedu.moon.controller.request.cms.school.CmsSchoolSearchRequest;
import cn.sedu.moon.controller.request.cms.school.CmsSchoolSetHotRequest;
import cn.sedu.moon.controller.response.cms.school.CmsSearchSchoolResponse;
import cn.sedu.moon.service.SchoolService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author wukong
 * @version : CmsSchoolController.java, v 0.1 2019年09月28日 3:21 PM Exp $
 */
@RestController
public class CmsSchoolController extends BaseController {

    @Autowired
    private SchoolService schoolService;

    /**
     * 搜索学校
     *
     * @param request request
     * @return resposne
     */
    @RequestMapping(value = "/moon/cms/school/search", method = RequestMethod.GET)
    @CmsAuth
    public Result search(CmsSchoolSearchRequest request) {

            return template.execute(new ControllerCallback<CmsSearchSchoolResponse>() {
                @Override
                public void checkParam() {
                }

                @Override
                public CmsSearchSchoolResponse execute() {
                    return schoolService.cmsSearchSchool(request);
                }
            });
    }


    /**
     * 设置热门学校
     *
     * @param request request
     * @return resposne
     */
    @RequestMapping(value = "/moon/cms/school/setHot", method = RequestMethod.POST)
    @CmsAuth
    public Result setHot(@RequestBody CmsSchoolSetHotRequest request) {

        return template.execute(new ControllerCallback<Boolean>() {
            @Override
            public void checkParam() {
                if (request.getSchoolId() == null) {
                    throw new BizException(ErrorCodeEnum.PARAM_CHECK_EXP, "学校id不能为空");
                }

                if (request.getType() == null) {
                    throw new BizException(ErrorCodeEnum.PARAM_CHECK_EXP, "热门类型不能为空");
                }
            }

            @Override
            public Boolean execute() {
                schoolService.cmsSetHotSchool(request);
                return true;
            }
        });
    }



}