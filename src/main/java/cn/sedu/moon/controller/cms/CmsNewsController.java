/*
 * Dian.com Inc.
 * Copyright (c) 2004-2019 All Rights Reserved.
 */
package cn.sedu.moon.controller.cms;

import cn.sedu.moon.common.Result;
import cn.sedu.moon.common.auth.CmsAuth;
import cn.sedu.moon.common.enums.ErrorCodeEnum;
import cn.sedu.moon.common.exception.BizException;
import cn.sedu.moon.common.template.ControllerCallback;
import cn.sedu.moon.controller.BaseController;
import cn.sedu.moon.controller.request.CmsNewsQueryRequest;
import cn.sedu.moon.controller.request.cms.StickNewsRequest;
import cn.sedu.moon.controller.request.news.AddNewsRequest;
import cn.sedu.moon.controller.request.news.BatchDeleteNewsRequest;
import cn.sedu.moon.controller.request.news.DeleteNewsRequest;
import cn.sedu.moon.controller.request.news.NewsCheckRequest;
import cn.sedu.moon.controller.response.SchoolNewsCheckDetailResponse;
import cn.sedu.moon.controller.response.SchoolNewsCheckResponse;
import cn.sedu.moon.service.NewsService;
import cn.sedu.moon.service.context.StickNewsContext;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

/**
 * cms 管理后台新闻相关接口
 *
 * @author ${wukong}
 * @version $Id: CmsNewsController.java, v 0.1 2019年03月26日 9:16 PM Exp $
 */
@RestController
public class CmsNewsController extends BaseController {

    @Autowired
    private NewsService newsService;

    /**
     * 按条件查询新闻总数
     *
     * @param request
     * @return
     */
    @RequestMapping(value = "/moon/news/checkListTotal", method = RequestMethod.GET)
    @CmsAuth
    public Result checkListTotal(CmsNewsQueryRequest request) {

        return template.execute(new ControllerCallback<Integer>() {
            @Override
            public void checkParam() {
            }

            @Override
            public Integer execute() {
                return newsService.checkListTotal(request);
            }
        });
    }

    /**
     * 按条件查询新闻列表
     *
     * @param request
     * @return
     */
    @RequestMapping(value = "/moon/news/checkList", method = RequestMethod.GET)
    @CmsAuth
    public Result checkList(CmsNewsQueryRequest request) {

        return template.execute(new ControllerCallback<List<SchoolNewsCheckResponse>>() {
            @Override
            public void checkParam() {
                if (request.getOffset() == null || request.getPageSize() == null) {
                    throw new BizException(ErrorCodeEnum.PARAM_CHECK_EXP, "翻页参数不能为空");
                }
            }

            @Override
            public List<SchoolNewsCheckResponse> execute() {
                return newsService.checkList(request);
            }
        });
    }

    /**
     * 查询新闻详情
     *
     * @param request
     * @return
     */
    @RequestMapping(value = "/moon/news/check/detail", method = RequestMethod.GET)
    @CmsAuth
    public Result checkDetail(NewsCheckRequest request) {

        return template.execute(new ControllerCallback<SchoolNewsCheckDetailResponse>() {
            @Override
            public void checkParam() {
                if (request.getNewsId() == null) {
                    throw new BizException(ErrorCodeEnum.PARAM_CHECK_EXP, "新闻id不能为空");
                }
            }

            @Override
            public SchoolNewsCheckDetailResponse execute() {
                return newsService.checkDetail(request.getNewsId());
            }
        });
    }

    /**
     * 更新新闻
     *
     * @param request id, title, content 都不可为空
     * @return
     */
    @RequestMapping(value = "/moon/news/check/detail/update", method = RequestMethod.POST)
    @CmsAuth
    public Result detailUpdate(@RequestBody NewsCheckRequest request) {

        return template.execute(new ControllerCallback<Boolean>() {
            @Override
            public void checkParam() {
                if (request.getNewsId() == null) {
                    throw new BizException(ErrorCodeEnum.PARAM_CHECK_EXP, "新闻id不能为空");
                }

                if (request.getTitle() == null) {
                    throw new BizException(ErrorCodeEnum.PARAM_CHECK_EXP, "新闻title不能为空");
                }

                if (request.getContent() == null) {
                    throw new BizException(ErrorCodeEnum.PARAM_CHECK_EXP, "新闻内容不能为空");
                }

                if (request.getCityCode() == null) {
                    throw new BizException(ErrorCodeEnum.PARAM_CHECK_EXP, "cityCode不能为空");
                }

                if (request.isHasNotice()) {
                    if (StringUtils.isBlank(request.getNoticeTitle())) {
                        throw new BizException(ErrorCodeEnum.PARAM_CHECK_EXP, "通知标题不能为空");
                    }

                    if (request.getNoticeHour() == null) {
                        throw new BizException(ErrorCodeEnum.PARAM_CHECK_EXP, "通知时间小时不能为空");
                    }

                    if (request.getNoticeMin() == null) {
                        throw new BizException(ErrorCodeEnum.PARAM_CHECK_EXP, "通知时间分钟不能为空");
                    }

                    if (StringUtils.isBlank(request.getNoticeDay())) {
                        throw new BizException(ErrorCodeEnum.PARAM_CHECK_EXP, "通知day不能为空");
                    }
                }

            }

            @Override
            public Boolean execute() {
                request.setUserId(getUserId());
                newsService.updateDetail(request);
                return true;
            }
        });
    }

    /**
     * 删除新闻
     *
     * @param request
     * @return
     */
    @RequestMapping(value = "/moon/news/delete", method = RequestMethod.POST)
    @CmsAuth
    public Result delete(@RequestBody DeleteNewsRequest request) {

        return template.execute(new ControllerCallback<Boolean>() {

            @Override
            public void checkParam() {
                if (request.getId() == null) {
                    throw new BizException(ErrorCodeEnum.PARAM_CHECK_EXP, "新闻id不能为空");
                }
            }

            @Override
            public Boolean execute() {
                newsService.deleteNews(request.getId());
                return true;
            }
        });
    }

    /**
     * 批量删除新闻
     *
     * @param request 新闻id列表不可为空
     * @return
     */
    @RequestMapping(value = "/moon/news/batchDelete", method = RequestMethod.POST)
    @CmsAuth
    public Result batchDelete(@RequestBody BatchDeleteNewsRequest request) {

        return template.execute(new ControllerCallback<Boolean>() {

            List<Long> idList = new ArrayList<>();

            @Override
            public void checkParam() {
                if (StringUtils.isBlank(request.getIds())) {
                    throw new BizException(ErrorCodeEnum.PARAM_CHECK_EXP, "新闻id不能为空");
                }

                String[] strArray = request.getIds().split(",");
                for (int i = 0; i < strArray.length; i++) {
                    Long idLong = Long.parseLong(strArray[i]);
                    idList.add(idLong);
                }
            }

            @Override
            public Boolean execute() {
                newsService.batchDeleteNews(idList);
                return true;
            }
        });
    }

    /**
     * 添加新闻
     *
     * @param request title, content 均不可为空
     * @return
     */
    @RequestMapping(value = "/moon/news/add", method = RequestMethod.POST)
    @CmsAuth
    public Result add(@RequestBody AddNewsRequest request) {

        return template.execute(new ControllerCallback<Boolean>() {

            @Override
            public void checkParam() {
                if (StringUtils.isBlank(request.getTitle())) {
                    throw new BizException(ErrorCodeEnum.PARAM_CHECK_EXP, "标题不能为空");
                }

                if (StringUtils.isBlank(request.getContent())) {
                    throw new BizException(ErrorCodeEnum.PARAM_CHECK_EXP, "内容不能为空");
                }

                if (request.isHasNotice()) {
                    if (StringUtils.isBlank(request.getNoticeTitle())) {
                        throw new BizException(ErrorCodeEnum.PARAM_CHECK_EXP, "通知标题不能为空");
                    }

                    if (request.getNoticeHour() == null) {
                        throw new BizException(ErrorCodeEnum.PARAM_CHECK_EXP, "通知时间小时不能为空");
                    }

                    if (request.getNoticeMin() == null) {
                        throw new BizException(ErrorCodeEnum.PARAM_CHECK_EXP, "通知时间分钟不能为空");
                    }

                    if (StringUtils.isBlank(request.getNoticeDay())) {
                        throw new BizException(ErrorCodeEnum.PARAM_CHECK_EXP, "通知day不能为空");
                    }
                }

            }

            @Override
            public Boolean execute() {
                request.setUserId(getUserId());
                newsService.addNews(request);
                return true;
            }
        });
    }

    /**
     * 将新闻设置为置顶
     *
     * @param request
     * @return
     */
    @RequestMapping(value = "/moon/news/stick", method = RequestMethod.POST)
    @CmsAuth
    public Result stick(@RequestBody StickNewsRequest request) {

        return template.execute(new ControllerCallback<Boolean>() {

            StickNewsContext context = new StickNewsContext();

            @Override
            public void checkParam() {
                if (request.getNewsId() == null) {
                    throw new BizException(ErrorCodeEnum.PARAM_CHECK_EXP, "新闻ID不能为空");
                }
            }

            @Override
            public void buildContext() {
                context.setUserId(getUserId());
                context.setNewsId(request.getNewsId());
            }

            @Override
            public Boolean execute() {
                newsService.stickNews(context);
                return true;
            }
        });
    }

    /**
     * 将新闻取消置顶
     *
     * @param request
     * @return
     */
    @RequestMapping(value = "/moon/news/deleteStick", method = RequestMethod.POST)
    @CmsAuth
    public Result deleteStick(@RequestBody StickNewsRequest request) {

        return template.execute(new ControllerCallback<Boolean>() {

            StickNewsContext context = new StickNewsContext();

            @Override
            public void checkParam() {
                if (request.getNewsId() == null) {
                    throw new BizException(ErrorCodeEnum.PARAM_CHECK_EXP, "新闻ID不能为空");
                }
            }

            @Override
            public void buildContext() {
                context.setNewsId(request.getNewsId());
                context.setUserId(getUserId());
            }

            @Override
            public Boolean execute() {
                newsService.deleteStick(context);
                return true;
            }
        });
    }





}