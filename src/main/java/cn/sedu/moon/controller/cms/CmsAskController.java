/*
 * Dian.com Inc.
 * Copyright (c) 2004-2019 All Rights Reserved.
 */
package cn.sedu.moon.controller.cms;

import cn.sedu.moon.common.Result;
import cn.sedu.moon.common.auth.CmsAuth;
import cn.sedu.moon.common.enums.ErrorCodeEnum;
import cn.sedu.moon.common.exception.BizException;
import cn.sedu.moon.common.template.ControllerCallback;
import cn.sedu.moon.controller.BaseController;
import cn.sedu.moon.controller.request.AskPublishRequest;
import cn.sedu.moon.controller.request.cms.ask.CmsAskDetailRequest;
import cn.sedu.moon.controller.request.cms.ask.CmsAskReplyRequest;
import cn.sedu.moon.controller.request.cms.ask.CmsSearchAskRequest;
import cn.sedu.moon.controller.response.cms.ask.CmsAskDetailResponse;
import cn.sedu.moon.controller.response.cms.ask.CmsAskPublishResponse;
import cn.sedu.moon.controller.response.cms.ask.CmsSearchAskResponse;
import cn.sedu.moon.service.AskService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author ${wukong}
 * @version $Id: CmsAskController.java, v 0.1 2019年07月08日 1:34 PM Exp $
 */
@RestController
@RequestMapping("/cms/ask")
public class CmsAskController extends BaseController {

    @Autowired
    private AskService askService;

    /**
     * 按条件查询问答列表
     *
     * @param request
     * @return
     */
    @RequestMapping(value = "/search", method = RequestMethod.GET)
    @CmsAuth
    public Result search(CmsSearchAskRequest request) {

        return template.execute(new ControllerCallback<CmsSearchAskResponse>() {
            @Override
            public void checkParam() {
                if (request.getOffset() == null || request.getPageSize() == null) {
                    throw new BizException(ErrorCodeEnum.PARAM_CHECK_EXP, "翻页参数不能为空");
                }
            }

            @Override
            public CmsSearchAskResponse execute() {
                return askService.cmsSearch(request);
            }
        });
    }

    /**
     * 查找问题详情
     *
     * @param request
     * @return
     */
    @RequestMapping(value = "/detail", method = RequestMethod.GET)
    @CmsAuth
    public Result detail(CmsAskDetailRequest request) {

        return template.execute(new ControllerCallback<CmsAskDetailResponse>() {
            @Override
            public void checkParam() {
                if (request.getId() == null) {
                    throw new BizException(ErrorCodeEnum.PARAM_CHECK_EXP, "新闻id不能为空");
                }
            }

            @Override
            public CmsAskDetailResponse execute() {
                return askService.cmsDetail(request.getId());
            }
        });
    }


    /**
     * 添加问题
     *
     * @return response
     */
    @RequestMapping(value = "/add", method = RequestMethod.POST)
    @CmsAuth
    public Result add(@RequestBody AskPublishRequest request) {

        return template.execute(new ControllerCallback<CmsAskPublishResponse>() {
            @Override
            public void checkParam() {
                if (StringUtils.isBlank(request.getTitle())) {
                    throw new BizException(ErrorCodeEnum.PARAM_CHECK_EXP, "问题标题不能为空");
                }
            }

            @Override
            public CmsAskPublishResponse execute() {
                Long askId = askService.cmsPublish(request, getUserId());
                CmsAskPublishResponse response = new CmsAskPublishResponse();
                response.setId(askId);
                return response;
            }
        });
    }

    /**
     * 回答问题
     *
     * @param request
     * @return
     */
    @RequestMapping(value = "/reply", method = RequestMethod.POST)
    @CmsAuth
    public Result reply(@RequestBody CmsAskReplyRequest request) {

        return template.execute(new ControllerCallback<Boolean>() {
            @Override
            public void checkParam() {

                if (request.getId() == null) {
                    throw new BizException(ErrorCodeEnum.PARAM_CHECK_EXP, "问答id不能为空");
                }

                if (StringUtils.isBlank(request.getContent())) {
                    throw new BizException(ErrorCodeEnum.PARAM_CHECK_EXP, "回复内容不能为空");
                }

                request.setUserId(getUserId());
            }

            @Override
            public Boolean execute() {
                askService.cmsReply(request);
                return true;
            }
        });
    }

    /**
     * 删除问题
     *
     * @param request
     * @return
     */
    @RequestMapping(value = "/delete", method = RequestMethod.POST)
    @CmsAuth
    public Result delete(@RequestBody CmsAskReplyRequest request) {

        return template.execute(new ControllerCallback<Boolean>() {
            @Override
            public void checkParam() {

                if (request.getId() == null) {
                    throw new BizException(ErrorCodeEnum.PARAM_CHECK_EXP, "问答id不能为空");
                }
                request.setUserId(getUserId());
            }

            @Override
            public Boolean execute() {
                askService.cmsDelete(request.getId(), getUserId());
                return true;
            }
        });
    }


}