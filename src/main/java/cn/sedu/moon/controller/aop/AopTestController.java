/*
 * Dian.com Inc.
 * Copyright (c) 2004-2019 All Rights Reserved.
 */
package cn.sedu.moon.controller.aop;

import cn.sedu.moon.common.Result;
import cn.sedu.moon.common.template.ControllerCallback;
import cn.sedu.moon.controller.BaseController;
import cn.sedu.moon.service.MusicPlayer;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author wukong
 * @version : AopTestController.java, v 0.1 2019年10月22日 3:06 PM Exp $
 */
@RestController
@Slf4j
public class AopTestController extends BaseController {

    @Autowired
    private MusicPlayer musicPlayer;

    @RequestMapping(value = "/moon/aopTest", method = RequestMethod.GET)
    public Result aopTest() {

        return template.execute(new ControllerCallback<Boolean>() {
            @Override
            public void checkParam() {
            }

            @Override
            public Boolean execute() {
                log.info("AopTestController -- before musicPlayer.play");
                musicPlayer.play();
                log.info("AopTestController -- after musicPlayer.play");
                return true;
            }
        });
    }

    @RequestMapping(value = "/moon/aopTest/async", method = RequestMethod.GET)
    public Result aopTestAsync() {

        return template.execute(new ControllerCallback<Boolean>() {
            @Override
            public void checkParam() {
            }

            @Override
            public Boolean execute() {
                log.info("AopTestController -- before musicPlayer.play");
                musicPlayer.asyncPlay();
                log.info("AopTestController -- after musicPlayer.play");
                return true;
            }
        });
    }

    @RequestMapping(value = "/moon/aopTest/saveSong", method = RequestMethod.GET)
    public Result transaction() {

        return template.execute(new ControllerCallback<Boolean>() {
            @Override
            public void checkParam() {
            }

            @Override
            public Boolean execute() {
                log.info("AopTestController -- before musicPlayer.saveSong");
                musicPlayer.saveSong();
                log.info("AopTestController -- after musicPlayer.saveSong");

                return true;
            }
        });
    }

    @RequestMapping(value = "/moon/aopTest/saveSong/transaction", method = RequestMethod.GET)
    public Result saveSongTransaction() {

        return template.execute(new ControllerCallback<Boolean>() {
            @Override
            public void checkParam() {
            }

            @Override
            public Boolean execute() {
                log.info("AopTestController -- before musicPlayer.saveSongTransaction");
                musicPlayer.saveSongTransaction();
                log.info("AopTestController -- after musicPlayer.saveSongTransaction");

                return true;
            }
        });
    }
}