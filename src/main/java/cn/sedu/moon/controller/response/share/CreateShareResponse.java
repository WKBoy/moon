/*
 * Dian.com Inc.
 * Copyright (c) 2004-2019 All Rights Reserved.
 */
package cn.sedu.moon.controller.response.share;

import cn.sedu.moon.controller.response.BaseResponse;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 *
 * @author ${wukong}
 * @version $Id: CreateShareResponse.java, v 0.1 2019年04月14日 3:50 PM Exp $
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class CreateShareResponse extends BaseResponse {

    private static final long serialVersionUID = -6541613178502061667L;

    /**
     * 分享id
      */
    private Long shareId;

    /**
     * 是否需要分享
     */
    private Boolean needShared = false;

    /**
     * 新闻类型
     */
    private Integer type = 0;


    /**
     * 需要分享的总次数
     */
    private Integer needShareTotal = 0;

    /**
     * 已经分享的次数
     */
    private Integer shareCount = 0;

}