package cn.sedu.moon.controller.response.subscriber;

import cn.sedu.moon.controller.response.BaseResponse;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;
import java.util.List;

@EqualsAndHashCode(callSuper = true)
@Data
public class ListBestWeekResponse extends BaseResponse {

    private static final long serialVersionUID = -3968577026655968839L;

    /**
     * id
     */
    private Long id;

    /**
     * 标题
     */
    private String title;


    /**
     * 新闻发布者
     */
    private String authorName;


    /**
     * 是否已读
     */
    private Boolean read;

    /**
     * 创建时间
     */
    private String createTime;


    /**
     * 内容简介
     */
    private String brief;

    /**
     * 缩略图
     */
    private String thumbnail;



    /**
     * 是否收藏
     */
    private Boolean collect;

    private Boolean light;
}
