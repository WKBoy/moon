/*
 * Dian.com Inc.
 * Copyright (c) 2004-2019 All Rights Reserved.
 */
package cn.sedu.moon.controller.response.cms.ask;

import java.util.List;

import cn.sedu.moon.controller.response.BaseResponse;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 *
 * @author ${wukong}
 * @version $Id: CmsSearchAskResponse.java, v 0.1 2019年07月08日 1:39 PM Exp $
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class CmsSearchAskResponse extends BaseResponse {

    private static final long serialVersionUID = 8858037882459655832L;

    Integer total;

    List<CmsAskResponse> list;
}