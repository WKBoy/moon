/*
 * Dian.com Inc.
 * Copyright (c) 2004-2019 All Rights Reserved.
 */
package cn.sedu.moon.controller.response;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 *
 * @author ${wukong}
 * @version $Id: SchoolNewsCheckResponse.java, v 0.1 2019年02月15日 3:30 PM Exp $
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class SchoolNewsCheckResponse extends SchoolNewsResponse {

    private static final long serialVersionUID = -6178977802940520909L;

    private String detailUrl;

    /**
     * 来源
     */
    private String src;

    /**
     * 缩略图
     */
    private String thumbnail;

    /**
     * 编辑者
     */
    private String editor;

    /**
     * 是否置顶
     */
    private Integer stick;

    /**
     * 上次操作置顶的人
     */
    private String sticker;

    /**
     * 阅读量
     */
    private Integer readCount;

    /**
     * 尝试推送量
     */
    private Integer noticeCount;

    /**
     * 推送成功量
     */
    private Integer noticeSuccessCount;

}