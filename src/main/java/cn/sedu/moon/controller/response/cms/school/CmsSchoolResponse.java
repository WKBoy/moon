/*
 * Dian.com Inc.
 * Copyright (c) 2004-2019 All Rights Reserved.
 */
package cn.sedu.moon.controller.response.cms.school;

import cn.sedu.moon.controller.response.BaseResponse;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author wukong
 * @version : CmsSearchSchoolResponse.java, v 0.1 2019年09月28日 3:25 PM Exp $
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class CmsSchoolResponse extends BaseResponse {

    private static final long serialVersionUID = -3360603265257399165L;

    private Long schoolId;

    private String schoolName;

    private String provinceName;

    private String cityName;

    private String areaName;

    /**
     * 学校类型，如 小学、中学
     */
    private String typeString;

    /**
     * 是否为市热门
     */
    private Boolean cityHot;

    /**
     * 是否为区热门
     */
    private Boolean areaHot;

}