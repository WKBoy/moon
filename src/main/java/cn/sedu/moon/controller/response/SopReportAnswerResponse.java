/*
 * Dian.com Inc.
 * Copyright (c) 2004-2019 All Rights Reserved.
 */
package cn.sedu.moon.controller.response;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 *
 * @author ${wukong}
 * @version $Id: SopReportAnswerResponse.java, v 0.1 2019年01月26日 5:18 PM Exp $
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class SopReportAnswerResponse extends BaseResponse {

    private static final long serialVersionUID = 1500389175416050828L;

    /**
     * SOP问题的关注人数
     */
    private Integer followCount;

    /**
     * 是否获取到了勋章
     */
    private Boolean isGetMedal;

    /**
     * 勋章标题
     */
    private String medalTitle;

    /**
     * 获取到的荣誉值数
     */
    private Integer gloryNum;


}