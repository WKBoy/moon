/*
 * Dian.com Inc.
 * Copyright (c) 2004-2019 All Rights Reserved.
 */
package cn.sedu.moon.controller.response.cms.school;

import cn.sedu.moon.controller.response.BaseResponse;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

/**
 * @author wukong
 * @version : CmsSearchSchoolResponse.java, v 0.1 2019年09月28日 3:30 PM Exp $
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class CmsSearchSchoolResponse extends BaseResponse {

    private static final long serialVersionUID = -3360603265257399165L;

    private Integer total;

    private List<CmsSchoolResponse> list;
}