/*
 * Dian.com Inc.
 * Copyright (c) 2004-2018 All Rights Reserved.
 */
package cn.sedu.moon.controller.response;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 *
 * @author ${wukong}
 * @version $Id: AreaNode.java, v 0.1 2018-04-11 16:25 Exp $
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class AreaNode extends BaseResponse {
    /** serialVersionUID */
    private static final long serialVersionUID = -7512541121043445618L;
    /**
     * 地区名
     */
    private String areaCode;
    /**
     * 地区编码
     */
    private String areaName;

    /**
     * 是否为叶子节点
     */
    private Boolean isLeaf;
}