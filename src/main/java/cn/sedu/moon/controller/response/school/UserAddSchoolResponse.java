/*
 * Dian.com Inc.
 * Copyright (c) 2004-2019 All Rights Reserved.
 */
package cn.sedu.moon.controller.response.school;

import cn.sedu.moon.controller.response.BaseResponse;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author wukong
 * @version : UserAddSchoolResponse.java, v 0.1 2019年09月02日 13:45 Exp $
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class UserAddSchoolResponse extends BaseResponse {

    private static final long serialVersionUID = 2646864942330499373L;

    private Long id;
}