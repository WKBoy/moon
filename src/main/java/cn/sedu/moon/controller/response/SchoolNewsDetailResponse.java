/*
 * Dian.com Inc.
 * Copyright (c) 2004-2018 All Rights Reserved.
 */
package cn.sedu.moon.controller.response;

import lombok.Data;

import java.util.List;

/**
 *
 * @author ${wukong}
 * @version $Id: SchoolNewsDetailResponse.java, v 0.1 2018年12月16日 6:22 PM Exp $
 */
@Data
public class SchoolNewsDetailResponse {

    /**
     * id
     */
    private Long id;

    /**
     * 标题
     */
    private String title;

    /**
     * tag 列表
     */
    private List<String> tagList;

    /**
     * 图片 列表
     */
    private List<String> imgList;

    /**
     * 新闻来源
     */
    private String authorName;

    /**
     * 是否关注 TODO
     */
    private Boolean follow;

    /**
     * 创建时间
     */
    private String createTime;

    /**
     * 详细内容
     */
    private String content;

    /**
     * 是否已收藏
     */
    private Boolean collect;

    /**
     * 是否需要点亮
     */
    private Boolean light = false;


    /**
     * 是否需要分享
     */
    private Boolean needShared = false;

    /**
     * 新闻类型
     */
    private Integer type = 0;


    /**
     * 需要分享的总次数
     */
    private Integer needShareTotal = 0;

    /**
     * 已经分享的次数
     */
    private Integer shareCount = 0;

    private Long shareId;

    /**
     * 文章相关的问答列表
     */
    private List<AskResponse> askList;

    /**
     * 是否有通知
     */
    private boolean hasNotice = false;

    /**
     * 通知信息
     */
    private NewsNoticeResponse newsNotice;

    /**
     * 用户是否订阅了通知
     */
    private boolean subscribedNotice = false;

    private boolean isShared = false;


}