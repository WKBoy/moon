/*
 * Dian.com Inc.
 * Copyright (c) 2004-2019 All Rights Reserved.
 */
package cn.sedu.moon.controller.response.share;

import cn.sedu.moon.common.constant.BizConstants;
import cn.sedu.moon.controller.response.BaseResponse;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

/**
 *
 * @author ${wukong}
 * @version $Id: LightCheckResponse.java, v 0.1 2019年07月15日 9:57 PM Exp $
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class LightCheckResponse extends BaseResponse {

    private static final long serialVersionUID = -2481120413284608779L;

    private String shareUserName;

    /**
     * 需要点亮才能看
     */
    private Boolean needLight = true;

    /**
     * 点亮的文章需要多少人点亮才能看
     */
    private Integer maxLightCount = BizConstants.NEWS_LIGHT_USER_COUNT;

    /**
     * 当前用户是否点亮过
     */
    private Boolean light = false;

    private List<LightUserResponse> userAvatarList;
}