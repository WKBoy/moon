/*
 * Dian.com Inc.
 * Copyright (c) 2004-2019 All Rights Reserved.
 */
package cn.sedu.moon.controller.response.share;

import cn.sedu.moon.common.enums.NewsTypeEnum;
import cn.sedu.moon.controller.response.BaseResponse;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author wukong
 * @version : CheckShareResponse.java, v 0.1 2019年08月06日 16:31 Exp $
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class CheckShareResponse extends BaseResponse {

    /**
     * 新闻类型
     */
    private NewsTypeEnum newsType;

    /**
     * 是否需要分享才能看
     */
    private Boolean isNeedShare;

    /**
     * 需要分享的总次数
     */
    private Integer needShareTotal;

    /**
     * 已经分享的次数
     */
    private Integer shareCount;

    private boolean isShared = false;
}