/*
 * Dian.com Inc.
 * Copyright (c) 2004-2019 All Rights Reserved.
 */
package cn.sedu.moon.controller.response;

import java.util.List;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 *
 * @author ${wukong}
 * @version $Id: SearchCollectNewsResponse.java, v 0.1 2019年06月23日 12:38 PM Exp $
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class SearchCollectNewsResponse extends BaseResponse {

    private static final long serialVersionUID = -4366075517591995885L;

    private Integer total;

    private List<SchoolNewsResponse> list;
}