/*
 * Dian.com Inc.
 * Copyright (c) 2004-2019 All Rights Reserved.
 */
package cn.sedu.moon.controller.response.share;

import cn.sedu.moon.controller.response.BaseResponse;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 *
 * @author ${wukong}
 * @version $Id: LightNewsResponse.java, v 0.1 2019年07月21日 9:30 AM Exp $
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class LightNewsResponse extends BaseResponse {

    private static final long serialVersionUID = -1521185904408080782L;

    private String name;

    private Long shareUserName;

    /**
     * 用户头像
     */
    private String src;

    private Long id;

}