/*
 * Dian.com Inc.
 * Copyright (c) 2004-2019 All Rights Reserved.
 */
package cn.sedu.moon.controller.response.cms.user;

import lombok.Data;

import java.io.Serializable;

/**
 * @author wukong
 * @version : CmsUserUvResponse.java, v 0.1 2019年10月12日 3:01 PM Exp $
 */
@Data
public class CmsUserUvResponse implements Serializable {

    private static final long serialVersionUID = -3299224099999841362L;

    /**
     * 周访问uv
     */
    private Integer weekUv;
}