/*
 * Dian.com Inc.
 * Copyright (c) 2004-2019 All Rights Reserved.
 */
package cn.sedu.moon.controller.response;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 *
 * @author ${wukong}
 * @version $Id: AskNewsResponse.java, v 0.1 2019年01月26日 2:54 PM Exp $
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class AskNewsResponse extends NewsResponse {

    private static final long serialVersionUID = -7891910470142290415L;

    /**
     * 问答id
     */
    private Long askId;

    /**
     * 问答标题
     */
    private String title;

    /**
     * 关注者数量
     */
    private Integer followCount;

    /**
     * 回答者数量
     */
    private Integer answerCount;

    /**发布者头像
     *
     */
    private String avatar;

    /**
     * 发布者昵称
     */
    private String nickName;


    private Boolean isFollow;
}