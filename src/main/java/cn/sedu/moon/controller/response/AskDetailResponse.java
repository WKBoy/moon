/*
 * Dian.com Inc.
 * Copyright (c) 2004-2019 All Rights Reserved.
 */
package cn.sedu.moon.controller.response;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

/**
 *
 * @author ${wukong}
 * @version $Id: AskDetailResponse.java, v 0.1 2019年01月19日 11:44 PM Exp $
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class AskDetailResponse extends BaseResponse {

    private static final long serialVersionUID = -1369456928100665987L;

    /**
     * 问题id
     */
    private Long id;

    /**
     * 用户是否为该问题的发布者
     */
    private Boolean isOwner;

    /**
     * 问题标题
     */
    private String title;

    /**
     * 问题详情
     */
    private String content;

    /**
     * 关注者数量
     */
    private Integer followCount;

    /**
     * 回答者数量
     */
    private Integer answerCount;

    /**
     * 是否关注
     */
    private Boolean isFollow;

    /**
     * 标签
     */
    private List<String> tags;

    private Boolean collect;

}