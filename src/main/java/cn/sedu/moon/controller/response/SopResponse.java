/*
 * Dian.com Inc.
 * Copyright (c) 2004-2019 All Rights Reserved.
 */
package cn.sedu.moon.controller.response;

import java.util.List;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 *
 * @author ${wukong}
 * @version $Id: SopResponse.java, v 0.1 2019年01月06日 6:15 PM Exp $
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class SopResponse extends NewsResponse {

    private static final long serialVersionUID = 3554083678148108236L;

    /**
     * sop 问题id
     */
    private Integer id;

    /**
     * 学校id
     */
    private Long schoolId;

    /**
     * sop 问题标题
     */
    private String title;

    /**
     * 问题类型
     * 1 单选型
     * 2 多选型
     * 3 文本输入型
     * 4 单选型 + 文本输入型
     * 5 多选型 + 文本输入型
     */
    private Integer sopType;

    private List<SopOption> optionList;

    /**
     * 关注人数
     */
    private Integer followCount;

    /**
     * 是否已经关注
     */
    private Boolean  isFollow;
}