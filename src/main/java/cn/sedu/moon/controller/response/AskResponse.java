/*
 * Dian.com Inc.
 * Copyright (c) 2004-2019 All Rights Reserved.
 */
package cn.sedu.moon.controller.response;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 *
 * @author ${wukong}
 * @version $Id: AskResponse.java, v 0.1 2019年01月19日 11:42 PM Exp $
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class AskResponse extends BaseResponse {

    private static final long serialVersionUID = 4659449268401183793L;

    /**
     * 问题id
     */
    private Long id;

    /**
     * 问题标题
     */
    private String title;

    /**
     * 关注者数量
     */
    private Integer followCount;

    /**
     * 回答者数量
     */
    private Integer answerCount;

}