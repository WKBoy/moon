package cn.sedu.moon.controller.response.census;

import cn.sedu.moon.controller.response.BaseResponse;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author lix
 * @version 1.0
 * @date 2019/12/3 23:19
 * @describe 文章编辑情况
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class EditCensusResponse extends BaseResponse {

    private static final long serialVersionUID = -701256128020039870L;

    /**
     * 新建文章
     */
    private Integer newNewsCount;

    /**
     * 原创文章
     */
    private Integer originalNewsCount;

    /**
     * 精编文章
     */
    private Integer editWellNewsCount;
}
