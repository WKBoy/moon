/*
 * Dian.com Inc.
 * Copyright (c) 2004-2019 All Rights Reserved.
 */
package cn.sedu.moon.controller.response.cms.user;

import lombok.Data;

import java.io.Serializable;

/**
 * @author wukong
 * @version : CmsUserSearchItemResponse.java, v 0.1 2019年09月22日 19:09 Exp $
 */
@Data
public class CmsUserSearchItemResponse implements Serializable {

    private static final long serialVersionUID = 9125565813575142721L;

    private Long userId;

    /**
     * 用户昵称
     */
    private String nickName;

    /**
     * 读书阶段
     */
    private String grade;

    private String province;

    private String city;

    /**
     * 所在地区
     */
    private String area;

    /**
     * 手机号
     */
    private String mobile;

    /**
     * 最近访问时间
     */
    private String lastLoginTime;

    private long lastLoginTimeLongValue;

}