/*
 * Dian.com Inc.
 * Copyright (c) 2004-2019 All Rights Reserved.
 */
package cn.sedu.moon.controller.response.school;

import cn.sedu.moon.controller.response.BaseResponse;
import cn.sedu.moon.controller.response.SopResponse;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

/**
 * @author wukong
 * @version : SchoolTabResponse.java, v 0.1 2019年10月07日 3:43 PM Exp $
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class SchoolTabResponse extends BaseResponse {

    private static final long serialVersionUID = -7490521257855779784L;

    private Long schoolId;

    private String img;

    private String name;

    private List<String> tags;

    private String areaName;

    private String address;

    private Integer followCount;

    private Integer commentCount;

    private String latestComment;

    private String distance;

    private SopResponse sopResponse;

    private String readTag;
}