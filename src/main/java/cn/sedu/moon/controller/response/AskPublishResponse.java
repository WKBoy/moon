/*
 * Dian.com Inc.
 * Copyright (c) 2004-2019 All Rights Reserved.
 */
package cn.sedu.moon.controller.response;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 *
 * @author ${wukong}
 * @version $Id: AskPublishResponse.java, v 0.1 2019年01月26日 12:24 PM Exp $
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class AskPublishResponse extends BaseResponse {

    private static final long serialVersionUID = -2213931337043839881L;

    /**
     * 问题id
     */
    Long id;
}