/*
 * Dian.com Inc.
 * Copyright (c) 2004-2018 All Rights Reserved.
 */
package cn.sedu.moon.controller.response;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 小区的 response
 *
 * @author ${wukong}
 * @version $Id: DistrictResponse.java, v 0.1 2018年12月11日 1:20 PM Exp $
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class DistrictResponse extends BaseResponse {

    private static final long serialVersionUID = -7516314512369666006L;

    /**
     * 小区ID
     */
    private Long districtId;

    /**
     * 小区名称
     */
    private String name;
}