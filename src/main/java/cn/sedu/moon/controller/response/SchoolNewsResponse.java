/*
 * Dian.com Inc.
 * Copyright (c) 2004-2018 All Rights Reserved.
 */
package cn.sedu.moon.controller.response;

import java.util.List;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 首页查询学校新闻的response
 *
 * @author ${wukong}
 * @version $Id: SchoolNewsResponse.java, v 0.1 2018年12月07日 3:19 PM Exp $
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class SchoolNewsResponse extends NewsResponse {

    private static final long serialVersionUID = 2822274877121298722L;

    /**
     * id
     */
    private Long id;

    /**
     * 标题
     */
    private String title;

    /**
     * tag 列表
     */
    private List<String> tagList;

    /**
     * 图片 列表
     */
    private List<String> imgList;

    /**
     * 新闻来源
     */
    private String authorName;

    /**
     * 是否关注 TODO
     */
    private Boolean follow;

    /**
     * 是否已读
     */
    private Boolean read;

    /**
     * 创建时间
     */
    private String createTime;

    /**
     * 更新时间
     */
    private String updateTime;

    /**
     * 内容简介
     */
    private String brief;

    /**
     * 缩略图
     */
    private String thumbnail;

    /**
     * 作者头像
     */
    private String authorAvatar;

    /**
     * 是否收藏
     */
    private Boolean collect;

    private Boolean light;

}