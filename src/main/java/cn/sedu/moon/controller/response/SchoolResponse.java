/*
 * Dian.com Inc.
 * Copyright (c) 2004-2018 All Rights Reserved.
 */
package cn.sedu.moon.controller.response;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 学校response
 *
 * @author ${wukong}
 * @version $Id: SchoolResponse.java, v 0.1 2018年12月11日 1:05 PM Exp $
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class SchoolResponse extends BaseResponse {

    private static final long serialVersionUID = -5850159502339116384L;

    /**
     * 学校ID
     */
    private Long schoolId;

    /**
     * 学校名称
     */
    private String name;

    /**
     * 学校类型
     */
    private Integer type;
}