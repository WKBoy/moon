/*
 * Dian.com Inc.
 * Copyright (c) 2004-2018 All Rights Reserved.
 */
package cn.sedu.moon.controller.response;

import java.util.List;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 查询学校详情的response
 *
 * @author ${wukong}
 * @version $Id: SchoolDetailResponse.java, v 0.1 2018年12月14日 6:25 PM Exp $
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class SchoolDetailResponse extends BaseResponse {

    private static final long serialVersionUID = 8778951248438501016L;

    /**
     * 学校ID
     */
    private Long schoolId;

    /**
     * 学校名称
     */
    private String name;

    /**
     * 学校介绍信息
     */
    private String desc;

    /**
     * 学校的详细地址
     */
    private String address;

    /**
     * 学校特征，省重点
     */
    private String attribute;

    /**
     * 小学，中学
     */
    private String type;

    /**
     * 公办，还是民办
     */
    private String property;

    /**
     * 电话号码
     */
    private String tel;

    /**
     * 学校标签
     */
    private List<String> tagList;
}