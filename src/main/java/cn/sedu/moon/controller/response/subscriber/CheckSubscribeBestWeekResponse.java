package cn.sedu.moon.controller.response.subscriber;

import cn.sedu.moon.controller.response.BaseResponse;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;

@EqualsAndHashCode(callSuper = true)
@Data
public class CheckSubscribeBestWeekResponse extends BaseResponse {


    /**
     * 订阅状态
     */
    private int subscribeStatus;


    /**
     * 订阅时间
     */
    private Date subscribeTime;


}
