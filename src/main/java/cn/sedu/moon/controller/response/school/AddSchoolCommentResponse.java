/*
 * Dian.com Inc.
 * Copyright (c) 2004-2019 All Rights Reserved.
 */
package cn.sedu.moon.controller.response.school;

import lombok.Data;

import java.io.Serializable;

/**
 * @author wukong
 * @version : AddSchoolCommentResponse.java, v 0.1 2019年10月08日 7:02 PM Exp $
 */
@Data
public class AddSchoolCommentResponse implements Serializable {

    private static final long serialVersionUID = -3100095411814562024L;

    private Long commentId;
}