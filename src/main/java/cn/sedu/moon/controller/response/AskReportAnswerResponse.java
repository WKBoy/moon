/*
 * Dian.com Inc.
 * Copyright (c) 2004-2019 All Rights Reserved.
 */
package cn.sedu.moon.controller.response;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 *
 * @author ${wukong}
 * @version $Id: AskReportAnswerResponse.java, v 0.1 2019年01月29日 8:25 PM Exp $
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class AskReportAnswerResponse extends BaseResponse {

    private static final long serialVersionUID = -1755399636373615178L;

    /**
     * 是否获取到了勋章
     */
    private Boolean isGetMedal;

    /**
     * 勋章标题
     */
    private String medalTitle;
}