/*
 * Dian.com Inc.
 * Copyright (c) 2004-2019 All Rights Reserved.
 */
package cn.sedu.moon.controller.response;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 *
 * @author ${wukong}
 * @version $Id: QueryGloryResponse.java, v 0.1 2019年04月01日 7:45 PM Exp $
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class QueryGloryResponse extends BaseResponse {

    private static final long serialVersionUID = 6091054367919527654L;

    /**
     * 荣誉值
     */
    private Long glory;
}