/*
 * Dian.com Inc.
 * Copyright (c) 2004-2018 All Rights Reserved.
 */
package cn.sedu.moon.controller.response;

import java.util.List;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 *
 * @author ${wukong}
 * @version $Id: SchoolRecommendResponse.java, v 0.1 2018年12月16日 11:01 AM Exp $
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class SchoolRecommendResponse extends BaseResponse {

    private static final long serialVersionUID = 970515282881626001L;

    /**
     * 学校id
     */
    private Long schoolId;

    /**
     * 学校名称
     */
    private String schoolName;

    /**
     * 学校标签
     */
    private List<String> tags;

    /**
     * 学校图片
     */
    private List<String> images;

    /**
     * 是否有详情页
     */
    private Boolean hasDetail;

}