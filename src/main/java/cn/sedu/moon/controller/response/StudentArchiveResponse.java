/*
 * Dian.com Inc.
 * Copyright (c) 2004-2019 All Rights Reserved.
 */
package cn.sedu.moon.controller.response;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 *
 * @author ${wukong}
 * @version $Id: StudentArchiveResponse.java, v 0.1 2019年01月26日 5:42 PM Exp $
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class StudentArchiveResponse extends BaseResponse {

    private static final long serialVersionUID = 7250652077328481941L;

    /**
     * 年级
     */
    private String grade;

    private Long gradeId;

    /**
     * 学校名称
     */
    private String schoolName;

    private Long schoolId;

    /**
     * 小区名称
     */
    private String districtName;

    private Long districtId;

    /**
     * 性别
     */
    private Integer gender;

    /**
     * 省
     */
    private String provinceName;

    private String provinceCode;
    /**
     * 市
     */
    private String cityName;

    private String cityCode;

    /**
     * 区
     */
    private String areaName;

    private String areaCode;
}