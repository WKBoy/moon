/*
 * Dian.com Inc.
 * Copyright (c) 2004-2019 All Rights Reserved.
 */
package cn.sedu.moon.controller.response;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 *
 * @author ${wukong}
 * @version $Id: SchoolNewsCheckDetailResponse.java, v 0.1 2019年02月17日 4:04 PM Exp $
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class SchoolNewsCheckDetailResponse extends BaseResponse {

    private static final long serialVersionUID = 6837934829512486532L;

    private Long id;

    private String title;

    private String content;

    private String cityCode;

    private String thumbnail;

    private String brief;

    private Integer type;

    private String askIdListString;

    private boolean hasNotice;

    private NewsNoticeResponse notice;
}