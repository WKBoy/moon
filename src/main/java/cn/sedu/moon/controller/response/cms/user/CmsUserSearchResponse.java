/*
 * Dian.com Inc.
 * Copyright (c) 2004-2019 All Rights Reserved.
 */
package cn.sedu.moon.controller.response.cms.user;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @author wukong
 * @version : CmsUserSearchResponse.java, v 0.1 2019年09月22日 19:09 Exp $
 */
@Data
public class CmsUserSearchResponse implements Serializable {

    private static final long serialVersionUID = 3604253717950630511L;

    private Integer total;

    private List<CmsUserSearchItemResponse> list;
}