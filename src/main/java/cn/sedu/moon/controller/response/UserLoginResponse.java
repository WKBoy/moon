/*
 * Dian.com Inc.
 * Copyright (c) 2004-2018 All Rights Reserved.
 */
package cn.sedu.moon.controller.response;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 用户登陆接口的response
 *
 * @author ${wukong}
 * @version $Id: UserLoginResponse.java, v 0.1 2018年12月10日 2:36 PM Exp $
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class UserLoginResponse extends BaseResponse {

    private static final long serialVersionUID = -7954464137784008539L;

    /**
     * 登陆成功后的token
     */
    private String token;

    private Long userId;

    private String nickName;

    /**
     * 是否上报过档案
     */
    private Boolean reportArchive;

    /**
     * 是否是新的用户
     */
    private Boolean newUser;

    /**
     * 是否已经授权过手机号
     */
    private Boolean hasMobile = false;

    /**
     * 是否进行过微信授权
     */
    private boolean hasWxAuth = false;
}