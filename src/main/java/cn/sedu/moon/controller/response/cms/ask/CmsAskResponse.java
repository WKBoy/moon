/*
 * Dian.com Inc.
 * Copyright (c) 2004-2019 All Rights Reserved.
 */
package cn.sedu.moon.controller.response.cms.ask;

import cn.sedu.moon.controller.response.BaseResponse;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 *
 * @author ${wukong}
 * @version $Id: CmsAskResponse.java, v 0.1 2019年07月08日 1:38 PM Exp $
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class CmsAskResponse extends BaseResponse {

    private static final long serialVersionUID = 8749549465903116918L;

    /**
     * 问题id
     */
    private Long id;

    /**
     * 问题标题
     */
    private String title;

    /**
     * 关注者数量
     */
    private Integer followCount;

    /**
     * 回答者数量
     */
    private Integer answerCount;

    /**
     * 提问者的ID
     */
    private Long userId;

    /**
     * 提问者的昵称
     */
    private String username;

    /**
     * 提问时间
     */
    private String createTime;
}