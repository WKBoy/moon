/*
 * Dian.com Inc.
 * Copyright (c) 2004-2018 All Rights Reserved.
 */
package cn.sedu.moon.controller.response;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 *
 * @author ${wukong}
 * @version $Id: ArchiveDescResponse.java, v 0.1 2018年12月30日 4:58 PM Exp $
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class ArchiveDescResponse extends BaseResponse {

    private static final long serialVersionUID = 2087305439093252657L;

    /**
     * 各区域报考竞争情况
     */
    private String districtDesc;

    /**
     * 公办民办差异等等
     */
    private String schoolPropertyDesc;

    /**
     * 孩子未来培养方向
     */
    private String growDirectionDesc;
}