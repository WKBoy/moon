/*
 * Dian.com Inc.
 * Copyright (c) 2004-2019 All Rights Reserved.
 */
package cn.sedu.moon.controller.response;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author wukong
 * @version : SchoolTabAskResponse.java, v 0.1 2019年10月28日 9:43 下午 Exp $
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class SchoolTabAskResponse extends BaseResponse {

    private static final long serialVersionUID = 8158971726391414829L;

    /**
     * 问题id
     */
    private Long askId;

    private Integer newsType = 3;

    /**
     * 问题标题
     */
    private String title;

    /**
     * 关注者数量
     */
    private Integer followCount;

    /**
     * 回答者数量
     */
    private Integer answerCount;
}