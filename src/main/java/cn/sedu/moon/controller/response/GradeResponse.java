/*
 * Dian.com Inc.
 * Copyright (c) 2004-2018 All Rights Reserved.
 */
package cn.sedu.moon.controller.response;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 获取年级列表的response
 *
 * @author ${wukong}
 * @version $Id: GradeResponse.java, v 0.1 2018年12月10日 10:01 PM Exp $
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class GradeResponse extends BaseResponse {

    private static final long serialVersionUID = 8537312470839127816L;

    /**
     * 年级id
     */
    private Long id;



    /**
     * 标题文字，如三年级
     */
    private String title;

}