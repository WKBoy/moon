/*
 * Dian.com Inc.
 * Copyright (c) 2004-2019 All Rights Reserved.
 */
package cn.sedu.moon.controller.response;

import java.util.List;

import cn.sedu.moon.dao.model.GrowDirection;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 *
 * @author ${wukong}
 * @version $Id: ArchiveChooseSchoolInfoResponse.java, v 0.1 2019年01月26日 5:58 PM Exp $
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class ArchiveChooseSchoolInfoResponse extends BaseResponse {

    private static final long serialVersionUID = 6384351201909394574L;


    private Long districtId;

    /**
     * 住宅地址
     */
    private String districtName;

    /**
     * 学校属性
     */
    private Integer schoolProperty;

    /**
     * 期望的发展方向
     */
    private List<GrowDirection> growDirectionList;


}