/*
 * Dian.com Inc.
 * Copyright (c) 2004-2018 All Rights Reserved.
 */
package cn.sedu.moon.controller.response;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 *
 * @author ${wukong}
 * @version $Id: GrowDirectionResponse.java, v 0.1 2018年12月16日 2:10 PM Exp $
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class GrowDirectionResponse extends BaseResponse {

    private static final long serialVersionUID = -5772880952044521954L;

    /**
     * 培养方向id
     */
    private Long id;

    /**
     * 培养方向名称
     */
    private String name;
}