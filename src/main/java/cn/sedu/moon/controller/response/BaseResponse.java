/*
 * Dian.com Inc.
 * Copyright (c) 2004-2018 All Rights Reserved.
 */
package cn.sedu.moon.controller.response;
import java.io.Serializable;
import lombok.Data;

/**
 * response 的基类
 *
 * @author ${wukong}
 * @version $Id: BaseResponse.java, v 0.1 2018年12月07日 3:19 PM Exp $
 */
@Data
public class BaseResponse implements Serializable {

    private static final long serialVersionUID = 5654909328385165373L;

    /**
     * 前端提交的值
     */
    private Long value;
}