/*
 * Dian.com Inc.
 * Copyright (c) 2004-2019 All Rights Reserved.
 */
package cn.sedu.moon.controller.response;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 *
 * @author ${wukong}
 * @version $Id: NewsResponse.java, v 0.1 2019年01月06日 6:23 PM Exp $
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class NewsResponse extends BaseResponse {

    private static final long serialVersionUID = 5025292252518898313L;

    /**
     *
     * 新闻类型
     * 1 normal
     * 2 sop
     * 3 ask 问答
     */
    private Integer newsType;

}