/*
 * Dian.com Inc.
 * Copyright (c) 2004-2018 All Rights Reserved.
 */
package cn.sedu.moon.controller.response;

import java.util.List;

import cn.sedu.moon.dao.model.GrowDirection;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 *
 * @author ${wukong}
 * @version $Id: QueryGrowDirectionResponse.java, v 0.1 2018年12月16日 2:13 PM Exp $
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class QueryGrowDirectionResponse extends BaseResponse {

    private static final long serialVersionUID = -1837089385367255509L;

    /**
     * 培养方向列表
     */
    private List<GrowDirectionResponse> list;

    /**
     * 文案
     */
    private String content;

}