/*
 * Dian.com Inc.
 * Copyright (c) 2004-2019 All Rights Reserved.
 */
package cn.sedu.moon.controller.response;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 *
 * @author ${wukong}
 * @version $Id: AskAnswerResponse.java, v 0.1 2019年01月19日 11:46 PM Exp $
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class AskAnswerResponse extends BaseResponse {

    private static final long serialVersionUID = -1447063753648490767L;

    private Long id;

    /**
     * 回答者昵称
     */
    private String userName;

    /**
     * 回答者头像
     */
    private String userAvatar;

    /**
     * 是否被采纳
     */
    private Boolean accepted;

    /**
     * 回答正文
     */
    private String content;

    /**
     * 创建时间
     */
    private String createTime;

    /**
     * 是否有用
     */
    private Boolean useful;

    /**
     * 多少人觉得有用
     */
    private Integer usefulCount;

}