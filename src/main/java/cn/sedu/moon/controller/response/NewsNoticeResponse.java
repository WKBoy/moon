/*
 * Dian.com Inc.
 * Copyright (c) 2004-2019 All Rights Reserved.
 */
package cn.sedu.moon.controller.response;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author wukong
 * @version : NewsNoticeResponse.java, v 0.1 2019年10月27日 1:01 上午 Exp $
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class NewsNoticeResponse extends BaseResponse {

    private static final long serialVersionUID = -5133170630872946071L;

    private Long noticeId;

    private Long newsId;

    /**
     * 提醒标题
     */
    private String title;

    /**
     * 提醒的时
     */
    private Integer noticeHour;

    /**
     * 提醒的分
     */
    private Integer noticeMin;

    /**
     * 提醒的日
     */
    private String noticeDay;
}