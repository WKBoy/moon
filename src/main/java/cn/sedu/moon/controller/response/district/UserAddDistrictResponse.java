/*
 * Dian.com Inc.
 * Copyright (c) 2004-2019 All Rights Reserved.
 */
package cn.sedu.moon.controller.response.district;

import cn.sedu.moon.controller.response.BaseResponse;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author wukong
 * @version : UserAddDistrictResponse.java, v 0.1 2019年09月02日 13:43 Exp $
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class UserAddDistrictResponse extends BaseResponse {

    private static final long serialVersionUID = -5175076132729257819L;

    /**
     * 小区id
     */
    private Long id;
}