/*
 * Dian.com Inc.
 * Copyright (c) 2004-2019 All Rights Reserved.
 */
package cn.sedu.moon.controller.response.cms.ask;

import cn.sedu.moon.controller.response.BaseResponse;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 *
 * @author ${wukong}
 * @version $Id: CmsAskDetailResponse.java, v 0.1 2019年07月09日 8:37 PM Exp $
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class CmsAskDetailResponse extends BaseResponse {

    private static final long serialVersionUID = 6583021520808459857L;

    private Long id;

    private String title;

    private String author;

    private Boolean collect;

}