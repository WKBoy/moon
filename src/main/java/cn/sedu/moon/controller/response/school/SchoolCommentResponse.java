/*
 * Dian.com Inc.
 * Copyright (c) 2004-2019 All Rights Reserved.
 */
package cn.sedu.moon.controller.response.school;

import cn.sedu.moon.controller.response.BaseResponse;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

/**
 * @author wukong
 * @version : SchoolCommentResponse.java, v 0.1 2019年10月08日 2:27 PM Exp $
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class SchoolCommentResponse extends BaseResponse {

    private static final long serialVersionUID = -7499675131301614346L;

    /**
     * 评论ID
     */
    private Long commentId;

    /**
     * 发表评论的用户的头像
     */
    private String avatar;

    /**
     * 发表评论的用户的昵称
     */
    private String nick;

    /**
     * 评论创建时间
     */
    private String createTime;

    /**
     * 评论内容
     */
    private String content;

    /**
     * 图片列表
     */
    private List<String> imgList;
}