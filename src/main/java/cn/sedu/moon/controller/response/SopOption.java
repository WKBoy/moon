/*
 * Dian.com Inc.
 * Copyright (c) 2004-2019 All Rights Reserved.
 */
package cn.sedu.moon.controller.response;

import java.io.Serializable;

import lombok.Data;

/**
 *
 * @author ${wukong}
 * @version $Id: SopOptionDO.java, v 0.1 2019年01月06日 6:18 PM Exp $
 */
@Data
public class SopOption implements Serializable {

    private static final long serialVersionUID = 2326461261277471154L;

    /**
     * 选项标题文案
     */
    private String title;

    /**
     * 选项值，数字
     */
    private Integer value;

    /**
     * 选择后是否展示输入框
     */
    private Boolean choiceShowInput;

    /**
     * 输入框提示文案
     */
    private String inputInfo;
}