/*
 * Dian.com Inc.
 * Copyright (c) 2004-2019 All Rights Reserved.
 */
package cn.sedu.moon.controller.response.cms.ask;

import cn.sedu.moon.controller.response.BaseResponse;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author wukong
 * @version : CmsAskPublishResponse.java, v 0.1 2019年09月09日 23:43 Exp $
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class CmsAskPublishResponse extends BaseResponse {

    private static final long serialVersionUID = 8909251523803925449L;

    private Long id;
}