/*
 * Dian.com Inc.
 * Copyright (c) 2004-2019 All Rights Reserved.
 */
package cn.sedu.moon.controller.response.share;

import cn.sedu.moon.controller.response.BaseResponse;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author
 * @version : LightUserResponse.java, v 0.1 2019年07月29日 15:35 Exp $
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class LightUserResponse extends BaseResponse {

    private Long userId;

    private String avatar;
}