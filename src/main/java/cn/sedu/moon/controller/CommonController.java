/*
 * Dian.com Inc.
 * Copyright (c) 2004-2018 All Rights Reserved.
 */
package cn.sedu.moon.controller;

import cn.sedu.moon.common.Result;
import cn.sedu.moon.common.auth.CmsAuth;
import cn.sedu.moon.common.cache.CacheKey;
import cn.sedu.moon.common.enums.ErrorCodeEnum;
import cn.sedu.moon.common.exception.BizException;
import cn.sedu.moon.common.template.ControllerCallback;
import cn.sedu.moon.controller.request.AreaRequest;
import cn.sedu.moon.controller.response.AreaNode;
import cn.sedu.moon.service.CommonService;
import cn.sedu.moon.thirdparty.WechatApi;
import cn.sedu.moon.thirdparty.response.WxGetAccessTokenResponse;
import com.alibaba.fastjson.JSONObject;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.redisson.api.RBucket;
import org.redisson.api.RedissonClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;
import sun.misc.BASE64Decoder;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.TimeUnit;

/**
 * 通用控制器，包括获取省市区信息等接口
 *
 * @author ${wukong}
 * @version $Id: CommonController.java, v 0.1 2018年10月25日 11:46 AM Exp $
 */
@RestController
@Slf4j
public class CommonController extends BaseController {

    @Autowired
    private CommonService commonService;

    public final String IMAGETYPES = "gif,jpg,jpeg,png,bmp";

    @Autowired
    private RedissonClient redissonClient;

    @Autowired
    private WechatApi wechatApi;

    @Value("${marketing.getWxcodeUrl}")
    private String getWxcodeUrl;

    /**
     * 查询省市区
     *
     * @return 查询结果
     */
    @RequestMapping(value = "/moon/common/area", method = RequestMethod.GET)
    public Result queryAreaInfo(AreaRequest request) {

        return template.execute(new ControllerCallback<List<AreaNode>>() {
            @Override
            public void checkParam() {
                if (StringUtils.isBlank(request.getParentCode())) {
                    request.setParentCode("-1");
                    //request.setParentCode("330100");
                }
            }

            @Override
            public List<AreaNode> execute() {
                return commonService.queryAreaInfo(null, request.getParentCode());
            }
        });
    }

    @RequestMapping(value = "/moon/cms/image/upload", method = RequestMethod.POST)
    @ResponseBody
    //@CmsAuth
    public String uploadCmsImage(HttpServletRequest request, @RequestParam MultipartFile file, HttpServletResponse httpServletResponse) {
        try {
            String clientFileName = file.getOriginalFilename();
            String savedUri = null;
            String fileName = clientFileName.trim().toLowerCase();
            if (fileName.endsWith("gif")) {
                savedUri = commonService.uploadImage(clientFileName, file.getBytes(), "gif", file.getBytes().length);
            } else {
                savedUri = commonService.uploadImage(clientFileName, file.getBytes(), "jpg", file.getBytes().length);
            }

            httpServletResponse.setContentType("text/html; charset=UTF-8");

            PrintWriter out = httpServletResponse.getWriter();

            JSONObject jsonObject = new JSONObject();
            jsonObject.put("url", savedUri);
            jsonObject.put("error", 0);

            out.println(jsonObject.toJSONString());
            out.close();
            return null;

        } catch (Exception e) {
            throw new BizException(ErrorCodeEnum.UPLOAD_EXP);
        }
    }

    @RequestMapping(value = "/moon/image/upload", method = RequestMethod.POST)
    @ResponseBody
    @CmsAuth
    public String uploadImage(HttpServletRequest request, @RequestParam MultipartFile file, HttpServletResponse httpServletResponse) {
        try {
            String clientFileName = file.getOriginalFilename();
            String savedUri = null;
            String fileName = clientFileName.trim().toLowerCase();
            if (fileName.endsWith("gif")) {
                savedUri = commonService.uploadImage(clientFileName, file.getBytes(), "gif", file.getBytes().length);
            } else {
                savedUri = commonService.uploadImage(clientFileName, file.getBytes(), "jpg", file.getBytes().length);
            }

            JSONObject jsonObject = new JSONObject();
            jsonObject.put("success", true);
            jsonObject.put("url", savedUri);
            jsonObject.put("code", "0");
            jsonObject.put("error", "0");

            return jsonObject.toJSONString();

        } catch (Exception e) {
            throw new BizException(ErrorCodeEnum.UPLOAD_EXP);
        }
    }

    /**
     * kindeditor 粘贴图片上传
     *
     */
    @RequestMapping(value = "/moon/image/upload/base64", method = RequestMethod.POST)
    public void imageUploadBase64(HttpServletRequest request, HttpServletResponse response) {
        try {
            String savedUri = null;
            String imageDataBase64 = request.getParameter("imageDataBase64");
            if (StringUtils.isEmpty(imageDataBase64)) {
                String[] arrImageData = imageDataBase64.split(",");
                String[] arrTypes = arrImageData[0].split(";");
                String[] arrImageType = arrTypes[0].split(":");
                String imageType = arrImageType[1];
                String imageTypeSuffix = imageType.split("/")[1];
                if ("base64".equalsIgnoreCase(arrTypes[1]) && this.IMAGETYPES.indexOf(imageTypeSuffix.toLowerCase()) != -1) {

                    // 上传图片
                    BASE64Decoder decoder = new BASE64Decoder();
                    byte[] decodeBuffer = decoder.decodeBuffer(arrImageData[1]);
                    SimpleDateFormat df = new SimpleDateFormat("yyyyMMdd");
                    String currFormat = df.format(new Date());
                    String clientFileName = currFormat + "/" + new Random().nextInt(100000);

                    if (imageTypeSuffix.toLowerCase().endsWith("gif")) {
                        savedUri = commonService.uploadImage(clientFileName, decodeBuffer, "gif", decodeBuffer.length);
                    } else {
                        savedUri = commonService.uploadImage(clientFileName, decodeBuffer, "jpg", decodeBuffer.length);
                    }

                    response.setContentType("text/html; charset=UTF-8");

                    PrintWriter out = response.getWriter();

                    JSONObject jsonObject = new JSONObject();
                    jsonObject.put("url", savedUri);
                    jsonObject.put("error", 0);

                    out.println(jsonObject.toJSONString());
                    out.close();
                }
            }
        } catch (Exception e) {

            try {
                log.error("上传图片发生异常: ", e);
                response.setContentType("text/html; charset=UTF-8");

                PrintWriter out = response.getWriter();

                JSONObject jsonObject = new JSONObject();
                jsonObject.put("msg", "上传图片失败");
                jsonObject.put("error", 0);

                out.println(jsonObject.toJSONString());
                out.close();
            } catch (Exception exception) {
                log.error("上传图片发生异常：", e);
            }

        }
    }

    @RequestMapping(value = "/moon/getWxCode", method = RequestMethod.GET)
    public void getwxcode(@RequestParam("userId") String userId, HttpServletResponse httpServletResponse) {

        try {
            // 1 获取accessToken
            RBucket<String> bucket = redissonClient.getBucket(CacheKey.WX_ACCESS_TOKEN_KEY);

            String accessToken;
            if (bucket.get() == null) {
                WxGetAccessTokenResponse accessTokenResponse = wechatApi.getAccessToken();
                if (accessTokenResponse.getAccessToken() == null) {
                    log.error("getwxcode 获取微信accessToken失败 response is {}", accessTokenResponse);
                    throw new BizException(ErrorCodeEnum.SYS_EXP, "获取微信accessToken失败");
                }

                // 将accessToken存入redis，失效时间为微信返回的时间 减去100秒
                bucket.set(accessTokenResponse.getAccessToken(), accessTokenResponse.getExpiresIn() - 100, TimeUnit.SECONDS);
                accessToken = accessTokenResponse.getAccessToken();

            } else {
                accessToken = bucket.get();
            }


            RestTemplate rest = new RestTemplate();
            InputStream inputStream;
            ServletOutputStream outputStream;

            // 2 获取小程序码
            if (org.apache.commons.lang.StringUtils.isNotBlank(accessToken)) {

                // 请求参数
                Map<String, String> map = new HashMap<>();
                map.put("page", "pages/index/index");
                map.put("scene", userId);
                String url = getWxcodeUrl + accessToken;

                MultiValueMap<String, String> headers = new LinkedMultiValueMap<>();
                HttpEntity requestEntity = new HttpEntity(map, headers);
                ResponseEntity<byte[]> entity = rest.exchange(url, HttpMethod.POST, requestEntity, byte[].class, new Object[0]);
                byte[] result = entity.getBody();
                inputStream = new ByteArrayInputStream(result);

                outputStream = httpServletResponse.getOutputStream();

                // 返回图片
                httpServletResponse.setHeader("content-type", "image/jpeg");
                httpServletResponse.setHeader("Content-Disposition", "attachment;filename=wxcode");
                httpServletResponse.setContentType("image/jpeg");

                try {
                    int len;
                    byte[] buf = new byte[1024];
                    while ((len = inputStream.read(buf, 0, 1024)) != -1) {
                        outputStream.write(buf, 0, len);
                    }
                    outputStream.flush();

                } catch (Exception e) {
                    log.error("outputStream操作异常:", e);

                } finally {
                    if (inputStream != null) {
                        try {
                            inputStream.close();
                        } catch (IOException e) {
                            log.error("关闭inputStream错误:", e);
                        }
                    }

                    if (outputStream != null) {
                        try {
                            outputStream.close();
                        } catch (IOException e) {
                            log.error("关闭outputStream错误:", e);
                        }
                    }
                }
            }

        } catch (Exception e) {
            log.error("获取小程序码失败，失败信息为:{}", e);
        }
    }

}