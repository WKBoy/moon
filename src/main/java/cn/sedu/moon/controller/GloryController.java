/*
 * Dian.com Inc.
 * Copyright (c) 2004-2019 All Rights Reserved.
 */
package cn.sedu.moon.controller;

import cn.sedu.moon.common.Result;
import cn.sedu.moon.common.auth.Auth;
import cn.sedu.moon.common.template.ControllerCallback;
import cn.sedu.moon.controller.response.QueryGloryResponse;
import cn.sedu.moon.service.GloryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author ${wukong}
 * @version $Id: GloryController.java, v 0.1 2019年04月01日 4:29 PM Exp $
 */
@RestController
public class GloryController extends BaseController {

    @Autowired
    private GloryService gloryService;

    /**
     * 用户荣誉值查询
     *
     * @return {@link QueryGloryResponse}
     *          glory: 荣誉值
     */
    @RequestMapping(value = "/moon/glory/query", method = RequestMethod.GET)
    @Auth
    public Result query() {

        return template.execute(new ControllerCallback<QueryGloryResponse>() {

            @Override
            public void checkParam() {
            }

            @Override
            public QueryGloryResponse execute() {

                tracker();

                QueryGloryResponse response = new QueryGloryResponse();
                response.setGlory(gloryService.getUserGlory(getUserId()));
                return response;
            }
        });
    }
}