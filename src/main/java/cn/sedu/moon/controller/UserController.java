/*
 * Dian.com Inc.
 * Copyright (c) 2004-2018 All Rights Reserved.
 */
package cn.sedu.moon.controller;

import cn.sedu.moon.common.Result;
import cn.sedu.moon.common.auth.Auth;
import cn.sedu.moon.common.constant.BizConstants;
import cn.sedu.moon.common.enums.ErrorCodeEnum;
import cn.sedu.moon.common.exception.BizException;
import cn.sedu.moon.common.template.ControllerCallback;
import cn.sedu.moon.common.util.AuthUtil;
import cn.sedu.moon.common.util.CacheKeyUtil;
import cn.sedu.moon.controller.request.GetSmsRequest;
import cn.sedu.moon.controller.request.UserBaseInfoReportRequest;
import cn.sedu.moon.controller.request.UserLoginRequest;
import cn.sedu.moon.controller.request.UserMobileReportRequest;
import cn.sedu.moon.controller.response.UserLoginResponse;
import cn.sedu.moon.dao.UserDAO;
import cn.sedu.moon.dao.model.User;
import cn.sedu.moon.service.SmsService;
import cn.sedu.moon.service.UserService;
import cn.sedu.moon.service.context.UserBaseInfoContext;
import cn.sedu.moon.service.context.UserLoginContext;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.redisson.api.RBucket;
import org.redisson.api.RedissonClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.io.UnsupportedEncodingException;
import java.util.List;

/**
 * 用户控制器
 *
 * @author ${wukong}
 * @version $Id: UserController.java, v 0.1 2018年12月10日 2:01 PM Exp $
 */
@RestController
@Slf4j
public class UserController extends BaseController {

    @Autowired
    private UserService userService;

    @Autowired
    private SmsService smsService;

    @Autowired
    private RedissonClient redissonClient;

    @Autowired
    private UserDAO userDAO;

    @RequestMapping(value = "/moon/user/login", method = RequestMethod.GET)
    public Result login(UserLoginRequest request) {

        return template.execute(new ControllerCallback<UserLoginResponse>() {

            UserLoginContext context = new UserLoginContext();

            @Override
            public void checkParam() {
                if (StringUtils.isBlank(request.getCode())) {
                    throw new BizException(ErrorCodeEnum.PARAM_CHECK_EXP, "微信code不能为空");
                }
            }

            @Override
            public void buildContext() {
                context.setCode(request.getCode());
                context.setIv(request.getIv());
                context.setEncryptedData(request.getEncryptedData());

                if (StringUtils.isNotBlank(request.getScene()) && !"undefined".equals(request.getScene())) {
                    Long shareUserId = null;
                    try {
                        shareUserId = Long.parseLong(request.getScene());
                    } catch (Exception e) {
                        log.error("login  Long.parseLong error", e);
                    }

                    if (shareUserId != null) {
                        context.setShareUserId(shareUserId);
                    }
                }
            }

            @Override
            public UserLoginResponse execute() {
                return userService.login(context);
            }
        });
    }

    @RequestMapping(value = "/moon/user/login/temp", method = RequestMethod.GET)
    public Result loginTemp() {

        return template.execute(new ControllerCallback<UserLoginResponse>() {

            UserLoginContext context = new UserLoginContext();

            @Override
            public void checkParam() {
            }

            @Override
            public UserLoginResponse execute() {
                return userService.tempLogin(context);
            }
        });
    }

    @RequestMapping(value = "/moon/user/getToken", method = RequestMethod.GET)
    public Result getToken(@RequestParam(value = "userId", required = true) Long userId) {

        return template.execute(new ControllerCallback<String>() {

            @Override
            public void checkParam() {
            }

            @Override
            public String execute() {
                String key = CacheKeyUtil.genUserTokenKey(userId);
                RBucket<String> bucket = redissonClient.getBucket(key);
                if (bucket.get() != null) {
                    return bucket.get();
                }

                return null;
            }
        });
    }

    @RequestMapping(value = "/moon/user/wxAuth", method = RequestMethod.GET)
    @Auth
    public Result wxAuth(UserLoginRequest request) {

        return template.execute(new ControllerCallback<Boolean>() {

            UserLoginContext context = new UserLoginContext();

            @Override
            public void checkParam() {
                if (StringUtils.isBlank(request.getCode())) {
                    throw new BizException(ErrorCodeEnum.PARAM_CHECK_EXP, "微信code不能为空");
                }
            }

            @Override
            public void buildContext() {
                context.setCode(request.getCode());
                context.setIv(request.getIv());
                context.setEncryptedData(request.getEncryptedData());
                context.setUserId(getUserId());
            }

            @Override
            public Boolean execute() {
                userService.wxAuth(context);
                return true;
            }
        });
    }

    @RequestMapping(value = "/moon/user/check", method = RequestMethod.GET)
    public Result check(UserLoginRequest request) {

        return template.execute(new ControllerCallback<UserLoginResponse>() {

            UserLoginContext context = new UserLoginContext();

            @Override
            public void checkParam() {
                if (StringUtils.isBlank(request.getCode())) {
                    throw new BizException(ErrorCodeEnum.PARAM_CHECK_EXP, "微信code不能为空");
                }
            }

            @Override
            public void buildContext() {
                context.setCode(request.getCode());
                context.setIv(request.getIv());
                context.setEncryptedData(request.getEncryptedData());

                if (StringUtils.isNotBlank(request.getScene()) && !"undefined".equals(request.getScene())) {
                    Long shareUserId = null;
                    try {
                        shareUserId = Long.parseLong(request.getScene());
                    } catch (Exception e) {
                        log.error("login  Long.parseLong error", e);
                    }

                    if (shareUserId != null) {
                        context.setShareUserId(shareUserId);
                    }
                }
            }

            @Override
            public UserLoginResponse execute() {
                return userService.login(context);
            }
        });
    }

    @RequestMapping(value = "/moon/user/reportMobile", method = RequestMethod.GET)
    @Auth
    public Result reportMobile(UserMobileReportRequest request) {

        return template.execute(new ControllerCallback<Boolean>() {

            @Override
            public void checkParam() {
                if (StringUtils.isBlank(request.getCode())) {
                    throw new BizException(ErrorCodeEnum.PARAM_CHECK_EXP, "微信code不能为空");
                }

                if (StringUtils.isBlank(request.getEncryptedData())) {
                    throw new BizException(ErrorCodeEnum.PARAM_CHECK_EXP, "encryptedData不能为空");
                }

                if (StringUtils.isBlank(request.getIv())) {
                    throw new BizException(ErrorCodeEnum.PARAM_CHECK_EXP, "iv不能为空");
                }

                request.setUserId(getUserId());
            }

            @Override
            public Boolean execute() {
                userService.reportMobile(request);
                return true;
            }
        });
    }

    /**
     * 上报用户的基本信息
     *
     * @param request
     * @return
     */
    @Auth
    @RequestMapping(value = "/moon/user/reportBaseInfo", method = RequestMethod.POST)
    public Result reportBaseInfo(@RequestBody UserBaseInfoReportRequest request) {

        return template.execute(new ControllerCallback<Boolean>() {

            UserBaseInfoContext context = new UserBaseInfoContext();

            @Override
            public void checkParam() {

            }

            @Override
            public void buildContext() {
                context.setUserId(getUserId());
                context.setAvatarUrl(request.getAvatarUrl());
                context.setCity(request.getCity());
                context.setProvince(request.getProvince());
                context.setCountry(request.getCountry());
                context.setGender(request.getGender());
                context.setLanguage(request.getLanguage());
                context.setNickName(request.getNickName());
            }

            @Override
            public Boolean execute() {
                userService.reportBaseInfo(context);
                return true;
            }
        });
    }

    /**
     * 获取短信验证码
     *
     * @param request request 对象，mobile 不可为空
     * @return 发送成功返回 true, 失败跑出异常
     */
    @RequestMapping(value = "/moon/user/getSmsCode", method = RequestMethod.POST)
    public Result getCode(@RequestBody GetSmsRequest request) {

        return template.execute(new ControllerCallback<Boolean>() {
            @Override
            public void checkParam() {
                if (StringUtils.isBlank(request.getMobile())) {
                    throw new BizException(ErrorCodeEnum.PARAM_CHECK_EXP, "手机号不可为空");
                }
            }

            @Override
            public Boolean execute() {
                smsService.sendToken(request.getMobile());
                return true;
            }
        });

    }

    @GetMapping(value = "/test/getToken/{userId}")
    public Result divideContract(@PathVariable Long userId) {
        return template.execute(new ControllerCallback<String>() {
            @Override
            public void checkParam() {

            }

            @Override
            public void buildContext() {

            }

            @Override
            public String execute() {
                User currentUser = userService.getUserById(userId);
                if (currentUser == null) {
                    throw new BizException(ErrorCodeEnum.SYS_EXP, "用户不存在");
                }


                String cacheKey = CacheKeyUtil.genUserTokenKey(userId);
                RBucket<String> bucket = redissonClient.getBucket(cacheKey);
                if (StringUtils.isNotBlank(bucket.get())) {
                    return bucket.get();
                }

                // 根据用户ID生成token
                String token;
                try {
                    token = AuthUtil.generateToken(userId + "");
                } catch (UnsupportedEncodingException e) {
                    throw new BizException(ErrorCodeEnum.SYS_EXP, "生成token异常");
                }

                RBucket<String> loginBucket = redissonClient.getBucket(cacheKey);
                loginBucket.set(token, BizConstants.USER_TOKEN_TIME_TO_LIVE, BizConstants.USER_TOKEN_LIVE_TIME_UNIT);
                return token;
            }
        });
    }

    @GetMapping(value = "/test/clearIndexCache")
    public Result clearIndexCache(@RequestParam("userId") Long userId) {
        return template.execute(new ControllerCallback<Boolean>() {
            @Override
            public void checkParam() {

            }

            @Override
            public void buildContext() {

            }

            @Override
            public Boolean execute() {
                User currentUser = userService.getUserById(userId);
                if (currentUser == null) {
                    throw new BizException(ErrorCodeEnum.SYS_EXP, "用户不存在");
                }

                String cacheKey = CacheKeyUtil.genUserNewsKey(userId);
                RBucket<List<Long>> bucket = redissonClient.getBucket(cacheKey);
                if (bucket.get() == null) {
                    return true;
                }

                bucket.delete();
                return true;
            }
        });
    }

    @GetMapping(value = "/test/clearUser")
    public Result clearUser(@RequestParam("userId") Long userId) {
        return template.execute(new ControllerCallback<Boolean>() {
            @Override
            public void checkParam() {

            }

            @Override
            public void buildContext() {

            }

            @Override
            public Boolean execute() {

                User user = new User();
                user.setId(userId);
                userDAO.removeRecord(user);
                return true;
            }
        });
    }

}