/*
 * Dian.com Inc.
 * Copyright (c) 2004-2018 All Rights Reserved.
 */
package cn.sedu.moon.controller;

import cn.sedu.moon.common.Result;
import cn.sedu.moon.common.auth.Auth;
import cn.sedu.moon.common.enums.ErrorCodeEnum;
import cn.sedu.moon.common.exception.BizException;
import cn.sedu.moon.common.template.ControllerCallback;
import cn.sedu.moon.controller.request.QuerySchoolNewsRequest;
import cn.sedu.moon.controller.request.SopReportRequest;
import cn.sedu.moon.controller.request.news.QueryNewsRequest;
import cn.sedu.moon.controller.response.NewsResponse;
import cn.sedu.moon.controller.response.SchoolNewsDetailResponse;
import cn.sedu.moon.controller.response.SchoolNewsResponse;
import cn.sedu.moon.controller.response.SopReportAnswerResponse;
import cn.sedu.moon.service.HomeService;
import cn.sedu.moon.service.NewsService;
import cn.sedu.moon.service.SopService;
import cn.sedu.moon.service.context.IndexAskContext;
import cn.sedu.moon.service.context.QueryNewsContext;
import cn.sedu.moon.service.context.SearchNewsContext;
import cn.sedu.moon.service.context.SopAnswerContext;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

/**
 * 首页控制器
 *
 * @author ${wukong}
 * @version $Id: HomeController.java, v 0.1 2018年12月07日 3:07 PM Exp $
 */
@RestController
public class HomeController extends BaseController {

    @Autowired
    private SopService sopService;

    @Autowired
    private HomeService homeService;

    @Autowired
    private NewsService newsService;

    @RequestMapping(value = "/moon/index/news", method = RequestMethod.GET)
    @Auth
    public Result schoolNews(QueryNewsRequest request) {

        return template.execute(new ControllerCallback<List<NewsResponse>>() {

            QueryNewsContext context = new QueryNewsContext();

            @Override
            public void checkParam() {
            }

            @Override
            public void buildContext() {
                context.setOffset(request.getOffset());
                context.setUserId(getUserId());
            }

            @Override
            public List<NewsResponse> execute() {

                tracker();

                if (StringUtils.isBlank(request.getKey())) {
                    context.setPageSize(8);
                    List<NewsResponse> list = homeService.indexNews(context);
                    if (CollectionUtils.isEmpty(list)) {
                        return searchNews(request);
                    } else {
                        return list;
                    }

                } else {
                    // 搜索新闻
                    return searchNews(request);
                }
            }
        });
    }

    private List<NewsResponse> searchNews(QueryNewsRequest request) {

        // 搜索新闻
        List<NewsResponse> responseList = new ArrayList<>();

        SearchNewsContext searchNewsContext = new SearchNewsContext();
        searchNewsContext.setOffset(request.getOffset());
        searchNewsContext.setPageSize(request.getPageSize());
        searchNewsContext.setKey(request.getKey());

        searchNewsContext.setUserId(getUserId());
        List<SchoolNewsResponse> schoolNewsResponseList = newsService.searchNews(searchNewsContext);
        if (schoolNewsResponseList != null && schoolNewsResponseList.size() > 0) {
            responseList.addAll(schoolNewsResponseList);
        }

        return responseList;
    }

    @RequestMapping(value = "/moon/sop/report", method = RequestMethod.POST)
    @Auth
    public Result sopReport(@RequestBody SopReportRequest request) {

        return template.execute(new ControllerCallback<SopReportAnswerResponse>() {

            SopAnswerContext context = new SopAnswerContext();

            @Override
            public void checkParam() {
                if (request.getId() == null) {
                    throw new BizException(ErrorCodeEnum.PARAM_CHECK_EXP, "SOP问题ID不可为空");
                }

                if (request.getSchoolId() == null) {
                    throw new BizException(ErrorCodeEnum.PARAM_CHECK_EXP, "学校ID不可为空");
                }
            }

            @Override
            public void buildContext() {
                context.setSubjectRefId(request.getId());
                context.setSchoolId(request.getSchoolId());
                context.setInputString(request.getInputString());
                context.setOptionList(request.getOptionList());
                context.setUserId(getUserId());
//                context.setUserId(2562L);
            }

            @Override
            public SopReportAnswerResponse execute() {
                tracker();
                return sopService.reportSopAnswer(context);
            }
        });
    }

    @RequestMapping(value = "/moon/index/newsDetail", method = RequestMethod.GET)
    @Auth
    public Result newsDetail(QuerySchoolNewsRequest request) {

        return template.execute(new ControllerCallback<SchoolNewsDetailResponse>() {
            @Override
            public void checkParam() {

                if (request.getId() == null) {
                    throw new BizException(ErrorCodeEnum.PARAM_CHECK_EXP, "新闻ID不能为空");
                }
            }

            @Override
            public SchoolNewsDetailResponse execute() {
                return newsService.newsDetail(request.getId(), getUserId());
            }
        });
    }


    /**
     * 问答首页
     *
     * @return request
     */
    @RequestMapping(value = "/moon/index/ask", method = RequestMethod.GET)
    @Auth
    public Result askIndex(QueryNewsRequest request) {

        return template.execute(new ControllerCallback<List<NewsResponse>>() {

            IndexAskContext context = new IndexAskContext();

            @Override
            public void checkParam() {
            }

            @Override
            public void buildContext() {
                context.setUserId(getUserId());
                context.setOffset(request.getOffset());
                context.setPageSize(request.getPageSize());
            }

            @Override
            public List<NewsResponse> execute() {

//                tracker();

                return homeService.indexAsk(context);
            }
        });
    }

    /**
     * 检查用户是否阅读过引导内容
     *
     * @return request
     */
    @RequestMapping(value = "/moon/index/checkIntroRead", method = RequestMethod.GET)
    @Auth
    public Result checkIntroRead() {

        return template.execute(new ControllerCallback<Boolean>() {

            @Override
            public void checkParam() {
            }

            @Override
            public Boolean execute() {
                return homeService.checkIntroRead(getUserId());
            }
        });
    }

    /**
     * 记录用过户已读引导内容
     *
     * @return request
     */
    @RequestMapping(value = "/moon/index/introRead", method = RequestMethod.GET)
    @Auth
    public Result introRead() {

        return template.execute(new ControllerCallback<Boolean>() {

            @Override
            public void checkParam() {
            }

            @Override
            public Boolean execute() {
                tracker();
                homeService.introRead(getUserId());
                return true;
            }
        });
    }
}