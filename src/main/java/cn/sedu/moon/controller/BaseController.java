/*
 * Dian.com Inc.
 * Copyright (c) 2004-2018 All Rights Reserved.
 */
package cn.sedu.moon.controller;

import cn.sedu.moon.common.constant.BizConstants;
import cn.sedu.moon.common.template.ControllerTemplate;
import cn.sedu.moon.service.TrackerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

/**
 * 控制器基类
 *
 * @author ${wukong}
 * @version $Id: BaseController.java, v 0.1 2018年12月07日 3:07 PM Exp $
 */
@RestController
public class BaseController {

    @Resource
    HttpServletRequest httpServletRequest;

    @Autowired
    private TrackerService trackerService;

    /**
     * 操作模板
     */
    @Resource
    @Qualifier("AbstractControllerTemplate")
    protected ControllerTemplate template;

    /**
     * 得到用户id
     * @return
     */
    protected Long getUserId() {
        return Long.parseLong((String)httpServletRequest.getAttribute(BizConstants.USER_ID));
    }

    protected void tracker() {
        String url = httpServletRequest.getRequestURL() + (httpServletRequest.getQueryString() == null ? "" : "?" + httpServletRequest.getQueryString());
        trackerService.record(url, getUserId());
    }

}