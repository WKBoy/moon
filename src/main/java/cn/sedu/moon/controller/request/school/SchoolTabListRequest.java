/*
 * Dian.com Inc.
 * Copyright (c) 2004-2019 All Rights Reserved.
 */
package cn.sedu.moon.controller.request.school;

import cn.sedu.moon.controller.request.BaseRequest;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author wukong
 * @version : SchoolTabListRequest.java, v 0.1 2019年10月07日 3:37 PM Exp $
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class SchoolTabListRequest extends BaseRequest {

    private static final long serialVersionUID = -5246313860877759931L;

    /**
     * 搜索关键字
     */
    private String key;

}