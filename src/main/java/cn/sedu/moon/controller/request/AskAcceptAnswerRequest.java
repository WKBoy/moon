/*
 * Dian.com Inc.
 * Copyright (c) 2004-2019 All Rights Reserved.
 */
package cn.sedu.moon.controller.request;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 *
 * @author ${wukong}
 * @version $Id: AskAcceptAnswerRequest.java, v 0.1 2019年01月26日 10:55 AM Exp $
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class AskAcceptAnswerRequest extends BaseRequest {

    private static final long serialVersionUID = 7756129553012212704L;

    /**
     * 问题id
     */
    private Long askId;

    /**
     * 答案id
     */
    private Long answerId;

}