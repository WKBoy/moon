/*
 * Dian.com Inc.
 * Copyright (c) 2004-2019 All Rights Reserved.
 */
package cn.sedu.moon.controller.request.news;

import cn.sedu.moon.controller.request.BaseRequest;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 *
 * @author ${wukong}
 * @version $Id: DeleteNewsRequest.java, v 0.1 2019年02月15日 12:08 PM Exp $
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class DeleteNewsRequest extends BaseRequest {

    private static final long serialVersionUID = -7283310855151147235L;

    /**
     * 新闻id
     */
    private Long id;
}