package cn.sedu.moon.controller.request.census;

import cn.sedu.moon.controller.request.BaseRequest;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author lix
 * @version 1.0
 * @date 2019/12/4 0:42
 * @describe
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class CityCodeNewsRequest extends BaseRequest {
    private static final long serialVersionUID = -5631613259504445115L;

    private String cityCode;
}
