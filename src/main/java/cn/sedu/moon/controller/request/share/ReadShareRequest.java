/*
 * Dian.com Inc.
 * Copyright (c) 2004-2019 All Rights Reserved.
 */
package cn.sedu.moon.controller.request.share;

import cn.sedu.moon.controller.request.BaseRequest;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author wukong
 * @version : ReadShareRequest.java, v 0.1 2019年08月06日 20:17 Exp $
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class ReadShareRequest extends BaseRequest {

    /**
     * 分享id
     */
    private Long shareId;

}