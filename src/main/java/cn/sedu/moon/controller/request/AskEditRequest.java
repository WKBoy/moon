/*
 * Dian.com Inc.
 * Copyright (c) 2004-2019 All Rights Reserved.
 */
package cn.sedu.moon.controller.request;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 *
 * @author ${wukong}
 * @version $Id: AskEditRequest.java, v 0.1 2019年01月26日 11:38 AM Exp $
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class AskEditRequest extends BaseRequest {

    private static final long serialVersionUID = -8769386959571592344L;

    private Long id;

    /**
     * 问题标题
     */
    private String title;

    /**
     * 问题内容
     */
    private String content;
}