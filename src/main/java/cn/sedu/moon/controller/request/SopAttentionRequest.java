/*
 * Dian.com Inc.
 * Copyright (c) 2004-2019 All Rights Reserved.
 */
package cn.sedu.moon.controller.request;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 *
 * @author ${wukong}
 * @version $Id: SopAttentionRequest.java, v 0.1 2019年01月26日 3:58 PM Exp $
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class SopAttentionRequest extends BaseRequest {

    private static final long serialVersionUID = 8088477619068714828L;

    /**
     * sop ref id
     */
    private Integer sopId;
}