/*
 * Dian.com Inc.
 * Copyright (c) 2004-2019 All Rights Reserved.
 */
package cn.sedu.moon.controller.request;

import java.util.Date;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 提交微信formId的请求体
 *
 * @author ${wukong}
 * @version $Id: ReportFormIdRequest.java, v 0.1 2019年04月02日 10:41 AM Exp $
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class ReportFormIdRequest extends BaseRequest {

    private static final long serialVersionUID = -4192178900847914113L;

    /**
     * formId
     */
    private String formId;
}