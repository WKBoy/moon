/*
 * Dian.com Inc.
 * Copyright (c) 2004-2019 All Rights Reserved.
 */
package cn.sedu.moon.controller.request;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 *
 * @author ${wukong}
 * @version $Id: DistrictUserReportRequest.java, v 0.1 2019年03月12日 2:13 AM Exp $
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class DistrictUserReportRequest extends BaseRequest {
    
    private static final long serialVersionUID = -1874619824904541865L;

    private String name;

    private String address;

    private String info;

    private String provinceCode;

    private String cityCode;

    private String areaCode;
}