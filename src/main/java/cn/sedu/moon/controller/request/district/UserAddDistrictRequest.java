/*
 * Dian.com Inc.
 * Copyright (c) 2004-2019 All Rights Reserved.
 */
package cn.sedu.moon.controller.request.district;

import cn.sedu.moon.controller.request.BaseRequest;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author wukong
 * @version : UserAddDistrictRequest.java, v 0.1 2019年09月02日 13:35 Exp $
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class UserAddDistrictRequest extends BaseRequest {

    private static final long serialVersionUID = -1257392855933676700L;

    /**
     */
    private String name;

    /**
     * 区编码
     */
    private String areaCode;

    /**
     * 地址
     */
    private String address;

    /**
     * 纬度
     */
    private Double lat;

    /**
     * 经度
     */
    private Double lng;

}