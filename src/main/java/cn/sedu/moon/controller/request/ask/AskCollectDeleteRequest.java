/*
 * Dian.com Inc.
 * Copyright (c) 2004-2019 All Rights Reserved.
 */
package cn.sedu.moon.controller.request.ask;

import cn.sedu.moon.controller.request.BaseRequest;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author wukong
 * @version : AskCollectDeleteRequest.java, v 0.1 2019年09月15日 14:39 Exp $
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class AskCollectDeleteRequest extends BaseRequest {

    private static final long serialVersionUID = 5358867221117598990L;

    private Long askId;
}