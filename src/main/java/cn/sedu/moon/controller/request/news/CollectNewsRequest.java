/*
 * Dian.com Inc.
 * Copyright (c) 2004-2019 All Rights Reserved.
 */
package cn.sedu.moon.controller.request.news;

import cn.sedu.moon.controller.request.BaseRequest;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 *
 * @author ${wukong}
 * @version $Id: CollectNewsRequest.java, v 0.1 2019年06月23日 12:28 PM Exp $
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class CollectNewsRequest extends BaseRequest {

    private static final long serialVersionUID = -4798600368849742215L;

    private Long newsId;
}