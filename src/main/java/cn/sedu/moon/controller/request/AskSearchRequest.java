/*
 * Dian.com Inc.
 * Copyright (c) 2004-2019 All Rights Reserved.
 */
package cn.sedu.moon.controller.request;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 *
 * @author ${wukong}
 * @version $Id: AskSearchRequest.java, v 0.1 2019年01月19日 11:17 PM Exp $
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class AskSearchRequest extends BaseRequest {

    private static final long serialVersionUID = 5112794402862058194L;

    /**
     * 搜索关键字
     */
    private String keyword;

}