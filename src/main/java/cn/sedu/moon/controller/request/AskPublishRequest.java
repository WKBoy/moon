/*
 * Dian.com Inc.
 * Copyright (c) 2004-2019 All Rights Reserved.
 */
package cn.sedu.moon.controller.request;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 *
 * @author ${wukong}
 * @version $Id: AskPublishRequest.java, v 0.1 2019年01月19日 11:12 PM Exp $
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class AskPublishRequest extends BaseRequest {

    private static final long serialVersionUID = 563687353252267377L;

    /**
     * 问题标题
     */
    private String title;

    /**
     * 问题内容
     */
    private String content;

    /**
     * 新闻id
     */
    private Long newsId;

}