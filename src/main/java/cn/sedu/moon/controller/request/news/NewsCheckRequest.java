/*
 * Dian.com Inc.
 * Copyright (c) 2004-2019 All Rights Reserved.
 */
package cn.sedu.moon.controller.request.news;

import cn.sedu.moon.controller.request.BaseRequest;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 *
 * @author ${wukong}
 * @version $Id: NewsCheckRequest.java, v 0.1 2019年02月15日 12:02 PM Exp $
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class NewsCheckRequest extends BaseRequest {

    private static final long serialVersionUID = 3730819458226003509L;

    /**
     * 新闻id
     */
    private Long newsId;

    private String content;

    private String title;

    private String cityCode;

    private String thumbnail;

    private String brief;

    private Integer light;

    private Integer type;

    private String askIdListString;

    private boolean hasNotice;

    private String noticeTitle;

    private Integer noticeHour;

    private Integer noticeMin;

    private String noticeDay;

}