/*
 * Dian.com Inc.
 * Copyright (c) 2004-2019 All Rights Reserved.
 */
package cn.sedu.moon.controller.request.school;

import cn.sedu.moon.controller.request.BaseRequest;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author wukong
 * @version : SchoolDetailRequest.java, v 0.1 2019年10月07日 3:39 PM Exp $
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class SchoolDetailRequest extends BaseRequest {

    private static final long serialVersionUID = 5619822065942681052L;

    private Long schoolId;
}