/*
 * Dian.com Inc.
 * Copyright (c) 2004-2018 All Rights Reserved.
 */
package cn.sedu.moon.controller.request;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 *
 * @author ${wukong}
 * @version $Id: DistrictNearbyRequest.java, v 0.1 2018年12月16日 12:03 AM Exp $
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class DistrictNearbyRequest extends BaseRequest {

    private static final long serialVersionUID = 208996470206080098L;

    /**
     * 纬度
     */
    private Double lat;

    /**
     * 经度
     */
    private Double lnt;

    /**
     * 搜索关键字
     */
    private String keyName;

    /**
     * 区编码
     */
    private String areaCode;

}