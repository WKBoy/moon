package cn.sedu.moon.controller.request.subscriber;

import cn.sedu.moon.controller.request.BaseRequest;
import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Data
public class CheckSubscribeBestWeekRequest extends BaseRequest {


}
