/*
 * Dian.com Inc.
 * Copyright (c) 2004-2019 All Rights Reserved.
 */
package cn.sedu.moon.controller.request;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 *
 * @author ${wukong}
 * @version $Id: SchoolUserReportRequest.java, v 0.1 2019年03月12日 2:12 AM Exp $
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class SchoolUserReportRequest extends BaseRequest {

    private static final long serialVersionUID = -7241502504511930595L;

    private String name;

    private String address;

    private String info;

    private String provinceCode;

    private String cityCode;

    private String areaCode;
}