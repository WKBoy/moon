/*
 * Dian.com Inc.
 * Copyright (c) 2004-2019 All Rights Reserved.
 */
package cn.sedu.moon.controller.request.news;

import cn.sedu.moon.controller.request.BaseRequest;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 *
 * @author ${wukong}
 * @version $Id: BatchDeleteNewsRequest.java, v 0.1 2019年03月25日 11:09 AM Exp $
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class BatchDeleteNewsRequest extends BaseRequest {

    private static final long serialVersionUID = 3667676970869590023L;

    /**
     * 用逗号分割的id字符串
     */
    private String ids;

}