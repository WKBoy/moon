/*
 * Dian.com Inc.
 * Copyright (c) 2004-2019 All Rights Reserved.
 */
package cn.sedu.moon.controller.request.cms.school;

import cn.sedu.moon.controller.request.BaseRequest;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author wukong
 * @version : CmsSchoolSetHotRequest.java, v 0.1 2019年09月28日 4:37 PM Exp $
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class CmsSchoolSetHotRequest extends BaseRequest {

    private static final long serialVersionUID = 1985508478731557301L;

    /**
     * 学校id
     */
    private Long schoolId;

    /**
     * 1 市热门  2 区热门
     */
    private Integer type;

    /**
     * 是否取消热门
     */
    private Boolean delete = false;
}