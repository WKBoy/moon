/*
 * Dian.com Inc.
 * Copyright (c) 2004-2019 All Rights Reserved.
 */
package cn.sedu.moon.controller.request;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 *
 * @author ${wukong}
 * @version $Id: NewsReadRequest.java, v 0.1 2019年03月15日 10:31 AM Exp $
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class NewsReadRequest extends BaseRequest {

    private static final long serialVersionUID = 6111009840126715388L;

    /**
     * 新闻id
     */
    private Long newsId;
}