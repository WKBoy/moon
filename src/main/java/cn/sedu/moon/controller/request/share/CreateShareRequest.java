/*
 * Dian.com Inc.
 * Copyright (c) 2004-2019 All Rights Reserved.
 */
package cn.sedu.moon.controller.request.share;

import cn.sedu.moon.controller.request.BaseRequest;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 *
 * @author ${wukong}
 * @version $Id: CreateShareRequest.java, v 0.1 2019年04月14日 3:50 PM Exp $
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class CreateShareRequest extends BaseRequest {

    private static final long serialVersionUID = 8184696889060911270L;

    /**
     * 新闻id
     */
    private Long newsId;
}