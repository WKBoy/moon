/*
 * Dian.com Inc.
 * Copyright (c) 2004-2018 All Rights Reserved.
 */
package cn.sedu.moon.controller.request;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 获取短信验证码请求
 *
 * @author ${wukong}
 * @version $Id: GetSmsRequest.java, v 0.1 2018年01月19日 3:25 PM Exp $
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class GetSmsRequest extends BaseRequest {

    private static final long serialVersionUID = -9110412922742102292L;

    /**
     * 手机号码
     */
    private String mobile;
}