/*
 * Dian.com Inc.
 * Copyright (c) 2004-2018 All Rights Reserved.
 */
package cn.sedu.moon.controller.request;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 登陆接口的request
 *
 * @author ${wukong}
 * @version $Id: UserLoginRequest.java, v 0.1 2018年12月10日 2:02 PM Exp $
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class UserLoginRequest extends BaseRequest {

    private static final long serialVersionUID = 7757671756119706270L;

    /**
     * 加密的数据
     */
    private String encryptedData;

    /**
     * 微信分配的iv
     */
    private String iv;

    /**
     * 微信分配的code
     */
    private String code;

    /**
     * 微信二维码扫码进入小程序登录会携带这个参数过来
     */
    private String scene;
}
