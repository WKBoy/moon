/*
 * Dian.com Inc.
 * Copyright (c) 2004-2018 All Rights Reserved.
 */
package cn.sedu.moon.controller.request;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 获取附近学校列表的 request
 *
 * @author ${wukong}
 * @version $Id: SchoolNearbyRequest.java, v 0.1 2018年12月11日 12:58 PM Exp $
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class SchoolNearbyRequest extends BaseRequest {

    private static final long serialVersionUID = 187816282117326038L;

    /**
     * 纬度
     */
    private Double lat;

    /**
     * 经度
     */
    private Double lnt;

    /**
     * 搜索关键字
     */
    private String keyName;

    /**
     * 学校类型
     */
    private Integer schoolType;

    /**
     * 区编码
     */
    private String areaCode;

}