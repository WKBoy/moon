/*
 * Dian.com Inc.
 * Copyright (c) 2004-2019 All Rights Reserved.
 */
package cn.sedu.moon.controller.request.share;

import cn.sedu.moon.controller.request.BaseRequest;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 *
 * @author ${wukong}
 * @version $Id: ShareNewsRequest.java, v 0.1 2019年04月14日 3:53 PM Exp $
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class ShareNewsRequest extends BaseRequest {

    private static final long serialVersionUID = 3224233196602027572L;

    /**
     * 分享id
     */
    private Long shareId;
}