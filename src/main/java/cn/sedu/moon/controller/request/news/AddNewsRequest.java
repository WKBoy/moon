/*
 * Dian.com Inc.
 * Copyright (c) 2004-2019 All Rights Reserved.
 */
package cn.sedu.moon.controller.request.news;

import cn.sedu.moon.controller.request.BaseRequest;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 *
 * @author ${wukong}
 * @version $Id: AddNewsRequest.java, v 0.1 2019年02月19日 7:40 PM Exp $
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class AddNewsRequest extends BaseRequest {

    private static final long serialVersionUID = -1132369780886966916L;

    /**
     * 标题
     */
    private String title;

    /**
     * 新增
     */
    private String content;

    /**
     * 城市编码
     */
    private String cityCode;

    private String thumbnail;

    private String brief;

    private Integer light = 0;

    /**
     * 新闻类型
     */
    private Integer type = 0;

    private String askIdListString;

    private boolean hasNotice;

    private String noticeTitle;

    private Integer noticeHour;

    private Integer noticeMin;

    private String noticeDay;
}