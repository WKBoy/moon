/*
 * Dian.com Inc.
 * Copyright (c) 2004-2018 All Rights Reserved.
 */
package cn.sedu.moon.controller.request;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 查询学校的request
 *
 * @author ${wukong}
 * @version $Id: QuerySchoolRequest.java, v 0.1 2018年12月14日 6:22 PM Exp $
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class QuerySchoolRequest extends BaseRequest {

    private static final long serialVersionUID = 7705759343903597462L;

    /**
     * 学校ID
     */
    private Long schoolId;

    /**
     * 1、家对口的公办1所（离的最近的公办）--离家最近的公办
     * 2、车程15分钟以内的民办2-3所（6公里以内）--车程15分钟以内的民办
     * 3、家所在区热门学校（6公里内、人工给出）--家所在区热门学校
     * 4、杭州热门民办（人工给出10所）--杭州市热门民办
     * 5、杭州热门民办（人工给出10所）--杭州市热门民办, 有爆表被调剂风险
     */
    private Integer queryType;
    
}