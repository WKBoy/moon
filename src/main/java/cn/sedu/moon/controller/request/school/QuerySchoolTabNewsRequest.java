/*
 * Dian.com Inc.
 * Copyright (c) 2004-2019 All Rights Reserved.
 */
package cn.sedu.moon.controller.request.school;

import cn.sedu.moon.controller.request.BaseRequest;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author wukong
 * @version : QuerySchoolTabNewsRequest.java, v 0.1 2019年10月08日 8:47 PM Exp $
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class QuerySchoolTabNewsRequest extends BaseRequest {

    private static final long serialVersionUID = 8694652831938944625L;

    private Long schoolId;
}