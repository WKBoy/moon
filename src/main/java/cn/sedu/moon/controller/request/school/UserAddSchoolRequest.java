/*
 * Dian.com Inc.
 * Copyright (c) 2004-2019 All Rights Reserved.
 */
package cn.sedu.moon.controller.request.school;

import cn.sedu.moon.controller.request.BaseRequest;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author wukong
 * @version : UserAddSchoolRequest.java, v 0.1 2019年09月01日 18:12 Exp $
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class UserAddSchoolRequest extends BaseRequest {

    /**
     * 学校名称
     */
    private String schoolName;

    /**
     * 1 幼儿园， 2小学， 3中学
     */
    private Integer type;

    /**
     * 区编码
     */
    private String areaCode;

    /**
     * 地址
     */
    private String address;

    /**
     * 纬度
     */
    private Double lat;

    /**
     * 经度
     */
    private Double lng;
}
