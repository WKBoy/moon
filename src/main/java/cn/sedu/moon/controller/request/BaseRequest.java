/*
 * Dian.com Inc.
 * Copyright (c) 2004-2018 All Rights Reserved.
 */
package cn.sedu.moon.controller.request;

import java.io.Serializable;

import lombok.Data;

/**
 * request 基类
 *
 * @author ${wukong}
 * @version $Id: BaseRequest.java, v 0.1 2018年12月10日 2:02 PM Exp $
 */
@Data
public class BaseRequest implements Serializable {

    private static final long serialVersionUID = 8252827203309573344L;

    /**
     * 翻页参数：偏移量
     */
    private Integer offset = 0;

    /**
     * 翻页参数：页大小
     */
    private Integer pageSize = 10;

    /**
     * 用户id
     */
    private Long userId;
}