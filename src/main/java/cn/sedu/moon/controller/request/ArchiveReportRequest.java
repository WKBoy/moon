/*
 * Dian.com Inc.
 * Copyright (c) 2004-2018 All Rights Reserved.
 */
package cn.sedu.moon.controller.request;

import java.util.List;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 上报学生档案的请求
 *
 * @author ${wukong}
 * @version $Id: ArchiveReportRequest.java, v 0.1 2018年12月14日 3:14 PM Exp $
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class ArchiveReportRequest extends BaseRequest {

    private static final long serialVersionUID = 8362572958013930277L;

    /**
     * 学生姓名
     */
    private String name;

    /**
     * 年级
     */
    private Long grade;

    /**
     * 省code
     */
    private String provinceCode;

    /**
     * 市code
     */
    private String cityCode;

    /**
     * 区code
     */
    private String areaCode;

    /**
     * 学校ID
     */
    private Long schoolId;

    /**
     * 性别，1 男，2 女
     */
    private Integer gender;

    /**
     * 学校属性，1 公办，2 民办 3 全选
     */
    private Integer schoolProperty;

    /**
     * 培养方向
     */
    private List<Long> growDirectionList;

    /**
     * 小区ID
     */
    private Long districtId;
}