/*
 * Dian.com Inc.
 * Copyright (c) 2004-2019 All Rights Reserved.
 */
package cn.sedu.moon.controller.request.cms;

import cn.sedu.moon.controller.request.BaseRequest;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 *
 * @author ${wukong}
 * @version $Id: StickNewsRequest.java, v 0.1 2019年04月19日 2:53 PM Exp $
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class StickNewsRequest extends BaseRequest {

    private static final long serialVersionUID = -638818433470462067L;

    /**
     * 新闻ID
     */
    private Long newsId;
}