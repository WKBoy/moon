/*
 * Dian.com Inc.
 * Copyright (c) 2004-2019 All Rights Reserved.
 */
package cn.sedu.moon.controller.request;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 *
 * @author ${wukong}
 * @version $Id: UserMobileReportRequest.java, v 0.1 2019年02月22日 4:15 PM Exp $
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class UserMobileReportRequest extends BaseRequest {

    private static final long serialVersionUID = 6448193306213417205L;

    private String encryptedData;

    private String iv;

    private String code;
}