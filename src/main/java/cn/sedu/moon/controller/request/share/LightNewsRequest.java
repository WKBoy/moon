/*
 * Dian.com Inc.
 * Copyright (c) 2004-2019 All Rights Reserved.
 */
package cn.sedu.moon.controller.request.share;

import cn.sedu.moon.controller.request.BaseRequest;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 *
 * @author ${wukong}
 * @version $Id: LightNewsRequest.java, v 0.1 2019年07月03日 6:17 PM Exp $
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class LightNewsRequest extends BaseRequest {

    private static final long serialVersionUID = -140709400363746067L;

    /**
     * 原分享者的用户id, 在原分享者分享给别人，别人打开的情况下才需要传这个参数上来
     */
    private Long shareUserId;

    /**
     * 新闻id
     */
    private Long newsId;
}