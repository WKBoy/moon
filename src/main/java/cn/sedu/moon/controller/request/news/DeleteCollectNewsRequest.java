/*
 * Dian.com Inc.
 * Copyright (c) 2004-2019 All Rights Reserved.
 */
package cn.sedu.moon.controller.request.news;

import cn.sedu.moon.controller.request.BaseRequest;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 *
 * @author ${wukong}
 * @version $Id: DeleteCollectNewsRequest.java, v 0.1 2019年06月23日 12:55 PM Exp $
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class DeleteCollectNewsRequest extends BaseRequest {

    private static final long serialVersionUID = -7045572140634313800L;

    private Long newsId;
}