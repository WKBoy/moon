/*
 * Dian.com Inc.
 * Copyright (c) 2004-2019 All Rights Reserved.
 */
package cn.sedu.moon.controller.request;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 *
 * @author ${wukong}
 * @version $Id: SopOptionAnswerRequest.java, v 0.1 2019年01月09日 5:30 PM Exp $
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class SopOptionAnswerRequest extends BaseRequest {

    private static final long serialVersionUID = 7729986650671365103L;

    /**
     * 选项id
     */
    private Integer id;

    /**
     * 输入文字
     */
    private String inputString;

}