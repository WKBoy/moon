/*
 * Dian.com Inc.
 * Copyright (c) 2004-2019 All Rights Reserved.
 */
package cn.sedu.moon.controller.request.cms.school;

import cn.sedu.moon.controller.request.BaseRequest;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author wukong
 * @version : CmsSchoolSearchRequest.java, v 0.1 2019年09月28日 3:23 PM Exp $
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class CmsSchoolSearchRequest extends BaseRequest {

    private static final long serialVersionUID = 7275007735720048019L;

    /**
     * 学校名称关键字
     */
    private String searchKey;
}