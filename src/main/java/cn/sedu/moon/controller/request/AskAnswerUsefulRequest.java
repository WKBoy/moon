/*
 * Dian.com Inc.
 * Copyright (c) 2004-2019 All Rights Reserved.
 */
package cn.sedu.moon.controller.request;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 *
 * @author ${wukong}
 * @version $Id: AskAnswerUsefulRequest.java, v 0.1 2019年01月29日 7:41 PM Exp $
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class AskAnswerUsefulRequest extends BaseRequest {

    private static final long serialVersionUID = -8285747644920095726L;

    private Long answerId;

    /**
     * true 为有用， false为没用
     */
    private Boolean useful;
}