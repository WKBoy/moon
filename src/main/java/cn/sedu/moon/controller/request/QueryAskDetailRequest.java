/*
 * Dian.com Inc.
 * Copyright (c) 2004-2019 All Rights Reserved.
 */
package cn.sedu.moon.controller.request;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 *
 * @author ${wukong}
 * @version $Id: QueryAskDetailRequest.java, v 0.1 2019年01月19日 11:19 PM Exp $
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class QueryAskDetailRequest extends BaseRequest {

    private static final long serialVersionUID = 3498490887307391068L;

    /**
     * 问题id
     */
    private Long id;

}