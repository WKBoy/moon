/*
 * Dian.com Inc.
 * Copyright (c) 2004-2019 All Rights Reserved.
 */
package cn.sedu.moon.controller.request.notice;

import cn.sedu.moon.controller.request.BaseRequest;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author wukong
 * @version : NoticeSubscribeRequest.java, v 0.1 2019年10月27日 12:54 上午 Exp $
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class NoticeSubscribeRequest extends BaseRequest {

    private static final long serialVersionUID = -4212652638793150231L;

    private Long noticeId;
}