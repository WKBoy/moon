/*
 * Dian.com Inc.
 * Copyright (c) 2004-2019 All Rights Reserved.
 */
package cn.sedu.moon.controller.request.cms.ask;

import cn.sedu.moon.controller.request.BaseRequest;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 *
 * @author ${wukong}
 * @version $Id: CmsAskDetailRequest.java, v 0.1 2019年07月09日 8:34 PM Exp $
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class CmsAskDetailRequest extends BaseRequest {

    private static final long serialVersionUID = -3142477925693735085L;

    private Long id;

}