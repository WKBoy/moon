/*
 * Dian.com Inc.
 * Copyright (c) 2004-2019 All Rights Reserved.
 */
package cn.sedu.moon.controller.request;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 *
 * @author ${wukong}
 * @version $Id: CmsNewsQueryRequest.java, v 0.1 2019年03月03日 11:18 PM Exp $
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class CmsNewsQueryRequest extends BaseRequest {

    private static final long serialVersionUID = 5047159120149515568L;

    /**
     * 根据编辑者过滤
     */
    private String editor;

    /**
     * 新闻来源
     */
    private String src;

    /**
     * 城市编码
     */
    private String cityCode;

    /**
     * 搜索关键字
     */
    private String searchKey;
}