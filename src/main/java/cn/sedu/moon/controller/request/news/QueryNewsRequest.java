/*
 * Dian.com Inc.
 * Copyright (c) 2004-2019 All Rights Reserved.
 */
package cn.sedu.moon.controller.request.news;

import cn.sedu.moon.controller.request.BaseRequest;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 *
 * @author ${wukong}
 * @version $Id: QueryNewsRequest.java, v 0.1 2019年02月17日 2:46 PM Exp $
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class QueryNewsRequest extends BaseRequest {

    private static final long serialVersionUID = 7721287547954124872L;

    /**
     * 翻页参数：偏移量
     */
    private Integer offset = 0;

    /**
     * 翻页参数：页大小
     */
    private Integer pageSize = 10;

    /**
     * 搜索关键字
     */
    private String key;

}