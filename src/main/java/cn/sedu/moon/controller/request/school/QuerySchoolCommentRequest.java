/*
 * Dian.com Inc.
 * Copyright (c) 2004-2019 All Rights Reserved.
 */
package cn.sedu.moon.controller.request.school;

import cn.sedu.moon.controller.request.BaseRequest;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author wukong
 * @version : QuerySchoolCommentRequest.java, v 0.1 2019年10月08日 6:55 PM Exp $
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class QuerySchoolCommentRequest extends BaseRequest {

    private static final long serialVersionUID = 3884381932785256070L;

    /**
     * 学校id
     */
    private Long schoolId;

}