/*
 * Dian.com Inc.
 * Copyright (c) 2004-2019 All Rights Reserved.
 */
package cn.sedu.moon.controller.request.cms.ask;

import cn.sedu.moon.controller.request.BaseRequest;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 *
 * @author ${wukong}
 * @version $Id: CmsSearchAskRequest.java, v 0.1 2019年07月08日 1:40 PM Exp $
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class CmsSearchAskRequest extends BaseRequest {

    private static final long serialVersionUID = 8560396660330791611L;

    private String keyword;

}