/*
 * Dian.com Inc.
 * Copyright (c) 2004-2018 All Rights Reserved.
 */
package cn.sedu.moon.controller.request;

import java.util.List;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 *
 * @author ${wukong}
 * @version $Id: SchoolAttentionRequest.java, v 0.1 2018年12月16日 11:25 AM Exp $
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class SchoolAttentionRequest extends BaseRequest {

    private static final long serialVersionUID = 5901032847223090481L;

    private List<Long> schoolList;
}