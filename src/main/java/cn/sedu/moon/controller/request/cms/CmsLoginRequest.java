/*
 * Dian.com Inc.
 * Copyright (c) 2004-2019 All Rights Reserved.
 */
package cn.sedu.moon.controller.request.cms;

import cn.sedu.moon.controller.request.BaseRequest;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 *
 * @author ${wukong}
 * @version $Id: CmsLoginRequest.java, v 0.1 2019年02月20日 6:52 PM Exp $
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class CmsLoginRequest extends BaseRequest {

    private static final long serialVersionUID = 8173107391382308149L;

    private String username;

    private String password;

}