/*
 * Dian.com Inc.
 * Copyright (c) 2004-2019 All Rights Reserved.
 */
package cn.sedu.moon.controller.request;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 *
 * @author ${wukong}
 * @version $Id: QueryGloryRequest.java, v 0.1 2019年04月01日 6:43 PM Exp $
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class QueryGloryRequest extends BaseRequest {

    private static final long serialVersionUID = 37710011093822510L;

}