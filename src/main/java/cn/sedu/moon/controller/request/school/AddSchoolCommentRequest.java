/*
 * Dian.com Inc.
 * Copyright (c) 2004-2019 All Rights Reserved.
 */
package cn.sedu.moon.controller.request.school;

import cn.sedu.moon.controller.request.BaseRequest;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

/**
 * @author wukong
 * @version : AddSchoolCommentRequest.java, v 0.1 2019年10月08日 6:56 PM Exp $
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class AddSchoolCommentRequest extends BaseRequest {

    private static final long serialVersionUID = -1155344834482710803L;

    private Long schoolId;

    private String content;

    private List<String> imgList;


}