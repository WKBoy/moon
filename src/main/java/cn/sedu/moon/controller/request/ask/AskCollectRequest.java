/*
 * Dian.com Inc.
 * Copyright (c) 2004-2019 All Rights Reserved.
 */
package cn.sedu.moon.controller.request.ask;

import cn.sedu.moon.controller.request.BaseRequest;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author wukong
 * @version : AskCollectRequest.java, v 0.1 2019年09月15日 14:38 Exp $
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class AskCollectRequest extends BaseRequest {

    private static final long serialVersionUID = 6839676442132818152L;

    private Long askId;
}