/*
 * Dian.com Inc.
 * Copyright (c) 2004-2019 All Rights Reserved.
 */
package cn.sedu.moon.controller.request;

import java.util.List;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 *
 * @author ${wukong}
 * @version $Id: SopReportRequest.java, v 0.1 2019年01月06日 6:21 PM Exp $
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class SopReportRequest extends BaseRequest {

    private static final long serialVersionUID = -1949107608798168997L;

    /**
     * 问题id
     */
    private Integer id;

    /**
     * 学校id
     */
    private Long schoolId;

    /**
     * 用户输入的文本
     */
    private String inputString;

    /**
     * 选项列表
     */
    private List<SopOptionAnswerRequest> optionList;
}