/*
 * Dian.com Inc.
 * Copyright (c) 2004-2019 All Rights Reserved.
 */
package cn.sedu.moon.controller.request.cms.ask;

import cn.sedu.moon.controller.request.BaseRequest;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 *
 * @author ${wukong}
 * @version $Id: CmsAskReplyRequest.java, v 0.1 2019年07月08日 1:47 PM Exp $
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class CmsAskReplyRequest extends BaseRequest {

    private static final long serialVersionUID = 1522171964123909502L;

    /**
     * 问题id
     */
    private Long id;

    /**
     * 答案内容
     */
    private String content;

}