/*
 * Dian.com Inc.
 * Copyright (c) 2004-2019 All Rights Reserved.
 */
package cn.sedu.moon.controller.request;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 *
 * @author ${wukong}
 * @version $Id: UserBaseInfoReportRequest.java, v 0.1 2019年01月06日 2:47 PM Exp $
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class UserBaseInfoReportRequest extends BaseRequest {

    private static final long serialVersionUID = 6854455004277402987L;

    /**
     * 昵称
     */
    private String nickName;

    private Integer gender;

    private String language;

    private String city;

    private String province;

    private String country;

    private String avatarUrl;

}