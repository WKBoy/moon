/*
 * Dian.com Inc.
 * Copyright (c) 2004-2018 All Rights Reserved.
 */
package cn.sedu.moon.controller.request;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 *
 * @author ${wukong}
 * @version $Id: QuerySchoolNewsRequest.java, v 0.1 2018年12月16日 6:25 PM Exp $
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class QuerySchoolNewsRequest extends BaseRequest {

    private static final long serialVersionUID = 3993242555388313402L;

    /**
     * 新闻ID
     */
    private Long id;

}