/*
 * Dian.com Inc.
 * Copyright (c) 2004-2019 All Rights Reserved.
 */
package cn.sedu.moon.controller.request.cms.user;

import cn.sedu.moon.controller.request.BaseRequest;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author wukong
 * @version : CmsSearchUserRequest.java, v 0.1 2019年09月22日 19:01 Exp $
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class CmsSearchUserRequest extends BaseRequest {

    private static final long serialVersionUID = 3302294640300290122L;

    private Integer offset = 0 ;

    private Integer pageSize = 4000;

    /**
     * 昵称
     */
    private String nickName;

    /**
     * 市编码
     */
    private String cityCode;

    /**
     * 阶段
     */
    private Long grade;
}