/*
 * Dian.com Inc.
 * Copyright (c) 2004-2018 All Rights Reserved.
 */
package cn.sedu.moon.controller.request;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 *
 * @author ${wukong}
 * @version $Id: AreaRequest.java, v 0.1 2018年10月25日 11:53 AM Exp $
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class AreaRequest extends BaseRequest {

    private static final long serialVersionUID = -1033570326361698825L;

    /**
     * 省市区父节点
     */
    private String parentCode;
}