/*
 * Dian.com Inc.
 * Copyright (c) 2004-2019 All Rights Reserved.
 */
package cn.sedu.moon.controller;

import cn.sedu.moon.common.Result;
import cn.sedu.moon.common.enums.ErrorCodeEnum;
import cn.sedu.moon.common.exception.BizException;
import cn.sedu.moon.common.template.ControllerCallback;
import cn.sedu.moon.controller.request.cms.CmsLoginRequest;
import cn.sedu.moon.service.CmsService;
import com.github.pagehelper.util.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * 管理后台控制器
 *
 * @author ${wukong}
 * @version $Id: CmsController.java, v 0.1 2019年02月20日 6:48 PM Exp $
 */
@RestController
public class CmsController extends BaseController {

    @Autowired
    private CmsService cmsService;

    @RequestMapping(value = "/moon/cms/login", method = RequestMethod.POST)
    public Result login(@RequestBody CmsLoginRequest request) {
        return template.execute(new ControllerCallback<String>() {
            @Override
            public void checkParam() {

                if (StringUtil.isEmpty(request.getUsername())) {
                    throw new BizException(ErrorCodeEnum.PARAM_CHECK_EXP, "用户名不能为空");
                }

                if (StringUtil.isEmpty(request.getPassword())) {
                    throw new BizException(ErrorCodeEnum.PARAM_CHECK_EXP, "密码不能为空");
                }
            }

            @Override
            public String execute() {
                return cmsService.login(request);
            }
        });
    }
}