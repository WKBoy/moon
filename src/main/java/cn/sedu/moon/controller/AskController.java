/*
 * Dian.com Inc.
 * Copyright (c) 2004-2019 All Rights Reserved.
 */
package cn.sedu.moon.controller;

import cn.sedu.moon.common.Result;
import cn.sedu.moon.common.auth.Auth;
import cn.sedu.moon.common.enums.ErrorCodeEnum;
import cn.sedu.moon.common.exception.BizException;
import cn.sedu.moon.common.template.ControllerCallback;
import cn.sedu.moon.controller.request.AskAcceptAnswerRequest;
import cn.sedu.moon.controller.request.AskAnswerUsefulRequest;
import cn.sedu.moon.controller.request.AskEditRequest;
import cn.sedu.moon.controller.request.AskPublishRequest;
import cn.sedu.moon.controller.request.AskReportAnswerRequest;
import cn.sedu.moon.controller.request.AskSearchRequest;
import cn.sedu.moon.controller.request.BaseRequest;
import cn.sedu.moon.controller.request.QueryAskDetailRequest;
import cn.sedu.moon.controller.request.ask.AskCollectDeleteRequest;
import cn.sedu.moon.controller.request.ask.AskCollectRequest;
import cn.sedu.moon.controller.response.AskAnswerResponse;
import cn.sedu.moon.controller.response.AskDetailResponse;
import cn.sedu.moon.controller.response.AskPublishResponse;
import cn.sedu.moon.controller.response.AskReportAnswerResponse;
import cn.sedu.moon.controller.response.AskResponse;
import cn.sedu.moon.service.AskService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 *
 * @author ${wukong}
 * @version $Id: AskController.java, v 0.1 2019年01月19日 11:11 PM Exp $
 */
@RestController
public class AskController extends BaseController {

    @Autowired
    private AskService askService;

    /**
     * 提交问题
     *
     * @return request
     */
    @RequestMapping(value = "/moon/ask/publish", method = RequestMethod.POST)
    @Auth
    public Result publish(@RequestBody AskPublishRequest request) {

        return template.execute(new ControllerCallback<AskPublishResponse>() {
            @Override
            public void checkParam() {
                if (StringUtils.isBlank(request.getTitle())) {
                    throw new BizException(ErrorCodeEnum.PARAM_CHECK_EXP, "问题标题不能为空");
                }
            }

            @Override
            public AskPublishResponse execute() {

                tracker();

                request.setUserId(getUserId());
                return askService.askPublish(request);
            }
        });
    }



    /**
     * 收藏问答
     *
     * @return request
     */
    @RequestMapping(value = "/moon/ask/collect", method = RequestMethod.POST)
    @Auth
    public Result collect(@RequestBody AskCollectRequest request) {

        return template.execute(new ControllerCallback<Boolean>() {
            @Override
            public void checkParam() {
                if (request.getAskId() == null) {
                    throw new BizException(ErrorCodeEnum.PARAM_CHECK_EXP, "问题id不能为空");
                }
            }

            @Override
            public Boolean execute() {
                request.setUserId(getUserId());
                askService.collect(getUserId(), request.getAskId());
                return true;
            }
        });
    }

    /**
     * 取消收藏问答
     *
     * @return request
     */
    @RequestMapping(value = "/moon/ask/collectDelete", method = RequestMethod.POST)
    @Auth
    public Result collectDelete(@RequestBody AskCollectDeleteRequest request) {

        return template.execute(new ControllerCallback<Boolean>() {
            @Override
            public void checkParam() {
                if (request.getAskId() == null) {
                    throw new BizException(ErrorCodeEnum.PARAM_CHECK_EXP, "问题id不能为空");
                }
            }

            @Override
            public Boolean execute() {
                request.setUserId(getUserId());
                askService.collectDelete(getUserId(), request.getAskId());
                return true;
            }
        });
    }

    @RequestMapping(value = "/moon/ask/edit", method = RequestMethod.POST)
    @Auth
    public Result edit(@RequestBody AskEditRequest request) {

        return template.execute(new ControllerCallback<Boolean>() {
            @Override
            public void checkParam() {

                if (request.getId() == null) {
                    throw new BizException(ErrorCodeEnum.PARAM_CHECK_EXP, "问题ID不能为空");
                }

                if (StringUtils.isBlank(request.getTitle())) {
                    throw new BizException(ErrorCodeEnum.PARAM_CHECK_EXP, "问题标题不能为空");
                }
            }

            @Override
            public Boolean execute() {
                request.setUserId(getUserId());
                askService.edit(request);
                return true;
            }
        });
    }

    /**
     * 按照关键字搜索问题
     *
     * @return request
     */
    @RequestMapping(value = "/moon/ask/search", method = RequestMethod.GET)
    @Auth
    public Result search(AskSearchRequest request) {

        return template.execute(new ControllerCallback<List<AskResponse>>() {
            @Override
            public void checkParam() {
            }

            @Override
            public List<AskResponse> execute() {
                return askService.search(request);
            }
        });
    }

    @RequestMapping(value = "/moon/ask/myAsk", method = RequestMethod.GET)
    @Auth
    public Result myAskList() {
        return template.execute(new ControllerCallback<List<AskResponse>>() {

            @Override
            public void checkParam() {

            }

            @Override
            public List<AskResponse> execute() {
                return askService.myAskList(getUserId());
            }
        });
    }

    @RequestMapping(value = "/moon/ask/myAttention", method = RequestMethod.GET)
    @Auth
    public Result myAttention() {
        return template.execute(new ControllerCallback<List<AskResponse>>() {

            @Override
            public void checkParam() {

            }

            @Override
            public List<AskResponse> execute() {
                return askService.myAttention(getUserId());
            }
        });
    }

    @RequestMapping(value = "/moon/ask/myAnswer", method = RequestMethod.GET)
    @Auth
    public Result myAnswer() {
        return template.execute(new ControllerCallback<List<AskResponse>>() {

            @Override
            public void checkParam() {

            }

            @Override
            public List<AskResponse> execute() {
                return askService.myAnswer(getUserId());
            }
        });
    }

    @RequestMapping(value = "/moon/ask/myCollect", method = RequestMethod.GET)
    @Auth
    public Result myCollect(BaseRequest request) {
        return template.execute(new ControllerCallback<List<AskResponse>>() {

            @Override
            public void checkParam() {

            }

            @Override
            public List<AskResponse> execute() {
                return askService.myCollect(getUserId(), request.getOffset(), request.getPageSize());
            }
        });
    }

    /**
     * 查找问题详情
     *
     * @return request
     */
    @RequestMapping(value = "/moon/ask/detail", method = RequestMethod.GET)
    @Auth
    public Result detail(QueryAskDetailRequest request) {

        return template.execute(new ControllerCallback<AskDetailResponse>() {
            @Override
            public void checkParam() {
                if (request.getId() == null){
                    throw new BizException(ErrorCodeEnum.PARAM_CHECK_EXP, "问题ID不能为空");
                }
            }

            @Override
            public AskDetailResponse execute() {

                tracker();

                request.setUserId(getUserId());
                return askService.askDetail(request);
            }
        });
    }

    /**
     * 查找问题答案列表
     *
     * @return request
     */
    @RequestMapping(value = "/moon/ask/answer/list", method = RequestMethod.GET)
    @Auth
    public Result answerList(QueryAskDetailRequest request) {

        return template.execute(new ControllerCallback<List<AskAnswerResponse>>() {
            @Override
            public void checkParam() {
                if (request.getId() == null){
                    throw new BizException(ErrorCodeEnum.PARAM_CHECK_EXP, "问题ID不能为空");
                }
            }

            @Override
            public List<AskAnswerResponse> execute() {
                request.setUserId(getUserId());
                return askService.queryAnswerList(request);
            }
        });
    }

    /**
     * 回答问题
     *
     * @return request
     */
    @RequestMapping(value = "/moon/ask/detail/reportAnswer", method = RequestMethod.POST)
    @Auth
    public Result reportAnswer(@RequestBody AskReportAnswerRequest request) {

        return template.execute(new ControllerCallback<AskReportAnswerResponse>() {
            @Override
            public void checkParam() {
                if (request.getId() == null) {
                    throw new BizException(ErrorCodeEnum.PARAM_CHECK_EXP, "问题ID不能为空");
                }

                if (StringUtils.isBlank(request.getContent())) {
                    throw new BizException(ErrorCodeEnum.PARAM_CHECK_EXP, "答案内容不能为空");
                }
            }

            @Override
            public AskReportAnswerResponse execute() {
                request.setUserId(getUserId());
                return askService.reportAnswer(request);
            }
        });
    }

    /**
     * 关注问题
     *
     * @return request
     */
    @RequestMapping(value = "/moon/ask/follow", method = RequestMethod.POST)
    @Auth
    public Result follow(@RequestBody AskReportAnswerRequest request) {

        return template.execute(new ControllerCallback<Boolean>() {
            @Override
            public void checkParam() {
                if (request.getId() == null) {
                    throw new BizException(ErrorCodeEnum.PARAM_CHECK_EXP, "问题ID不能为空");
                }
            }

            @Override
            public Boolean execute() {
                tracker();
                askService.follow(request.getId(), getUserId());
                return true;
            }
        });
    }

    @RequestMapping(value = "/moon/ask/accept", method = RequestMethod.POST)
    @Auth
    public Result accept(@RequestBody AskAcceptAnswerRequest request) {

        return template.execute(new ControllerCallback<Boolean>() {
            @Override
            public void checkParam() {
                if (request.getAskId() == null) {
                    throw new BizException(ErrorCodeEnum.PARAM_CHECK_EXP, "问题ID不能为空");
                }

                if (request.getAnswerId() == null) {
                    throw new BizException(ErrorCodeEnum.PARAM_CHECK_EXP, "答案ID不能为空");
                }
            }

            @Override
            public Boolean execute() {
                askService.accept(request.getAskId(), request.getAnswerId(), getUserId());
                return true;
            }
        });
    }

    @RequestMapping(value = "/moon/ask/useful", method = RequestMethod.POST)
    @Auth
    public Result useful(@RequestBody AskAnswerUsefulRequest request) {

        return template.execute(new ControllerCallback<Boolean>() {
            @Override
            public void checkParam() {
                if (request.getAnswerId() == null) {
                    throw new BizException(ErrorCodeEnum.PARAM_CHECK_EXP, "答案ID不能为空");
                }

                if (request.getUseful() == null) {
                    throw new BizException(ErrorCodeEnum.PARAM_CHECK_EXP, "useful不能为空");
                }
            }

            @Override
            public Boolean execute() {

                if (request.getUseful()) {
                    askService.useful(request.getAnswerId(), getUserId());
                } else {
                    askService.unUseful(request.getAnswerId(), getUserId());
                }

                return true;
            }
        });
    }





}