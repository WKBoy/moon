/*
 * Dian.com Inc.
 * Copyright (c) 2004-2018 All Rights Reserved.
 */
package cn.sedu.moon.controller;

import cn.sedu.moon.common.Result;
import cn.sedu.moon.common.auth.Auth;
import cn.sedu.moon.common.enums.ErrorCodeEnum;
import cn.sedu.moon.common.exception.BizException;
import cn.sedu.moon.common.template.ControllerCallback;
import cn.sedu.moon.controller.request.QuerySchoolRequest;
import cn.sedu.moon.controller.request.SchoolAttentionRequest;
import cn.sedu.moon.controller.request.school.UserAddSchoolRequest;
import cn.sedu.moon.controller.response.SchoolDetailResponse;
import cn.sedu.moon.controller.response.SchoolRecommendResponse;
import cn.sedu.moon.controller.response.SchoolResponse;
import cn.sedu.moon.controller.response.school.UserAddSchoolResponse;
import cn.sedu.moon.service.SchoolService;
import cn.sedu.moon.service.context.QuerySchoolContext;
import cn.sedu.moon.service.context.SchoolAttentionContext;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * 学校控制器
 *
 * @author ${wukong}
 * @version $Id: SchoolController.java, v 0.1 2018年12月14日 6:05 PM Exp $
 */
@RestController
public class SchoolController extends BaseController {

    @Autowired
    private SchoolService schoolService;

    @Auth
    @RequestMapping(value = "/moon/2.0/recommend/schoolList", method = RequestMethod.GET)
    public Result queryRecommendSchool(QuerySchoolRequest request) {

        return template.execute(new ControllerCallback<List<SchoolRecommendResponse>>() {

            QuerySchoolContext context = new QuerySchoolContext();

            @Override
            public void checkParam() {
                if (request.getOffset() == null || request.getPageSize() == null) {
                    throw new BizException(ErrorCodeEnum.PARAM_CHECK_EXP, "翻页参数不能为空");
                }

                if (request.getQueryType() == null) {
                    throw new BizException(ErrorCodeEnum.PARAM_CHECK_EXP, "查询类型不能为空");
                }
            }

            @Override
            public void buildContext() {
                context.setUserId(getUserId());
//                context.setUserId(2562L);
                context.setOffset(request.getOffset());
                context.setPageSize(request.getPageSize());
                context.setRecommendType(request.getQueryType());
            }

            @Override
            public List<SchoolRecommendResponse> execute() {
                tracker();
                return schoolService.queryRecommendSchoolWithType(context);
            }
        });
    }


    @Auth
    @RequestMapping(value = "/moon/intro/recommend/schoolList", method = RequestMethod.GET)
    public Result recommendSchoolList(QuerySchoolRequest request) {

        return template.execute(new ControllerCallback<List<SchoolRecommendResponse>>() {

            QuerySchoolContext context = new QuerySchoolContext();

            @Override
            public void checkParam() {
                if (request.getOffset() == null || request.getPageSize() == null) {
                    throw new BizException(ErrorCodeEnum.PARAM_CHECK_EXP, "翻页参数不能为空");
                }
            }

            @Override
            public void buildContext() {
                context.setUserId(getUserId());
                //context.setUserId(2489L);
                context.setOffset(request.getOffset());
                context.setPageSize(request.getPageSize());
            }

            @Override
            public List<SchoolRecommendResponse> execute() {
                return schoolService.recommendSchoolList(context);
            }
        });
    }

    @Auth
    @RequestMapping(value = "/moon/school/detail", method = RequestMethod.GET)
    public Result detail(QuerySchoolRequest request) {

        return template.execute(new ControllerCallback<SchoolDetailResponse>() {
            @Override
            public void checkParam() {
                if (request.getSchoolId() == null) {
                    throw new BizException(ErrorCodeEnum.PARAM_CHECK_EXP, "学校ID不能为空");
                }
            }

            @Override
            public SchoolDetailResponse execute() {
                return schoolService.detail(request.getSchoolId());
            }
        });
    }

    @Auth
    @RequestMapping(value = "/moon/school/userAdd", method = RequestMethod.POST)
    public Result userAdd(@RequestBody UserAddSchoolRequest request) {

        return template.execute(new ControllerCallback<UserAddSchoolResponse>() {
            @Override
            public void checkParam() {

                if (StringUtils.isBlank(request.getSchoolName())) {
                    throw new BizException(ErrorCodeEnum.PARAM_CHECK_EXP, "学校名称不能为空");
                }

                if (StringUtils.isBlank(request.getAddress())) {
                    throw new BizException(ErrorCodeEnum.PARAM_CHECK_EXP, "学校地址不能为空");
                }

                if (StringUtils.isBlank(request.getAreaCode())) {
                    throw new BizException(ErrorCodeEnum.PARAM_CHECK_EXP, "学校区编码不能为空");
                }

                if (request.getLat() == null || request.getLng() == null) {
                    throw new BizException(ErrorCodeEnum.PARAM_CHECK_EXP, "学校经纬度不能为空");
                }

                if (request.getType() == null) {
                    throw new BizException(ErrorCodeEnum.PARAM_CHECK_EXP, "学校类型不能为空");
                }
            }

            @Override
            public UserAddSchoolResponse execute() {
                return schoolService.userAddSchool(request);
            }
        });
    }

    @Auth
    @RequestMapping(value = "/user/attention/schoolList", method = RequestMethod.POST)
    public Result attentionSchoolList(@RequestBody QuerySchoolRequest request) {

        return template.execute(new ControllerCallback<List<SchoolResponse>>() {

            QuerySchoolContext context = new QuerySchoolContext();

            @Override
            public void checkParam() {
                if (request.getOffset() == null || request.getPageSize() == null) {
                    throw new BizException(ErrorCodeEnum.PARAM_CHECK_EXP, "翻页参数不能为空");
                }
            }

            @Override
            public void buildContext() {
                context.setUserId(getUserId());
                context.setOffset(request.getOffset());
                context.setPageSize(request.getPageSize());
            }

            @Override
            public List<SchoolResponse> execute() {
                return schoolService.attentionSchoolList(context);
            }
        });
    }


    @Auth
    @RequestMapping(value = "/school/attention/report", method = RequestMethod.POST)
    public Result attentionSchoolReport(@RequestBody SchoolAttentionRequest request) {

        return template.execute(new ControllerCallback<Boolean>() {

            SchoolAttentionContext context = new SchoolAttentionContext();

            @Override
            public void checkParam() {
                if (request.getSchoolList() == null || request.getSchoolList().size() == 0) {
                    throw new BizException(ErrorCodeEnum.PARAM_CHECK_EXP, "请至少选择一个学校");
                }
            }

            @Override
            public void buildContext() {
                context.setSchoolList(request.getSchoolList());
                context.setUserId(getUserId());
            }

            @Override
            public Boolean execute() {
                schoolService.attentionSchoolReport(context);
                return true;
            }
        });
    }

}