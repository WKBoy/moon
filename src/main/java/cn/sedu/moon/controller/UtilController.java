/*
 * Dian.com Inc.
 * Copyright (c) 2004-2019 All Rights Reserved.
 */
package cn.sedu.moon.controller;

import cn.sedu.moon.common.enums.SchoolPropertyEnum;
import cn.sedu.moon.common.enums.SchoolTypeEnum;
import cn.sedu.moon.dao.SchoolDAO;
import cn.sedu.moon.dao.model.School;
import cn.sedu.moon.dao.mongo.SchoolLocation;
import com.github.pagehelper.ISelect;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.index.GeospatialIndex;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author wukong
 * @version : UtilController.java, v 0.1 2019年08月21日 14:25 Exp $
 */
@RestController
@Slf4j
public class UtilController extends BaseController {

    @Autowired
    private MongoTemplate mongoTemplate;

    @Autowired
    private SchoolDAO schoolDAO;

    @RequestMapping(value = "/moon/util/mongo/init", method = RequestMethod.GET)
    public String mongoInit() {

        log.info("------------------ mongoInit Start-----------------");
        initData();
        log.info("------------------ mongoInit End-----------------");

        return "ok";
    }

    private void initData() {
        // 先清空
        mongoTemplate.dropCollection(SchoolLocation.class);

        // 创建索引
        mongoTemplate.indexOps(SchoolLocation.class).ensureIndex(new GeospatialIndex("position"));

        // 查找学校数据
        School querySchoolModel = new School();
        querySchoolModel.setDeleted(0);

        Integer offset = 0;
        Integer pageSize = 1000;
        Integer count = -1;

        Integer totalCount = 0;

        while (count != 0) {

            System.out.println("offset is " + offset);

            Page<School> page = PageHelper.offsetPage(offset, pageSize).doSelectPage(new ISelect() {
                @Override
                public void doSelect() {
                    schoolDAO.listRecord(querySchoolModel);
                }
            });

            count = page.size();
            totalCount += count;
            List<School> schoolList = page.getResult();

            for (School school : schoolList) {
                if (school.getLng() != null && school.getLat() != null) {

                    SchoolLocation schoolLocation = new SchoolLocation();
                    schoolLocation.setPosition(new double[]{school.getLng(), school.getLat()});
                    schoolLocation.setSchoolId(school.getId());
                    schoolLocation.setName(school.getName());

                    // 公办，民办
                    if (SchoolPropertyEnum.GONGBAN.getChineseName().equals(school.getProperty())) {
                        schoolLocation.setProperty(SchoolPropertyEnum.GONGBAN.getCode());
                    } else if (SchoolPropertyEnum.MINBAN.getChineseName().equals(school.getProperty())) {
                        schoolLocation.setProperty(SchoolPropertyEnum.MINBAN.getCode());
                    } else {
                        schoolLocation.setProperty(0);
                    }

                    // 小学，初中，高中
                    if (SchoolTypeEnum.YOUERYUAN.getChineseName().equals(school.getType())) {
                        schoolLocation.setType(SchoolTypeEnum.YOUERYUAN.getCode());
                    } else if (SchoolTypeEnum.XIAOXUE.getChineseName().equals(school.getType())) {
                        schoolLocation.setType(SchoolTypeEnum.XIAOXUE.getCode());
                    } else if (SchoolTypeEnum.CHUZHONG.getChineseName().equals(school.getType())) {
                        schoolLocation.setType(SchoolTypeEnum.CHUZHONG.getCode());
                    } else if (SchoolTypeEnum.GAOZHONG.getChineseName().equals(school.getType())) {
                        schoolLocation.setType(SchoolTypeEnum.GAOZHONG.getCode());
                    }

                    mongoTemplate.save(schoolLocation);
                }
            }

            offset += pageSize;
        }

        System.out.println("total count is " + totalCount);
        System.out.println("Finish");
    }
}