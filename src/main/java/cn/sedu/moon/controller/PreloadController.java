/*
 * Dian.com Inc.
 * Copyright (c) 2004-2019 All Rights Reserved.
 */
package cn.sedu.moon.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author wukong
 * @version : PreloadController.java, v 0.1 2019年12月05日 7:44 下午 Exp $
 */
@RestController
public class PreloadController extends BaseController  {

    @RequestMapping(value = {"/venice/preload", "/preload"})
    public @ResponseBody
    String preload() {
        return "OK";
    }

}