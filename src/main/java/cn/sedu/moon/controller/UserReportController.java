/*
 * Dian.com Inc.
 * Copyright (c) 2004-2019 All Rights Reserved.
 */
package cn.sedu.moon.controller;

import cn.sedu.moon.common.Result;
import cn.sedu.moon.common.auth.Auth;
import cn.sedu.moon.common.enums.ErrorCodeEnum;
import cn.sedu.moon.common.exception.BizException;
import cn.sedu.moon.common.template.ControllerCallback;
import cn.sedu.moon.controller.request.DistrictUserReportRequest;
import cn.sedu.moon.controller.request.SchoolUserReportRequest;
import cn.sedu.moon.service.UserReportService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author ${wukong}
 * @version $Id: UserReportController.java, v 0.1 2019年03月12日 2:16 AM Exp $
 */
@RestController
public class UserReportController extends BaseController {

    @Autowired
    private UserReportService userReportService;

    @RequestMapping(value = "/moon/user/report/school", method = RequestMethod.POST)
    @Auth
    public Result school(@RequestBody SchoolUserReportRequest request) {

        return template.execute(new ControllerCallback<Boolean>() {

            @Override
            public void checkParam() {

                if (StringUtils.isBlank(request.getName())) {
                    throw new BizException(ErrorCodeEnum.PARAM_CHECK_EXP, "名称不能为空");
                }

                if (StringUtils.isBlank(request.getProvinceCode())
                        || StringUtils.isBlank(request.getCityCode())
                        || StringUtils.isBlank(request.getAreaCode())) {
                    throw new BizException(ErrorCodeEnum.PARAM_CHECK_EXP, "省市区编码不可为空");
                }
            }

            @Override
            public Boolean execute() {
                request.setUserId(getUserId());
                userReportService.reportSchool(request);
                return true;
            }
        });
    }

    @RequestMapping(value = "/moon/user/report/district", method = RequestMethod.POST)
    @Auth
    public Result district(@RequestBody DistrictUserReportRequest request) {

        return template.execute(new ControllerCallback<Boolean>() {

            @Override
            public void checkParam() {
                if (StringUtils.isBlank(request.getName())) {
                    throw new BizException(ErrorCodeEnum.PARAM_CHECK_EXP, "名称不能为空");
                }

                if (StringUtils.isBlank(request.getProvinceCode())
                        || StringUtils.isBlank(request.getCityCode())
                        || StringUtils.isBlank(request.getAreaCode())) {
                    throw new BizException(ErrorCodeEnum.PARAM_CHECK_EXP, "省市区编码不可为空");
                }
            }

            @Override
            public Boolean execute() {
                request.setUserId(getUserId());
                userReportService.reportDistrict(request);
                return true;
            }
        });
    }
}