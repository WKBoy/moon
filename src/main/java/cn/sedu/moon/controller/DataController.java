/*
 * Dian.com Inc.
 * Copyright (c) 2004-2019 All Rights Reserved.
 */
package cn.sedu.moon.controller;

import cn.sedu.moon.common.Result;
import cn.sedu.moon.common.enums.ErrorCodeEnum;
import cn.sedu.moon.common.exception.BizException;
import cn.sedu.moon.common.template.ControllerCallback;
import cn.sedu.moon.controller.request.AreaRequest;
import cn.sedu.moon.service.DataService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author wukong
 * @version : DataController.java, v 0.1 2019年10月31日 9:19 下午 Exp $
 */
@RestController
public class DataController extends BaseController {

    @Autowired
    private DataService dataService;

    @RequestMapping(value = "/moon/data/task", method = RequestMethod.GET)
    public Result dataTask(AreaRequest request) {

        return template.execute(new ControllerCallback<Boolean>() {
            @Override
            public void checkParam() {
//                if (StringUtils.isBlank(request.getParentCode())) {
//                    throw new BizException(ErrorCodeEnum.SYS_EXP,  "parentCode 不能为空");
//                }
            }

            @Override
            public Boolean execute() {
                dataService.processGdDataAsync();
//                dataService.fetchGdData(request.getParentCode());
                return true;
            }
        });
    }

    @RequestMapping(value = "/moon/data/pvuv", method = RequestMethod.GET)
    public Result pvuv(AreaRequest request) {

        return template.execute(new ControllerCallback<Boolean>() {
            @Override
            public void checkParam() {
                if (StringUtils.isBlank(request.getParentCode())) {
                    throw new BizException(ErrorCodeEnum.SYS_EXP, "parentCode 不能为空");
                }
            }

            @Override
            public Boolean execute() {
                dataService.fetchGdData(request.getParentCode());
                return true;
            }
        });
    }





}