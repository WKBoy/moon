/*
 * Dian.so Inc.
 * Copyright (c) 2016-2019 All Rights Reserved.
 */
package cn.sedu.moon.dao;

import cn.sedu.moon.dao.model.GdSource;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;


/**
 * 继承于BaseDAO，默认有五个方法实现
 * listRecord、getRecord、saveRecord、removeRecord、updateRecord
 *
 * @author MBG工具生成
 * @version $Id: GdSourceDAO.java, v 0.1 2019-10-31 23:38:54 Exp $
 */
@Mapper
public interface GdSourceDAO extends BaseDAO<GdSource> {

    int saveBatch(@Param("list") List<GdSource> list);

    Integer count(GdSource gdSource);
}