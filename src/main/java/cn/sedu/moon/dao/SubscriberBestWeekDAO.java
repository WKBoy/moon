package cn.sedu.moon.dao;

import cn.sedu.moon.dao.model.SubscriberBestWeek;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface SubscriberBestWeekDAO extends BaseDAO<SubscriberBestWeek> {


    /**
     * 删除用户订阅记录
     *
     * @param userId
     * @return
     */
   int deleteByUserId(Long userId);



}
