/*
 * Dian.so Inc.
 * Copyright (c) 2016-2018 All Rights Reserved.
 */
package cn.sedu.moon.dao;

import cn.sedu.moon.dao.model.StuSch;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;


/**
 * 继承于BaseDAO，默认有五个方法实现
 * listRecord、getRecord、saveRecord、removeRecord、updateRecord
 *
 * @author MBG工具生成
 * @version $Id: StuSchDAO.java, v 0.1 2018-12-05 16:23:27 Exp $
 */
@Mapper
public interface StuSchDAO extends BaseDAO<StuSch> {

    int deleteByUserId(StuSch stuSch);

    /**
     * 根据用户ID获取所有想读学校idlist
     * @param userId
     * @return
     */
    List<Long> getSchoolIdListByUserId(Long userId);

    /**
     * 查看用户是否有几个想读学校
     * @param userId
     * @return
     */
    Integer countByStuId(@Param("userId") Long userId);

    /**
     * 查看该学校有多少人想读
     * @param schoolId
     * @return
     */
    Integer countBySchoolId(@Param("schoolId") Long schoolId);
}