/*
 * Dian.so Inc.
 * Copyright (c) 2016-2019 All Rights Reserved.
 */
package cn.sedu.moon.dao;

import cn.sedu.moon.dao.model.LogNewsDetail;
import org.apache.ibatis.annotations.Mapper;


/**
 * 继承于BaseDAO，默认有五个方法实现
 * listRecord、getRecord、saveRecord、removeRecord、updateRecord
 *
 * @author MBG工具生成
 * @version $Id: LogNewsDetailDAO.java, v 0.1 2019-08-24 16:42:08 Exp $
 */
@Mapper
public interface LogNewsDetailDAO extends BaseDAO<LogNewsDetail> {

    /**
     * 统计总量
     *
     * @param logNewsDetail model
     * @return 总量
     */
    Integer countRecord(LogNewsDetail logNewsDetail);
}