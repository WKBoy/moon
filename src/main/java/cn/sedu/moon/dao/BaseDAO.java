/*
 * Dian.com Inc.
 * Copyright (c) 2004-2018 All Rights Reserved.
 */
package cn.sedu.moon.dao;

import java.util.List;

import cn.sedu.moon.dao.model.BaseModel;
import org.springframework.dao.DataAccessException;

/**
 * 基础增删改查
 *
 * @author ${baizhang}
 * @version $Id: BaseDAO.java, v 0.1 2018-08-29 下午8:41 Exp $
 */
@SuppressWarnings("unused")
public interface BaseDAO<B extends BaseModel<B>> {
    /**
     * 查询数据返回列表
     *
     * @param model     对象model
     * @return 对象列表
     * @throws DataAccessException  exception
     */
    List<B> listRecord(B model) throws DataAccessException;

    /**
     * 查询
     * 返回结果只有一条，否则抛出异常
     *
     * @param model     对象model
     * @return 查询结果
     * @throws DataAccessException  exception
     */
    B getRecord(B model) throws DataAccessException;

    /**
     * 对象保存
     *
     * @param model     对象model
     * @return 保存结果（受影响行数）
     * @throws DataAccessException  exception
     */
    int saveRecord(B model) throws DataAccessException;

    /**
     * 更新对象
     *
     * @param model     对象model
     * @return 更新结果（受影响行数）
     * @throws DataAccessException  exception
     */
    int updateRecord(B model) throws DataAccessException;

    /**
     * 删除记录
     *
     * @param model     对象model
     * @return 删除结果
     * @throws DataAccessException  exception
     */
    int removeRecord(B model) throws DataAccessException;
}