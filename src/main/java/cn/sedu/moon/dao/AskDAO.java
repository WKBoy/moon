/*
 * Dian.so Inc.
 * Copyright (c) 2016-2019 All Rights Reserved.
 */
package cn.sedu.moon.dao;

import java.util.List;

import cn.sedu.moon.dao.model.Ask;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * 继承于BaseDAO，默认有五个方法实现
 * listRecord、getRecord、saveRecord、removeRecord、updateRecord
 *
 * @author MBG工具生成
 * @version $Id: AskDAO.java, v 0.1 2019-01-19 23:16:00 Exp $
 */
@Mapper
public interface AskDAO extends BaseDAO<Ask> {

    List<Ask> listRecordBySchoolNameList(@Param("list") List<String> list);

    List<Ask> search(Ask ask);

}