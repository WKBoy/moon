/*
 * Dian.so Inc.
 * Copyright (c) 2016-2019 All Rights Reserved.
 */
package cn.sedu.moon.dao;

import cn.sedu.moon.controller.response.SopResponse;
import cn.sedu.moon.dao.model.SopSubject;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;


/**
 * 继承于BaseDAO，默认有五个方法实现
 * listRecord、getRecord、saveRecord、removeRecord、updateRecord
 *
 * @author MBG工具生成
 * @version $Id: SopSubjectDAO.java, v 0.1 2019-01-09 11:41:50 Exp $
 */
@Mapper
public interface SopSubjectDAO extends BaseDAO<SopSubject> {

    SopSubject getRecordByRefId(Integer refId);

    /**
     * 获取未转为标签的sop问题第一条
     * @param sopIdList
     * @return
     */
    SopSubject getFirstSop(@Param("list")List<Integer> sopIdList);
}