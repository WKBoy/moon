/*
 * Dian.so Inc.
 * Copyright (c) 2016-2018 All Rights Reserved.
 */
package cn.sedu.moon.dao;

import java.util.List;

import cn.sedu.moon.dao.model.District;
import org.apache.ibatis.annotations.Mapper;

/**
 * 继承于BaseDAO，默认有五个方法实现
 * listRecord、getRecord、saveRecord、removeRecord、updateRecord
 *
 * @author MBG工具生成
 * @version $Id: DistrictDAO.java, v 0.1 2018-12-16 00:29:47 Exp $
 */
@Mapper
public interface DistrictDAO extends BaseDAO<District> {

    /**
     * 根据小区名称模糊搜索
     *
     * @param keyName 关键字
     * @return
     */
    List<District> queryByKeyName(String keyName);

}