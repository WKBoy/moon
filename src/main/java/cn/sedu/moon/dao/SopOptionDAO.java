/*
 * Dian.so Inc.
 * Copyright (c) 2016-2019 All Rights Reserved.
 */
package cn.sedu.moon.dao;

import cn.sedu.moon.dao.model.SopOptionDO;
import org.apache.ibatis.annotations.Mapper;


/**
 * 继承于BaseDAO，默认有五个方法实现
 * listRecord、getRecord、saveRecord、removeRecord、updateRecord
 *
 * @author MBG工具生成
 * @version $Id: SopOptionDAO.java, v 0.1 2019-01-09 14:15:49 Exp $
 */
@Mapper
public interface SopOptionDAO extends BaseDAO<SopOptionDO> {
    /**
     * 根据option表中的ref_id和subject_ref_id获取内容
     * @param optionId
     * @param subjectRefId
     * @return
     */
    String selectContentByOptionId(Integer optionId, Integer subjectRefId);
}