/*
 * Dian.so Inc.
 * Copyright (c) 2016-2018 All Rights Reserved.
 */
package cn.sedu.moon.dao;

import cn.sedu.moon.dao.model.StudentGrade;
import org.apache.ibatis.annotations.Mapper;


/**
 * 继承于BaseDAO，默认有五个方法实现
 * listRecord、getRecord、saveRecord、removeRecord、updateRecord
 *
 * @author MBG工具生成
 * @version $Id: StudentGradeDAO.java, v 0.1 2018-12-05 16:23:09 Exp $
 */
@Mapper
public interface StudentGradeDAO extends BaseDAO<StudentGrade> {
}