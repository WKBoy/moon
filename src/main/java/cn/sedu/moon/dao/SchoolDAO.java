/*
 * Dian.so Inc.
 * Copyright (c) 2016-2018 All Rights Reserved.
 */
package cn.sedu.moon.dao;

import cn.sedu.moon.dao.model.School;
import cn.sedu.moon.dao.model.SchoolExtendDO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;


/**
 * 继承于BaseDAO，默认有五个方法实现
 * listRecord、getRecord、saveRecord、removeRecord、updateRecord
 *
 * @author MBG工具生成
 * @version $Id: SchoolDAO.java, v 0.1 2018-12-05 16:39:41 Exp $
 */
@Mapper
public interface SchoolDAO extends BaseDAO<School> {

    /**
     * 按条件count
     *
     * @param school
     * @return
     */
    Integer countRecord(School school);

    /**
     * 根据学校名称关键字模糊搜索
     *
     * @param schoolExtendDO
     * @return
     */
    List<School> queryByKeyName(SchoolExtendDO schoolExtendDO);

    List<School> listRecordByAreaInfoAndHighScore(School school);

    List<School> listHotInCity(School school);


    /**
     * 通过主键查询
     *
     * @param id
     * @return
     */
    School selectByPrimaryKey(Long id);

    /**
     * 获取所有学校的Id和name
     * @return
     */
    List<School> listNews(@Param("cityCode") String cityCode);

    /**
     * 根据schoolIds获取学校名称
     * @param schoolId
     * @return
     */
    String getSchoolNameById(@Param("schoolId") Long schoolId);

    /**
     * 根据schoolIds获取所有学校名称
     * @param schoolIdList
     * @return
     */
    List<String> getSchoolNameByIds(@Param("list") List<Long> schoolIdList);

    /**
     * 根据schoolIds获取所有学校
     * @param likeSchoolIdList
     * @return
     */
    List<School> getSchoolByIds(@Param("list") List<Long> likeSchoolIdList);

    List<School> listAllSchoolNameAndId();

    /**
     * 通过IDList获取学校名称，省市区等
     * @param schoolIdList
     * @return
     */
    List<School> getSchoolSimpleByIds(@Param("list")List<Long> schoolIdList);

    /**
     * 通过ID获取学校名称，省市区等
     * @param schoolId
     * @return
     */
    School getSchoolSimpleById(Long schoolId);

    /**
     * 通过关键词和城市编码获取学校
     * @param name
     * @param cityCode
     * @return
     */
    List<School> listSchoolBySearchKey(@Param("name") String name, @Param("cityCode") String cityCode);
}