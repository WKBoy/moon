/*
 * Dian.so Inc.
 * Copyright (c) 2016-2019 All Rights Reserved.
 */
package cn.sedu.moon.dao;

import cn.sedu.moon.dao.model.NewsStick;
import org.apache.ibatis.annotations.Mapper;


/**
 * 继承于BaseDAO，默认有五个方法实现
 * listRecord、getRecord、saveRecord、removeRecord、updateRecord
 *
 * @author MBG工具生成
 * @version $Id: NewsStickDAO.java, v 0.1 2019-04-19 11:52:55 Exp $
 */
@Mapper
public interface NewsStickDAO extends BaseDAO<NewsStick> {
}