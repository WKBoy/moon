/*
 * Dian.so Inc.
 * Copyright (c) 2016-2019 All Rights Reserved.
 */
package cn.sedu.moon.dao.model;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 *
 * @author MBG工具生成
 * @version $Id: SopSubject.java, v 0.1 2019-01-09 11:41:50 Exp $
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class SopSubject extends BaseModel<SopSubject> {
    /** serialVersionUID */
    private static final long serialVersionUID = 174527040130157702L;

    private Long id;

    /**
     * ref_id
     */
    private Integer refId;

    /**
     * 问题的标题
     */
    private String title;

    /**
     * 问题类型，1 单选，2 多选，3 输入
     */
    private Integer type;

    /**
     * 浏览人数
     */
    private Long viewCount;
}