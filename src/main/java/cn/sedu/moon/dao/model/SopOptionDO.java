/*
 * Dian.so Inc.
 * Copyright (c) 2016-2019 All Rights Reserved.
 */
package cn.sedu.moon.dao.model;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * TODO
 *
 * @author MBG工具生成
 * @version $Id: SopOptionDO.java, v 0.1 2019-01-09 14:15:49 Exp $
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class SopOptionDO extends BaseModel<SopOptionDO> {
    /** serialVersionUID */
    private static final long serialVersionUID = 175694709956079282L;

    private Long id;

    /**
     * ref_id
     */
    private Integer refId;

    /**
     * 选项对应的题目ID
     */
    private Integer subjectRefId;

    /**
     * 选项的文本内容
     */
    private String content;

    /**
     * 当用户选中该选项的时候展示输入框
     */
    private Integer chooseShowInput;

    /**
     * 输入框提示文案
     */
    private String inputInfo;
}