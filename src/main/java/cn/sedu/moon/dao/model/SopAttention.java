/*
 * Dian.so Inc.
 * Copyright (c) 2016-2019 All Rights Reserved.
 */
package cn.sedu.moon.dao.model;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * TODO
 *
 * @author MBG工具生成
 * @version $Id: SopAttention.java, v 0.1 2019-01-26 15:56:10 Exp $
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class SopAttention extends BaseModel<SopAttention> {
    /** serialVersionUID */
    private static final long serialVersionUID = 174570968110614289L;

    private Long id;

    /**
     * 问题id
     */
    private Integer sopId;

    /**
     * 问题关者的用户ID
     */
    private Long attentionUserId;
}