/*
 * Dian.so Inc.
 * Copyright (c) 2016-2019 All Rights Reserved.
 */
package cn.sedu.moon.dao.model;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * TODO
 *
 * @author MBG工具生成
 * @version $Id: DistrictGd.java, v 0.1 2019-02-10 23:56:38 Exp $
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class DistrictGd extends BaseModel<DistrictGd> {
    /** serialVersionUID */
    private static final long serialVersionUID = 170131562024730640L;

    private Long id;

    private String province;

    private String city;

    private String area;

    private String name;

    private String address;

    /**
     * 经度
     */
    private Double lng;

    /**
     * 纬度
     */
    private Double lat;

    private String rating;

    private String gdId;

    private String shapeRegion;


    /**
     * 省编码
     */
    private String provinceCode;

    /**
     * 市编码
     */
    private String cityCode;

    /**
     * 区编码
     */
    private String areaCode;

    /**
     * 是否为用户添加的学校 1 是 0 否
     */
    private Integer userAdd;
}