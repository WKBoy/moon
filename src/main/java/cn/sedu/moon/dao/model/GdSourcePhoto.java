/*
 * Dian.so Inc.
 * Copyright (c) 2016-2019 All Rights Reserved.
 */
package cn.sedu.moon.dao.model;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * TODO
 *
 * @author MBG工具生成
 * @version $Id: GdSourcePhoto.java, v 0.1 2019-11-01 17:59:59 Exp $
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class GdSourcePhoto extends BaseModel<GdSourcePhoto> {
    /** serialVersionUID */
    private static final long serialVersionUID = 170616718449547423L;

    private Long id;

    /**
     * 高德id
     */
    private String gdId;

    /**
     * url
     */
    private String url;
}