/*
 * Dian.so Inc.
 * Copyright (c) 2016-2019 All Rights Reserved.
 */
package cn.sedu.moon.dao.model;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * TODO
 *
 * @author MBG工具生成
 * @version $Id: SchoolComment.java, v 0.1 2019-10-08 18:32:55 Exp $
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class SchoolComment extends BaseModel<SchoolComment> {
    /** serialVersionUID */
    private static final long serialVersionUID = 173461744672518919L;

    private Long id;

    /**
     * 学校id
     */
    private Long schoolId;

    /**
     * 评论内容
     */
    private String comment;

    /**
     * 评论者的用户id
     */
    private Long userId;

    /**
     * 评论者的头像
     */
    private String avatar;

    /**
     * 评论者的昵称
     */
    private String nickname;
}