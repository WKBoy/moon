/*
 * Dian.so Inc.
 * Copyright (c) 2016-2019 All Rights Reserved.
 */
package cn.sedu.moon.dao.model;

import java.util.Date;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 *
 * @author MBG工具生成
 * @version $Id: SchoolNewsSrc.java, v 0.1 2019-01-09 19:44:10 Exp $
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class SchoolNewsSrc extends BaseModel<SchoolNewsSrc> {
    /** serialVersionUID */
    private static final long serialVersionUID = 172685886118624373L;

    private Long id;

    /**
     * 搜学网学校id
     */
    private String sxSchoolId;

    /**
     * 搜学网新闻id
     */
    private String sxNewsId;

    /**
     * 新闻标题
     */
    private String title;

    /**
     * 作者
     */
    private String author;

    /**
     * 浏览次数
     */
    private Integer viewCount;

    /**
     * 新闻url
     */
    private String url;

    /**
     * 缩略图
     */
    private String thumbnail;

    /**
     * 新闻创建时间
     */
    private Date newsCreateTime;

    private String mainContent;
}