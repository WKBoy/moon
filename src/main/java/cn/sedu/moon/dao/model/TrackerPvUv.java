/*
 * Dian.so Inc.
 * Copyright (c) 2016-2019 All Rights Reserved.
 */
package cn.sedu.moon.dao.model;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * TODO
 *
 * @author MBG工具生成
 * @version $Id: TrackerPvUv.java, v 0.1 2019-11-10 23:10:48 Exp $
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class TrackerPvUv extends BaseModel<TrackerPvUv> {
    /** serialVersionUID */
    private static final long serialVersionUID = 177883877579965886L;

    private Long id;

    /**
     * 请求路径
     */
    private String url;

    /**
     * 埋点标题描述
     */
    private String title;

    /**
     * pv
     */
    private Integer pv;

    /**
     * uv
     */
    private Integer uv;
}