/*
 * Dian.so Inc.
 * Copyright (c) 2016-2018 All Rights Reserved.
 */
package cn.sedu.moon.dao.model;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 *
 * @author MBG工具生成
 * @version $Id: GrowDirection.java, v 0.1 2018-12-05 16:23:32 Exp $
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class GrowDirection extends BaseModel<GrowDirection> {
    /** serialVersionUID */
    private static final long serialVersionUID = 171311474789182267L;

    private Long id;

    /**
     * 标题，如美术
     */
    private String title;
}