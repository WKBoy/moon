/*
 * Dian.com Inc.
 * Copyright (c) 2004-2018 All Rights Reserved.
 */
package cn.sedu.moon.dao.model;

import java.io.Serializable;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

/**
 * baseModel
 * 常用字段放入base
 *
 * 使用offset pageSize参数作为分页参数，分页优先使用优先使用pageNo，在pageNo和offset同时传入的情况下
 * 查询分页以pageNo为准
 *
 * @author ${baizhang}
 * @version $Id: BaseModel.java, v 0.1 2018-06-05 下午9:41 Exp $
 */
@Data
@SuppressWarnings("unused")
public class BaseModel<B extends BaseModel<B>> implements Serializable {
    /** serialVersionUID */
    private static final long serialVersionUID = -1711899675342850998L;

    /**
     * 逻辑删除（1.true 0.false default 0）
     */
    private Integer deleted;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 更新时间
     */
    private Date updateTime;

    /**
     * 分页pageNo
     */
    @JsonIgnore
    private Integer pageNo;

    /**
     * 分页offset
     */
    @JsonIgnore
    private Integer offset;

    /**
     * 每页显示条数
     */
    @JsonIgnore
    private Integer pageSize = 20;

}
