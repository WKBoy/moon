/*
 * Dian.so Inc.
 * Copyright (c) 2016-2019 All Rights Reserved.
 */
package cn.sedu.moon.dao.model;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * TODO
 *
 * @author MBG工具生成
 * @version $Id: LogNewsDetail.java, v 0.1 2019-08-24 16:42:08 Exp $
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class LogNewsDetail extends BaseModel<LogNewsDetail> {
    /** serialVersionUID */
    private static final long serialVersionUID = 172261292776149747L;

    private Long id;

    /**
     * 新闻id
     */
    private Long newsId;

    /**
     * 用户id
     */
    private Long userId;
}