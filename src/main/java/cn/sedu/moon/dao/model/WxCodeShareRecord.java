/*
 * Dian.so Inc.
 * Copyright (c) 2016-2019 All Rights Reserved.
 */
package cn.sedu.moon.dao.model;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * TODO
 *
 * @author MBG工具生成
 * @version $Id: WxCodeShareRecord.java, v 0.1 2019-12-15 10:48:42 Exp $
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class WxCodeShareRecord extends BaseModel<WxCodeShareRecord> {
    /** serialVersionUID */
    private static final long serialVersionUID = 175820182955550087L;

    private Long id;

    /**
     * 分享者的id
     */
    private Long shareUserId;

    /**
     * 读者id
     */
    private Long userId;
}