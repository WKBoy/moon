/*
 * Dian.so Inc.
 * Copyright (c) 2016-2019 All Rights Reserved.
 */
package cn.sedu.moon.dao.model;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;

/**
 * TODO
 *
 * @author MBG工具生成
 * @version $Id: NewsNotice.java, v 0.1 2019-10-27 00:39:36 Exp $
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class NewsNotice extends BaseModel<NewsNotice> {
    /** serialVersionUID */
    private static final long serialVersionUID = 172883476847854088L;

    private Long id;

    private Long newsId;

    /**
     * 提醒标题
     */
    private String title;

    /**
     * 提醒的时
     */
    private Integer noticeHour;

    /**
     * 提醒的分
     */
    private Integer noticeMin;

    private String noticeDay;

    /**
     * 是否推送过
     */
    private Integer push;

    /**
     * 推送时间
     */
    private Date pushTime;

}