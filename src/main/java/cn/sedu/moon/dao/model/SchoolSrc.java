/*
 * Dian.so Inc.
 * Copyright (c) 2016-2019 All Rights Reserved.
 */
package cn.sedu.moon.dao.model;

import java.math.BigDecimal;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * TODO
 *
 * @author MBG工具生成
 * @version $Id: SchoolSrc.java, v 0.1 2019-01-09 19:43:55 Exp $
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class SchoolSrc extends BaseModel<SchoolSrc> {
    /** serialVersionUID */
    private static final long serialVersionUID = 171802853558491058L;

    private Long id;

    /**
     * 搜学网的id
     */
    private String sxId;

    /**
     * 详情页url
     */
    private String detailUrl;

    /**
     * 学校缩略图
     */
    private String thumbnail;

    /**
     * 学校网址
     */
    private String website;

    /**
     * 课程
     */
    private String kc;

    /**
     * 省份信息
     */
    private String province;

    /**
     * 城市
     */
    private String city;

    /**
     * 区
     */
    private String area;

    /**
     * 学校名称
     */
    private String name;

    /**
     * 重点、示范等信息
     */
    private String attribute;

    /**
     * 公办、民办
     */
    private String property;

    /**
     * 幼儿园、小学、中学、职业学校
     */
    private String type;

    /**
     * 地址信息
     */
    private String address;

    /**
     * 联系电话
     */
    private String telephone;

    /**
     * 学校经度
     */
    private BigDecimal lng;

    /**
     * 学校纬度
     */
    private BigDecimal lat;

    /**
     * 学校介绍
     */
    private String introduction;
}