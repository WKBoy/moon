/*
 * Dian.so Inc.
 * Copyright (c) 2016-2019 All Rights Reserved.
 */
package cn.sedu.moon.dao.model;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * TODO
 *
 * @author MBG工具生成
 * @version $Id: SchoolImgSrc.java, v 0.1 2019-01-13 15:29:46 Exp $
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class SchoolImgSrc extends BaseModel<SchoolImgSrc> {
    /** serialVersionUID */
    private static final long serialVersionUID = 177634663026939735L;

    private Long id;

    /**
     * 搜学网id
     */
    private String sxId;

    /**
     * 图片
     */
    private String images;
}