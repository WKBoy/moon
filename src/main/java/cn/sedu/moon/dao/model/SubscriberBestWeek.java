package cn.sedu.moon.dao.model;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class SubscriberBestWeek extends BaseModel<SubscriberBestWeek> {


    private static final long serialVersionUID = 6292941330671328886L;

    private Long id;


    private Long userId;

}
