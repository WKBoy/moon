/*
 * Dian.so Inc.
 * Copyright (c) 2016-2019 All Rights Reserved.
 */
package cn.sedu.moon.dao.model;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * TODO
 *
 * @author MBG工具生成
 * @version $Id: NewsRead.java, v 0.1 2019-03-15 10:29:25 Exp $
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class NewsRead extends BaseModel<NewsRead> {
    /** serialVersionUID */
    private static final long serialVersionUID = 179077124351454310L;

    private Long id;

    /**
     * 用户ID
     */
    private Long userId;

    /**
     * 新闻id
     */
    private Long newsId;
}