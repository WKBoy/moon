/*
 * Dian.so Inc.
 * Copyright (c) 2016-2019 All Rights Reserved.
 */
package cn.sedu.moon.dao.model;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * TODO
 *
 * @author MBG工具生成
 * @version $Id: GdDistrictSource.java, v 0.1 2019-11-01 15:38:24 Exp $
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class GdDistrictSource extends BaseModel<GdDistrictSource> {
    /** serialVersionUID */
    private static final long serialVersionUID = 173352638211354695L;

    private Long id;

    /**
     * adcode
     */
    private String adcode;

    /**
     * 名称
     */
    private String name;

    /**
     * 级别
     */
    private String level;

    /**
     * 经度
     */
    private Double lng;

    /**
     * 纬度
     */
    private Double lat;

    private String parentCode;
}