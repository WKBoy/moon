/*
 * Dian.so Inc.
 * Copyright (c) 2016-2019 All Rights Reserved.
 */
package cn.sedu.moon.dao.model;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * TODO
 *
 * @author MBG工具生成
 * @version $Id: AskUseful.java, v 0.1 2019-01-29 19:33:16 Exp $
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class AskUseful extends BaseModel<AskUseful> {
    /** serialVersionUID */
    private static final long serialVersionUID = 176295553238579009L;

    private Long id;

    /**
     * 答案id
     */
    private Long answerId;

    /**
     * 1 有用，2 没用
     */
    private Integer type;

    /**
     * 问题关者的用户ID
     */
    private Long userId;
}