/*
 * Dian.so Inc.
 * Copyright (c) 2016-2019 All Rights Reserved.
 */
package cn.sedu.moon.dao.model;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 *
 * @author MBG工具生成
 * @version $Id: TrackerPoint.java, v 0.1 2019-11-10 22:50:50 Exp $
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class TrackerPoint extends BaseModel<TrackerPoint> {
    /** serialVersionUID */
    private static final long serialVersionUID = 173887977513522923L;

    private Long id;

    /**
     * 请求路径
     */
    private String url;

    /**
     * 埋点标题描述
     */
    private String title;
}