/*
 * Dian.so Inc.
 * Copyright (c) 2016-2019 All Rights Reserved.
 */
package cn.sedu.moon.dao.model;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * TODO
 *
 * @author MBG工具生成
 * @version $Id: UserGlory.java, v 0.1 2019-04-01 15:02:39 Exp $
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class UserGlory extends BaseModel<UserGlory> {
    /** serialVersionUID */
    private static final long serialVersionUID = 171499011322324471L;

    private Long id;

    /**
     * 用户id
     */
    private Long userId;

    /**
     * 荣誉值
     */
    private Long glory;
}