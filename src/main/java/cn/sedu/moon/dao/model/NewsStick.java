/*
 * Dian.so Inc.
 * Copyright (c) 2016-2019 All Rights Reserved.
 */
package cn.sedu.moon.dao.model;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * TODO
 *
 * @author MBG工具生成
 * @version $Id: NewsStick.java, v 0.1 2019-04-19 11:52:55 Exp $
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class NewsStick extends BaseModel<NewsStick> {
    /** serialVersionUID */
    private static final long serialVersionUID = 171024213322078362L;

    private Long id;

    /**
     * 新闻id
     */
    private Long newsId;
}