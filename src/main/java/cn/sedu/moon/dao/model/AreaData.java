/*
 * Dian.so Inc.
 * Copyright (c) 2016-2018 All Rights Reserved.
 */
package cn.sedu.moon.dao.model;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * TODO
 *
 * @author MBG工具生成
 * @version $Id: AreaData.java, v 0.1 2018-11-12 16:59:38 Exp $
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class AreaData extends BaseModel<AreaData> {
    /** serialVersionUID */
    private static final long serialVersionUID = 170946567715438005L;

    private Long id;

    /**
     * 省市区表述信息
     */
    private String areaName;

    /**
     * 省市区编码
     */
    private String areaCode;

    /**
     * 用来区分省市区类别
     */
    private Integer areaLevel;

    /**
     * 上级的编码
     */
    private String parentCode;
}