/*
 * Dian.so Inc.
 * Copyright (c) 2016-2019 All Rights Reserved.
 */
package cn.sedu.moon.dao.model;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * TODO
 *
 * @author MBG工具生成
 * @version $Id: AskAttention.java, v 0.1 2019-01-19 23:09:54 Exp $
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class AskAttention extends BaseModel<AskAttention> {
    /** serialVersionUID */
    private static final long serialVersionUID = 174853929592683274L;

    private Long id;

    /**
     * 问题id
     */
    private Long askId;

    /**
     * 问题关者的用户ID
     */
    private Long attentionUserId;
}