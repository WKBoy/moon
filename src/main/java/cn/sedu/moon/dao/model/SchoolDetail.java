/*
 * Dian.so Inc.
 * Copyright (c) 2016-2019 All Rights Reserved.
 */
package cn.sedu.moon.dao.model;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * TODO
 *
 * @author MBG工具生成
 * @version $Id: SchoolDetail.java, v 0.1 2019-02-15 21:24:20 Exp $
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class SchoolDetail extends BaseModel<SchoolDetail> {
    /** serialVersionUID */
    private static final long serialVersionUID = 178763117333654794L;

    private Long id;

    /**
     * 学校id
     */
    private Long schoolId;

    /**
     * 课程
     */
    private String kc;

    /**
     * 学校的介绍信息
     */
    private String introduction;
}