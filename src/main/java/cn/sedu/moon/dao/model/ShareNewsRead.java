/*
 * Dian.so Inc.
 * Copyright (c) 2016-2019 All Rights Reserved.
 */
package cn.sedu.moon.dao.model;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * TODO
 *
 * @author MBG工具生成
 * @version $Id: ShareNewsRead.java, v 0.1 2019-08-07 14:51:56 Exp $
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class ShareNewsRead extends BaseModel<ShareNewsRead> {
    /** serialVersionUID */
    private static final long serialVersionUID = 176935777667757874L;

    private Long id;

    /**
     * 分享id
     */
    private Long shareId;

    /**
     * 读者id
     */
    private Long userId;

    private Long newsId;

    private Long shareUserId;
}