/*
 * Dian.so Inc.
 * Copyright (c) 2016-2019 All Rights Reserved.
 */
package cn.sedu.moon.dao.model;

import java.util.Date;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author MBG工具生成
 * @version $Id: NewsDetail.java, v 0.1 2019-02-17 13:09:43 Exp $
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class NewsDetail extends BaseModel<NewsDetail> {
    /** serialVersionUID */
    private static final long serialVersionUID = 171451369665082917L;

    private Long id;

    /**
     * 新闻id
     */
    private Long newsId;

    /**
     * 作者
     */
    private String author;

    /**
     * 新闻创建时间
     */
    private Date newsCreateTime;

    /**
     * 浏览量
     */
    private Integer viewCount;

    /**
     * 回复数
     */
    private Integer replyCount;

    /**
     * 新闻来源
     */
    private String src;

    /**
     * 新闻标题
     */
    private String title;

    /**
     * 新闻缩略图
     */
    private String thumbnail;

    /**
     * 新闻正文
     */
    private String content;
}