/*
 * Dian.so Inc.
 * Copyright (c) 2016-2019 All Rights Reserved.
 */
package cn.sedu.moon.dao.model;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * TODO
 *
 * @author MBG工具生成
 * @version $Id: UserGloryRecord.java, v 0.1 2019-04-01 15:04:27 Exp $
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class UserGloryRecord extends BaseModel<UserGloryRecord> {
    /** serialVersionUID */
    private static final long serialVersionUID = 178230904144278410L;

    private Long id;

    /**
     * 学校id
     */
    private Long userId;

    /**
     * 0 增加，1 减少
     */
    private Integer changeType;

    /**
     * 业务类型
     */
    private Integer bizType;

    /**
     * 图片
     */
    private String value;
}