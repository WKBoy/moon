/*
 * Dian.so Inc.
 * Copyright (c) 2016-2019 All Rights Reserved.
 */
package cn.sedu.moon.dao.model;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * TODO
 *
 * @author MBG工具生成
 * @version $Id: AskAnswer.java, v 0.1 2019-01-19 23:09:42 Exp $
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class AskAnswer extends BaseModel<AskAnswer> {
    /** serialVersionUID */
    private static final long serialVersionUID = 170459522158324386L;

    private Long id;

    /**
     * 问题id
     */
    private Long askId;

    /**
     * 回答者用户ID
     */
    private Long answerUserId;

    /**
     * 回答者的用户昵称
     */
    private String answerUserNick;

    /**
     * 回答者的用户头像
     */
    private String answerUserAvatar;

    /**
     * 答案是否被采纳
     */
    private Boolean accepted;

    /**
     * 答案内容
     */
    private String content;

    /**
     * 是否是管理员回答的，0 不是，1是
     */
    private Integer replyAdmin;
}