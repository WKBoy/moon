/*
 * Dian.so Inc.
 * Copyright (c) 2016-2019 All Rights Reserved.
 */
package cn.sedu.moon.dao.model;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author MBG工具生成
 * @version $Id: ShareNewsLight.java, v 0.1 2019-07-03 18:12:30 Exp $
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class ShareNewsLight extends BaseModel<ShareNewsLight> {
    /** serialVersionUID */
    private static final long serialVersionUID = 171824866889396538L;

    private Long id;

    /**
     * 新闻id
     */
    private Long newsId;

    /**
     * 分享者，被点亮用户id
     */
    private Long userId;

    /**
     * 点亮的用户id
     */
    private Long userLightId;
}