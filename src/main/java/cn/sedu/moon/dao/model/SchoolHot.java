/*
 * Dian.so Inc.
 * Copyright (c) 2016-2019 All Rights Reserved.
 */
package cn.sedu.moon.dao.model;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * TODO
 *
 * @author MBG工具生成
 * @version $Id: SchoolHot.java, v 0.1 2019-08-27 11:03:46 Exp $
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class SchoolHot extends BaseModel<SchoolHot> {
    /** serialVersionUID */
    private static final long serialVersionUID = 173170208984022575L;

    private Long id;

    /**
     * 学校id
     */
    private Long schoolId;

    /**
     * 学校类型
     */
    private Integer type;

    /**
     * 公办民办信息，1:公办，2:民办
     */
    private Integer property;

    /**
     * 省编码
     */
    private String provinceCode;

    /**
     * 市编码
     */
    private String cityCode;

    /**
     * 区编码
     */
    private String areaCode;

    /**
     * 市热门
     */
    private Integer cityHot;

    /**
     * 区热门
     */
    private Integer areaHot;
}