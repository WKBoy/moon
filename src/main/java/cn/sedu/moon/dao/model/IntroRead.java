/*
 * Dian.so Inc.
 * Copyright (c) 2016-2019 All Rights Reserved.
 */
package cn.sedu.moon.dao.model;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 *
 * @author MBG工具生成
 * @version $Id: IntroRead.java, v 0.1 2019-09-07 16:01:34 Exp $
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class IntroRead extends BaseModel<IntroRead> {
    /** serialVersionUID */
    private static final long serialVersionUID = 170402071612032415L;

    private Long id;

    /**
     * 用户id
     */
    private Long userId;
}