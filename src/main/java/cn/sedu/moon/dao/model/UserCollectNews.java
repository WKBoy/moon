/*
 * Dian.so Inc.
 * Copyright (c) 2016-2019 All Rights Reserved.
 */
package cn.sedu.moon.dao.model;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 *
 * @author MBG工具生成
 * @version $Id: UserCollectNews.java, v 0.1 2019-06-23 12:26:35 Exp $
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class UserCollectNews extends BaseModel<UserCollectNews> {
    /** serialVersionUID */
    private static final long serialVersionUID = 179334685414542335L;

    private Long id;

    /**
     * 新闻id
     */
    private Long newsId;

    /**
     * 用户id
     */
    private Long userId;
}