/*
 * Dian.so Inc.
 * Copyright (c) 2016-2019 All Rights Reserved.
 */
package cn.sedu.moon.dao.model;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * TODO
 *
 * @author MBG工具生成
 * @version $Id: SchoolImage.java, v 0.1 2019-02-15 21:24:30 Exp $
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class SchoolImage extends BaseModel<SchoolImage> {
    /** serialVersionUID */
    private static final long serialVersionUID = 179989313668479256L;

    private Long id;

    /**
     * 学校id
     */
    private Long schoolId;

    /**
     * 图片
     */
    private String img;
}