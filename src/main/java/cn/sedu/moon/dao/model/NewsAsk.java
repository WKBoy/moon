/*
 * Dian.so Inc.
 * Copyright (c) 2016-2019 All Rights Reserved.
 */
package cn.sedu.moon.dao.model;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 *
 * @author MBG工具生成
 * @version $Id: NewsAsk.java, v 0.1 2019-09-09 23:37:15 Exp $
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class NewsAsk extends BaseModel<NewsAsk> {
    /** serialVersionUID */
    private static final long serialVersionUID = 170143817598695610L;

    private Long id;

    /**
     * 新闻id
     */
    private Long newsId;

    /**
     * 问题id
     */
    private Long askId;

    /**
     * 操作者
     */
    private String operator;
}