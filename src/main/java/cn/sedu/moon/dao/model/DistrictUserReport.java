/*
 * Dian.so Inc.
 * Copyright (c) 2016-2019 All Rights Reserved.
 */
package cn.sedu.moon.dao.model;

import java.math.BigDecimal;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 *
 * @author MBG工具生成
 * @version $Id: DistrictUserReport.java, v 0.1 2019-03-12 02:06:50 Exp $
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class DistrictUserReport extends BaseModel<DistrictUserReport> {
    /** serialVersionUID */
    private static final long serialVersionUID = 177856941348736762L;

    private Integer id;

    private Long userId;

    private String provinceCode;

    private String cityCode;

    private String areaCode;

    private String name;

    private String address;

    /**
     * 经度
     */
    private BigDecimal lng;

    /**
     * 纬度
     */
    private BigDecimal lat;

    private String info;
}