/*
 * Dian.so Inc.
 * Copyright (c) 2016-2018 All Rights Reserved.
 */
package cn.sedu.moon.dao.model;

import java.math.BigDecimal;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * TODO
 *
 * @author MBG工具生成
 * @version $Id: StudentArchives.java, v 0.1 2018-12-30 16:00:38 Exp $
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class StudentArchives extends BaseModel<StudentArchives> {
    /** serialVersionUID */
    private static final long serialVersionUID = 171311216140157910L;

    private Long id;

    /**
     * 用户ID
     */
    private Long userId;

    /**
     * 学生姓名
     */
    private String name;

    /**
     * 学生所在学校ID
     */
    private Long schoolId;

    /**
     * 学生所在学校名称
     */
    private String schoolName;

    /**
     * 性别: 1 男，2 女
     */
    private Integer gender;

    /**
     * 年级ID
     */
    private Long gradeId;

    /**
     * 年级
     */
    private String grade;

    /**
     * 小区ID
     */
    private Long districtHouseId;

    /**
     * 小区名称
     */
    private String districtHouseName;

    /**
     * 意向的学校类别 1 公办，2 民办，3 所有
     */
    private Integer schoolProperty;

    /**
     * 培养方向, id列表, 用逗号隔开
     */
    private String growDirectionIds;

    /**
     * 培养方向, 描述，用，隔开，如 美术, 音乐
     */
    private String growDirection;

    /**
     * 省编码
     */
    private String provinceCode;

    /**
     * 市编码
     */
    private String cityCode;

    /**
     * 区编码
     */
    private String areaCode;

    /**
     * 省名称
     */
    private String provinceName;

    /**
     * 市名称
     */
    private String cityName;

    /**
     * 区名称
     */
    private String areaName;

    /**
     * 经度
     */
    private BigDecimal lng;

    /**
     * 纬度
     */
    private BigDecimal lat;
}