/*
 * Dian.so Inc.
 * Copyright (c) 2016-2018 All Rights Reserved.
 */
package cn.sedu.moon.dao.model;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 *
 * @author MBG工具生成
 * @version $Id: StudentGrade.java, v 0.1 2018-12-05 16:23:09 Exp $
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class StudentGrade extends BaseModel<StudentGrade> {
    /** serialVersionUID */
    private static final long serialVersionUID = 170434731184199630L;

    private Long id;

    /**
     * 标题，如三年级
     */
    private String title;
}