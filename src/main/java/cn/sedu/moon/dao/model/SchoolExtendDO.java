/*
 * Dian.com Inc.
 * Copyright (c) 2004-2018 All Rights Reserved.
 */
package cn.sedu.moon.dao.model;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 *
 * @author ${wukong}
 * @version $Id: SchoolExtendDO.java, v 0.1 2018年12月15日 11:43 PM Exp $
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class SchoolExtendDO extends School {

    private static final long serialVersionUID = -7211195393488890081L;

    /**
     * 按照学校名称关键字检索时所使用的字段
     */
    private String keyName;

}