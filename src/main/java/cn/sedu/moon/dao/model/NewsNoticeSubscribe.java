/*
 * Dian.so Inc.
 * Copyright (c) 2016-2019 All Rights Reserved.
 */
package cn.sedu.moon.dao.model;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * TODO
 *
 * @author MBG工具生成
 * @version $Id: NewsNoticeSubscribe.java, v 0.1 2019-10-27 00:39:45 Exp $
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class NewsNoticeSubscribe extends BaseModel<NewsNoticeSubscribe> {
    /** serialVersionUID */
    private static final long serialVersionUID = 178414612834946053L;

    private Long id;

    /**
     * 用户id
     */
    private Long userId;

    /**
     * 提醒id
     */
    private Long noticeId;
}