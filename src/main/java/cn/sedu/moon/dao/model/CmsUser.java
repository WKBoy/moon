/*
 * Dian.so Inc.
 * Copyright (c) 2016-2019 All Rights Reserved.
 */
package cn.sedu.moon.dao.model;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * TODO
 *
 * @author MBG工具生成
 * @version $Id: CmsUser.java, v 0.1 2019-02-22 17:44:38 Exp $
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class CmsUser extends BaseModel<CmsUser> {
    /** serialVersionUID */
    private static final long serialVersionUID = 171823460602311152L;

    private Long id;

    /**
     * 昵称
     */
    private String nickname;

    /**
     * 手机号码
     */
    private String mobile;

    /**
     * 登录名
     */
    private String loginName;

    /**
     * 密码
     */
    private String password;

    /**
     * 用户头像
     */
    private String avatar;
}