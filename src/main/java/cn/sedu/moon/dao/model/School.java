/*
 * Dian.so Inc.
 * Copyright (c) 2016-2018 All Rights Reserved.
 */
package cn.sedu.moon.dao.model;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author MBG工具生成
 * @version $Id: School.java, v 0.1 2018-12-05 16:39:41 Exp $
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class School extends BaseModel<School> {
    /** serialVersionUID */
    private static final long serialVersionUID = 173489848678063295L;

    private Long id;

    private String province;

    private String city;

    private String area;

    private String name;

    private String attribute;

    private Integer property;

    private Integer type;

    private String address;

    private String telephone;

    /**
     * 学校经度
     */
    private Double lng;

    /**
     * 学校纬度
     */
    private Double lat;

    /**
     * 学校介绍
     */
    private String introduction;

    /**
     * 区内排名
     */
    private Integer areaScore;

    /**
     * 搜索关键词, 逗号隔开
     */
    private String searchKey;

    /**
     * 是否为热门学校
     */
    private Integer hot;

    /**
     * 省编码
     */
    private String provinceCode;

    /**
     * 市编码
     */
    private String cityCode;

    /**
     * 区编码
     */
    private String areaCode;

    /**
     * 是不是用户添加的 0:系统添加， 1:用户自己添加的学校
     */
    private Integer userAdd;

}