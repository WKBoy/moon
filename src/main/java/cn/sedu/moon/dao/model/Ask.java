/*
 * Dian.so Inc.
 * Copyright (c) 2016-2019 All Rights Reserved.
 */
package cn.sedu.moon.dao.model;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * TODO
 *
 * @author MBG工具生成
 * @version $Id: Ask.java, v 0.1 2019-01-19 23:16:00 Exp $
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class Ask extends BaseModel<Ask> {
    /** serialVersionUID */
    private static final long serialVersionUID = 172593294040490211L;

    private Long id;

    /**
     * 发布者用户ID
     */
    private Long publisher;

    /**
     * 问题标题
     */
    private String title;

    /**
     * 问题内容
     */
    private String content;

    /**
     * 是否为管理员发布的问题
     */
    private Integer admin;
}