/*
 * Dian.so Inc.
 * Copyright (c) 2016-2019 All Rights Reserved.
 */
package cn.sedu.moon.dao.model;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;

/**
 *
 * @author MBG工具生成
 * @version $Id: User.java, v 0.1 2019-01-05 11:29:47 Exp $
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class User extends BaseModel<User> {
    /** serialVersionUID */
    private static final long serialVersionUID = 176104660560103910L;

    private Long id;

    private String wxOpenId;

    private String wxUnionid;

    /**
     * 用户头像
     */
    private String avatar;

    /**
     * 昵称
     */
    private String nickname;

    /**
     * 省份
     */
    private String province;

    /**
     * 市
     */
    private String city;

    /**
     * 性别: 1 男，2 女
     */
    private Integer gender;

    /**
     * 手机号码
     */
    private String mobile;

    /**
     * 家庭住址
     */
    private String address;

    /**
     * 密码
     */
    private String password;

    private Integer mock;

    private Integer tempUser;

    private Date startTime;

    private Date endTime;
}