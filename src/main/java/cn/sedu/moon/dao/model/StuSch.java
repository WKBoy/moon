/*
 * Dian.so Inc.
 * Copyright (c) 2016-2018 All Rights Reserved.
 */
package cn.sedu.moon.dao.model;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 *
 * @author MBG工具生成
 * @version $Id: StuSch.java, v 0.1 2018-12-05 16:23:27 Exp $
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class StuSch extends BaseModel<StuSch> {
    /** serialVersionUID */
    private static final long serialVersionUID = 176237668236331582L;

    private Long id;

    /**
     * 标题，如美术
     */
    private Long userId;

    /**
     * 逻辑删除，默认为0， 1为删除
     */
    private Long schoolId;
}