/*
 * Dian.so Inc.
 * Copyright (c) 2016-2019 All Rights Reserved.
 */
package cn.sedu.moon.dao.model;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * TODO
 *
 * @author MBG工具生成
 * @version $Id: UserCollectAsk.java, v 0.1 2019-09-15 14:50:11 Exp $
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class UserCollectAsk extends BaseModel<UserCollectAsk> {
    /** serialVersionUID */
    private static final long serialVersionUID = 173468369607023832L;

    private Long id;

    /**
     * 新闻id
     */
    private Long askId;

    /**
     * 用户id
     */
    private Long userId;
}