/*
 * Dian.so Inc.
 * Copyright (c) 2016-2019 All Rights Reserved.
 */
package cn.sedu.moon.dao.model;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * TODO
 *
 * @author MBG工具生成
 * @version $Id: SchoolCommentImg.java, v 0.1 2019-10-08 18:33:02 Exp $
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class SchoolCommentImg extends BaseModel<SchoolCommentImg> {
    /** serialVersionUID */
    private static final long serialVersionUID = 177978865778560607L;

    private Long id;

    /**
     * 评论id
     */
    private Long commentId;

    /**
     * 评论图片
     */
    private String img;
}