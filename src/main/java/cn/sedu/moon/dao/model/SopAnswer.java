/*
 * Dian.so Inc.
 * Copyright (c) 2016-2019 All Rights Reserved.
 */
package cn.sedu.moon.dao.model;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 *
 * @author MBG工具生成
 * @version $Id: SopAnswer.java, v 0.1 2019-01-09 11:49:11 Exp $
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class SopAnswer extends BaseModel<SopAnswer> {
    /** serialVersionUID */
    private static final long serialVersionUID = 179806339163937211L;

    private Long id;

    /**
     * 用户ID
     */
    private Long userId;

    /**
     * 学校ID
     */
    private Long schoolId;

    /**
     * 题目ID
     */
    private Integer subjectRefId;

    /**
     * 选项ID
     */
    private Integer optionRefId;

    /**
     * 用户输入的文本内容
     */
    private String optionInputText;
}