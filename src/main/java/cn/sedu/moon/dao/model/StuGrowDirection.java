/*
 * Dian.so Inc.
 * Copyright (c) 2016-2018 All Rights Reserved.
 */
package cn.sedu.moon.dao.model;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 *
 * @author MBG工具生成
 * @version $Id: StuGrowDirection.java, v 0.1 2018-12-16 10:20:53 Exp $
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class StuGrowDirection extends BaseModel<StuGrowDirection> {

    /** serialVersionUID */
    private static final long serialVersionUID = 172406575592629388L;

    private Long id;

    /**
     * 用户ID
     */
    private Long userId;

    /**
     * 培养方向ID
     */
    private Long growDirectionId;

    /**
     * 培养方向的名字，如美术
     */
    private String growDirectionTitle;
}