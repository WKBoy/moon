/*
 * Dian.so Inc.
 * Copyright (c) 2016-2019 All Rights Reserved.
 */
package cn.sedu.moon.dao.model;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * TODO
 *
 * @author MBG工具生成
 * @version $Id: Tracker.java, v 0.1 2019-11-04 11:37:33 Exp $
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class Tracker extends BaseModel<Tracker> {
    /** serialVersionUID */
    private static final long serialVersionUID = 171439047824169711L;

    private Long id;

    /**
     * 请求路径
     */
    private String url;

    private Long userId;
}