/*
 * Dian.so Inc.
 * Copyright (c) 2016-2018 All Rights Reserved.
 */
package cn.sedu.moon.dao.model;

import java.math.BigDecimal;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 *
 * @author MBG工具生成
 * @version $Id: District.java, v 0.1 2018-12-16 00:29:47 Exp $
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class District extends BaseModel<District> {
    /** serialVersionUID */
    private static final long serialVersionUID = 178878615357152485L;

    private Long id;

    /**
     * 小区名称
     */
    private String communityName;

    /**
     * 对口初中
     */
    private String primarySchoolName;

    /**
     * 对口小学
     */
    private String juniorSchoolName;

    /**
     * 小区经度
     */
    private BigDecimal lng;

    /**
     * 小区纬度
     */
    private BigDecimal lat;
}