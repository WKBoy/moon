/*
 * Dian.so Inc.
 * Copyright (c) 2016-2019 All Rights Reserved.
 */
package cn.sedu.moon.dao.model;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * TODO
 *
 * @author MBG工具生成
 * @version $Id: UserMedal.java, v 0.1 2019-01-26 17:07:11 Exp $
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class UserMedal extends BaseModel<UserMedal> {
    /** serialVersionUID */
    private static final long serialVersionUID = 173736250723417637L;

    private Long id;

    /**
     * 用户ID
     */
    private Long userId;

    /**
     * 勋章类型
     */
    private Integer type;

    /**
     * 勋章描述信息
     */
    private String title;
}