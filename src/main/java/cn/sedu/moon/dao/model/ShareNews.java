/*
 * Dian.so Inc.
 * Copyright (c) 2016-2019 All Rights Reserved.
 */
package cn.sedu.moon.dao.model;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 *
 * @author MBG工具生成
 * @version $Id: ShareNews.java, v 0.1 2019-04-14 16:23:36 Exp $
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class ShareNews extends BaseModel<ShareNews> {
    /** serialVersionUID */
    private static final long serialVersionUID = 171480144729127459L;

    private Long id;

    /**
     * 新闻id
     */
    private Long newsId;

    /**
     * 用户id
     */
    private Long userId;

    /**
     * 1 已创建，2 已分享
     */
    private Integer status;
}