/*
 * Dian.so Inc.
 * Copyright (c) 2016-2019 All Rights Reserved.
 */
package cn.sedu.moon.dao.model;

import cn.sedu.moon.thirdparty.dto.GdPoiPhotoItem;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

/**
 * TODO
 *
 * @author MBG工具生成
 * @version $Id: GdSource.java, v 0.1 2019-10-31 23:38:54 Exp $
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class GdSource extends BaseModel<GdSource> {
    /** serialVersionUID */
    private static final long serialVersionUID = 176842769121924460L;

    private Long id;

    /**
     * 高德id
     */
    private String gdId;

    /**
     * 名称
     */
    private String name;

    /**
     * 地址
     */
    private String address;

    /**
     * 省编码
     */
    private String provinceCode;

    /**
     * 省编码
     */
    private String provinceName;

    /**
     * 市编码
     */
    private String cityCode;

    /**
     * 市名称
     */
    private String cityName;

    /**
     * 区编码
     */
    private String areaCode;

    /**
     * 区名称
     */
    private String areaName;

    /**
     * 经度
     */
    private Double lng;

    /**
     * 纬度
     */
    private Double lat;

    /**
     * 1 学校 2 小区
     */
    private Integer type;

    private List<GdPoiPhotoItem> photos;
}