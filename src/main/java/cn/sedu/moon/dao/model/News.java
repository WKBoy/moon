/*
 * Dian.so Inc.
 * Copyright (c) 2016-2019 All Rights Reserved.
 */
package cn.sedu.moon.dao.model;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;

/**
 * @author MBG工具生成
 * @version $Id: News.java, v 0.1 2019-02-15 13:51:53 Exp $
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class News extends BaseModel<News> {

    /** serialVersionUID */
    private static final long serialVersionUID = 170135561915156554L;

    private Long id;

    /**
     * 作者
     */
    private String author;

    /**
     * 新闻创建时间
     */
    private Date newsCreateTime;

    /**
     * 新闻来源
     */
    private String src;

    /**
     * 新闻标题
     */
    private String title;

    /**
     * 新闻缩略图
     */
    private String thumbnail;

    /**
     * 详情页url
     */
    private String detailUrl;

    /**
     * 新闻简介
     */
    private String brief;

    /**
     * 是否是第三方的文章
     */
    private Integer thirdParty;

    /**
     * 编辑者
     */
    private String editor;

    /**
     * 城市编码
     */
    private String cityCode;

    /**
     * 搜索关键字
     */
    private String searchKey;

    /**
     * 是否置顶
     */
    private Integer stick;

    /**
     * 操作置顶的人
     */
    private String sticker;

    /**
     * 是否为要点亮的文章
     */
    private Integer light;

    /**
     * 新闻类型
     */
    private Integer type;

}