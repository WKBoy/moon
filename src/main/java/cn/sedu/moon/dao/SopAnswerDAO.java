/*
 * Dian.so Inc.
 * Copyright (c) 2016-2019 All Rights Reserved.
 */
package cn.sedu.moon.dao;

import cn.sedu.moon.dao.model.SopAnswer;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;


/**
 * 继承于BaseDAO，默认有五个方法实现
 * listRecord、getRecord、saveRecord、removeRecord、updateRecord
 *
 * @author MBG工具生成
 * @version $Id: SopAnswerDAO.java, v 0.1 2019-01-09 11:49:11 Exp $
 */
@Mapper
public interface SopAnswerDAO extends BaseDAO<SopAnswer> {

    List<SopAnswer> selectSubjectIdList(SopAnswer sopAnswer);

    /**
     * 获取某SOP的答案个数
     * @param subjectRefId
     * @return
     */
    Integer selectCountBySubjectRefId(Integer subjectRefId);

    /**
     * 获取回答最多的答案的最大个数
     * 问答题
     * @param schoolId
     * @param subjectRefId
     * @return
     */
    Integer selectCountBySubjectRefIdMaxText(@Param("schoolId") Long schoolId, @Param("subjectRefId") Integer subjectRefId);

    /**
     * 获取回答最多的答案的最大个数
     * 单选题
     * @param schoolId
     * @param subjectRefId
     * @return
     */
    Integer selectCountBySubjectRefIdMaxOpt(@Param("schoolId") Long schoolId, @Param("subjectRefId") Integer subjectRefId);

    /**
     * 获取回答最多的答案的最大个数的选项
     * 单选题
     * @param schoolId
     * @param subjectRefId
     * @return
     */
    Integer selectBySubjectRefIdMaxOpt(@Param("schoolId") Long schoolId, @Param("subjectRefId") Integer subjectRefId);

    /**
     * 获取回答最多的答案的最大个数的选项
     * 问答题
     * @param schoolId
     * @param subjectRefId
     * @return
     */
    String selectBySubjectRefIdMaxText(@Param("schoolId")  Long schoolId, @Param("subjectRefId") Integer subjectRefId);
}