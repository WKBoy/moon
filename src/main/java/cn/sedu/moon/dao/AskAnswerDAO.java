/*
 * Dian.so Inc.
 * Copyright (c) 2016-2019 All Rights Reserved.
 */
package cn.sedu.moon.dao;

import java.util.List;

import cn.sedu.moon.dao.model.AskAnswer;
import org.apache.ibatis.annotations.Mapper;


/**
 * 继承于BaseDAO，默认有五个方法实现
 * listRecord、getRecord、saveRecord、removeRecord、updateRecord
 *
 * @author MBG工具生成
 * @version $Id: AskAnswerDAO.java, v 0.1 2019-01-19 23:09:42 Exp $
 */
@Mapper
public interface AskAnswerDAO extends BaseDAO<AskAnswer> {

    Integer countRecord(AskAnswer askAnswer);

    List<AskAnswer> selectMyAnsweredAskId(AskAnswer askAnswer);
}