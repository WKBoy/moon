package cn.sedu.moon.dao.mongo;

import cn.sedu.moon.thirdparty.request.WxNotifyMessagePushRequest;
import cn.sedu.moon.thirdparty.response.WxNotifyMessagePushResponse;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.io.Serializable;
import java.util.Date;
import java.util.Map;

/**
 * @author lix
 * date 2019-12-10 15:12
 */
@Data
@Document(collection = "wxMessagePushContent")
public class WxMessagePushContent implements Serializable {
    private static final long serialVersionUID = 7443675181699726707L;
    @Id
    private String id;

    private Long userId;
    /**
     * 微信用户openId
     */
    private String toUser;

    private String templateId;

    private String page;

    private String formId;

    private Map data;

    private Integer isSuccess;

    private WxNotifyMessagePushResponse response;

    private String createTime;
}
