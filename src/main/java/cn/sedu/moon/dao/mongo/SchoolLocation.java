/*
 * Dian.com Inc.
 * Copyright (c) 2004-2019 All Rights Reserved.
 */
package cn.sedu.moon.dao.mongo;

import java.io.Serializable;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 *
 * @author ${wukong}
 * @version $Id: SchoolLocation.java, v 0.1 2019年02月18日 5:54 PM Exp $
 */
@Document(collection = "schoolLocation")
public class SchoolLocation implements Serializable {

    private static final long serialVersionUID = -670928334879664838L;

    // 学校id
    @Id
    private Long schoolId;

    private String name;

    // 位置信息
    private double[] position;

    // 学校类型
    private Integer type;

    /**
     * 学校公办还是民办
     */
    private Integer property;

    /**
     * Getter method for property <tt>schoolId</tt>.
     *
     * @return property value of schoolId
     */
    public Long getSchoolId() {
        return schoolId;
    }

    /**
     * Setter method for property <tt>schoolId</tt>.
     *
     * @param schoolId  value to be assigned to property schoolId
     */
    public void setSchoolId(final Long schoolId) {
        this.schoolId = schoolId;
    }

    /**
     * Getter method for property <tt>position</tt>.
     *
     * @return property value of position
     */
    public double[] getPosition() {
        return position;
    }

    /**
     * Setter method for property <tt>position</tt>.
     *
     * @param position  value to be assigned to property position
     */
    public void setPosition(final double[] position) {
        this.position = position;
    }

    /**
     * Getter method for property <tt>type</tt>.
     *
     * @return property value of type
     */
    public Integer getType() {
        return type;
    }

    /**
     * Setter method for property <tt>type</tt>.
     *
     * @param type  value to be assigned to property type
     */
    public void setType(final Integer type) {
        this.type = type;
    }

    /**
     * Getter method for property <tt>name</tt>.
     *
     * @return property value of name
     */
    public String getName() {
        return name;
    }

    /**
     * Setter method for property <tt>name</tt>.
     *
     * @param name  value to be assigned to property name
     */
    public void setName(final String name) {
        this.name = name;
    }

    /**
     * Getter method for property <tt>property</tt>.
     *
     * @return property value of property
     */
    public Integer getProperty() {
        return property;
    }

    /**
     * Setter method for property <tt>property</tt>.
     *
     * @param property  value to be assigned to property property
     */
    public void setProperty(final Integer property) {
        this.property = property;
    }
}