/*
 * Dian.com Inc.
 * Copyright (c) 2004-2019 All Rights Reserved.
 */
package cn.sedu.moon.dao.mongo;

import cn.sedu.moon.common.enums.SchoolPropertyEnum;
import cn.sedu.moon.common.enums.SchoolTypeEnum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.geo.Box;
import org.springframework.data.geo.Point;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 *
 * @author ${wukong}
 * @version $Id: SchoolLocationRepository.java, v 0.1 2019年02月18日 6:00 PM Exp $
 */
@Repository
public class SchoolLocationRepository {

    @Autowired
    private MongoTemplate mongoTemplate;

    /**
     * 按圆形查找
     *
     * @param point
     * @param maxDistance
     * @return
     */
    public List<SchoolLocation> findCircleNear(Point point, double maxDistance) {
        Query query = new Query(Criteria.where("position").near(point).maxDistance(maxDistance / 111.12));
        return mongoTemplate.find(query, SchoolLocation.class);
    }

    public List<SchoolLocation> findCircleNear(Point point,
                                               double maxDistance,
                                               SchoolPropertyEnum schoolProperty,
                                               SchoolTypeEnum schoolType,
                                               int offset,
                                               int pageSize) {
        Query query = new Query(Criteria.where("position").near(point).maxDistance(maxDistance / 111.12));

        if (schoolProperty != null) {
            query.addCriteria(Criteria.where("property").is(schoolProperty.getCode()));
        }

        if (schoolType != null) {
            query.addCriteria(Criteria.where("type").is(schoolType.getCode()));
        }
        query.skip(offset);
        query.limit(pageSize);

        return mongoTemplate.find(query, SchoolLocation.class);
    }

    /**
     * 按方形查找
     *
     * @param lowerLeft
     * @param upperRight
     * @return
     */
    public List<SchoolLocation> findBoxWithin(Point lowerLeft, Point upperRight) {
        Query query = new Query(Criteria.where("position").within(new Box(lowerLeft, upperRight)));
        return mongoTemplate.find(query, SchoolLocation.class);
    }
}