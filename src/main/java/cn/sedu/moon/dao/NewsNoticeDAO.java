/*
 * Dian.so Inc.
 * Copyright (c) 2016-2019 All Rights Reserved.
 */
package cn.sedu.moon.dao;

import cn.sedu.moon.dao.model.NewsNotice;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;


/**
 * 继承于BaseDAO，默认有五个方法实现
 * listRecord、getRecord、saveRecord、removeRecord、updateRecord
 *
 * @author MBG工具生成
 * @version $Id: NewsNoticeDAO.java, v 0.1 2019-10-27 00:39:36 Exp $
 */
@Mapper
public interface NewsNoticeDAO extends BaseDAO<NewsNotice> {

    List<NewsNotice> selectWaitPush();
}