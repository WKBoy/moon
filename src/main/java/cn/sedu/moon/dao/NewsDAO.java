/*
 * Dian.so Inc.
 * Copyright (c) 2016-2019 All Rights Reserved.
 */
package cn.sedu.moon.dao;

import cn.sedu.moon.dao.model.News;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.Date;
import java.util.List;

/**
 * 继承于BaseDAO，默认有五个方法实现
 * listRecord、getRecord、saveRecord、removeRecord、updateRecord
 *
 * @author MBG工具生成
 * @version $Id: NewsDAO.java, v 0.1 2019-02-15 13:51:53 Exp $
 */
@Mapper
public interface NewsDAO extends BaseDAO<News> {

    Integer countAll(News news);

    List<News> listRecordEditor(@Param("cityCode") String cityCode);

    List<News> listRecordBySchoolNameList(@Param("list") List<String> list, @Param("cityCode") String cityCode);

    List<News> listRecordByKeywordsList(@Param("list") List<String> list, @Param("cityCode") String cityCode);

    List<News> search(News news);

    int stickNewsByDateDuration(@Param("startTime") Date startTime, @Param("endTime") Date endTime);

    int removeSystemStick();

    /**
     * 获取最新一篇文章
     *
     * @param news
     * @return
     */
    News getTheTopRecord(News news);

    /**
     * 获取当前城市的人工置顶新闻
     * @param cityCode
     * @return
     */
    List<Long> listNewsIdManStick(String cityCode);

    /**
     * 获取当前城市的系统置顶新闻
     * @param cityCode
     * @return
     */
    List<Long> listNewsIdSystemStick(String cityCode);

    List<Long> listRecordIdBySchoolNameList(@Param("list") List<String> list, @Param("cityCode") String cityCode);

    List<Long> listRecordIdByKeywordsList(@Param("list") List<String> list, @Param("cityCode") String cityCode);

    Integer getCreateNewsNumByDateDuration(@Param("start") Date start, @Param("end") Date end, @Param("cityCode") String cityCode);

    Integer getEditWellNewsNumByDateDuration(@Param("start") Date start, @Param("end") Date end, @Param("cityCode") String cityCode);

    News getNewNewsBySchoolName(@Param("list") List<String> list, @Param("cityCode") String cityCode);

//    Integer getOriginalNewsNumByDateDuration(Date now, Date plusDay);
}