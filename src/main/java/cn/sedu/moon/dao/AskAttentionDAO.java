/*
 * Dian.so Inc.
 * Copyright (c) 2016-2019 All Rights Reserved.
 */
package cn.sedu.moon.dao;

import cn.sedu.moon.dao.model.AskAttention;
import org.apache.ibatis.annotations.Mapper;


/**
 * 继承于BaseDAO，默认有五个方法实现
 * listRecord、getRecord、saveRecord、removeRecord、updateRecord
 *
 * @author MBG工具生成
 * @version $Id: AskAttentionDAO.java, v 0.1 2019-01-19 23:09:54 Exp $
 */
@Mapper
public interface AskAttentionDAO extends BaseDAO<AskAttention> {

    Integer countRecord(AskAttention askAttention);
}