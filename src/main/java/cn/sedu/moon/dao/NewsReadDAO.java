/*
 * Dian.so Inc.
 * Copyright (c) 2016-2019 All Rights Reserved.
 */
package cn.sedu.moon.dao;

import cn.sedu.moon.dao.model.NewsRead;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;


/**
 * 继承于BaseDAO，默认有五个方法实现
 * listRecord、getRecord、saveRecord、removeRecord、updateRecord
 *
 * @author MBG工具生成
 * @version $Id: NewsReadDAO.java, v 0.1 2019-03-15 10:29:25 Exp $
 */
@Mapper
public interface NewsReadDAO extends BaseDAO<NewsRead> {
    /**
     * 获取用户已读的文章ID
     * @param userId
     * @return
     */
    List<Long> listRecordIdByUserId(Long userId);
}