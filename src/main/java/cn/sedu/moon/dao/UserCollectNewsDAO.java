/*
 * Dian.so Inc.
 * Copyright (c) 2016-2019 All Rights Reserved.
 */
package cn.sedu.moon.dao;

import cn.sedu.moon.dao.model.UserCollectNews;
import org.apache.ibatis.annotations.Mapper;


/**
 * 继承于BaseDAO，默认有五个方法实现
 * listRecord、getRecord、saveRecord、removeRecord、updateRecord
 *
 * @author MBG工具生成
 * @version $Id: UserCollectNewsDAO.java, v 0.1 2019-06-23 12:26:35 Exp $
 */
@Mapper
public interface UserCollectNewsDAO extends BaseDAO<UserCollectNews> {

    int logicDeleteByUserIdAndNewsId(UserCollectNews userCollectNews);
}