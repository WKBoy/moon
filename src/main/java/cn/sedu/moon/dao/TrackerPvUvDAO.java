/*
 * Dian.so Inc.
 * Copyright (c) 2016-2019 All Rights Reserved.
 */
package cn.sedu.moon.dao;

import cn.sedu.moon.dao.model.TrackerPvUv;
import org.apache.ibatis.annotations.Mapper;


/**
 * 继承于BaseDAO，默认有五个方法实现
 * listRecord、getRecord、saveRecord、removeRecord、updateRecord
 *
 * @author MBG工具生成
 * @version $Id: TrackerPvUvDAO.java, v 0.1 2019-11-10 23:10:48 Exp $
 */
@Mapper
public interface TrackerPvUvDAO extends BaseDAO<TrackerPvUv> {
}