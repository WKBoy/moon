/*
 * Dian.so Inc.
 * Copyright (c) 2016-2019 All Rights Reserved.
 */
package cn.sedu.moon.dao;

import cn.sedu.moon.dao.model.User;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;


/**
 * 继承于BaseDAO，默认有五个方法实现
 * listRecord、getRecord、saveRecord、removeRecord、updateRecord
 *
 * @author MBG工具生成
 * @version $Id: UserDAO.java, v 0.1 2019-01-05 11:29:47 Exp $
 */
@Mapper
public interface UserDAO extends BaseDAO<User> {

    List<User> searchByNick(String nickName);

    Integer uv(User user);

    String getUserNameById(Long parentId);

    Integer countRecord(User user);
}