/*
 * Dian.so Inc.
 * Copyright (c) 2016-2019 All Rights Reserved.
 */
package cn.sedu.moon.dao;

import cn.sedu.moon.dao.model.DistrictGd;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;


/**
 * 继承于BaseDAO，默认有五个方法实现
 * listRecord、getRecord、saveRecord、removeRecord、updateRecord
 *
 * @author MBG工具生成
 * @version $Id: DistrictGdDAO.java, v 0.1 2019-02-10 23:56:38 Exp $
 */
@Mapper
public interface DistrictGdDAO extends BaseDAO<DistrictGd> {

    List<DistrictGd> queryByKeyName(String name);

    Integer countRecord(DistrictGd districtGd);
}