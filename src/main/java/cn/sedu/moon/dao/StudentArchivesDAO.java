/*
 * Dian.so Inc.
 * Copyright (c) 2016-2018 All Rights Reserved.
 */
package cn.sedu.moon.dao;

import cn.sedu.moon.dao.model.StudentArchives;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;


/**
 * 继承于BaseDAO，默认有五个方法实现
 * listRecord、getRecord、saveRecord、removeRecord、updateRecord
 *
 * @author MBG工具生成
 * @version $Id: StudentArchivesDAO.java, v 0.1 2018-12-30 16:00:38 Exp $
 */
@Mapper
public interface StudentArchivesDAO extends BaseDAO<StudentArchives> {
    /**
     * 获取在校生家长ID
     * @param schoolId
     * @return
     */
    List<Long> listUserIdBySchoolId(Long schoolId);

    /**
     * 根据用户ID获取学生所在学校ID
     * @param userId
     * @return
     */
    Long getReadingSchoolId(Long userId);
}