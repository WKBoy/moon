package cn.sedu.moon.common;

import net.sf.json.JSONObject;
import org.apache.commons.codec.binary.Base64;

/**
 * 封装对外访问方法
 * @author liuyazhuang
 *
 */
public class WXCore {

    private static final String WATERMARK = "watermark";
    private static final String APPID     = "appid";

    /**
     * 解密数据
     * @return
     * @throws Exception
     */
    public static String decrypt(String appId, String encryptedData, String sessionKey, String iv) {
        String result = "";
        try {
            AES aes = new AES();
            byte[] resultByte = aes.decrypt(Base64.decodeBase64(encryptedData), Base64.decodeBase64(sessionKey), Base64.decodeBase64(iv));
            if (null != resultByte && resultByte.length > 0) {
                result = new String(WxPKCS7Encoder.decode(resultByte));
                JSONObject jsonObject = JSONObject.fromObject(result);
                String decryptAppid = jsonObject.getJSONObject(WATERMARK).getString(APPID);
                if (!appId.equals(decryptAppid)) {
                    result = "";
                }
            }
        } catch (Exception e) {
            result = "";
            e.printStackTrace();
        }
        return result;
    }

//    public static void main(String[] args) throws Exception {
////        String appId = "wx8f90fe8698685640";
////        String secret = "43c27172d7d6a8b7faffca3e241582f3";
////
////        String code = "011ADigC1yPh650n6XdC1m3jgC1ADigq";
////
////
////        String getSessionKeyUrl = "https://api.weixin.qq.com/sns/jscode2session?appid=" + appId + "&secret=" + secret + "&js_code=" + code + "&grant_type"
////                + "=authorization_code";
////
////        String response = HttpUtil.get(getSessionKeyUrl);
////
////        System.out.println("response is "  + response);
//
//
//        String sessionKey = "qQp1gxu8H+KnOBAyhuRYdg==";
//        String appId = "wx326612a0e19cc895";
//
//        String encryptedData = "9JeBt7mo3i6W6yyqDKfcF0/LE4hPxvJpi89YjsZJmRUswfPAAi/1rC5agxf9VIyXWyRTLjMdE9ztHz9bhUkMcQ6AiXIeZmHPgRoW1ln7v5QsDp0C9BZ069msJ1tsbpnzNbEQxcHu6flukVKTatLCn5gqFkdQJCTKxyonWMR6aSTBpFRtTFnQ75GtJzsVf6PIFNR+5pXO0q/MhP+jKOQlNzatOtzgrq5XBb9nOIMBaR7TKn/m6vk4is0zDoGxAhKI0klSLkvAlaaq4qhdIRGOVLb8GidagcTLIwZuQWo4uVZb4e1spwV4SCMZhRSy2BmnDQiAq2oWzqtHRj3854/+gYZvtH80rDrSnlAcYFlQME5wTGhxM5qJZqxatiXo0ilqm/TpGWIn0sNNOXKJsWPp8vT1f8dZy2w1muuySYbvefMzQGS9a/e4+htc6DlrcjTZaVt31sRscYeScgprZrYLJ17ejJlJ+AxCD7c0BUbwg/I=";
//        String iv = "Zv4EfYx6r+EV6NJiHk4+UA==";
//
//        System.out.println(decrypt(appId, encryptedData, sessionKey, iv));
//
//    }
}
