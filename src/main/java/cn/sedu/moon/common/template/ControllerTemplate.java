/*
 * Dian.com Inc.
 * Copyright (c) 2004-2018 All Rights Reserved.
 */
package cn.sedu.moon.common.template;

import cn.sedu.moon.common.Result;

/**
 * Controller模板接口
 *
 * @author ${baizhang}
 * @version $Id: ControllerTemplate.java, v 0.1 2018-04-16 下午5:19 Exp $
 */
@SuppressWarnings("unused")
public interface ControllerTemplate<T> {
    /**
     * 接口执行(框架实现)
     *
     * @param action            执行的接口
     * @return 返回结果
     */
    default Result<T> execute(ControllerCallback<T> action) {
        try {
            begin();
            return doBiz(action);
        } finally {
            after();
        }
    }

    /**
     * 接口执行(业务实现)
     *
     * @param action     执行的接口
     * @return 返回结果
     */
    Result<T> doBiz(ControllerCallback<T> action);

    /**
     * 开始
     * 用于上下文初始化
     */
    default void begin() {
    }

    /**
     * 结束
     * 用于上下文清理
     */
    default void after() {
    }
}
