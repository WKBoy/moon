/*
 * Dian.com Inc.
 * Copyright (c) 2004-2018 All Rights Reserved.
 */
package cn.sedu.moon.common.template;

import cn.sedu.moon.common.ErrorUtil;
import cn.sedu.moon.common.Result;
import cn.sedu.moon.common.exception.BizException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * Controller操作模板
 * 需要获取应用名称，使用了Spring boot的默认配置应用名称，最好使用Spring boot项目，并有相应配置
 *
 * @author ${baizhang}
 * @version $Id: AbstractControllerTemplate.java, v 0.1 2018-04-16 下午5:24 Exp $
 */
@Slf4j
@Component("AbstractControllerTemplate")
@SuppressWarnings("unused")
public class AbstractControllerTemplate<T> implements ControllerTemplate<T> {

    @Value("${spring.application.name}")
    private String appName;

    @Override
    public Result<T> doBiz(final ControllerCallback<T> action) {
        final Result<T> result = new Result<>();
        try {
            //检查参数
            action.checkParam();
            // 构建上下文
            action.buildContext();
            // 幂等处理(决策无幂等操作)
            action.checkConcurrent();
            // 执行
            final T execute = action.execute();
            // 返回
            result.setData(execute);
            result.setSuccess(true);
            return result;
        } catch (BizException ex) {
            log.error("业务处理异常：{}", ex.getMessage());
            return ErrorUtil.buildBizProcess(result, ex);
        } finally {
            log.debug("{}-操作结果：{}", appName, result);
        }

    }
}
