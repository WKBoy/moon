/*
 * Dian.com Inc.
 * Copyright (c) 2004-2018 All Rights Reserved.
 */
package cn.sedu.moon.common.template;

/**
 * Controller模板方法回调接口
 * 幂等控制，有写入操作的情况下，要求必须实现方法细节
 *
 * @author ${baizhang}
 * @version $Id: ControllerCallback.java, v 0.1 2018-04-16 下午5:14 Exp $
 */
public interface ControllerCallback<T> {
    /**
     * 检查参数
     */
    void checkParam();

    /**
     * 构建上下文
     * 目的：起到承上启下的作用，将外部参数转换为内部execute需要的参数
     */
    default void buildContext() {}

    /**
     * 幂等控制
     * 幂等是针对有写入的操作要求必须实现的方法
     */
    default void checkConcurrent() {}

    /**
     * 执行接口
     *
     * @return 结果
     */
    T execute();
}
