/*
 * Dian.com Inc.
 * Copyright (c) 2004-2018 All Rights Reserved.
 */
package cn.sedu.moon.common.exception;

import cn.sedu.moon.common.ErrorContext;
import cn.sedu.moon.common.enums.ErrorCodeEnum;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 需要重试的异常
 *
 * @author ${wukong}
 * @version $Id: BizException.java, v 0.1 2017年07月04日 上午11:17 Exp $
 */
@Data
@EqualsAndHashCode(callSuper = false)
@SuppressWarnings("unused")
public class BizException extends RuntimeException {

    /** serialVersionUID */
    private static final long serialVersionUID = 6136824916931940285L;

    /** 异常错误代码 */
    private ErrorCodeEnum errorCode;

    /** 错误上下文 */
    private ErrorContext errorContext;

    /** 错误上下文*/
    private Throwable originalThrowable;

    /**
     * 创建一DataQueryException
     *
     * @param errorCode 系统定义异常 当前异常
     * @param errorContext 外部系统发生异常
     */
    @SuppressWarnings("unused")
    public BizException(ErrorCodeEnum errorCode, ErrorContext errorContext) {
        super(errorCode.getDescription());
        this.errorCode = errorCode;
        this.errorContext = errorContext;
    }

    /**
     * 创建一DataQueryException
     *
     * <p>内部异常时使用</p>
     *
     * @param errorCode   内部错误码
     * @param message     异常信息
     */
    public BizException(ErrorCodeEnum errorCode, String message) {
        super(message);
        this.errorCode = errorCode;
    }

    /**
     * 创建一DataQueryException
     *
     * @param errorCode 错误码
     */
    public BizException(ErrorCodeEnum errorCode, Throwable originalThrowable) {
        super(errorCode.getDescription());
        this.errorCode = errorCode;
        this.originalThrowable = originalThrowable;
    }

    /**
     * 创建一DataQueryException,保存原始异常信息
     *
     * @param errorCode 错误码
     */
    public BizException(ErrorCodeEnum errorCode) {
        super(errorCode.getDescription());
        this.errorCode = errorCode;
    }

    /**
     * 异常构造函数
     *
     * @param errorCode  错误码
     * @param message               异常信息
     * @param e                     异常堆栈
     */
    public BizException(ErrorCodeEnum errorCode, String message, Throwable e) {
        super(message, e);
        this.errorCode = errorCode;
    }

}

