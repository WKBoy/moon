/*
 * Dian.com Inc.
 * Copyright (c) 2004-2018 All Rights Reserved.
 */
package cn.sedu.moon.common;

import java.io.Serializable;

import lombok.Data;

/**
 * 错误信息上下文
 * 原则上该类应该放error模块，由于依赖引入的层级关系，会出现嵌套依赖的情况，所以放顶层模块
 *
 * @author ${baizhang}
 * @version $Id: ErrorContext.java, v 0.1 2018-04-16 上午11:01 Exp $
 */
@Data
public class ErrorContext implements Serializable {
    /** serialVersionUID */
    private static final long serialVersionUID = -171785465778410092L;

    /**
     * 错误信息
     */
    private String errorMsg;

    /**
     * 应用名称
     */
    private String appName;

    /**
     * 应用所在服务器ip
     */
    private String ip;
}
