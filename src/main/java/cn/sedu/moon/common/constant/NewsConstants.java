package cn.sedu.moon.common.constant;

/**
 * @author lix
 * @version 1.0
 * @date 2019/12/1 22:27
 * @describe
 */
public class NewsConstants {
    /**
     * 新用户文章id
     */
    public static final Long NEW_USER_NEWS_ID = 47844L;
}
