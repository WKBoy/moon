/*
 * Dian.com Inc.
 * Copyright (c) 2004-2018 All Rights Reserved.
 */
package cn.sedu.moon.common.constant;

import java.util.concurrent.TimeUnit;

/**
 * 业务常量
 * :
 * @author ${wukong}
 * @version $Id: BizConstants.java, v 0.1 2018年12月07日 5:40 PM Exp $
 */
public class BizConstants {

    /**
     * http请求头中保存用户登录token的key
     */
    public static final String XD_USER_TOKEN = "X-MOON-TOKEN";

    /**
     * 用户头
     */
    public static final String USER_ID = "User-Id";

    /**
     * 用户 token 的有效时间, 30天：2592000秒
     */
    public static final long USER_TOKEN_TIME_TO_LIVE = 2592000L;

    /**
     * 用户 token 有效时间的单位
     */
    public static final TimeUnit USER_TOKEN_LIVE_TIME_UNIT = TimeUnit.SECONDS;

    /**
     * redis锁的等待时间
     */
    public static final Integer REDIS_LOCK_WAIT_SECONDS = 1;

    /**
     * redis锁的释放时间
     */
    public static final Integer REDIS_LOCK_RELEASE_SECONDS = 30;

    /**
     * 每天用户可以不用分享的情况下阅读几篇文章
     */
    public static final Integer NEWS_FREE_READ_TIMES = 3;

    /**
     * 点亮的文章需要多少人点亮才能看
     */
    public static final Integer NEWS_LIGHT_USER_COUNT = 3;

    /**
     * 应用LOGO
     */
    public static final String APP_LOGO_URL = "http://moon-img.oss-cn-hangzhou.aliyuncs.com/moon/2019/08/07/1024w_1024h_9F3381565147345.jpeg";

    /**
     * 默认学校的图片
     */
    public static final String SCHOOL_ICON_DEFAULT_URL = "http://moon-img.oss-cn-hangzhou.aliyuncs.com/moon/2019/11/03/457w_457h_6B2CD1572783974.png";

    /**
     * 默认问答回答者的头像
     */
    public static final String ASK_DEFAULT_ANSWER_AVATAR = "http://moon-img.oss-cn-hangzhou.aliyuncs.com/moon/2019/11/21/108w_108h_FD6C81574350171.png";

    /**
     * 默认问答回答者的名称
     */
    public static final String ASK_DEFAULT_ANSWER_NAME = "小鹿初学者";

    /**
     * 应用名称
     */
    public static final String APP_NAME = "小鹿上学";

    /**
     * 微信获取sessionKey的授权类型
     */
    public static final String WX_APP_GRANT_TYPE_SESSION_KEY = "authorization_code";


    /**
     * 每周精选微信消息推送模板id
     */
    public static final String WX_MESSAGE_PUSH_TEMPLATE_ID_BESTWEEK = "wHC8d1I7Pqt0xMVeUm7fRVrXvRqMSr0HxJWnJ43zWyo";

    /**
     * 新文章推送模板id
     */
    public static final String NEW_NEWS_NOTICE_PUSH_TEMPLATE_ID = "9Emokneex4uav7AMOeogiTphNOsoUG21KoBrCU7VO4o";

    /**
     * 在校生家长评论通知
     */
    public static final String COMMENT_HELP_NOTICE_PUSH_TEMPLATE_ID = "KTuaWVDD89EJwW7gCo2UCgUfJkuMUUIVN3JGo8_FJsg";

    /**
     *  newsNotice 模版id
     */
    public static final String WX_MESSAGE_PUSH_TEMPLATE_ID_NEWS_NOTICE = "gfAvfgQsR72k2qlpS1618uFFtoMM2pbBejhVOAiJDq4";

    /**
     * 注册成功通知模板
     */
    public static final String REGISTER_SUCCESS_PUSH_TEMPLATE_ID = "Z6BlG93dUEvl23siPTXAnOcYh9pKteq-AdxwgQLs6NY";

    /**
     * 新用户进入录入信息页面后，1个小时后未注册成功则发送消息的模板
     */
    public static final String NOT_ARCHIVE_TEMPLATE_ID = "9Emokneex4uav7AMOeogiTphNOsoUG21KoBrCU7VO4o";

    /**
     * 新用户用户注册完成后1小时内未阅读过新闻，发送消息
     */
    public static final String ARCHIVE_BUT_NOT_READ_NEWS_TEMPLATE_ID = "9Emokneex4uav7AMOeogiTphNOsoUG21KoBrCU7VO4o";
}