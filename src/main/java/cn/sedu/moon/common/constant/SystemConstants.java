/*
 * Dian.com Inc.
 * Copyright (c) 2004-2019 All Rights Reserved.
 */
package cn.sedu.moon.common.constant;

/**
 * @author wukong
 * @version : SystemConstants.java, v 0.1 2019年11月18日 10:23 上午 Exp $
 */
public class SystemConstants {

    /**
     * local配置文件名字
     */
    public static final String SPRING_PROFILE_LOCAL = "local";

    /**
     * dev配置文件名字
     */
    public static final String SPRING_PROFILE_DEV = "dev";

    /**
     * pre配置文件名字
     */
    public static final String SPRING_PROFILE_PRE = "pre";


    /**
     * real配置文件名字
     */
    public static final String SPRING_PROFILE_REAL = "real";

}