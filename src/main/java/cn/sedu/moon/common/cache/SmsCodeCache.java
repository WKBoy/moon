/*
 * Dian.com Inc.
 * Copyright (c) 2004-2018 All Rights Reserved.
 */
package cn.sedu.moon.common.cache;

import java.io.Serializable;

import lombok.Data;

/**
 * 短信验证码缓存对象
 *
 * @author ${wukong}
 * @version $Id: SmsCodeCache.java, v 0.1 2018年01月17日 4:42 PM Exp $
 */
@Data
@SuppressWarnings("unused")
public class SmsCodeCache implements Serializable {

    private static final long serialVersionUID = -7810091534552335570L;

    /**
     * 验证码
     */
    private String code;

    /**
     * 创建时间
     */
    private long createTime;

    /**
     * 上次发送时间, 该参数用来控制两次发送短信验证码之间的时长
     */
    private long lastSendTime;

    /**
     * 发送次数
     */
    private int sendTimes;
}