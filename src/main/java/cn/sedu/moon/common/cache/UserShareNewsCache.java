/*
 * Dian.com Inc.
 * Copyright (c) 2004-2019 All Rights Reserved.
 */
package cn.sedu.moon.common.cache;

import java.io.Serializable;

import lombok.Data;

/**
 * 缓存用户分享的信息
 *
 * @author ${wukong}
 * @version $Id: UserShareNewsCache.java, v 0.1 2019年05月13日 2:22 PM Exp $
 */
@Data
public class UserShareNewsCache implements Serializable {

    private static final long serialVersionUID = 1829548935887638124L;

    /**
     * 不用分享阅读的次数
     */
    private Integer freeReadCount;

    /**
     * 0 未分享 1 分享过
     */
    private Integer shared;

}