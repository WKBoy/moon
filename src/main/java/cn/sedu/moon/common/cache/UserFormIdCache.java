/*
 * Dian.com Inc.
 * Copyright (c) 2004-2019 All Rights Reserved.
 */
package cn.sedu.moon.common.cache;

import java.io.Serializable;
import java.util.Date;

import lombok.Data;

/**
 *
 * @author ${wukong}
 * @version $Id: UserFormIdCache.java, v 0.1 2019年04月02日 11:23 AM Exp $
 */
@Data
public class UserFormIdCache implements Serializable {

    private static final long serialVersionUID = -8297593539457249387L;

    /**
     * formId
     */
    private String formId;

    /**
     * 创建时间
     */
    private Date createTime;
}