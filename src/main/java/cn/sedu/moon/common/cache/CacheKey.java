/*
 * Dian.com Inc.
 * Copyright (c) 2004-2018 All Rights Reserved.
 */
package cn.sedu.moon.common.cache;

/**
 * 缓存 key 相关的常量
 *
 * @author ${wukong}
 * @version $Id: CacheKey.java, v 0.1 2018年12月10日 3:29 PM Exp $
 */
public class CacheKey {

    /**
     * 应用名
     */
    public static final String APP_NAME = "MOON";

    /**
     * 短信验证码前缀
     */
    public static final String SMS_CODE_PREFIX = "SMS_CODE";

    /**
     * 用户登陆TOKEN前缀
     */
    public static final String USER_TOKEN_PREFIX = "USER_TOKEN";


    public static final String USER_SESSION_KEY_PREFIX = "USER_SESSION_KEY";

    /**
     * CMS用户登陆TOKEN前缀
     */
    public static final String USER_CMS_TOKEN_PREFIX = "USER_CMS_TOKEN";

    /**
     * 下划线
     */
    public static final String UNDERLINE = "_";

    /**
     * 缓存用户新闻数据的key
     */
    public static final String USER_NEWS_KEY_PREFIX = "USER_NEWS_KEY";

    /**
     * 缓存学校tab学校列表
     */
    public static final String SCHOOL_TAB_USER_SCHOOL_KEY = "SCHOOL_TAB_USER_SCHOOL_KEY";

    /**
     * 缓存用户首页推荐的问答数据的key
     */
    public static final String USER_ASK_KEY_PREFIX = "USER_ASK_KEY";

    /**
     * 缓存用户推荐学校数据的key
     */
    public static final String USER_RECOMMEND_SCHOOL_KEY_PREFIX = "USER_RECOMMEND_SCHOOL_KEY";

    /**
     * 变更荣誉值的 redis 锁
     */
    public static final String GLORY_UPDATE_LOCK_KEY_PREFIX = "GLORY_UPDATE_LOCK_";

    /**
     * 上报信息的redis锁
     */
    public static final String REPORT_ARCHIVE_LOCK_KEY_PREFIX = "REPORT_ARCHIVE_LOCK_KEY_PREFIX_";

    /**
     * 发布问题的redis锁
     */
    public static final String PUBLISH_ASK_LOCK_KEY_PREFIX = "PUBLISH_ASK_LOCK_KEY_PREFIX_";

    /**
     * 用户formid缓存队列
     *
     * 规则：前缀_日期（0301）_用户form_id
     */
    public static final String PUSH_USER_DAY_QUEUE_PREFIX = "du_f_%s_%s";


    public static final String USER_SHARE_NEWS_KEY_PREFIX = "USER_SHARE_NEWS_KEY_PREFIX";

    /**
     * 缓存accessToken的key
     */
    public static final String WX_ACCESS_TOKEN_KEY = "WX_ACCESS_TOKEN_KEY";

    /**
     * 置顶新闻任务的分布式锁的key
     */
    public static final String LOCK_TASK_STICK_NEWS = "LOCK_TASK_STICK_NEWS";

    /**
     * 用户新文章推送每天计数的key
     */
    public static final String USER_NEW_NEWS_DAY_NOTICE_KEY_PREFIX = "USER_NEW_NEWS_DAY_NOTICE_KEY_PREFIX";

    /**
     * 用户新文章推送每周计数的key
     */
    public static final String USER_NEW_NEWS_WEEK_NOTICE_KEY_PREFIX = "USER_NEW_NEWS_WEEK_NOTICE_KEY_PREFIX";
    /**
     * 学校tag的key
     */
    public static final String SCHOOL_SCHOOL_TAG_KEY_PREFIX = "SCHOOL_SCHOOL_TAG_KEY_PREFIX";
    /**
     * 新用户IDList的key
     */
    public static final String OLD_USER_LIST_KEY_PREFIX = "OLD_USER_LIST_KEY_PREFIX";
    /**
     * 新用户文章阅读的key
     */
    public static final String NEW_USER_NEWS_READ_KEY_PREFIX = "NEW_USER_NEWS_READ_KEY_PREFIX";
    /**
     * 用户提问涉及到的学校list
     */
    public static final String USER_ASK_PUBLISH_KEY_PREFIX = "USER_ASK_PUBLISH_KEY_PREFIX";
    /**
     * 用户想问涉及到的学校list
     */
    public static final String USER_ASK_FOLLOW_KEY_PREFIX = "USER_ASK_FOLLOW_KEY_PREFIX";
    /**
     * 用户分享涉及到的学校list
     */
    public static final String USER_SHARE_KEY_PREFIX = "USER_SHARE_KEY_PREFIX";

    /**
     * 推送news notice 的任务
     */
    public static final String LOCK_TASK_NEWS_NOTICE_PUSH = "LOCK_TASK_NEWS_NOTICE_PUSH";

    /**
     * 缓存每周精选的新闻的key
     */
    public static final String WEEKLY_SELECTED_NEWS_PREFIX = "WEEKLY_SELECTED_NEWS";

    public static final String NEWS_NOTICE_COUNT_KEY_PREFIX = "NEWS_NOTICE_COUNT_KEY_PREFIX";
    /**
     * 新用户Map
     */
    public static final String NEW_USER_MAP_KEY_PREFIX = "NEW_USER_MAP_KEY_PREFIX";

    /**
     * 新用户第一次登陆（新增用户时）
     */
    public static final String NEW_USER_NOT_ARCHIVE_MAP_KEY_PREFIX = "NEW_USER_NOT_ARCHIVE_MAP_KEY_PREFIX";

    /**
     * 新用户第一次填写在读信息
     */
    public static final String NEW_USER_ARCHIVE_MAP_KEY_PREFIX = "NEW_USER_ARCHIVE_MAP_KEY_PREFIX";

    /**
     * 发布文章推送成功数
     */
    public static final String NEWS_NOTICE_SUCCESS_COUNT_KEY_PREFIX = "NEWS_NOTICE_SUCCESS_COUNT_KEY_PREFIX";
}