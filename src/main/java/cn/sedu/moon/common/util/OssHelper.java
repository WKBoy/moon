/*
 * Dian.com Inc.
 * Copyright (c) 2004-2019 All Rights Reserved.
 */
package cn.sedu.moon.common.util;

import java.io.InputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.aliyun.oss.ClientException;
import com.aliyun.oss.OSSClient;
import com.aliyun.oss.OSSException;
import com.aliyun.oss.model.ObjectMetadata;
import org.springframework.util.DigestUtils;

/**
 *
 * @author ${wukong}
 * @version $Id: OssHelper.java, v 0.1 2019年03月04日 2:00 PM Exp $
 */
public class OssHelper {

    private static final String ACCESS_ID   = "LTAIsYEZwRQdEbjZ";
    private static final String ACCESS_KEY  = "FeVeXa9RHeIMLFAoLeemVjNE7vVECS";
    private static final String END_POINT   = "http://oss-cn-hangzhou.aliyuncs.com";
    private static final String BUCKET_NAME = "moon-img";

    private static final String urlPrefix = "http://moon-img.oss-cn-hangzhou.aliyuncs.com";

    private static final OSSClient ossClient = new OSSClient(END_POINT, ACCESS_ID, ACCESS_KEY);

    public static String uploadFile(String saveFileName, InputStream input,
                                  String contentType, long contentLength)
            throws OSSException, ClientException {

        ObjectMetadata objectMeta = new ObjectMetadata();
        objectMeta.setContentLength(contentLength);
        objectMeta.setContentType(contentType);
        ossClient.putObject(BUCKET_NAME, saveFileName, input, objectMeta);

        return urlPrefix + "/" + saveFileName;
    }

    public static String getSaveFileName(String originalFilename,
                                         String contentType,
                                         String sizeHex,
                                         int width, int height) {

        String fileToken = DigestUtils.md5DigestAsHex((originalFilename + contentType + sizeHex).getBytes())
                .substring(0, 5).toUpperCase();
        String suffix = getSuffix(originalFilename);
        Date now = new Date();
        DateFormat dateFormat = new SimpleDateFormat("/YYYY/MM/dd/");
        return "moon" + dateFormat.format(now) + width + "w_" + height + "h_"
                + fileToken
                + now.getTime() / 1000
                + "." + suffix;
    }

    public static String getSuffix(String fileName) {
        return fileName.substring(fileName.lastIndexOf(".") + 1).trim().toLowerCase();
    }



}