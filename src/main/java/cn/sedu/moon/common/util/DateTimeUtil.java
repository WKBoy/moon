package cn.sedu.moon.common.util;

import org.apache.commons.lang3.StringUtils;

import java.text.SimpleDateFormat;
import java.util.Date;

public class DateTimeUtil {


    /**
     * 时间格式转换成String
     *
     * @param date
     * @param format
     * @return
     */
    public static String dateToString(Date date, String format) {
        if (date == null) return "";
        format = StringUtils.isEmpty(format) ? "yyyy-MM-dd HH:mm:ss" : format;
        SimpleDateFormat formatter = new SimpleDateFormat(format);
        String dateString = formatter.format(date);
        return dateString;

    }
}
