/*
 * Dian.com Inc.
 * Copyright (c) 2004-2019 All Rights Reserved.
 */
package cn.sedu.moon.common.util;

import java.io.IOException;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;

import lombok.extern.slf4j.Slf4j;

/**
 *
 * @author ${wukong}
 * @version $Id: WxMiniClient.java, v 0.1 2019年04月02日 5:43 PM Exp $
 */
@Slf4j
public class WxMiniClient {

    public static final String SEND_MSG_URL = "https://api.weixin.qq.com/cgi-bin/message/wxopen/template/send";

    public static String sendTemplateMessage(WxTemplateMessage messsage) {

        JSONObject jsonObject = new JSONObject();

        // 接口调用凭证
        jsonObject.put("access_token", null);
        // 用户的formId
        jsonObject.put("form_id", null);
        // 模版内容
        jsonObject.put("data", null);
        // 模版id
        jsonObject.put("template_id", null);
        // 点击模板卡片后的跳转页面，仅限本小程序内的页面。支持带参数,（示例index?foo=bar）。该字段不填则模板无跳转。
        jsonObject.put("page", null);

        try {
            String response = HttpUtil.postByJson(SEND_MSG_URL, jsonObject.toJSONString());

            JSONObject responseJson = JSON.parseObject(response);

            if (responseJson == null || !responseJson.containsKey("errcode") || responseJson.getInteger("errcode") != 0) {
                System.out.println("Error: sendTemplateMessage response is " + response);
            }

            log.info("sendTemplateMessage response is {}", response);


        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;

    }

}