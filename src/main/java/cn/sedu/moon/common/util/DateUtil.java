/*
 * Dian.com Inc.
 * Copyright (c) 2004-2018 All Rights Reserved.
 */
package cn.sedu.moon.common.util;

import org.joda.time.DateTime;
import org.joda.time.Interval;

import java.util.Calendar;
import java.util.Date;

/**
 * 日期工具类
 *
 * @author huixiong
 * @version $Id: DateUtil.java, v 0.1 2018-09-15 15:45 Exp $
 */
public class DateUtil {

    public static Date beginOfDay(Date date) {
        DateTime dateTime = new DateTime(date);
        return dateTime.withTimeAtStartOfDay().toDate();
    }

    public static Date endOfDay(Date date) {
        Calendar dateEnd = Calendar.getInstance();
        dateEnd.setTime(date);
        dateEnd.set(Calendar.HOUR_OF_DAY, 23);
        dateEnd.set(Calendar.MINUTE, 59);
        dateEnd.set(Calendar.SECOND, 59);
        return dateEnd.getTime();
    }

    public static Date plusDay(Date date, int plus) {
        DateTime dateTime = new DateTime(date);
        return dateTime.plusDays(plus).toDate();
    }

    public static long minutesBetween(Date start, Date end) {
        Interval interval = new Interval(start.getTime(), end.getTime());
        return interval.toPeriod().getMinutes();
    }

    /**
     * 两个时间点间隔天数，不足一天为0, 如果start在end之后，返回为负数
     * @param start
     * @param end
     * @return
     */
    public static long daysBetween(Date start, Date end) {
        if (start.after(end)) {
            return - daysBetween(end, start);
        }
        Interval interval = new Interval(start.getTime(), end.getTime());
        return interval.toDuration().getStandardDays();
    }

    /**
     * 两个时间点间隔的自然日
     * @param start
     * @param end
     * @return
     */
    public static long natureDaysBetween(Date start, Date end) {
        return daysBetween(new DateTime(start).withTimeAtStartOfDay().toDate(), new DateTime(end).withTimeAtStartOfDay().toDate());
    }

    public static boolean isAfter(Date date, Date end) {
        DateTime dateTime = new DateTime(date);
        return dateTime.isAfter(end.getTime());
    }
//
//    public static void main(String[] args) {
//        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//
//        Date lastDay = plusDay(new Date(), -1);
//
//        Date startDate = beginOfDay(lastDay);
//        Date endDate = endOfDay(lastDay);
//        System.out.println("startDate ---- " + sdf.format(startDate));
//        System.out.println("endDate ---- " + sdf.format(endDate));
//    }

}