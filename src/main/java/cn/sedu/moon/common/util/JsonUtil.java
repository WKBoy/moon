/*
 * Dian.com Inc.
 * Copyright (c) 2004-2018 All Rights Reserved.
 */
package cn.sedu.moon.common.util;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * jackson 实现json、对象的转换操作
 *
 * @author ${baizhang}
 * @version $Id: JsonUtil.java, v 0.1 2018-09-18 下午10:10 Exp $
 */
public class JsonUtil {
    private static ObjectMapper mapper;

    static {
        mapper = new ObjectMapper();
        // Include.NON_NULL 属性为NULL 不序列化
        mapper.setSerializationInclusion(Include.NON_NULL);
        SimpleDateFormat smt = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        // 时间类型格式化
        mapper.setDateFormat(smt);
        // 处理json中缺少映射字段异常
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
    }

    /**
     * 对象转JSON字符串
     *
     * @param obj           被转换对象
     * @return json字符串
     */
    public static <T> String beanToJson(T obj) {
        if (null == obj) {
            return null;
        }
        try {
            return mapper.writeValueAsString(obj);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * JSON字符串转换为对象
     *
     * @param json          json字符串
     * @param claxx         转换类型
     * @param <T>           泛型类型
     * @return 转换后对象
     */
    public static <T> T jsonToBean(String json, Class<T> claxx) {
        T t = null;
        try {
            t = mapper.readValue(json, claxx);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return t;
    }

    /**
     * 对象转换map
     *
     * @param obj           对象
     * @param <T>           对象类型
     * @return map
     */
    public static <T> Map beanToMap(T obj) {
        Map map = null;
        try {
            String json = beanToJson(obj);
            map = mapper.readValue(json, Map.class);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return map;
    }
}
