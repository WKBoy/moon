/*
 * Dian.com Inc.
 * Copyright (c) 2004-2018 All Rights Reserved.
 */
package cn.sedu.moon.common.util;

import cn.sedu.moon.common.cache.CacheKey;

/**
 * 缓存 key 生成 工具类
 *
 * @author ${wukong}
 * @version $Id: CacheKeyUtil.java, v 0.1 2018年12月10日 3:27 PM Exp $
 */
public class CacheKeyUtil {

    public static String genSmsCodeCacheKey(String mobile) {
        StringBuilder sb = new StringBuilder();
        sb.append(CacheKey.APP_NAME);
        sb.append(CacheKey.UNDERLINE);
        sb.append(CacheKey.SMS_CODE_PREFIX);
        sb.append(CacheKey.UNDERLINE);
        sb.append(mobile);
        return sb.toString();
    }

    public static String genUserTokenKey(Long userId) {
        StringBuilder sb = new StringBuilder();
        sb.append(CacheKey.APP_NAME);
        sb.append(CacheKey.UNDERLINE);
        sb.append(CacheKey.USER_TOKEN_PREFIX);
        sb.append(CacheKey.UNDERLINE);
        sb.append(userId);
        return sb.toString();
    }

    public static String genUserSessionKeyCacheKey(Long userId) {
        StringBuilder sb = new StringBuilder();
        sb.append(CacheKey.APP_NAME);
        sb.append(CacheKey.UNDERLINE);
        sb.append(CacheKey.USER_SESSION_KEY_PREFIX);
        sb.append(CacheKey.UNDERLINE);
        sb.append(userId);
        return sb.toString();
    }

    public static String genUserCmsTokenKey(Long userId) {
        StringBuilder sb = new StringBuilder();
        sb.append(CacheKey.APP_NAME);
        sb.append(CacheKey.UNDERLINE);
        sb.append(CacheKey.USER_CMS_TOKEN_PREFIX);
        sb.append(CacheKey.UNDERLINE);
        sb.append(userId);
        return sb.toString();
    }

    public static String genUserNewsKey(Long userId) {
        StringBuilder sb = new StringBuilder();
        sb.append(CacheKey.APP_NAME);
        sb.append(CacheKey.UNDERLINE);
        sb.append(CacheKey.USER_NEWS_KEY_PREFIX);
        sb.append(CacheKey.UNDERLINE);
        sb.append(userId);
        return sb.toString();
    }

    public static String genSchoolTabUserSchoolKey(Long userId) {

        StringBuilder sb = new StringBuilder();
        sb.append(CacheKey.APP_NAME);
        sb.append(CacheKey.UNDERLINE);
        sb.append(CacheKey.SCHOOL_TAB_USER_SCHOOL_KEY);
        sb.append(CacheKey.UNDERLINE);
        sb.append(userId);
        return sb.toString();

    }

    public static String genUserAskKey(Long userId) {
        StringBuilder sb = new StringBuilder();
        sb.append(CacheKey.APP_NAME);
        sb.append(CacheKey.UNDERLINE);
        sb.append(CacheKey.USER_ASK_KEY_PREFIX);
        sb.append(CacheKey.UNDERLINE);
        sb.append(userId);
        return sb.toString();
    }

    public static String genUserRecommendSchoolKey(Long userId) {
        StringBuilder sb = new StringBuilder();
        sb.append(CacheKey.APP_NAME);
        sb.append(CacheKey.UNDERLINE);
        sb.append(CacheKey.USER_RECOMMEND_SCHOOL_KEY_PREFIX);
        sb.append(CacheKey.UNDERLINE);
        sb.append(userId);
        return sb.toString();
    }

    public static String genUserShareNewsKey(Long userId) {
        StringBuilder sb = new StringBuilder();
        sb.append(CacheKey.APP_NAME);
        sb.append(CacheKey.UNDERLINE);
        sb.append(CacheKey.USER_SHARE_NEWS_KEY_PREFIX);
        sb.append(CacheKey.UNDERLINE);
        sb.append(userId);
        return sb.toString();
    }

    public static String genUserNewNewsDayNoticeKey(Long userId) {
        StringBuilder sb = new StringBuilder();
        sb.append(CacheKey.APP_NAME);
        sb.append(CacheKey.UNDERLINE);
        sb.append(CacheKey.USER_NEW_NEWS_DAY_NOTICE_KEY_PREFIX);
        sb.append(CacheKey.UNDERLINE);
        sb.append(userId);
        return sb.toString();
    }

    public static String genWeeklySelectedNewsKey(Long userId) {
        StringBuilder sb = new StringBuilder();
        sb.append(CacheKey.APP_NAME);
        sb.append(CacheKey.UNDERLINE);
        sb.append(CacheKey.WEEKLY_SELECTED_NEWS_PREFIX);
        sb.append(CacheKey.UNDERLINE);
        sb.append(userId);
        return sb.toString();
    }

    public static String genUserNewNewsWeekNoticeKey(Long userId) {
        StringBuilder sb = new StringBuilder();
        sb.append(CacheKey.APP_NAME);
        sb.append(CacheKey.UNDERLINE);
        sb.append(CacheKey.USER_NEW_NEWS_WEEK_NOTICE_KEY_PREFIX);
        sb.append(CacheKey.UNDERLINE);
        sb.append(userId);
        return sb.toString();
    }

    public static String genSchoolTagKey(Long schoolId) {
        StringBuilder sb = new StringBuilder();
        sb.append(CacheKey.APP_NAME);
        sb.append(CacheKey.UNDERLINE);
        sb.append(CacheKey.SCHOOL_SCHOOL_TAG_KEY_PREFIX);
        sb.append(CacheKey.UNDERLINE);
        sb.append(schoolId);
        return sb.toString();
    }

    public static String genOldUserListKey(Long userId) {
        StringBuilder sb = new StringBuilder();
        sb.append(CacheKey.APP_NAME);
        sb.append(CacheKey.UNDERLINE);
        sb.append(CacheKey.OLD_USER_LIST_KEY_PREFIX);
        sb.append(CacheKey.UNDERLINE);
        sb.append(userId);
        return sb.toString();
    }

    public static String genNewUserNewsReadKey(Long userId) {
        StringBuilder sb = new StringBuilder();
        sb.append(CacheKey.APP_NAME);
        sb.append(CacheKey.UNDERLINE);
        sb.append(CacheKey.NEW_USER_NEWS_READ_KEY_PREFIX);
        sb.append(CacheKey.UNDERLINE);
        sb.append(userId);
        return sb.toString();
    }

    public static String genUserAskPublishKey(Long userId) {
        StringBuilder sb = new StringBuilder();
        sb.append(CacheKey.APP_NAME);
        sb.append(CacheKey.UNDERLINE);
        sb.append(CacheKey.USER_ASK_PUBLISH_KEY_PREFIX);
        sb.append(CacheKey.UNDERLINE);
        sb.append(userId);
        return sb.toString();
    }

    public static String genUserAskFollowKey(Long userId) {
        StringBuilder sb = new StringBuilder();
        sb.append(CacheKey.APP_NAME);
        sb.append(CacheKey.UNDERLINE);
        sb.append(CacheKey.USER_ASK_FOLLOW_KEY_PREFIX);
        sb.append(CacheKey.UNDERLINE);
        sb.append(userId);
        return sb.toString();
    }

    public static String genUserShareKey(Long userId) {
        StringBuilder sb = new StringBuilder();
        sb.append(CacheKey.APP_NAME);
        sb.append(CacheKey.UNDERLINE);
        sb.append(CacheKey.USER_SHARE_KEY_PREFIX);
        sb.append(CacheKey.UNDERLINE);
        sb.append(userId);
        return sb.toString();
    }

    public static String genNewsNoticeCountKey(Long newsId) {
        StringBuilder sb = new StringBuilder();
        sb.append(CacheKey.APP_NAME);
        sb.append(CacheKey.UNDERLINE);
        sb.append(CacheKey.NEWS_NOTICE_COUNT_KEY_PREFIX);
        sb.append(CacheKey.UNDERLINE);
        sb.append(newsId);
        return sb.toString();
    }

    public static String genNewUserMapKey() {
        StringBuilder sb = new StringBuilder();
        sb.append(CacheKey.APP_NAME);
        sb.append(CacheKey.UNDERLINE);
        sb.append(CacheKey.NEW_USER_MAP_KEY_PREFIX);
        sb.append(CacheKey.UNDERLINE);
        return sb.toString();
    }

    public static String genNewUserNotArchiveMapKey() {
        StringBuilder sb = new StringBuilder();
        sb.append(CacheKey.APP_NAME);
        sb.append(CacheKey.UNDERLINE);
        sb.append(CacheKey.NEW_USER_NOT_ARCHIVE_MAP_KEY_PREFIX);
        sb.append(CacheKey.UNDERLINE);
        return sb.toString();
    }

    public static String genNewUserArchiveMapKey() {
        StringBuilder sb = new StringBuilder();
        sb.append(CacheKey.APP_NAME);
        sb.append(CacheKey.UNDERLINE);
        sb.append(CacheKey.NEW_USER_ARCHIVE_MAP_KEY_PREFIX);
        sb.append(CacheKey.UNDERLINE);
        return sb.toString();
    }

    public static String genNewsNoticeSuccessCountKey(Long id) {
        StringBuilder sb = new StringBuilder();
        sb.append(CacheKey.APP_NAME);
        sb.append(CacheKey.UNDERLINE);
        sb.append(CacheKey.NEWS_NOTICE_SUCCESS_COUNT_KEY_PREFIX);
        sb.append(CacheKey.UNDERLINE);
        return sb.toString();
    }
}