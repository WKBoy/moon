package cn.sedu.moon.common.util;

import java.io.IOException;
import java.util.Map;

import okhttp3.FormBody;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class HttpUtil {

    private static OkHttpClient client = new OkHttpClient.Builder().retryOnConnectionFailure(true).build();


    public static String get(String url) throws IOException {
        Request request = new Request.Builder()
                .url(url)
                .build();
        Response response = client.newCall(request).execute();
        return response.body().string();
    }

    public static String postByForm(String url, Map<String, String> param) throws IOException {
        FormBody.Builder builder = new FormBody.Builder();
        for (Map.Entry<String, String> entry : param.entrySet()) {
            builder.add(entry.getKey(), entry.getValue());
        }
        FormBody body = builder.build();
        Request request = new Request.Builder()
                .url(url)
                .post(body)
                .build();
        Response response = client.newCall(request).execute();
        return response.body().string();
    }


    public static String postByXml(String url, String xml) throws IOException {
        RequestBody body = RequestBody.create(MediaType.parse("application/xml;charset=UTF-8"), xml);
        Request request = new Request.Builder()
                .url(url)
                .post(body)
                .build();
        Response response = client.newCall(request).execute();
        return response.body().string();
    }

    public static String postByJson(String url, String json) throws IOException {
        RequestBody body = RequestBody.create(MediaType.parse("application/json;charset=UTF-8"), json);
        Request request = new Request.Builder()
                .url(url)
                .post(body)
                .build();
        Response response = client.newCall(request).execute();
        return response.body().string();
    }

    public static Response postByJsonReturnResponse(String url, String json) throws IOException {
        RequestBody body = RequestBody.create(MediaType.parse("application/json;charset=UTF-8"), json);
        Request request = new Request.Builder()
                .url(url)
                .post(body)
                .build();
        return client.newCall(request).execute();
    }

    //public static void main(String[] args) throws Exception {
    //    CreatePaymentRequest request = new CreatePaymentRequest();
    //    request.setUserId(300259L);
    //    request.setPayAmount(Long.valueOf(990));
    //    request.setBizId(123L);
    //    request.setBizType(BizConstants.ORDER_CREATE_PAYMENT_BIZTYPE);
    //    request.setPayType(BizConstants.ORDER_CREATE_PAYMENT_BIZTYPE);
    //    System.out.println(JSONUtils.toJSONString(request));
    //    Map<String, String> map = Maps.newHashMap();
    //    map.put("data", JSONUtils.toJSONString(request));
    //    System.out.println(postByForm("http://bdev.dian.so/lhc/1.0/payment/createPayment", map));
    //}
}
