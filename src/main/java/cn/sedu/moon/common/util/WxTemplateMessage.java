/*
 * Dian.com Inc.
 * Copyright (c) 2004-2019 All Rights Reserved.
 */
package cn.sedu.moon.common.util;

import java.io.Serializable;

import lombok.Data;

/**
 *
 * @author ${wukong}
 * @version $Id: WxTemplateMessage.java, v 0.1 2019年04月02日 5:49 PM Exp $
 */
@Data
public class WxTemplateMessage implements Serializable {

    private static final long serialVersionUID = 5300966351169383126L;

}