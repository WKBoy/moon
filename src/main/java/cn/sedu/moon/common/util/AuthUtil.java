/*
 * Dian.com Inc.
 * Copyright (c) 2004-2018 All Rights Reserved.
 */
package cn.sedu.moon.common.util;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.Random;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang3.StringUtils;

/**
 * 授权相关工具类
 *
 * @author ${wukong}
 * @version $Id: AuthUtil.java, v 0.1 2018年01月17日 2:47 PM Exp $
 */
@Slf4j
@SuppressWarnings("unused")
public class AuthUtil {

    private static final String accessTokenSalt = "afj98! #$%#2asdf12&*^&#21413as";
    private static final String CHARSET="UTF-8";

    /**
     * 用户登录后生成token
     *
     * @return token
     */
    public static String generateToken(String userId) throws UnsupportedEncodingException {

        // 生成一个六位的随机数
        Random random = new Random();
        long tmpCode = random.nextInt(899999) + 100000;
        String accessToken = String.valueOf(tmpCode);
        String token = DigestUtils.sha1Hex(accessToken + accessTokenSalt).toUpperCase() + userId;
        return getBase64(token);
    }

    public static String getBase64(String str) throws UnsupportedEncodingException {

        byte[] b = str.getBytes(CHARSET);
        if (b != null) {
            return Base64.encodeBase64String(b);
        }

        return null;
    }

    /**
     * Base64解密
     *
     * @param str
     * @return
     */
    public static String getFromBase64(String str) throws IOException {
        String result = null;
        if (str != null) {
            byte[] b = Base64.decodeBase64(str);
            result = new String(b, CHARSET);
        }
        return result;
    }

    /**
     * 从token 中获取用户Id
     *
     * @param token token
     * @return
     */
    public static String getUserIdFromToken(String token) throws IOException {
        String userId;

        if (StringUtils.isEmpty(token)) {
            return null;
        }

        String tmpString = AuthUtil.getFromBase64(token);

        if (tmpString.length() < 41) {
            return null;
        }

        userId = tmpString.substring(40, tmpString.length());

        return userId;
    }

}