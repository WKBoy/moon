/*
 * Dian.com Inc.
 * Copyright (c) 2004-2019 All Rights Reserved.
 */
package cn.sedu.moon.common.util;


import org.springframework.core.env.Environment;

/**
 * @author wukong
 * @version : ProfileUtil.java, v 0.1 2019年11月18日 10:20 上午 Exp $
 */
public class ProfileUtil {

    public static String getActiveProfiles(Environment env) {

        String[] profiles = env.getActiveProfiles();
        if (profiles.length == 0) {
            return "NULL";
        }

        return profiles[0];
    }
}