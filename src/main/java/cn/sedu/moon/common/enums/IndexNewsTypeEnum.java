/*
 * Dian.com Inc.
 * Copyright (c) 2004-2019 All Rights Reserved.
 */
package cn.sedu.moon.common.enums;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 首页新闻类型的枚举
 *
 * @author ${wukong}
 * @version $Id: IndexNewsTypeEnum.java, v 0.1 2019年01月09日 2:36 PM Exp $
 */
@SuppressWarnings("unused")
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public enum IndexNewsTypeEnum {

    /**
     * 新闻
     */
    NEWS(1, "NEWS", "新闻", "新闻"),

    /**
     * SOP问题
     */
    SOP(2, "SOP", "SOP问题", "SOP问题"),

    /**
     * ASK问题
     */
    ASK(3, "ASK", "ASK问题", "ASK问题"),
    ;

    /**
     * 枚举编码
     */
    @Getter
    private final Integer code;

    /**
     * 英文名
     */
    @Getter
    private final String englishName;

    /**
     * 中文名
     */
    @Getter
    private final String chineseName;

    /**
     * 枚举描述信息
     */
    @Getter
    private final String description;

    /**
     * 根据编码查询枚举。
     *
     * @param code 编码。
     * @return 枚举。
     */
    public static IndexNewsTypeEnum getByCode(Integer code) {
        for (IndexNewsTypeEnum value : IndexNewsTypeEnum.values()) {
            if (code.equals(value.getCode())) {
                return value;
            }
        }
        return null;
    }

    public static IndexNewsTypeEnum getByChineseName(String name) {
        for (IndexNewsTypeEnum value : IndexNewsTypeEnum.values()) {
            if (name.equals(value.getChineseName())) {
                return value;
            }
        }
        return null;
    }

}