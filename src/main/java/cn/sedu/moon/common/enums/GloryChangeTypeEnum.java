/*
 * Dian.com Inc.
 * Copyright (c) 2004-2019 All Rights Reserved.
 */
package cn.sedu.moon.common.enums;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 *
 * @author ${wukong}
 * @version $Id: GloryChangeTypeEnum.java, v 0.1 2019年04月01日 3:49 PM Exp $
 */
@SuppressWarnings("unused")
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public enum GloryChangeTypeEnum {

    ADD(1, "ADD", "增加荣誉值", "增加荣誉值", "增加荣誉值"),

    MINUS(2, "MINUS", "减少荣誉值", "减少荣誉值", "减少荣誉值"),;

    /**
     * 枚举编码
     */
    @Getter
    private final Integer code;

    /**
     * 英文名
     */
    @Getter
    private final String englishName;

    /**
     * 中文名
     */
    @Getter
    private final String chineseName;

    /**
     * 勋章标题，文案
     */
    @Getter
    private final String title;

    /**
     * 枚举描述信息
     */
    @Getter
    private final String description;

    /**
     * 根据编码查询枚举。
     *
     * @param code 编码。
     * @return 枚举。
     */
    public static GloryChangeBizTypeEnum getByCode(Integer code) {
        for (GloryChangeBizTypeEnum value : GloryChangeBizTypeEnum.values()) {
            if (code.equals(value.getCode())) {
                return value;
            }
        }
        return null;
    }

}