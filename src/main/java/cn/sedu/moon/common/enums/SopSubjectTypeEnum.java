/*
 * Dian.com Inc.
 * Copyright (c) 2004-2019 All Rights Reserved.
 */
package cn.sedu.moon.common.enums;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 *
 * @author ${wukong}
 * @version $Id: SopSubjectTypeEnum.java, v 0.1 2019年01月09日 1:38 PM Exp $
 */
@SuppressWarnings("unused")
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public enum SopSubjectTypeEnum {

    /**
     * 单选
     */
    SINGLE_CHOICE(1, "SINGLE_CHOICE", "单选", "单选"),

    /**
     * 多选
     */
    MULTI_CHOICE(2, "MULTI_CHOICE", "多选", "多选"),

    /**
     * 输入文本
     */
    INPUT_TEXT(3, "INPUT_TEXT", "输入文本", "输入文本"),

    /**
     * 单选 + 输入文本
     */
    SINGLE_CHOICE_AND_INPUT_TEXT(4,"SINGLE_CHOICE_AND_INPUT_TEXT","单选及输入文本","单选及输入文本"),

    /**
     * 多选 + 输入文本
     */
    MULTI_CHOICE_AND_INPUT_TEXT(5, "MULTI_CHOICE_AND_INPUT_TEXT", "多选及输入文本", "多选及输入文本"),
    ;

    /**
     * 枚举编码
     */
    @Getter
    private final Integer code;

    /**
     * 英文名
     */
    @Getter
    private final String englishName;

    /**
     * 中文名
     */
    @Getter
    private final String chineseName;

    /**
     * 枚举描述信息
     */
    @Getter
    private final String description;

    /**
     * 根据编码查询枚举。
     *
     * @param code 编码。
     * @return 枚举。
     */
    public static SopSubjectTypeEnum getByCode(Integer code) {
        for (SopSubjectTypeEnum value : SopSubjectTypeEnum.values()) {
            if (code.equals(value.getCode())) {
                return value;
            }
        }
        return null;
    }

    public static SopSubjectTypeEnum getByChineseName(String name) {
        for (SopSubjectTypeEnum value : SopSubjectTypeEnum.values()) {
            if (name.equals(value.getChineseName())) {
                return value;
            }
        }
        return null;
    }
}