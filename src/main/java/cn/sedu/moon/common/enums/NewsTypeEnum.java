/*
 * Dian.com Inc.
 * Copyright (c) 2004-2019 All Rights Reserved.
 */
package cn.sedu.moon.common.enums;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 新闻类型枚举类
 *
 * @author wukong
 * @version : NewsTypeEnum.java, v 0.1 2019年08月06日 17:14 Exp $
 */
@SuppressWarnings("unused")
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public enum NewsTypeEnum {

    /**
     * 普通新闻
     */
    NORMAL(0, 0, 0, 0, "普通新闻"),

    /**
     * 精编文章
     */
    SELECTED(1, 1, 2, 1, "精编文章"),

    /**
     * 资料档案
     */
    DOC(2, 2, 3, 1, "资料档案"),
    ;

    /**
     * 枚举编码
     */
    @Getter
    private final Integer code;

    /**
     * 解锁文章需要分享的次数
     */
    @Getter
    private final Integer shareTotal;

    /**
     * [首次]分享解锁文章的荣誉值奖励
     */
    @Getter
    private final Integer shareGloryReward;

    /**
     * [首次]分享被点击浏览后，原分享者可得的荣誉值奖励
     */
    @Getter
    private final Integer readShareGloryReward;

    /**
     * 枚举描述信息
     */
    @Getter
    private final String description;

    /**
     * 根据编码查询枚举
     *
     * @param code 编码
     * @return 枚举
     */
    public static NewsTypeEnum getByCode(Integer code) {
        for (NewsTypeEnum value : NewsTypeEnum.values()) {
            if (code.equals(value.getCode())) {
                return value;
            }
        }
        return null;
    }

}