/*
 * Dian.com Inc.
 * Copyright (c) 2004-2018 All Rights Reserved.
 */
package cn.sedu.moon.common.enums;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 *
 * @author ${wukong}
 * @version $Id: SchoolTypeEnum.java, v 0.1 2018年12月16日 12:25 PM Exp $
 */
@SuppressWarnings("unused")
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public enum SchoolTypeEnum {

    /**
     * 幼儿园
     */
    YOUERYUAN(1, "幼儿园", "幼儿园", "幼儿园"),

    /**
     * 小学
     */
    XIAOXUE(2, "XIAOXUE", "小学", "小学"),

    /**
     * 中学
     */
    ZHONGXUE(3, "ZHONGXUE", "中学", "中学"),

    /**
     * 职业学校
     */
    ZHIYE(4, "ZHIYE", "职业学校", "职业学校"),

    /**
     * 初中
     */
    CHUZHONG(5,"CHUZHONG","初中","初中"),

    /**
     * 高中
     */
    GAOZHONG(6, "GAOZHONG", "高中", "高中"),
    ;

    /**
     * 枚举编码
     */
    @Getter
    private final Integer code;

    /**
     * 英文名
     */
    @Getter
    private final String englishName;

    /**
     * 中文名
     */
    @Getter
    private final String chineseName;

    /**
     * 枚举描述信息
     */
    @Getter
    private final String description;

    /**
     * 根据编码查询枚举。
     *
     * @param code 编码。
     * @return 枚举。
     */
    public static SchoolTypeEnum getByCode(Integer code) {
        for (SchoolTypeEnum value : SchoolTypeEnum.values()) {
            if (code.equals(value.getCode())) {
                return value;
            }
        }
        return null;
    }

    public static SchoolTypeEnum getByChineseName(String name) {
        for (SchoolTypeEnum value : SchoolTypeEnum.values()) {
            if (name.equals(value.getChineseName())) {
                return value;
            }
        }
        return null;
    }

}