/*
 * Dian.com Inc.
 * Copyright (c) 2004-2019 All Rights Reserved.
 */
package cn.sedu.moon.common.enums;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 勋章类型枚举
 *
 * @author ${wukong}
 * @version $Id: MedalTypeEnum.java, v 0.1 2019年01月26日 5:11 PM Exp $
 */
@SuppressWarnings("unused")
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public enum MedalTypeEnum {

    /**
     * 第一个回答SOP问题的人
     */
    SOP_FIRST_ANSWER(1, "SOP_FIRST_ANSWER", "第一个回答SOP问题的人", "首次作出好回答", "第一个回答SOP问题的人"),
    ;

    /**
     * 枚举编码
     */
    @Getter
    private final Integer code;

    /**
     * 英文名
     */
    @Getter
    private final String englishName;

    /**
     * 中文名
     */
    @Getter
    private final String chineseName;

    /**
     * 勋章标题，文案
     */
    @Getter
    private final String title;

    /**
     * 枚举描述信息
     */
    @Getter
    private final String description;

    /**
     * 根据编码查询枚举。
     *
     * @param code 编码。
     * @return 枚举。
     */
    public static MedalTypeEnum getByCode(Integer code) {
        for (MedalTypeEnum value : MedalTypeEnum.values()) {
            if (code.equals(value.getCode())) {
                return value;
            }
        }
        return null;
    }
}