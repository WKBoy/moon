/*
 * Dian.com Inc.
 * Copyright (c) 2004-2017 All Rights Reserved.
 */
package cn.sedu.moon.common.enums;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 错误码
 *
 * <p><b>平台内部唯一</b></p>
 *
 * <p>
 * code对应统一错误码14~16位; errorLevel对应统一错误码的第4位（错误级别）; type对应于统一错误码的第5位（错误类型）。
 * </p>
 *
 * 内部错误码的code取值区间暂定如下：
 *  <ul>
 *      <li>未知异常[000]</li>
 *      <li>系统异常[001]</li>
 *      <li>RPC异常[010~099]</li>
 *      <li>请求参数校验异常[100-149]</li>
 *      <li>程序控制异常[150-199]</li>
 *      <li>数据库操作异常[200-299]</li>
 *      <li>业务规则异常[300-999]</li>
 *  </ul>
 * </p>z
 *
 * @author ${wukong}
 * @version $Id: ErrorCodeEnum.java, v 0.1 2017年07月04日 上午11:58 Exp $
 */
@SuppressWarnings("unused")
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public enum ErrorCodeEnum {
    /** 未知异常 */
    UNKNOWN_EXP("000", "UNKNOWN_EXP", "未知异常", "未知异常"),

    /** 系统异常 */
    SYS_EXP("001", "SYS_EXP", "系统异常", "系统异常"),
    UPLOAD_EXP("002", "UPLOAD_EXP", "上传文件异常", "上传文件异常"),

    /** 幂等控制异常、重复操作 */
    IDEMPOTENT_CONTROL_EXP("100", "IDEMPOTENT_CONTROL_EXP", "不要重复操作哦~", "不要重复操作哦~"),

    /** 参数校验异常 */
    PARAM_CHECK_EXP("101", "PARAM_CHECK_EXP", "参数校验异常", "参数校验异常"),

    /** 发送短信验证码失败 */
    SMS_CODE_SEND_ERROR("300", "SMS_CODE_SEND_ERROR", "发送短信验证码失败", "发送短信验证码失败"),

    /** 短信验证码发送太频繁 */
    SMS_CODE_SEND_TOO_FREQUENT("301", "SMS_CODE_SEND_TOO_FREQUENT", "短信验证码发送太频繁", "短信验证码发送太频繁"),

    /** 短信验证码校验失败 */
    SMS_CODE_CHECK_FAIL("302", "SMS_CODE_CHECK_FAIL", "短信验证码校验失败", "短信验证码校验失败"),

    /** 用户不存在 */
    USER_NOT_EXIST("303",  "USER_NOT_EXIST", "用户不存在", "用户不存在"),

    /** SOP问题不存在 */
    SOP_SUBJECT_NOT_EXIST("304", "SOP_SUBJECT_NOT_EXIST", "SOP问题不存在", "SOP问题不存在"),

    /** 用户未填写基本信息 */
    USER_NOT_ARCHIVE("305", "USER_NOT_ARCHIVE", "用户未填写基本信息", "用户未填写基本信息"),


    /** 用户已经订阅 */
    SUBSCRIBER_ERROR("600", "SUBSCRIBER_ERROR", "用户订阅操作失败", "用户(订阅)操作失败"),
    SUBSCRIBER_EXIST("601", "SUBSCRIBER_EXIST", "用户已经订阅", "用户已经订阅"),
    SUBSCRIBER_NOT_EXIST("602", "SUBSCRIBER_NOT_EXIST", "用户还未订阅", "用户还未订阅"),
    SUBSCRIBER_SUBSCRIBE_ERROR("603", "SUBSCRIBER_SUBSCRIBE_ERROR", "用户订阅操作失败", "用户订阅失败"),
    SUBSCRIBER_UNSUBSCRIBE_ERROR("604", "SUBSCRIBER_UNSUBSCRIBE_ERROR", "用户取消订阅操作失败", "用户取消订阅操作失败"),

    ;

    /** 错误编码  */
    @Getter
    private final String code;
    /** 错误英文名 */
    @Getter
    private final String englishName;
    /** 错误中文名 */
    @Getter
    private final String chineseName;
    /** 错误描述说明 */
    @Getter
    private final String description;

    /**
     * 根据编码查询枚举。
     *
     * @param code 编码。
     * @return 枚举。
     */
    public static ErrorCodeEnum getByCode(String code) {
        for (ErrorCodeEnum value : ErrorCodeEnum.values()) {
            if (code.equals(value.getCode())) {
                return value;
            }
        }

        return null;
    }
}