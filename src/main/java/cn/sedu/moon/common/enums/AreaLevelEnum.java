/*
 * Dian.com Inc.
 * Copyright (c) 2004-2019 All Rights Reserved.
 */
package cn.sedu.moon.common.enums;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 *
 *
 * @author wukong
 * @version : AreaLevelEnum.java, v 0.1 2019年08月25日 14:53 Exp $
 */
@SuppressWarnings("unused")
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public enum AreaLevelEnum {

    /**
     * 省
     */
    PROVINCE(1, "省"),

    /**
     * 市
     */
    CITY(2, "市"),

    /**
     * 区
     */
    AREA(3, "区"),
    ;

    /**
     * 枚举编码
     */
    @Getter
    private final Integer code;

    /**
     * 枚举描述信息
     */
    @Getter
    private final String description;

    /**
     * 根据编码查询枚举。
     *
     * @param code 编码。
     * @return 枚举。
     */
    public static AreaLevelEnum getByCode(Integer code) {
        for (AreaLevelEnum value : AreaLevelEnum.values()) {
            if (code.equals(value.getCode())) {
                return value;
            }
        }
        return null;
    }

}