/*
 * Dian.com Inc.
 * Copyright (c) 2004-2019 All Rights Reserved.
 */
package cn.sedu.moon.common.enums;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 *
 * @author ${wukong}
 * @version $Id: GloryChangeBizTypeEnum.java, v 0.1 2019年04月01日 3:37 PM Exp $
 */
@SuppressWarnings("unused")
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public enum GloryChangeBizTypeEnum {

    /**
     * 回答SOP问题获取荣誉值
     */
    SOP(1, "SOP", "回答SOP问题获取荣誉值", "回答SOP问题获取荣誉值", "回答SOP问题获取荣誉值"),

    /**
     * [首次]分享解锁文章的荣誉值奖励
     */
    SHARE_UNLOCK_NEWS(2, "SHARE_UNLOCK_NEWS", "[首次]分享解锁文章的荣誉值奖励", "[首次]分享解锁文章的荣誉值奖励", "[首次]分享解锁文章的荣誉值奖励"),

    /**
     * [首次]分享被点击浏览后，原分享者可得的荣誉值奖励
     */
    READ_SHARE(3, "READ_SHARE", "[首次]分享被点击浏览后，原分享者可得的荣誉值奖励", "[首次]分享被点击浏览后，原分享者可得的荣誉值奖励", "[首次]分享被点击浏览后，原分享者可得的荣誉值奖励"),


    ;

    /**
     * 枚举编码
     */
    @Getter
    private final Integer code;

    /**
     * 英文名
     */
    @Getter
    private final String englishName;

    /**
     * 中文名
     */
    @Getter
    private final String chineseName;

    /**
     * 勋章标题，文案
     */
    @Getter
    private final String title;

    /**
     * 枚举描述信息
     */
    @Getter
    private final String description;

    /**
     * 根据编码查询枚举。
     *
     * @param code 编码。
     * @return 枚举。
     */
    public static GloryChangeBizTypeEnum getByCode(Integer code) {
        for (GloryChangeBizTypeEnum value : GloryChangeBizTypeEnum.values()) {
            if (code.equals(value.getCode())) {
                return value;
            }
        }
        return null;
    }

}