package cn.sedu.moon.common.enums;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor(access = AccessLevel.PRIVATE)
public enum SubscribeStatusEnum {


    /**
     * 未订阅
     */
    UNSUBSCRIBE(0, "UNSUBSCRIBE", "未订阅", "未订阅", "未订阅"),
    /**
     * 已订阅
     */
    SUBSCRIBED(1, "SUBSCRIBED", "已订阅", "已订阅", "已订阅"),

    ;

    /**
     * 枚举编码
     */
    @Getter
    private final Integer code;

    /**
     * 英文名
     */
    @Getter
    private final String englishName;

    /**
     * 中文名
     */
    @Getter
    private final String chineseName;

    /**
     * 勋章标题，文案
     */
    @Getter
    private final String title;

    /**
     * 枚举描述信息
     */
    @Getter
    private final String description;

    /**
     * 根据编码查询枚举。
     *
     * @param code 编码。
     * @return 枚举。
     */
    public static SubscribeStatusEnum getByCode(Integer code) {
        for (SubscribeStatusEnum value : SubscribeStatusEnum.values()) {
            if (code.equals(value.getCode())) {
                return value;
            }
        }
        return null;
    }
}
