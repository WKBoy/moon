/*
 * Dian.com Inc.
 * Copyright (c) 2004-2019 All Rights Reserved.
 */
package cn.sedu.moon.common.enums;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author wukong
 * @version : GdSourceTypeEnum.java, v 0.1 2019年11月19日 10:45 上午 Exp $
 */
@SuppressWarnings("unused")
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public enum  GdSourceTypeEnum {

    /**
     * 幼儿园
     */
    YOUERYUAN(1, "幼儿园"),

    /**
     * 小学
     */
    XIAOXUE(2,"小学"),

    /**
     * 初中
     */
    CHUZHONG(3,"初中"),

    /**
     * 高中
     */
    GAOZHONG(4,"高中"),

    /**
     * 小区
     */
    XIAOQU(5,"小区"),
    ;

    /**
     * 枚举编码
     */
    @Getter
    private final Integer code;

    /**
     * 枚举描述信息
     */
    @Getter
    private final String description;

    /**
     * 根据编码查询枚举。
     *
     * @param code 编码。
     * @return 枚举。
     */
    public static GdSourceTypeEnum getByCode(Integer code) {
        for (GdSourceTypeEnum value : GdSourceTypeEnum.values()) {
            if (code.equals(value.getCode())) {
                return value;
            }
        }
        return null;
    }
}