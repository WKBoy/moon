/*
 * Dian.com Inc.
 * Copyright (c) 2004-2019 All Rights Reserved.
 */
package cn.sedu.moon.common.enums;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 *
 * @author ${wukong}
 * @version $Id: ShareStatusEnum.java, v 0.1 2019年04月14日 4:42 PM Exp $
 */
@SuppressWarnings("unused")
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public enum ShareStatusEnum {

    /**
     * 已创建
     */
    CREATED(1, "CREATED", "已创建", "已创建"),

    /**
     * 已分享
     */
    SHARED(2, "SHARED", "已分享", "已分享"),
    ;

    /**
     * 枚举编码
     */
    @Getter
    private final Integer code;

    /**
     * 英文名
     */
    @Getter
    private final String englishName;

    /**
     * 中文名
     */
    @Getter
    private final String chineseName;

    /**
     * 枚举描述信息
     */
    @Getter
    private final String description;

    /**
     * 根据编码查询枚举。
     *
     * @param code 编码。
     * @return 枚举。
     */
    public static ShareStatusEnum getByCode(Integer code) {
        for (ShareStatusEnum value : ShareStatusEnum.values()) {
            if (code.equals(value.getCode())) {
                return value;
            }
        }
        return null;
    }

    public static ShareStatusEnum getByChineseName(String name) {
        for (ShareStatusEnum value : ShareStatusEnum.values()) {
            if (name.equals(value.getChineseName())) {
                return value;
            }
        }
        return null;
    }
}