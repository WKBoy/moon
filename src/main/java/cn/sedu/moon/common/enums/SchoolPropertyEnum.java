/*
 * Dian.com Inc.
 * Copyright (c) 2004-2018 All Rights Reserved.
 */
package cn.sedu.moon.common.enums;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 学校属性(公办、民办)
 *
 * @author ${wukong}
 * @version $Id: SchoolPropertyEnum.java, v 0.1 2018年12月16日 1:46 PM Exp $
 */
@SuppressWarnings("unused")
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public enum SchoolPropertyEnum {

    /**
     * 公办
     */
    GONGBAN(1, "GONGBAN", "公办", "公办"),

    /**
     * 民办
     */
    MINBAN(2, "XIAOXUE", "民办", "民办")
    ;

    /**
     * 枚举编码
     */
    @Getter
    private final Integer code;

    /**
     * 英文名
     */
    @Getter
    private final String englishName;

    /**
     * 中文名
     */
    @Getter
    private final String chineseName;

    /**
     * 枚举描述信息
     */
    @Getter
    private final String description;

    /**
     * 根据编码查询枚举。
     *
     * @param code 编码。
     * @return 枚举。
     */
    public static SchoolPropertyEnum getByCode(Integer code) {
        for (SchoolPropertyEnum value : SchoolPropertyEnum.values()) {
            if (code.equals(value.getCode())) {
                return value;
            }
        }
        return null;
    }

    public static SchoolPropertyEnum getByChineseName(String name) {
        for (SchoolPropertyEnum value : SchoolPropertyEnum.values()) {
            if (name.equals(value.getChineseName())) {
                return value;
            }
        }
        return null;
    }

}