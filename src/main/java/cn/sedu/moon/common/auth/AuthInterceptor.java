/*
 * Dian.com Inc.
 * Copyright (c) 2004-2018 All Rights Reserved.
 */
package cn.sedu.moon.common.auth;

import cn.sedu.moon.common.constant.BizConstants;
import cn.sedu.moon.common.constant.SystemConstants;
import cn.sedu.moon.common.util.AuthUtil;
import cn.sedu.moon.common.util.CacheKeyUtil;
import cn.sedu.moon.common.util.JsonUtil;
import cn.sedu.moon.common.util.ProfileUtil;
import cn.sedu.moon.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.redisson.api.RBucket;
import org.redisson.api.RedissonClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

/**
 * 消费者端用户登录身份验证拦截器
 *
 * @author ${wukong}
 * @version $Id: AuthInterceptor.java, v 0.1 2018年10月23日 5:53 PM Exp $
 */
@Slf4j
public class AuthInterceptor implements HandlerInterceptor {

    @Autowired
    private UserService userService;

    @Autowired
    private RedissonClient redissonClient;

    @Autowired
    private Environment environment;

    @Override
    public boolean preHandle(final HttpServletRequest request, final HttpServletResponse response, final Object handler) throws Exception {

        HandlerMethod method = (HandlerMethod) handler;
        Auth auth = method.getMethod().getAnnotation(Auth.class);

        if (auth == null) {

            CmsAuth cmsAuth = method.getMethod().getAnnotation(CmsAuth.class);
            if (cmsAuth == null) {
                return true;
            }

            // 获取token
            String token = request.getHeader(BizConstants.XD_USER_TOKEN);
            if (!checkCmsToken(token)) {
                response.setCharacterEncoding("utf-8");
                response.setContentType("application/json; charset=utf-8");

                PrintWriter out = response.getWriter();
                Map<String, Object> resMap = new HashMap<>(5);
                resMap.put("success", false);
                resMap.put("code", 206);
                resMap.put("msg", "token过期，请重新登陆");
                out.print(JsonUtil.beanToJson(resMap));
                out.flush();
                out.close();
                return false;
            }

            // 传递用户ID
            String userId = AuthUtil.getUserIdFromToken(token);
            request.setAttribute(BizConstants.USER_ID, userId);
            return true;
        }

        // 获取token
        String token = request.getHeader(BizConstants.XD_USER_TOKEN);

        if (!checkToken(token)) {
            response.setCharacterEncoding("utf-8");
            response.setContentType("application/json; charset=utf-8");

            PrintWriter out = response.getWriter();
            Map<String, Object> resMap= new HashMap<>(5);
            resMap.put("success", false);
            resMap.put("code", 206);
            resMap.put("msg", "token过期，请重新登陆");
            out.print(JsonUtil.beanToJson(resMap));
            out.flush();
            out.close();
            return false;
        }

        // 传递用户ID
        String userId = AuthUtil.getUserIdFromToken(token);
        request.setAttribute(BizConstants.USER_ID,  userId);
        return true;
    }

    private Boolean checkToken(String token) throws IOException {

        if (StringUtils.isBlank(token)) {
            return false;
        }

        String userIdString = AuthUtil.getUserIdFromToken(token);
        if (StringUtils.isBlank(userIdString)) {
            return false;
        }

        // 如果是local环境，不进行token校验
        String profileName = ProfileUtil.getActiveProfiles(environment);
        if (SystemConstants.SPRING_PROFILE_LOCAL.equals(profileName)) {
            return true;
        }

        Long userId = Long.parseLong(userIdString);
        String cacheKey = CacheKeyUtil.genUserTokenKey(userId);
        RBucket<String> bucket = redissonClient.getBucket(cacheKey);
        if (bucket == null) {
            return false;
        }

        String cacheToken = bucket.get();
        return token.equals(cacheToken);

    }

    private Boolean checkCmsToken(String token) throws IOException {

        if (StringUtils.isBlank(token)) {
            return false;
        }

        String userIdString = AuthUtil.getUserIdFromToken(token);
        if (StringUtils.isBlank(userIdString)) {
            return false;
        }

        Long userId = Long.parseLong(userIdString);
        String cacheKey = CacheKeyUtil.genUserCmsTokenKey(userId);
        RBucket<String> bucket = redissonClient.getBucket(cacheKey);
        if (bucket == null) {
            return false;
        }

        String cacheToken = bucket.get();
        return token.equals(cacheToken);

    }


    @Override
    public void postHandle(final HttpServletRequest request, final HttpServletResponse response, final Object handler,
                           final ModelAndView modelAndView)
            throws Exception {

    }

    @Override
    public void afterCompletion(final HttpServletRequest request, final HttpServletResponse response, final Object handler,
                                final Exception ex) throws Exception {

    }
}