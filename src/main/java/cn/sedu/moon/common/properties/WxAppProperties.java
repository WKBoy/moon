/*
 * Dian.com Inc.
 * Copyright (c) 2004-2019 All Rights Reserved.
 */
package cn.sedu.moon.common.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * 微信小程序参数
 *
 * @author wukong
 * @version : WxAppProperties.java, v 0.1 2019年09月03日 13:50 Exp $
 */
@Data
@Component
@ConfigurationProperties(prefix = "weixin.app")
public class WxAppProperties {

    /**
     * 微信小程序应用id
     */
    private String appId;

    /**
     * 微信小程序应用的secret
     */
    private String secret;
}