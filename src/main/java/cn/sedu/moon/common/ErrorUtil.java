/*
 * Dian.com Inc.
 * Copyright (c) 2004-2018 All Rights Reserved.
 */
package cn.sedu.moon.common;

import cn.sedu.moon.common.exception.BizException;

/**
 * 错误信息构建工具类
 *
 * @author ${baizhang}
 * @version $Id: ErrorUtil.java, v 0.1 2018-04-16 上午11:25 Exp $
 */
@SuppressWarnings("unused")
public class ErrorUtil {

    /**
     * 构建业务处理失败异常返回对象
     *
     * @param result                Result
     * @param e                     异常信息
     * @return Result对象
     */
    public static Result buildBizProcess(Result result, BizException e) {
        result.setSuccess(false);
        result.setMsg(e.getMessage());
        result.setCode(e.getErrorCode().getCode());
        return result;
    }
}
