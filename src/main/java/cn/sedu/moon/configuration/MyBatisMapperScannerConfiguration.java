/*
 * Dian.com Inc.
 * Copyright (c) 2004-2017 All Rights Reserved.
 */
package cn.sedu.moon.configuration;

import org.mybatis.spring.mapper.MapperScannerConfigurer;
import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * mybatis扫描接口
 *
 * @author ${baizhang}
 * @version $Id: MapperScannerConfiguration.java, v 0.1 2017-11-27 下午7:07 Exp $
 */
@Configuration
//初始化之前，先初始化MyBatisConfiguration
@AutoConfigureAfter(MyBatisConfiguration.class)
public class MyBatisMapperScannerConfiguration {
    @Bean
    public MapperScannerConfigurer mapperScannerConfiguration() {
        MapperScannerConfigurer mapper = new MapperScannerConfigurer();
        mapper.setSqlSessionFactoryBeanName("sqlSessionFactory");
        mapper.setBasePackage("cn.sedu.moon.dao");
        return mapper;
    }
}