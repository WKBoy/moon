/*
 * Dian.so Inc.
 * Copyright (c) 2004-2017 All Rights Reserved.
 */
package cn.sedu.moon.configuration;

import java.sql.SQLException;

import com.alibaba.druid.pool.DruidDataSource;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

/**
 * Druid数据库连接池配置初始化
 *
 * @author ${baizhang}
 * @version $Id: MyBatisConfiguration.java, v 0.1 2017-11-27 下午6:52 Exp $
 */
@Configuration
public class DruidConfiguration {

    private static final String MYSQL_PREFIX = "druid.datasource.mysql";

    /**
     * Mysql数据连接池初始化
     */
    @Getter
    @Setter
    @ConfigurationProperties(prefix = MYSQL_PREFIX)
    @SuppressWarnings("unused")
    class MysqlDataSourceProperties {
        private String  url;
        private String  userName;
        private String  password;
        private String  driverClassName;
        private Integer initialSize;
        private Integer minIdle;
        private Integer maxActive;
        private Integer maxWait;
        private Integer timeBetweenEvictionRunsMillis;
        private Integer minEvictableIdleTimeMillis;
        private String  validationQuery;
        private Boolean testWhileIdle;
        private Boolean testOnBorrow;
        private Boolean testOnReturn;
        private Boolean poolPreparedStatements;
        private Integer maxPoolPreparedStatementPerConnectionSize;
        private String  filters;
        private String  connectionProperties;

        /**
         * Bean     //声明其为Bean实例
         * Primary  //在同样的DataSource中，首先使用被标注的DataSource
         * @return DataSource
         */
        @Bean(name = "mysqlDataSource", initMethod = "init", destroyMethod = "close")
        @Primary
        public DruidDataSource dataSource() {
            DruidDataSource datasource = new DruidDataSource();
            datasource.setUrl(url);
            datasource.setUsername(userName);
            datasource.setPassword(password);
            datasource.setDriverClassName(driverClassName);

            //configuration
            datasource.setInitialSize(initialSize);
            datasource.setMinIdle(minIdle);
            datasource.setMaxActive(maxActive);
            datasource.setMaxWait(maxWait);
            datasource.setTimeBetweenEvictionRunsMillis(timeBetweenEvictionRunsMillis);
            datasource.setMinEvictableIdleTimeMillis(minEvictableIdleTimeMillis);
            datasource.setValidationQuery(validationQuery);
            datasource.setTestWhileIdle(testWhileIdle);
            datasource.setTestOnBorrow(testOnBorrow);
            datasource.setTestOnReturn(testOnReturn);
            datasource.setPoolPreparedStatements(poolPreparedStatements);
            datasource.setMaxPoolPreparedStatementPerConnectionSize(maxPoolPreparedStatementPerConnectionSize);
            try {
                datasource.setFilters(filters);
            } catch (SQLException e) {
                e.printStackTrace();
            }
            datasource.setConnectionProperties(connectionProperties);
            return datasource;
        }

    }

}