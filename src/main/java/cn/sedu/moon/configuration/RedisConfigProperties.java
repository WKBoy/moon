/*
 * Dian.com Inc.
 * Copyright (c) 2004-2018 All Rights Reserved.
 */
package cn.sedu.moon.configuration;

import javax.validation.constraints.NotNull;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * redis 配置属性类
 *
 * @author ${baizhang}
 * @version $Id: RedisConfigProperties.java, v 0.1 2018-04-19 下午3:17 Exp $
 */
@Data
@ConfigurationProperties(prefix = RedisConfigProperties.MOFA_CACHE_REDIS_PREFIX)
public class RedisConfigProperties {
    public static final String MOFA_CACHE_REDIS_PREFIX = "cache.redis";

    /**
     * redis地址：redis://域名:端口
     */
    @NotNull
    private String address;

    /**
     * 密码
     */
    private String password;

    /**
     * key过期时间（单位：天）
     */
    @NotNull
    private Integer expireTime;

    /**
     * 连接池对象
     */
    @NotNull
    private PoolConfig poolConfig;

    /**
     * 连接池配置
     */
    @Data
    public static class PoolConfig {
        /**
         * 最小空闲连接数
         */
        @NotNull
        private Integer minIdle;

        /**
         * 连接池大小
         */
        @NotNull
        private Integer poolSize;

        /**
         * 连接超时时间
         */
        @NotNull
        private Integer connectTimeout;

        /**
         * 命令等待超时时间，从命令发送成功开始计时
         */
        @NotNull
        private Integer maxWaitTimeout;

        /**
         * 命令失败重试次数
         */
        @NotNull
        private Integer retryAttempts;

        /**
         * 命令失败重试时间间隔
         */
        @NotNull
        private Integer retryInterval;

    }
}
