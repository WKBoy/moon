/*
 * Dian.com Inc.
 * Copyright (c) 2004-2018 All Rights Reserved.
 */
package cn.sedu.moon.configuration;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.redisson.Redisson;
import org.redisson.api.RedissonClient;
import org.redisson.config.Config;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * redisson config
 *
 * @author ${baizhang}
 * @version $Id: RedissonConfiguration.java, v 0.1 2018-07-23 下午3:16 Exp $
 */
@Slf4j
@Configuration
@EnableConfigurationProperties(value = RedisConfigProperties.class)
@ConditionalOnProperty(prefix = "cache.redis.", name = {"address", "password"})
public class RedissonConfiguration {
    private final RedisConfigProperties redisConfigProperties;

    @Autowired
    public RedissonConfiguration(final RedisConfigProperties redisConfigProperties) {this.redisConfigProperties = redisConfigProperties;}

    public Config config() {
        Config config = new Config();
        // 单节点模式
        config.useSingleServer().setAddress(redisConfigProperties.getAddress());
        if (StringUtils.isNotBlank(redisConfigProperties.getPassword())) {
            // 密码
            config.useSingleServer().setPassword(redisConfigProperties.getPassword());
        }

        // 最大允许重试次数
        config.useSingleServer().setRetryAttempts(redisConfigProperties.getPoolConfig().getRetryAttempts());
        // 重试间隔时间
        config.useSingleServer().setRetryInterval(redisConfigProperties.getPoolConfig().getRetryInterval());
        // 尝试成功，数据等待超时时间
        config.useSingleServer().setTimeout(redisConfigProperties.getPoolConfig().getMaxWaitTimeout());
        // 连接超时时间
        config.useSingleServer().setConnectTimeout(redisConfigProperties.getPoolConfig().getConnectTimeout());
        // 连接池大小
        config.useSingleServer().setConnectionPoolSize(redisConfigProperties.getPoolConfig().getPoolSize());
        // 最小连接数
        config.useSingleServer().setConnectionMinimumIdleSize(redisConfigProperties.getPoolConfig().getMinIdle());

        return config;
    }

    @Bean
    public RedissonClient redissonClient() {
        return Redisson.create(config());
    }

}