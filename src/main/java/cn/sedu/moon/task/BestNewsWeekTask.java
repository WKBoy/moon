package cn.sedu.moon.task;

import cn.sedu.moon.common.cache.CacheKey;
import cn.sedu.moon.common.constant.BizConstants;
import cn.sedu.moon.service.MessagePushService;
import cn.sedu.moon.service.NewsService;
import lombok.extern.slf4j.Slf4j;
import org.redisson.api.RLock;
import org.redisson.api.RedissonClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.concurrent.TimeUnit;

/**
 * 每周精选任务
 *
 * @author guanzhong
 * @version : BestNewsWeekTask.java, v 0.1 2019年11月13日00:47:19
 */
@Component
@Slf4j
public class BestNewsWeekTask {


    @Resource
    private RedissonClient redissonClient;

    @Autowired
    private MessagePushService messagePushService;

    /**
     * 定时推送每周精选
     *
     * 表moon_news_detail、moon_news_notice_subscribe是针对“答疑专场”所设计，考虑到后面两个模块的差异性，也与悟空讨论，建议是设计新的表；
     * 同时由于时间推送相关比较固定，不使用数据库定时任务配置，直接通过代码注解方式
     * 每周六早上10点推送：0 0 10 ? * 6
     */
    @Scheduled(cron = "0 13 18 ? * 5")
    public void pushBestNewsWeekMessage() {
        RLock lock = redissonClient.getLock(CacheKey.LOCK_TASK_STICK_NEWS);
        try {
            if (lock.tryLock(BizConstants.REDIS_LOCK_WAIT_SECONDS, BizConstants.REDIS_LOCK_RELEASE_SECONDS, TimeUnit.SECONDS)) {

                log.info("pushBestNewsWeekMessage invoke");
                // 主要业务逻辑处理
                messagePushService.pushBestNewsWeekMessage();
            }

        } catch (Exception e) {
            log.error("pushBestNewsWeekMessage error: {}", e.getMessage());
        } finally {
            lock.unlock();
        }
    }

}
