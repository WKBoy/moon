package cn.sedu.moon.task;

import cn.sedu.moon.common.cache.CacheKey;
import cn.sedu.moon.common.constant.BizConstants;
import cn.sedu.moon.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.redisson.api.RLock;
import org.redisson.api.RedissonClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.concurrent.TimeUnit;

/**
 * @author lix
 * @version 1.0
 * @date 2019/12/14 20:07
 * @describe
 */
@Component
@Slf4j
public class NotArchiveTask {

    @Resource
    private RedissonClient redissonClient;

    @Autowired
    private UserService userService;

    @Scheduled(cron = "0 0/10 * * * ?")
    public void notArchivePush() {
        RLock lock = redissonClient.getLock(CacheKey.NEW_USER_NOT_ARCHIVE_MAP_KEY_PREFIX);
        try {
            if (lock.tryLock(BizConstants.REDIS_LOCK_WAIT_SECONDS, BizConstants.REDIS_LOCK_RELEASE_SECONDS, TimeUnit.SECONDS)) {
                // 推送一小时还未注册完成的用户
                userService.pushOneHourNotArchiveUser();
            }
        } catch (Exception e) {
            log.error("tickLastDayNews error: {}", e.getMessage());
        } finally {
            lock.unlock();
        }
    }
}
