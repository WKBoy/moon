/*
 * Dian.com Inc.
 * Copyright (c) 2004-2019 All Rights Reserved.
 */
package cn.sedu.moon.task;

import cn.sedu.moon.common.cache.CacheKey;
import cn.sedu.moon.common.constant.BizConstants;
import cn.sedu.moon.service.NewsService;
import lombok.extern.slf4j.Slf4j;
import org.redisson.api.RLock;
import org.redisson.api.RedissonClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.concurrent.TimeUnit;

/**
 * @author wukong
 * @version : NewsTask.java, v 0.1 2019年10月12日 11:24 AM Exp $
 */
@Component
@Slf4j
public class NewsTask {

    @Resource
    private RedissonClient redissonClient;

    @Autowired
    private NewsService newsService;

    @Scheduled(cron = "0 0 0 * * ?")
    public void tickLastDayNews() {
        RLock lock = redissonClient.getLock(CacheKey.LOCK_TASK_STICK_NEWS);
        try {
            if (lock.tryLock(BizConstants.REDIS_LOCK_WAIT_SECONDS, BizConstants.REDIS_LOCK_RELEASE_SECONDS, TimeUnit.SECONDS)) {
                // 置顶昨天创建的新的文章
                newsService.stickLastDayCreatedNews();
            }

        } catch (Exception e) {
            log.error("tickLastDayNews error: {}", e.getMessage());
        } finally {
            lock.unlock();
        }
    }

}