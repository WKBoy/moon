package cn.sedu.moon.task;

import cn.sedu.moon.common.cache.CacheKey;
import cn.sedu.moon.common.constant.BizConstants;
import cn.sedu.moon.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.redisson.api.RLock;
import org.redisson.api.RedissonClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.concurrent.TimeUnit;

/**
 * @author lix
 * @version 1.0
 * @date 2019/12/15 0:25
 * @describe
 */
@Component
@Slf4j
public class ArchiveNotReadNewsTask {
    @Resource
    private RedissonClient redissonClient;

    @Autowired
    private UserService userService;

    @Scheduled(cron = "0 0/10 * * * ?")
    public void notArchivePush() {
        RLock lock = redissonClient.getLock(CacheKey.NEW_USER_ARCHIVE_MAP_KEY_PREFIX);
        try {
            if (lock.tryLock(BizConstants.REDIS_LOCK_WAIT_SECONDS, BizConstants.REDIS_LOCK_RELEASE_SECONDS, TimeUnit.SECONDS)) {
                // 推送注册完成一小时还未阅读过新闻的用户
                userService.pushOneHourArchiveUserNotReadNews();
            }
        } catch (Exception e) {
            log.error("tickLastDayNews error: {}", e.getMessage());
        } finally {
            lock.unlock();
        }
    }
}
