/*
 * Dian.com Inc.
 * Copyright (c) 2004-2019 All Rights Reserved.
 */
package cn.sedu.moon.task;

import cn.sedu.moon.common.cache.CacheKey;
import cn.sedu.moon.common.constant.BizConstants;
import cn.sedu.moon.dao.NewsNoticeDAO;
import cn.sedu.moon.dao.model.NewsNotice;
import cn.sedu.moon.service.MessageTaskService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.redisson.api.RLock;
import org.redisson.api.RedissonClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * @author wukong
 * @version : NewNoticePushTask.java, v 0.1 2019年12月08日 4:16 下午 Exp $
 */
@Slf4j
@Component
public class NewNoticePushTask {

    @Resource
    private RedissonClient redissonClient;

    @Autowired
    private NewsNoticeDAO newsNoticeDAO;

    @Autowired
    private MessageTaskService messageTaskService;

    @Scheduled(cron = "0 0/10 * * * ?")
    public void pushNewsNotice() {
        RLock lock = redissonClient.getLock(CacheKey.LOCK_TASK_NEWS_NOTICE_PUSH);
        try {
            if (lock.tryLock(BizConstants.REDIS_LOCK_WAIT_SECONDS, BizConstants.REDIS_LOCK_RELEASE_SECONDS, TimeUnit.SECONDS)) {
                log.info("pushNewsNotice start");

                List<NewsNotice> newsNoticeList = newsNoticeDAO.selectWaitPush();
                if (CollectionUtils.isNotEmpty(newsNoticeList)) {
                    for (NewsNotice newsNotice : newsNoticeList) {
                        // 更新状态
                        NewsNotice updateModel = new NewsNotice();
                        updateModel.setId(newsNotice.getId());
                        updateModel.setPush(1);
                        updateModel.setUpdateTime(new Date());

                        newsNoticeDAO.updateRecord(updateModel);

                        // 发送微信服务通知
                        messageTaskService.pushNewsNoticeMessage(newsNotice);

                        log.info("pushNewsNotice end");
                    }
                }
            }

        } catch (Exception e) {
            log.error("pushNewsNotice error: {}", e.getMessage());
        } finally {
            lock.unlock();
        }
    }

}