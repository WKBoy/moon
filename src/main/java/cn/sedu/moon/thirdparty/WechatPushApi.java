/*
 * Dian.com Inc.
 * Copyright (c) 2004-2019 All Rights Reserved.
 */
package cn.sedu.moon.thirdparty;

import cn.sedu.moon.thirdparty.request.WxNotifyMessagePushRequest;
import cn.sedu.moon.thirdparty.response.WxNotifyMessagePushResponse;

/**
 * 微信推送的接口
 *
 * @author ${wukong}
 * @version $Id: WechatPushApi.java, v 0.1 2019年04月17日 3:45 PM Exp $
 */
public interface WechatPushApi {

    /**
     * 发送微信服务通知
     *
     * @param accessToken 接口调用凭证, request 请求体
     * @return response
     */
    WxNotifyMessagePushResponse sendNotifyPushMessage(String accessToken, WxNotifyMessagePushRequest request);
}