/*
 * Dian.com Inc.
 * Copyright (c) 2004-2019 All Rights Reserved.
 */
package cn.sedu.moon.thirdparty.impl;

import com.alibaba.fastjson.JSON;

import cn.sedu.moon.common.enums.ErrorCodeEnum;
import cn.sedu.moon.common.exception.BizException;
import cn.sedu.moon.common.util.HttpUtil;
import cn.sedu.moon.thirdparty.WechatPushApi;
import cn.sedu.moon.thirdparty.request.WxNotifyMessagePushRequest;
import cn.sedu.moon.thirdparty.response.WxNotifyMessagePushResponse;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Service;

/**
 *
 * @author ${wukong}
 * @version $Id: WechatPushApiImpl.java, v 0.1 2019年04月22日 4:46 PM Exp $
 */
@Service
@Slf4j
public class WechatPushApiImpl implements WechatPushApi {

    private static final String WX_PUSH_URL = "https://api.weixin.qq.com/cgi-bin/message/wxopen/template/send?access_token=";

    @Override
    public WxNotifyMessagePushResponse sendNotifyPushMessage(final String accessToken, final WxNotifyMessagePushRequest request) {

        String paramJsonString = JSON.toJSONString(request);

        log.info("【微信发送模版消息】请求参数: {}", request);

        String result = null;
        try {
            String url = WX_PUSH_URL + accessToken;
            result = HttpUtil.postByJson(url, paramJsonString);
            if (StringUtils.isBlank(result)) {
                log.error("【微信发送模版消息】发送失败，请求参数{} ", JSON.toJSON(request));
                throw new BizException(ErrorCodeEnum.SYS_EXP, "【微信发送模版消息】发送失败");
            }
        } catch (Exception e) {
            log.error("微信发送模版消息异常：{}", e);
            if (StringUtils.isBlank(result)) {
                throw new BizException(ErrorCodeEnum.SYS_EXP, "【微信发送模版消息】发送失败");
            }
        }

        log.info("【微信API发放卡券】响应结果：{}", result);

        return JSON.parseObject(result, WxNotifyMessagePushResponse.class);
    }
}