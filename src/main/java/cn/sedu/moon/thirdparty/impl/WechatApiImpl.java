/*
 * Dian.com Inc.
 * Copyright (c) 2004-2019 All Rights Reserved.
 */
package cn.sedu.moon.thirdparty.impl;

import cn.sedu.moon.common.cache.CacheKey;
import cn.sedu.moon.thirdparty.response.WxMsgSecCheckResponse;
import com.alibaba.fastjson.JSON;

import cn.sedu.moon.common.enums.ErrorCodeEnum;
import cn.sedu.moon.common.exception.BizException;
import cn.sedu.moon.common.util.HttpUtil;
import cn.sedu.moon.thirdparty.WechatApi;
import cn.sedu.moon.thirdparty.response.WxGetAccessTokenResponse;
import com.alibaba.fastjson.JSONObject;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.redisson.api.RBucket;
import org.redisson.api.RedissonClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

/**
 * 微信通用接口api
 *
 * @author ${wukong}
 * @version $Id: WechatApiImpl.java, v 0.1 2019年04月22日 7:49 PM Exp $
 */
@Service
@Slf4j
public class WechatApiImpl implements WechatApi {

    private static final String WX_APP_ID  = "wx8f90fe8698685640";
    private static final String WX_SECRET = "43c27172d7d6a8b7faffca3e241582f3";
    private static final String WX_GRANT_TYPE = "client_credential";

    @Autowired
    private RedissonClient redissonClient;

    @Override
    public WxGetAccessTokenResponse getAccessToken() {

        String result = null;
        try {
            String url = "https://api.weixin.qq.com/cgi-bin/token?grant_type=" + WX_GRANT_TYPE +
                    "&appid=" + WX_APP_ID + "&secret=" + WX_SECRET;

            result = HttpUtil.get(url);
            if (StringUtils.isBlank(result)) {
                log.error("【微信获取accessToken】发送失败, url {} ",  url);
                throw new BizException(ErrorCodeEnum.SYS_EXP, "【微信获取accessToken】发送失败");
            }
        } catch (Exception e) {
            log.error("微信获取accessToken异常：{}", e);
            if (StringUtils.isBlank(result)) {
                throw new BizException(ErrorCodeEnum.SYS_EXP, "【微信获取accessToken】发送失败");
            }
        }

        log.info("【微信获取accessToken】响应结果：{}", result);

        return JSON.parseObject(result, WxGetAccessTokenResponse.class);
    }


    @Override
    public WxMsgSecCheckResponse contentCheck(final String content) {


        RBucket<String> bucket = redissonClient.getBucket(CacheKey.WX_ACCESS_TOKEN_KEY);

        String accessToken;
        if (bucket.get() == null) {
            WxGetAccessTokenResponse response = getAccessToken();
            if (response.getAccessToken() == null) {
                log.error("contentCheck 获取微信accessToken失败 response is {}", response);
                throw new BizException(ErrorCodeEnum.SYS_EXP, "获取微信accessToken失败");
            }

            // 将accessToken存入redis，失效时间为微信返回的时间 减去100秒
            bucket.set(response.getAccessToken(), response.getExpiresIn() - 100, TimeUnit.SECONDS);
            accessToken = response.getAccessToken();

        } else {
            accessToken = bucket.get();
        }

        // 获取
        String url = "https://api.weixin.qq.com/wxa/msg_sec_check?access_token=" + accessToken;
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("content", content);
        try {
            String result = HttpUtil.postByJson(url, jsonObject.toJSONString());
            return JSON.parseObject(result, WxMsgSecCheckResponse.class);
        } catch (IOException e) {
            throw new BizException(ErrorCodeEnum.SYS_EXP, "【微信内容安全审核接口】发送失败");
        }

    }
}