/*
 * Dian.com Inc.
 * Copyright (c) 2004-2019 All Rights Reserved.
 */
package cn.sedu.moon.thirdparty.response;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;

import java.io.Serializable;

/**
 * @author wukong
 * @version : WxMsgSecCheckResponse.java, v 0.1 2019年08月30日 20:11 Exp $
 */
@Data
public class WxMsgSecCheckResponse implements Serializable {

    /**
     * 错误码
     */
    @JSONField(name = "errcode")
    private Integer errorCode;

    /**
     * 错误信息
     */
    @JSONField(name = "errmsg")
    private String errMsg;

}