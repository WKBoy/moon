/*
 * Dian.com Inc.
 * Copyright (c) 2004-2019 All Rights Reserved.
 */
package cn.sedu.moon.thirdparty.response;

import java.io.Serializable;

import com.alibaba.fastjson.annotation.JSONField;

import lombok.Data;

/**
 *
 * @author ${wukong}
 * @version $Id: WxGetAccessTokenResponse.java, v 0.1 2019年04月22日 7:43 PM Exp $
 */
@Data
public class WxGetAccessTokenResponse implements Serializable {

    private static final long serialVersionUID = -105616005920881447L;

    /**
     * 获取到的凭证, 如果出现错误，access_token为null
     */
    @JSONField(name = "access_token")
    private String accessToken;

    /**
     * 凭证有效时间，单位：秒，目前是7200秒之内的值
     */
    @JSONField(name = "expires_in")
    private Integer expiresIn;

    /**
     * 错误码
     */
    @JSONField(name = "errorcode")
    private Integer errorCode;

    /**
     * 错误信息
     */
    @JSONField(name = "errmsg")
    private String errMsg;

}