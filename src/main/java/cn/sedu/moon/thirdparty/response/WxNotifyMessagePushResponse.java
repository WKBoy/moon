/*
 * Dian.com Inc.
 * Copyright (c) 2004-2019 All Rights Reserved.
 */
package cn.sedu.moon.thirdparty.response;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

/**
 * 发送微信模版消息的response 对象
 *
 * @author ${wukong}
 * @version $Id: WxNotifyMessagePushResponse.java, v 0.1 2019年04月17日 3:48 PM Exp $
 */
@Data
public class WxNotifyMessagePushResponse implements Serializable {

    private static final long serialVersionUID = -2536970462984876978L;

    @JsonProperty("errcode")
    private Integer errCode;

    @JsonProperty("errmsg")
    private String errMsg;
}