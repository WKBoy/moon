/*
 * Dian.com Inc.
 * Copyright (c) 2004-2019 All Rights Reserved.
 */
package cn.sedu.moon.thirdparty;

import cn.sedu.moon.thirdparty.response.WxGetAccessTokenResponse;
import cn.sedu.moon.thirdparty.response.WxMsgSecCheckResponse;

/**
 * 微信通用api
 *
 * @author ${wukong}
 * @version $Id: WechatApi.java, v 0.1 2019年04月22日 6:51 PM Exp $
 */
public interface WechatApi {

    /**
     * 获取微信接口凭证(accessToken)
     *
     * @return response
     */
    WxGetAccessTokenResponse getAccessToken();

    /**
     * 内容审核
     *
     * @param content content
     * @return response
     */
    WxMsgSecCheckResponse contentCheck(String content);


}