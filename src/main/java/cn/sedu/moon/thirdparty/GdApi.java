/*
 * Dian.com Inc.
 * Copyright (c) 2004-2019 All Rights Reserved.
 */
package cn.sedu.moon.thirdparty;

import cn.sedu.moon.thirdparty.dto.GdDistrictResponse;
import cn.sedu.moon.thirdparty.dto.GdPoiSearchResponse;
import cn.sedu.moon.thirdparty.fallback.GdApiFallbackFactory;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * @author wukong
 * @version : GdApi.java, v 0.1 2019年10月31日 9:34 下午 Exp $
 */
@FeignClient(name = "gd", url = "https://restapi.amap.com", fallbackFactory = GdApiFallbackFactory.class)
public interface GdApi {

    /***
     * poi关键字搜索
     *
     * @param key 授权key
     * @param keywords 搜索关键字
     * @param areaCode 区编码
     * @param cityLimit 城市编码
     * @param offset 页大小
     * @param page 页码
     * @return response
     */
    @RequestMapping(value = "/v3/place/text", method = RequestMethod.GET, consumes = MediaType.TEXT_PLAIN_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    GdPoiSearchResponse poiSearch(@RequestParam("key") String key,
                                     @RequestParam("keywords") String keywords,
                                     @RequestParam("city") String areaCode,
                                     @RequestParam(value = "citylimit") boolean cityLimit,
                                     @RequestParam(value = "offset") Integer offset,
                                     @RequestParam(value = "page") Integer page,
                                     @RequestParam(value = "extensions", defaultValue = "all") String extensions);


    /**
     * 行政区域查询
     *
     * @param key 授权key
     * @param subDistrict
     *
     * 可选值：0、1、2、3等数字，并以此类推
     *
     * 0：不返回下级行政区；
     * 1：返回下一级行政区；
     * 2：返回下两级行政区；
     * 3：返回下三级行政区；
     *
     * @return response
     */
    @RequestMapping(value = "/v3/config/district", method = RequestMethod.GET, consumes = MediaType.TEXT_PLAIN_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    GdDistrictResponse districtSearch(@RequestParam("key") String key,
                                      @RequestParam(value = "subdistrict", defaultValue = "3") int subDistrict);
}