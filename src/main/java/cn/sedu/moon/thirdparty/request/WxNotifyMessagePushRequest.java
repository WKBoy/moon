/*
 * Dian.com Inc.
 * Copyright (c) 2004-2019 All Rights Reserved.
 */
package cn.sedu.moon.thirdparty.request;

import java.io.Serializable;
import java.util.Map;

import com.alibaba.fastjson.annotation.JSONField;

import lombok.Data;

/**
 * 发送微信模版消息的入参
 *
 * @author ${wukong}
 * @version $Id: WxNotifyMessagePushRequest.java, v 0.1 2019年04月17日 3:47 PM Exp $
 */
@Data
public class WxNotifyMessagePushRequest implements Serializable {

    private static final long serialVersionUID = -3150128075323513185L;

    /**
     * 微信用户openId
     */
    @JSONField(name = "touser")
    private String toUser;

    @JSONField(name = "template_id")
    private String templateId;

    @JSONField(name = "page")
    private String page;

    @JSONField(name = "form_id")
    private String formId;

    @JSONField(name = "data")
    private Map data;

    @JSONField(name = "emphasis_keyword")
    private String emphasisKeyword;

}