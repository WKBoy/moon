/*
 * Dian.com Inc.
 * Copyright (c) 2004-2019 All Rights Reserved.
 */
package cn.sedu.moon.thirdparty.fallback;

import cn.sedu.moon.thirdparty.WechatClient;
import cn.sedu.moon.thirdparty.dto.WxAppSessionKey;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * 调用微信接口的fallback
 *
 * @author wukong
 * @version : WechatClientFallback.java, v 0.1 2019年08月27日 23:47 Exp $
 */
@Service
@Slf4j
public class WechatClientFallback implements WechatClient {

    @Override
    public WxAppSessionKey code2Session(final String appId, final String appSecret, final String code, final String grantType) {
        log.error("|wechat fallback|code2Session |params:{}-{}-{}-{}", appId, appSecret, code, grantType);
        return null;
    }
}