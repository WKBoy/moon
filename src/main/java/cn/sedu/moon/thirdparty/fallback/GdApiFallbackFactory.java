/*
 * Dian.com Inc.
 * Copyright (c) 2004-2019 All Rights Reserved.
 */
package cn.sedu.moon.thirdparty.fallback;

import cn.sedu.moon.thirdparty.GdApi;
import cn.sedu.moon.thirdparty.dto.GdDistrictResponse;
import cn.sedu.moon.thirdparty.dto.GdPoiSearchResponse;
import feign.hystrix.FallbackFactory;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * @author wukong
 * @version : GdApiFallbackFactory.java, v 0.1 2019年10月31日 9:57 下午 Exp $
 */
@Service
@Slf4j(topic = "DATA-TASK")
public class GdApiFallbackFactory implements FallbackFactory<GdApi> {

    @Override
    public GdApi create(final Throwable cause) {

        return new GdApi() {
            @Override
            public GdPoiSearchResponse poiSearch(final String key, final String keywords, final String areaCode, final boolean cityLimit, final Integer offset, final Integer page, final String extentions) {
                log.error("poi search error: keywords:{}, areaCode:{}, offset:{}, page:{},", keywords, areaCode, offset, page);
                log.error("poi search error: exception:", cause);

                GdPoiSearchResponse response = new GdPoiSearchResponse();
                response.setStatus(0);
                response.setInfo(cause.getMessage());
                return response;
            }

            @Override
            public GdDistrictResponse districtSearch(final String key, final int subDistrict) {

                log.error("district search error: subDistrict:{},", subDistrict);
                log.error("district search error: exception:", cause);

                GdDistrictResponse response = new GdDistrictResponse();
                response.setStatus(0);
                response.setInfo(cause.getMessage());
                return response;
            }
        };
    }
}