/*
 * Dian.com Inc.
 * Copyright (c) 2004-2019 All Rights Reserved.
 */
package cn.sedu.moon.thirdparty;

import cn.sedu.moon.thirdparty.dto.WxAppSessionKey;
import cn.sedu.moon.thirdparty.fallback.WechatClientFallback;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * @author wukong
 * @version : WechatClient.java, v 0.1 2019年09月03日 13:53 Exp $
 */
@FeignClient(name = "wechat", url = "https://api.weixin.qq.com", fallback = WechatClientFallback.class)
public interface WechatClient {

    /**
     * 登录凭证校验
     *
     * @param appId     小程序 appId
     * @param appSecret 小程序 appSecret
     * @param code      登录时获取的 code
     * @param grantType 授权类型，此处只需填写 authorization_code
     * @return 微信返回字符串
     */
    @RequestMapping(value = "/sns/jscode2session", method = RequestMethod.GET, consumes = MediaType.TEXT_PLAIN_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    WxAppSessionKey code2Session(@RequestParam("appid") String appId,
                                 @RequestParam("secret") String appSecret, @RequestParam(name = "js_code") String code,
                                 @RequestParam(name = "grant_type", defaultValue = "authorization_code") String grantType);


}