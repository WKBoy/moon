/*
 * Dian.com Inc.
 * Copyright (c) 2004-2019 All Rights Reserved.
 */
package cn.sedu.moon.thirdparty.dto;

import lombok.Data;

import java.io.Serializable;

/**
 * @author wukong
 * @version : GdPoiPhotoItem.java, v 0.1 2019年11月01日 5:39 下午 Exp $
 */
@Data
public class GdPoiPhotoItem implements Serializable {

    private static final long serialVersionUID = -2108007040717965001L;

    private String url;

}
