/*
 * Dian.com Inc.
 * Copyright (c) 2004-2019 All Rights Reserved.
 */
package cn.sedu.moon.thirdparty.dto;

import lombok.Data;

/**
 * @author wukong
 * @version : WxAppUserInfo.java, v 0.1 2019年10月09日 11:48 AM Exp $
 */
@Data
public class WxAppUserInfo {

    private String unionId;

    private String nickName;

    private String avatarUrl;

    private String openId;

    private String country;
    private String province;
    private String city;

    private String sex;
    private String language;
    private String sessionKey;

    private String mobile;
}