/*
 * Dian.com Inc.
 * Copyright (c) 2004-2019 All Rights Reserved.
 */
package cn.sedu.moon.thirdparty.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @author wukong
 * @version : GdPoiItem.java, v 0.1 2019年10月31日 9:50 下午 Exp $
 */
@Data
public class GdPoiItem implements Serializable {

    private static final long serialVersionUID = -5326703381381761115L;

    /**
     * 唯一id
     */
    private String id;

    private String name;

    private String address;

    private String location;

    @JsonProperty("pcode")
    private String provinceCode;

    @JsonProperty("pname")
    private String provinceName;

    @JsonProperty("citycode")
    private String cityCode;

    @JsonProperty("cityname")
    private String cityName;

    @JsonProperty("adcode")
    private String areaCode;

    @JsonProperty("adname")
    private String areaName;

    @JsonProperty("photos")
    private List<GdPoiPhotoItem> photos;

}