/*
 * Dian.com Inc.
 * Copyright (c) 2004-2019 All Rights Reserved.
 */
package cn.sedu.moon.thirdparty.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @author wukong
 * @version : GdDistrictResponse.java, v 0.1 2019年11月01日 3:22 下午 Exp $
 */
@Data
public class GdDistrictResponse implements Serializable {

    private static final long serialVersionUID = -966660167602010438L;

    private Integer status;

    private String info;

    @JsonProperty("infocode")
    private String infoCode;

    private Integer count;

    private List<GdDistrict> districts;
}