/*
 * Dian.com Inc.
 * Copyright (c) 2004-2019 All Rights Reserved.
 */
package cn.sedu.moon.thirdparty.dto;

import cn.sedu.moon.controller.response.BaseResponse;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

/**
 * @author wukong
 * @version : GdPoiSearchResponse.java, v 0.1 2019年10月31日 9:45 下午 Exp $
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class GdPoiSearchResponse extends BaseResponse {

    private static final long serialVersionUID = -609057996232377611L;

    /**
     * 0：请求失败；1：请求成功
     */
    private Integer status;

    private String info;

    private Integer count;

    @JsonProperty("pois")
    private List<GdPoiItem> poiList;
}