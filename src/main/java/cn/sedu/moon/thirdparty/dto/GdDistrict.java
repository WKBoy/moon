/*
 * Dian.com Inc.
 * Copyright (c) 2004-2019 All Rights Reserved.
 */
package cn.sedu.moon.thirdparty.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @author wukong
 * @version : GdDistrict.java, v 0.1 2019年11月01日 3:19 下午 Exp $
 */
@Data
public class GdDistrict implements Serializable {

    private static final long serialVersionUID = 6944880835763915415L;

    @JsonProperty("adcode")
    private String adCode;

    @JsonProperty("name")
    private String name;

    @JsonProperty("center")
    private String center;

    @JsonProperty("level")
    private String level;

    @JsonProperty("districts")
    private List<GdDistrict> districts;
}