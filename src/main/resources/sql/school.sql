
DROP TABLE IF EXISTS moon_school_detail;
CREATE TABLE moon_school_detail (
  id                BIGINT       NOT NULL AUTO_INCREMENT,
  school_id BIGINT      NOT NULL COMMENT '学校id',
  kc    VARCHAR(512)   NOT NULL COMMENT '课程',
  introduction  TEXT NOT NULL COMMENT '学校的介绍信息',
  `deleted`         TINYINT      NOT NULL DEFAULT 0 COMMENT '逻辑删除，默认为0， 1为删除',
  `create_time`     DATETIME     NOT NULL,
  `update_time`     DATETIME     NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY index_btree_school_id (school_id) USING BTREE
)
  ENGINE = InnoDB
  COMMENT '学校描述信息表';

DROP TABLE IF EXISTS moon_school_image;
CREATE TABLE moon_school_image (
  id            BIGINT       NOT NULL AUTO_INCREMENT,
  school_id     BIGINT       NOT NULL COMMENT '学校id',
  img           TEXT NOT NULL COMMENT '图片',
  `deleted`     TINYINT      NOT NULL DEFAULT 0
  COMMENT '逻辑删除，默认为0， 1为删除',
  `create_time` DATETIME     NOT NULL,
  `update_time` DATETIME     NOT NULL,
  PRIMARY KEY (`id`),
  KEY index_btree_school_id (school_id) USING BTREE
)
  ENGINE = InnoDB
  COMMENT '学校图片表';




DROP TABLE IF EXISTS moon_school_hot;
CREATE TABLE moon_school_hot (
  id            BIGINT       NOT NULL AUTO_INCREMENT,
  school_id     BIGINT       NOT NULL COMMENT '学校id',
  type TINYINT NOT NULL COMMENT '学校类型',
  property TINYINT COMMENT '公办民办信息，1:公办，2:民办',
  province_code VARCHAR(10) NOT NULL COMMENT '省编码',
  city_code VARCHAR(10) NOT NULL COMMENT '市编码',
  area_code VARCHAR(10) NOT NULL COMMENT '区编码',
  city_hot TINYINT NOT NULL DEFAULT 0 COMMENT '市热门',
  area_hot TINYINT NOT NULL DEFAULT 0 COMMENT '区热门',
  `deleted`     TINYINT      NOT NULL DEFAULT 0
  COMMENT '逻辑删除，默认为0， 1为删除',
  `create_time` DATETIME     NOT NULL,
  `update_time` DATETIME     NOT NULL,
  PRIMARY KEY (`id`),
  KEY index_btree_school_id (school_id) USING BTREE,
  KEY index_btree_city_code (city_code) USING BTREE,
  KEY index_btree_area_code (area_code) USING BTREE
)
  ENGINE = InnoDB
  COMMENT '热门学校表';



