

DROP TABLE IF EXISTS moon_intro_read;
CREATE TABLE moon_intro_read (
  id                BIGINT   NOT NULL AUTO_INCREMENT,
  user_id BIGINT   NOT NULL COMMENT '用户id',
  create_time       DATETIME NOT NULL,
  update_time       DATETIME NOT NULL,
  PRIMARY KEY (id),
  KEY index_btree_user_id (user_id) USING BTREE
)
  ENGINE = InnoDB
  COMMENT '首页引导读取记录';