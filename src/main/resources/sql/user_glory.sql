
DROP TABLE IF EXISTS moon_user_glory;
CREATE TABLE moon_user_glory (
  id                BIGINT       NOT NULL AUTO_INCREMENT,
  user_id           BIGINT       NOT NULL COMMENT '用户id',
  glory             BIGINT       NOT NULL COMMENT '荣誉值',
  `deleted`         TINYINT      NOT NULL DEFAULT 0 COMMENT '逻辑删除，默认为0， 1为删除',
  `create_time`     DATETIME     NOT NULL,
  `update_time`     DATETIME     NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY index_btree_user_id (user_id) USING BTREE
)
  ENGINE = InnoDB
  COMMENT '用户荣誉值表';

DROP TABLE IF EXISTS moon_user_glory_record;
CREATE TABLE moon_user_glory_record (
  id            BIGINT       NOT NULL AUTO_INCREMENT,
  user_id     BIGINT       NOT NULL COMMENT '学校id',
  value       TEXT NOT NULL COMMENT '图片',
  change_type TINYINT NOT NULL DEFAULT 0 COMMENT '0 增加，1 减少',
  biz_type     INT NOT NULL COMMENT '业务类型',
  `deleted`     TINYINT      NOT NULL DEFAULT 0 COMMENT '逻辑删除，默认为0， 1为删除',
  `create_time` DATETIME     NOT NULL,
  `update_time` DATETIME     NOT NULL,
  PRIMARY KEY (`id`),
  KEY index_btree_user_id (user_id) USING BTREE
)
  ENGINE = InnoDB
  COMMENT '用户荣誉值变更记录表';