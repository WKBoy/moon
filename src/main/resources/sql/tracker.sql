
# 置顶文章表
DROP TABLE IF EXISTS moon_tracker;
CREATE TABLE moon_tracker
(
    id          BIGINT       NOT NULL AUTO_INCREMENT,
    url         VARCHAR(512) NOT NULL COMMENT '请求路径',
    user_id     BIGINT COMMENT '用户id',
    deleted     TINYINT      NOT NULL DEFAULT 0 COMMENT '逻辑删除，默认为0， 1为删除',
    create_time DATETIME     NOT NULL,
    update_time DATETIME     NOT NULL,
    PRIMARY KEY (id)
)
    ENGINE = InnoDB
    COMMENT '埋点';


# 置顶文章表
DROP TABLE IF EXISTS moon_tracker_point;
CREATE TABLE moon_tracker_point
(
    id          BIGINT       NOT NULL AUTO_INCREMENT,
    url         VARCHAR(512) NOT NULL COMMENT '请求路径',
    title     VARCHAR(64) COMMENT '埋点标题描述',
    deleted     TINYINT      NOT NULL DEFAULT 0 COMMENT '逻辑删除，默认为0， 1为删除',
    create_time DATETIME     NOT NULL,
    update_time DATETIME     NOT NULL,
    PRIMARY KEY (id)
)
    ENGINE = InnoDB
    COMMENT '埋点配置表';

DROP TABLE IF EXISTS moon_tracker_pv_uv;
CREATE TABLE moon_tracker_pv_uv
(
    id          BIGINT       NOT NULL AUTO_INCREMENT,
    url         VARCHAR(512) NOT NULL COMMENT '请求路径',
    title       VARCHAR(64) COMMENT '埋点标题描述',
    pv          INTEGER NOT NULL DEFAULT 0 COMMENT 'pv',
    uv          INTEGER NOT NULL DEFAULT 0 COMMENT 'uv',
    deleted     TINYINT      NOT NULL DEFAULT 0 COMMENT '逻辑删除，默认为0， 1为删除',
    create_time DATETIME     NOT NULL,
    update_time DATETIME     NOT NULL,
    PRIMARY KEY (id)
)
    ENGINE = InnoDB
    COMMENT '埋点pvuv统计结果表';