

CREATE TABLE `moon_district_user_report` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` BIGINT NOT NULL,
  `province_code` varchar(128) DEFAULT NULL,
  `city_code` varchar(128) DEFAULT NULL,
  `area_code` varchar(128) DEFAULT NULL,
  `name` varchar(128) DEFAULT NULL,
  `address` varchar(512) DEFAULT NULL,
  `info` TEXT DEFAULT NULL,
  `create_time` datetime NOT NULL,
  `update_time` datetime NOT NULL,
  `lng` decimal(20,16) DEFAULT NULL COMMENT '经度',
  `lat` decimal(20,16) DEFAULT NULL COMMENT '纬度',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `name` (`name`)
) ENGINE=InnoDB  COMMENT='用户提供的小区信息表';


CREATE TABLE `moon_school_user_report` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` BIGINT NOT NULL,
  `province_code` varchar(128) DEFAULT NULL,
  `city_code` varchar(128) DEFAULT NULL,
  `area_code` varchar(128) DEFAULT NULL,
  `name` varchar(128) DEFAULT NULL,
  `address` varchar(512) DEFAULT NULL,
  `info` TEXT DEFAULT NULL,
  `create_time` datetime NOT NULL,
  `update_time` datetime NOT NULL,
  `lng` decimal(20,16) DEFAULT NULL COMMENT '经度',
  `lat` decimal(20,16) DEFAULT NULL COMMENT '纬度',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `name` (`name`)
) ENGINE=InnoDB  COMMENT='用户提供的学校信息表';

