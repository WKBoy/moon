DROP TABLE IF EXISTS moon_user_medal;
CREATE TABLE moon_user_medal (
  id          BIGINT   NOT NULL AUTO_INCREMENT,
  user_id   BIGINT   NOT NULL COMMENT '用户ID',
  type      TINYINT NOT NULL COMMENT '勋章类型',
  title     VARCHAR(128) COMMENT '勋章描述信息',
  deleted     TINYINT  NOT NULL DEFAULT 0
  COMMENT '逻辑删除，默认为0， 1为删除',
  create_time DATETIME NOT NULL,
  update_time DATETIME NOT NULL,
  PRIMARY KEY (id),
  KEY index_btree_user_id (user_id) USING BTREE,
  KEY index_btree_type (type) USING BTREE
)
  ENGINE = InnoDB
  COMMENT '用户勋章表';

DROP TABLE IF EXISTS moon_sop_attention;
CREATE TABLE moon_sop_attention (
  id                BIGINT   NOT NULL AUTO_INCREMENT,
  sop_id            BIGINT   NOT NULL COMMENT '问题id',
  attention_user_id BIGINT   NOT NULL COMMENT '问题关者的用户ID',
  deleted           TINYINT  NOT NULL DEFAULT 0 COMMENT '逻辑删除，默认为0， 1为删除',
  create_time       DATETIME NOT NULL,
  update_time       DATETIME NOT NULL,
  PRIMARY KEY (id),
  KEY index_btree_ask_id (sop_id) USING BTREE,
  KEY index_btree_attention_user_id (attention_user_id) USING BTREE
)
  ENGINE = InnoDB
  COMMENT 'SOP问题关注表';