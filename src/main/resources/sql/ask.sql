DROP TABLE IF EXISTS moon_ask;
CREATE TABLE moon_ask (
  id                BIGINT   NOT NULL AUTO_INCREMENT,
  publisher         BIGINT   NOT NULL COMMENT '发布者用户ID',
  title VARCHAR(128) COMMENT '问题标题',
  content           TEXT COMMENT '问题内容',
  deleted           TINYINT  NOT NULL DEFAULT 0 COMMENT '逻辑删除，默认为0， 1为删除',
  create_time       DATETIME NOT NULL,
  update_time       DATETIME NOT NULL,
  PRIMARY KEY (id),
  KEY index_btree_publisher (publisher) USING BTREE
)
  ENGINE = InnoDB
  COMMENT '问答模块问题表';

DROP TABLE IF EXISTS moon_ask_answer;
CREATE TABLE moon_ask_answer (
  id          BIGINT   NOT NULL AUTO_INCREMENT,
  ask_id      BIGINT NOT NULL COMMENT '问题id',
  answer_user_id   BIGINT   NOT NULL COMMENT '回答者用户ID',
  content     TEXT COMMENT '答案内容',
  accepted    BOOLEAN NOT NULL DEFAULT FALSE COMMENT '答案是否被采纳',
  deleted     TINYINT  NOT NULL DEFAULT 0 COMMENT '逻辑删除，默认为0， 1为删除',
  create_time DATETIME NOT NULL,
  update_time DATETIME NOT NULL,
  PRIMARY KEY (id),
  KEY index_btree_ask_id (ask_id) USING BTREE,
  KEY index_btree_answer_user_id (answer_user_id) USING BTREE
)
  ENGINE = InnoDB
  COMMENT '问答模块答案表';

DROP TABLE IF EXISTS moon_ask_attention;
CREATE TABLE moon_ask_attention (
  id             BIGINT   NOT NULL AUTO_INCREMENT,
  ask_id         BIGINT   NOT NULL COMMENT '问题id',
  attention_user_id  BIGINT NOT NULL COMMENT '问题关者的用户ID',
  deleted        TINYINT  NOT NULL DEFAULT 0
  COMMENT '逻辑删除，默认为0， 1为删除',
  create_time    DATETIME NOT NULL,
  update_time    DATETIME NOT NULL,
  PRIMARY KEY (id),
  KEY index_btree_ask_id (ask_id) USING BTREE,
  KEY index_btree_attention_user_id (attention_user_id) USING BTREE
)
  ENGINE = InnoDB
  COMMENT '问题关注表';

DROP TABLE IF EXISTS moon_ask_useful;
CREATE TABLE moon_ask_useful (
  id                BIGINT   NOT NULL AUTO_INCREMENT,
  answer_id            BIGINT   NOT NULL COMMENT '问题id',
  type              TINYINT  COMMENT '1 有用，2 没用',
  user_id           BIGINT   NOT NULL COMMENT '问题关者的用户ID',
  deleted           TINYINT  NOT NULL DEFAULT 0 COMMENT '逻辑删除，默认为0， 1为删除',
  create_time       DATETIME NOT NULL,
  update_time       DATETIME NOT NULL,
  PRIMARY KEY (id),
  KEY index_btree_answer_id (answer_id) USING BTREE,
  KEY index_btree_user_id (user_id) USING BTREE,
  KEY index_btree_type (type) USING BTREE
)
  ENGINE = InnoDB
  COMMENT '问题有用没用表';



