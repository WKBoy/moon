
# 置顶文章表
DROP TABLE IF EXISTS moon_news_stick;
CREATE TABLE moon_news_stick (
  id           BIGINT NOT NULL AUTO_INCREMENT,
  news_id  BIGINT NOT NULL COMMENT '新闻id',
  deleted     TINYINT  NOT NULL DEFAULT 0 COMMENT '逻辑删除，默认为0， 1为删除',
  create_time DATETIME NOT NULL,
  update_time DATETIME NOT NULL,
  PRIMARY KEY (id)
)
  ENGINE = InnoDB
  COMMENT '被置顶的文章';