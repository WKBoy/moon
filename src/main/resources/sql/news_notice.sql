# 存放新闻列表的表, 新闻总表'
DROP TABLE IF EXISTS moon_news_notice;
CREATE TABLE moon_news_notice
(
    id               BIGINT   NOT NULL AUTO_INCREMENT,
    news_id         BIGINT   NOT NULL,
    title VARCHAR(128) comment '提醒标题',
    notice_hour     INTEGER COMMENT '提醒的时',
    notice_min      INTEGER COMMENT '提醒的分',
    notice_day      VARCHAR(16) COMMENT '提醒的天, 格式：2019-10-26',
    deleted          TINYINT  NOT NULL DEFAULT 0 COMMENT '逻辑删除，默认为0， 1为删除',
    create_time      DATETIME NOT NULL,
    update_time      DATETIME NOT NULL,
    KEY `idx_btree_news_id` (news_id) USING BTREE,
     PRIMARY KEY (id)
)
    ENGINE = InnoDB
    COMMENT '新闻通知配置表';


# 存放新闻列表的表, 新闻总表'
DROP TABLE IF EXISTS moon_news_notice_subscribe;
CREATE TABLE moon_news_notice_subscribe
(
    id               BIGINT   NOT NULL AUTO_INCREMENT,
    user_id          BIGINT COMMENT '用户id',
    notice_id        BIGINT COMMENT '提醒id',
    deleted          TINYINT  NOT NULL DEFAULT 0 COMMENT '逻辑删除，默认为0， 1为删除',
    create_time      DATETIME NOT NULL,
    update_time      DATETIME NOT NULL,
    KEY `idx_btree_user_id` (user_id) USING BTREE,
    KEY `idx_btree_notice_id` (notice_id) USING BTREE,
    PRIMARY KEY (id)
)
    ENGINE = InnoDB
    COMMENT '新闻通知订阅表';

