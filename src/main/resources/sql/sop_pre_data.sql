INSERT INTO
moon_sop_subject (
  ref_id, title, type, deleted, create_time, update_time
) VALUES (
    1, '{0}一年级有英语课吗?', 1, 0, now(), now()
), (
    2, '{0}是公办还是民办?', 1, 0, now(), now()
), (
  3, '{0}是双语吗?', 1, 0, now(), now()
), (
  4, '{0}一学期学费多少?', 3, 0, now(), now()
), (
  5, '{0}招几个班，每个班多少人?', 3, 0, now(), now()
), (
  6, '{0}食宿情况?', 2, 0, now(), now()
), (
  7, '{0}对口初中(高中)是什么?', 3, 0, now(), now()
), (
  8, '{0}学校多大?', 3, 0, now(), now()
), (
  9, '{0}有对外交流吗? 有的话，是什么国家', 3, 0, now(), now()
), (
  10, '{0}一个班有几位老师负责?', 3, 0, now(), now()
),  (
  11, '{0}学校食宿条件怎么样? 餐标是多少？', 3, 0, now(), now()
);


INSERT INTO
moon_sop_option (
  ref_id, subject_ref_id, content, deleted, create_time, update_time, choose_show_input, input_info
) VALUES (
  1, 1, '有', 0, now(), now(), 0, ''
),  (
  2, 1, '无', 0, now(), now(), 0, ''
), (
  3, 2, '公办', 0, now(), now(), 0, ''
), (
  4, 2, '民办', 0, now(), now(), 0, ''
), (
  5, 3, '是', 0, now(), now(), 0, ''
), (
  6, 3, '否', 0, now(), now(), 0, ''
), (
  7, 6, '寄宿', 0, now(), now(), 0, ''
), (
  8, 6, '走读', 0, now(), now(), 0, ''
);
