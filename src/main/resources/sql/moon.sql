-- 用户表
-- mobile 字段唯一索引
CREATE TABLE `moon_user` (
  `id`             BIGINT       NOT NULL AUTO_INCREMENT,
  `wx_open_id`     VARCHAR(128),
  `wx_unionid`     VARCHAR(128),
  `avatar`         VARCHAR(128) COMMENT '用户头像',
  `nickname`       VARCHAR(64) COMMENT '昵称',
  `province`       VARCHAR(64) COMMENT '省份',
  `city`           VARCHAR(64) COMMENT '市',
  `gender`         TINYINT COMMENT '性别: 1 男，2 女',
  `mobile`         VARCHAR(20)  COMMENT '手机号码',
  `address`        VARCHAR(128) COMMENT '家庭住址',
  `password`       VARCHAR(64)  COMMENT '密码',
  `deleted` TINYINT NOT NULL DEFAULT 0 COMMENT '逻辑删除，默认为0， 1为删除',
  `create_time`    DATETIME     NOT NULL,
  `update_time`    DATETIME     NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE = InnoDB COMMENT '用户表';


-- 学生档案表
DROP TABLE IF EXISTS `moon_student_archives`;
CREATE TABLE `moon_student_archives` (
  `id`          BIGINT NOT NULL AUTO_INCREMENT,
  `user_id`     BIGINT NOT NULL COMMENT '用户ID',
  `name`        VARCHAR(64) COMMENT '学生姓名',
  `school_id`   BIGINT COMMENT '学生所在学校ID',
  `school_name` VARCHAR(128) COMMENT '学校名称',
  `gender`      TINYINT NOT NULL DEFAULT 1 COMMENT '性别: 1 男，2 女',
  `grade_id`    INTEGER COMMENT '年级ID',
  `grade`    VARCHAR(32) COMMENT '年级',
  `district_house_id` BIGINT COMMENT '小区ID',
  `district_house_name` VARCHAR(64) COMMENT '小区名称',
  `school_property` TINYINT COMMENT '意向的学校类别 1 公办，2 民办，3 所有',
  `grow_direction_ids` VARCHAR(32) COMMENT '培养方向, id列表, 用逗号隔开',
  `grow_direction` VARCHAR(128) COMMENT '培养方向, 描述，用，隔开，如 美术, 音乐',
  `province_code` VARCHAR(10) COMMENT '省编码',
  `city_code` VARCHAR(10) COMMENT '市编码',
  `area_code` VARCHAR(10) COMMENT '区编码',
  `province_name` VARCHAR(128) COMMENT '省名称',
  `city_name` VARCHAR(128) COMMENT '市名称',
  `area_name` VARCHAR(128) COMMENT '区名称',
  `lng` DECIMAL(20, 16) DEFAULT NULL COMMENT '经度',
  `lat` DECIMAL(20, 16) DEFAULT NULL COMMENT '纬度',
  `deleted`     TINYINT  NOT NULL DEFAULT 0 COMMENT '逻辑删除，默认为0， 1为删除',
  `create_time` DATETIME NOT NULL,
  `update_time` DATETIME NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_btree_school_id` (`school_id`) USING BTREE
) ENGINE = InnoDB COMMENT '学生档案表';

-- 年级表
-- title 字段唯一索引
CREATE TABLE `moon_student_grade` (
  `id`                  BIGINT   NOT NULL AUTO_INCREMENT,
  `title`               VARCHAR(32) NOT NULL COMMENT '标题，如三年级',
  `deleted`             TINYINT  NOT NULL DEFAULT 0 COMMENT '逻辑删除，默认为0， 1为删除',
  `create_time`         DATETIME NOT NULL,
  `update_time`         DATETIME NOT NULL,
  PRIMARY KEY (`id`)
)
  ENGINE = InnoDB
  COMMENT '学生年级表';

-- 培养方向表
CREATE TABLE `moon_grow_direction` (
  `id`          BIGINT      NOT NULL AUTO_INCREMENT,
  `title`       VARCHAR(32) NOT NULL COMMENT '标题，如美术',
  `deleted`     TINYINT     NOT NULL DEFAULT 0 COMMENT '逻辑删除，默认为0， 1为删除',
  `create_time` DATETIME    NOT NULL,
  `update_time` DATETIME    NOT NULL,
  PRIMARY KEY (`id`)
)
ENGINE = InnoDB
COMMENT '培养方向表';

-- 学生选择的培养方向表
CREATE TABLE `moon_stu_grow_direction` (
  `id`          BIGINT      NOT NULL AUTO_INCREMENT,
  `user_id`     BIGINT      NOT NULL COMMENT '用户ID',
  `grow_direction_id` BIGINT NOT NULL COMMENT '培养方向ID',
  `grow_direction_title` VARCHAR(32) NOT NULL COMMENT '培养方向的名字，如美术',
  `deleted`     TINYINT     NOT NULL DEFAULT 0 COMMENT '逻辑删除，默认为0， 1为删除',
  `create_time` DATETIME    NOT NULL,
  `update_time` DATETIME    NOT NULL,
  PRIMARY KEY (`id`)
)
  ENGINE = InnoDB
  COMMENT '学生选择的培养方向表';


-- 学生意向学校表
CREATE TABLE `moon_stu_sch` (
  `id`          BIGINT      NOT NULL AUTO_INCREMENT,
  `student_id`  VARCHAR(32) NOT NULL COMMENT '标题，如美术',
  `school_id`     TINYINT     NOT NULL DEFAULT 0 COMMENT '逻辑删除，默认为0， 1为删除',
  `create_time` DATETIME    NOT NULL,
  `update_time` DATETIME    NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_btree_student_id` (`student_id`) USING BTREE
)
ENGINE = InnoDB COMMENT '学生意向学校表';

-- 学校表
CREATE TABLE `moon_school` (
  `id`           BIGINT  NOT NULL AUTO_INCREMENT,
  `province`     VARCHAR(20)      DEFAULT NULL COMMENT '省份信息',
  `city`         VARCHAR(64)      DEFAULT NULL COMMENT '城市',
  `area`         VARCHAR(64)      DEFAULT NULL COMMENT '区',
  `name`         VARCHAR(128)      DEFAULT NULL COMMENT '学校名称',
  `attribute`    VARCHAR(20)      DEFAULT NULL COMMENT '重点、示范等信息',
  `property`     VARCHAR(10)      DEFAULT NULL COMMENT '公办、民办',
  `type`         VARCHAR(10)      DEFAULT NULL COMMENT '幼儿园、小学、中学、职业学校',
  `address`      VARCHAR(512)      DEFAULT NULL COMMENT '地址信息',
  `telephone`    VARCHAR(128)      DEFAULT NULL COMMENT '联系电话',
  `create_time`  DATETIME NOT NULL,
  `update_time`  DATETIME NOT NULL,
  `lng`          DECIMAL(20, 16)   DEFAULT NULL
  COMMENT '学校经度',
  `lat`          DECIMAL(20, 16)   DEFAULT NULL
  COMMENT '学校纬度',
  `introduction` TEXT COMMENT '学校介绍',
  PRIMARY KEY (`id`),
  KEY `idx_btree_school_name` (`name`) USING BTREE

)
  ENGINE = InnoDB
  COMMENT '学校信息表';

-- 小区表
CREATE TABLE `moon_district` (
  `id`                  BIGINT   NOT NULL         AUTO_INCREMENT,
  `create_time`         TIMESTAMP NULL             DEFAULT NULL,
  `update_time`         TIMESTAMP NULL             DEFAULT NULL,
  `community_name`      VARCHAR(256)
                        CHARACTER SET utf8mb4
                        COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '小区名称',
  `primary_school_name` VARCHAR(256)
                        CHARACTER SET utf8mb4
                        COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '对口初中',
  `junior_school_name`  VARCHAR(256)
                        CHARACTER SET utf8mb4
                        COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '对口小学',
  `lng`                 DECIMAL(20, 16)            DEFAULT NULL
  COMMENT '小区经度',
  `lat`                 DECIMAL(20, 16)            DEFAULT NULL
  COMMENT '小区纬度',
  PRIMARY KEY (`id`),
  KEY `idx_btree_school_community_name` (`community_name`) USING BTREE
)
  ENGINE = InnoDB
  COMMENT '小区信息表';


DROP TABLE IF EXISTS `moon_area_data`;
CREATE TABLE `moon_area_data` (
  `id`          BIGINT       NOT NULL AUTO_INCREMENT,
  `area_name`   VARCHAR(128) NOT NULL
  COMMENT '省市区表述信息',
  `area_code`   VARCHAR(10)  NOT NULL
  COMMENT '省市区编码',
  `area_level`  TINYINT      NOT NULL
  COMMENT '用来区分省市区类别',
  `parent_code` VARCHAR(10)  NOT NULL
  COMMENT '上级的编码',
  PRIMARY KEY (`id`),
  KEY `index_btree_parent_code` (`parent_code`) USING BTREE
)
  ENGINE = InnoDB
  COMMENT '省市区信息表';


-- SOP题目表

DROP TABLE IF EXISTS moon_sop_subject;
CREATE TABLE moon_sop_subject (
  id       BIGINT NOT NULL AUTO_INCREMENT,
  ref_id   INTEGER NOT NULL COMMENT 'ref_id',
  title    VARCHAR(512)  NOT NULL COMMENT '问题的标题',
  type     INTEGER NOT NULL COMMENT '问题类型，1 单选，2 多选，3 输入',
  view_count BIGINT NOT NULL DEFAULT 0 COMMENT '浏览人数',
  `deleted`     TINYINT  NOT NULL DEFAULT 0 COMMENT '逻辑删除，默认为0， 1为删除',
  `create_time` DATETIME NOT NULL,
  `update_time` DATETIME NOT NULL,
  PRIMARY KEY (`id`),
  KEY index_btree_ref_id (`ref_id`) USING BTREE
)
  ENGINE = InnoDB
  COMMENT 'SOP问题表';

-- SOP 问题选项表

DROP TABLE IF EXISTS moon_sop_option;
CREATE TABLE moon_sop_option (
  id            BIGINT       NOT NULL AUTO_INCREMENT,
  ref_id        INTEGER      NOT NULL COMMENT 'ref_id',
  subject_ref_id INTEGER     NOT NULL COMMENT '选项对应的题目ID',
  content       VARCHAR(512) NOT NULL COMMENT '选项的文本内容',
  choose_show_input  INTEGER NOT NULL DEFAULT 0 COMMENT '当用户选中该选项的时候展示输入框',
  input_info    VARCHAR(512) COMMENT '输入框提示文案',
  `deleted`     TINYINT      NOT NULL DEFAULT 0 COMMENT '逻辑删除，默认为0， 1为删除',
  `create_time` DATETIME     NOT NULL,
  `update_time` DATETIME     NOT NULL,
  PRIMARY KEY (`id`),
  KEY index_btree_ref_id (ref_id) USING BTREE,
  KEY index_btree_subject_ref_id (subject_ref_id) USING BTREE
)
  ENGINE = InnoDB
  COMMENT 'SOP问题选项表';

# sop 答题表

DROP TABLE IF EXISTS moon_sop_answer;
CREATE TABLE moon_sop_answer (
  id             BIGINT       NOT NULL AUTO_INCREMENT,
  user_id        BIGINT NOT NULL COMMENT '用户ID',
  school_id      BIGINT NOT NULL COMMENT '学校ID',
  subject_ref_id INTEGER      NOT NULL COMMENT '题目ID',
  option_ref_id  INTEGER      COMMENT '选项ID, 文本输入型问题的答案，该字段可以为空',
  option_input_text TEXT COMMENT '用户输入的文本内容',
  deleted      TINYINT      NOT NULL DEFAULT 0 COMMENT '逻辑删除，默认为0， 1为删除',
  create_time  DATETIME     NOT NULL,
  update_time  DATETIME     NOT NULL,
  PRIMARY KEY (id),
  KEY index_btree_user_id (user_id) USING BTREE,
  KEY index_btree_school_id (school_id) USING BTREE,
  KEY index_btree_option_ref_id (option_ref_id) USING BTREE,
  KEY index_btree_subject_ref_id (subject_ref_id) USING BTREE
)
  ENGINE = InnoDB
  COMMENT 'SOP答题表';