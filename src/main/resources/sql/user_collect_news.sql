
# 用户收藏文章
DROP TABLE IF EXISTS moon_news_stick;
CREATE TABLE moon_news_stick (
  id           BIGINT NOT NULL AUTO_INCREMENT,
  news_id  BIGINT NOT NULL COMMENT '新闻id',
  user_id  BIGINT NOT NULL COMMENT '用户id',
  deleted     TINYINT  NOT NULL DEFAULT 0 COMMENT '逻辑删除，默认为0， 1为删除',
  create_time DATETIME NOT NULL,
  update_time DATETIME NOT NULL,
  PRIMARY KEY (id),
  KEY index_btree_user_id (user_id) USING BTREE,
  KEY index_btree_news_id (news_id) USING BTREE
)
  ENGINE = InnoDB
  COMMENT '用户收藏的文章列表';