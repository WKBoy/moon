DROP TABLE IF EXISTS moon_log_news_detail;
CREATE TABLE moon_log_news_detail (
  id                BIGINT   NOT NULL AUTO_INCREMENT,
  news_id            BIGINT   NOT NULL COMMENT '新闻id',
  user_id BIGINT   NOT NULL COMMENT '用户id',
  create_time       DATETIME NOT NULL,
  update_time       DATETIME NOT NULL,
  PRIMARY KEY (id),
  KEY index_btree_news_id (news_id) USING BTREE,
  KEY index_btree_user_id (user_id) USING BTREE
)
  ENGINE = InnoDB
  COMMENT '新闻详情页的log';