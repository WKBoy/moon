
DROP TABLE moon_cms_user;

CREATE TABLE `moon_cms_user` (
  `id`          BIGINT   NOT NULL AUTO_INCREMENT,
  `avatar`      TEXT COMMENT '用户头像',
  `nickname`    VARCHAR(128) COMMENT '昵称',
  `mobile`      VARCHAR(20) COMMENT '手机号码',
  `login_name`  VARCHAR(128) COMMENT '登录名',
  `password`    VARCHAR(128) COMMENT '密码',
  `deleted`     TINYINT  NOT NULL DEFAULT 0 COMMENT '逻辑删除，默认为0， 1为删除',
  `create_time` DATETIME NOT NULL,
  `update_time` DATETIME NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY unique_index_btree_login_name (login_name) USING BTREE
)
  ENGINE = InnoDB
  COMMENT 'cms后台用户表';