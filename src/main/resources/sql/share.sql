DROP TABLE IF EXISTS moon_share_news;
CREATE TABLE moon_share_news (
  id            BIGINT   NOT NULL AUTO_INCREMENT,
  news_id       BIGINT   NOT NULL
  COMMENT '新闻id',
  user_id       BIGINT   NOT NULL
  COMMENT '用户id',
  status        TINYINT  NOT NULL DEFAULT 1
  COMMENT '1 已创建，2 已分享',
  `deleted`     TINYINT  NOT NULL DEFAULT 0
  COMMENT '逻辑删除，默认为0， 1为删除',
  `create_time` DATETIME NOT NULL,
  `update_time` DATETIME NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_btree_union_user_id_news_id` (`user_id`, `news_id`) USING BTREE
)
  ENGINE = InnoDB
  COMMENT '学校描述信息表';


DROP TABLE IF EXISTS moon_share_news_light;
CREATE TABLE moon_share_news_light (
  id            BIGINT   NOT NULL AUTO_INCREMENT,
  news_id       BIGINT   NOT NULL COMMENT '新闻id',
  user_id       BIGINT   NOT NULL COMMENT '分享者，被点亮用户id',
  user_light_id BIGINT   NOT NULL COMMENT '点亮的用户id',
  `deleted`     TINYINT  NOT NULL DEFAULT 0 COMMENT '逻辑删除，默认为0， 1为删除',
  `create_time` DATETIME NOT NULL,
  `update_time` DATETIME NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_btree_union_user_id_news_id` (`user_id`, `news_id`) USING BTREE
)
  ENGINE = InnoDB
  COMMENT '新闻分享点亮记录表';



DROP TABLE IF EXISTS moon_share_news_read;
CREATE TABLE moon_share_news_read (
    id  BIGINT NOT NULL AUTO_INCREMENT,
    share_id BIGINT NOT NULL COMMENT '分享id',
    user_id BIGINT NOT NULL COMMENT '读者id',
    `deleted`     TINYINT  NOT NULL DEFAULT 0 COMMENT '逻辑删除，默认为0， 1为删除',
    `create_time` DATETIME NOT NULL,
    `update_time` DATETIME NOT NULL,
    PRIMARY KEY (`id`),
    KEY `idx_btree_union_user_id` (`user_id`) USING BTREE,
    KEY `idx_btree_union_share_id` (`share_id`) USING BTREE,
    UNIQUE KEY `idx_btree_union_unique_user_id_share_id` (`user_id`, `share_id`) USING BTREE
)


alter table moon_share_news_read add COLUMN news_id bigint(20) NOT NULL COMMENT '新闻id';
alter table moon_share_news_read add COLUMN share_user_id bigint(20) NOT NULL COMMENT '原分享者id';


DROP TABLE IF EXISTS moon_wx_code_share_record;
CREATE TABLE moon_wx_code_share_record (
    id  BIGINT NOT NULL AUTO_INCREMENT,
    share_user_id BIGINT NOT NULL COMMENT '分享者的id',
    user_id BIGINT NOT NULL COMMENT '读者id',
    `deleted`     TINYINT  NOT NULL DEFAULT 0 COMMENT '逻辑删除，默认为0， 1为删除',
    `create_time` DATETIME NOT NULL,
    `update_time` DATETIME NOT NULL,
    PRIMARY KEY (`id`),
    KEY `idx_btree_union_share_user_id` (`share_user_id`) USING BTREE,
    KEY `idx_btree_union_user_id` (`user_id`) USING BTREE,
    UNIQUE KEY `idx_btree_union_unique_user_id_share_id` (`share_user_id`, `user_id`) USING BTREE
)