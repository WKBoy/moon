

# 存放新闻列表的表, 新闻总表'
DROP TABLE IF EXISTS moon_news;
CREATE TABLE moon_news (
  id           BIGINT NOT NULL AUTO_INCREMENT,
  title        TEXT DEFAULT NULL COMMENT '新闻标题',
  thumbnail    TEXT DEFAULT NULL COMMENT '新闻缩略图',
  author       VARCHAR(256) DEFAULT NULL COMMENT '作者',
  detail_url   TEXT DEFAULT NULL COMMENT '详情页url',
  brief   TEXT DEFAULT NULL COMMENT '新闻简介',
  news_create_time  TIMESTAMP,
  src         VARCHAR(256) DEFAULT NULL COMMENT '新闻来源',
  deleted     TINYINT  NOT NULL DEFAULT 0 COMMENT '逻辑删除，默认为0， 1为删除',
  stick       TINYINT NOT NULL DEFAULT 0 COMMENT '置顶',
  create_time DATETIME NOT NULL,
  update_time DATETIME NOT NULL,
  PRIMARY KEY (id)
)
  ENGINE = InnoDB
  COMMENT '存放新闻列表的表, 新闻总表';

DROP TABLE IF EXISTS moon_news_detail;
CREATE TABLE moon_news_detail (
  id               BIGINT   NOT NULL AUTO_INCREMENT,
  news_id          BIGINT NOT NULL COMMENT '新闻id',
  title            TEXT DEFAULT NULL COMMENT '新闻标题',
  thumbnail        TEXT DEFAULT NULL COMMENT '新闻缩略图',
  content           TEXT DEFAULT NULL COMMENT '新闻正文',
  author           VARCHAR(256)      DEFAULT NULL COMMENT '作者',
  news_create_time TIMESTAMP COMMENT '新闻创建时间',
  view_count       INTEGER COMMENT '浏览量',
  reply_count      INTEGER COMMENT '回复数',
  src              VARCHAR(256)      DEFAULT NULL COMMENT '新闻来源',
  deleted          TINYINT  NOT NULL DEFAULT 0 COMMENT '逻辑删除，默认为0， 1为删除',
  create_time      DATETIME NOT NULL,
  update_time      DATETIME NOT NULL,
  PRIMARY KEY (id)
)

# 记录新闻已读未读
CREATE TABLE moon_news_read (
  id BIGINT NOT NULL AUTO_INCREMENT,
  user_id BIGINT NOT NULL COMMENT '用户ID',
  news_id BIGINT NOT NULL COMMENT '新闻id',
  create_time DATETIME NOT NULL,
  update_time DATETIME NOT NULL,
  deleted TINYINT NOT NULL DEFAULT 0 COMMENT '逻辑删除，默认为0， 1为删除',
  KEY `idx_btree_union_user_id_news_id` (`user_id`, `news_id`) USING BTREE,
  PRIMARY KEY (id)
)




CREATE TABLE moon_news_ask(
    id BIGINT NOT NULL AUTO_INCREMENT,
    news_id BIGINT NOT NULL COMMENT '新闻id',
    ask_id BIGINT NOT NULL COMMENT '问题id',
    operator VARCHAR(20) NOT NULL DEFAULT '' COMMENT '操作者',
    create_time DATETIME NOT NULL,
    update_time DATETIME NOT NULL,
    deleted     TINYINT  NOT NULL DEFAULT 0 COMMENT '逻辑删除，默认为0， 1为删除',
    KEY idx_btree_union_news_id(news_id) USING BTREE,
    PRIMARY KEY (id)
)





