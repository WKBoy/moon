
# 高德数据源表
DROP TABLE IF EXISTS gd_source;
CREATE TABLE gd_source (
  id           BIGINT NOT NULL AUTO_INCREMENT,
  gd_id      varchar(64) comment '高德id',
  name       varchar(128) comment '名称',
  address varchar(1024) comment '地址',
  province_code varchar(64) comment '省编码',
  province_name varchar(64) comment '省编码',
  city_code varchar(64) comment '市编码',
  city_name varchar(64) comment '市名称',
  area_code varchar(64) comment '区编码',
  area_name varchar(64) comment '区名称',
  lng decimal(20, 16) DEFAULT NULL COMMENT '经度',
  lat decimal(20, 16) DEFAULT NULL COMMENT '纬度',
  type TINYINT comment '1 学校 2 小区',
  deleted     TINYINT  NOT NULL DEFAULT 0 COMMENT '逻辑删除，默认为0， 1为删除',
  create_time DATETIME NOT NULL,
  update_time DATETIME NOT NULL,
  PRIMARY KEY (id)
)
  ENGINE = InnoDB
  COMMENT '高德数据源表';


# 高德数据源表
DROP TABLE IF EXISTS gd_district_source;
CREATE TABLE gd_district_source
(
    id            BIGINT   NOT NULL AUTO_INCREMENT,
    adcode         varchar(64) comment 'adcode',
    name          varchar(128) comment '名称',
    level       varchar(64) comment '级别',
    lng           decimal(20, 16)   DEFAULT NULL COMMENT '经度',
    lat           decimal(20, 16)   DEFAULT NULL COMMENT '纬度',
    deleted       TINYINT  NOT NULL DEFAULT 0 COMMENT '逻辑删除，默认为0， 1为删除',
    create_time   DATETIME NOT NULL,
    update_time   DATETIME NOT NULL,
    parent_code varchar(64) comment 'parent_code',
    PRIMARY KEY (id)
)
    ENGINE = InnoDB
    COMMENT '高德省市区数据源表';


DROP TABLE IF EXISTS gd_source_photo;
CREATE TABLE gd_source_photo
(
    id          BIGINT   NOT NULL AUTO_INCREMENT,
    gd_id varchar(64) comment '高德id',
    url       varchar(1024) comment 'url',
    deleted     TINYINT  NOT NULL DEFAULT 0 COMMENT '逻辑删除，默认为0， 1为删除',
    create_time DATETIME NOT NULL,
    update_time DATETIME NOT NULL,
    PRIMARY KEY (id)
)
    ENGINE = InnoDB
    COMMENT '高德数据源图片表';

