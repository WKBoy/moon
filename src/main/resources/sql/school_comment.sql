DROP TABLE IF EXISTS moon_school_comment;
CREATE TABLE moon_school_comment
(
    id            BIGINT       NOT NULL AUTO_INCREMENT,
    school_id     BIGINT       NOT NULL COMMENT '学校id',
    comment       VARCHAR(1024) NOT NULL COMMENT '评论内容',
    user_id   BIGINT DEFAULT NULL COMMENT '评论者的用户id',
    avatar varchar(1024) DEFAULT NULL COMMENT '评论者的头像',
    nickname varchar(64) DEFAULT NULL COMMENT '评论者的昵称',
    `deleted`     TINYINT      NOT NULL DEFAULT 0 COMMENT '逻辑删除，默认为0， 1为删除',
    `create_time` DATETIME     NOT NULL,
    `update_time` DATETIME     NOT NULL,
    PRIMARY KEY (`id`),
    KEY index_btree_school_id (school_id) USING BTREE ,
    KEY index_btree_user_id (user_id) USING BTREE
)
    ENGINE = InnoDB
    COMMENT '学校评论表';

DROP TABLE IF EXISTS moon_school_comment_img;
CREATE TABLE moon_school_comment_img
(
    id            BIGINT        NOT NULL AUTO_INCREMENT,
    comment_id    BIGINT        NOT NULL COMMENT '评论id',
    img           VARCHAR(1024) NOT NULL COMMENT '评论图片',
    `deleted`     TINYINT       NOT NULL DEFAULT 0 COMMENT '逻辑删除，默认为0， 1为删除',
    `create_time` DATETIME      NOT NULL,
    `update_time` DATETIME      NOT NULL,
    PRIMARY KEY (`id`),
    KEY index_btree_comment_id (comment_id) USING BTREE
)
    ENGINE = InnoDB
    COMMENT '学校评论图片表';