/*
 * Dian.com Inc.
 * Copyright (c) 2004-2019 All Rights Reserved.
 */
package cn.sedu.moon;

import cn.sedu.moon.dao.NewsDetailDAO;
import cn.sedu.moon.dao.model.NewsDetail;
import org.apache.commons.lang3.StringUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @author wukong
 * @version : DomTest.java, v 0.1 2019年08月19日 11:38 Exp $
 */
public class DomTest extends BaseTest {


    @Autowired
    private NewsDetailDAO newsDetailDAO;

    @Test
    public void getNewsBriefTest() {

        NewsDetail queryNewsModel = new NewsDetail();
        queryNewsModel.setNewsId(260L);

        NewsDetail newsDetail = newsDetailDAO.getRecord(queryNewsModel);

        if (newsDetail == null || newsDetail.getContent() == null) {
            System.out.println("新闻详情不存在");
            return;
        }

        Document doc = Jsoup.parse(newsDetail.getContent());

        // 缩略图
        Elements elements = doc.getElementsByTag("img");
        if (elements != null && elements.size() > 0) {
            Element element = elements.get(0);
            String imgSrc =  element.attr("src");
            System.out.println("img src: " + imgSrc);
        }

        // 简介
        String text = doc.text();
        if (StringUtils.isNotBlank(text) && text.length() > 60) {
           text = text.substring(0, 60);
        }

        System.out.println("brief : " + text);
    }
}