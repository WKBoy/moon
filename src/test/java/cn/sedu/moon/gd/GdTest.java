/*
 * Dian.com Inc.
 * Copyright (c) 2004-2019 All Rights Reserved.
 */
package cn.sedu.moon.gd;

import cn.sedu.moon.BaseTest;
import cn.sedu.moon.dao.GdDistrictSourceDAO;
import cn.sedu.moon.dao.model.GdDistrictSource;
import cn.sedu.moon.service.DataService;
import cn.sedu.moon.thirdparty.GdApi;
import cn.sedu.moon.thirdparty.dto.GdDistrict;
import cn.sedu.moon.thirdparty.dto.GdDistrictResponse;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.util.CollectionUtils;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;

/**
 * @author wukong
 * @version : GdTest.java, v 0.1 2019年10月31日 10:02 下午 Exp $
 */
@Slf4j
public class GdTest extends BaseTest {

    @Value("${gd.key}")
    private String gdKey;

    @Autowired
    private GdApi gdApi;

    @Autowired
    private DataService dataService;

    @Resource
    private GdDistrictSourceDAO gdDistrictSourceDAO;

    @Test
    public void poiSearchTest() {
//        GdPoiSearchResponse response = gdApi.poiSearch(gdKey, "面馆", "110108", true, 20, 1);
//        System.out.println("response: " + JSON.toJSONString(response));

//        dataService.fetchGdData();

    }

    @Test
    public void districtSearchTest() {

        GdDistrictResponse gdDistrictResponse = gdApi.districtSearch(gdKey, 3);

        // save数据库
        if (!CollectionUtils.isEmpty(gdDistrictResponse.getDistricts())) {
            saveDistrictInfo(gdDistrictResponse.getDistricts(), null);
        }
    }

    private void saveDistrictInfo(List<GdDistrict> gdDistricts, String parentCode) {

        for (GdDistrict gdDistrict : gdDistricts) {
            GdDistrictSource gdDistrictSource = new GdDistrictSource();
            gdDistrictSource.setAdcode(gdDistrict.getAdCode());
            gdDistrictSource.setLevel(gdDistrict.getLevel());
            gdDistrictSource.setName(gdDistrict.getName());
            gdDistrictSource.setParentCode(parentCode);
            gdDistrictSource.setCreateTime(new Date());
            gdDistrictSource.setUpdateTime(new Date());
            gdDistrictSource.setDeleted(0);

            // 经纬度
            if (StringUtils.isNotBlank(gdDistrict.getCenter())) {
                try {
                    String[] subStrings = gdDistrict.getCenter().split(",");
                    if (subStrings.length > 1) {
                        gdDistrictSource.setLng(Double.parseDouble(subStrings[0]));
                        gdDistrictSource.setLat(Double.parseDouble(subStrings[1]));
                    }
                } catch (Exception e) {
                    log.error("queryGd parse location info error:", e);
                }
            }

            gdDistrictSourceDAO.saveRecord(gdDistrictSource);

            if (!CollectionUtils.isEmpty(gdDistrict.getDistricts())) {
                saveDistrictInfo(gdDistrict.getDistricts(), gdDistrict.getAdCode());
            }
        }
    }

    @Test
    public void processCityInfo() {

        dataService.processGdProvinceCode();

    }

    @Test
    public void processGdDataAsync() {
        dataService.processGdDataAsync();
    }

}