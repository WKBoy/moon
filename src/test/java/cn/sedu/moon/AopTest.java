/*
 * Dian.com Inc.
 * Copyright (c) 2004-2019 All Rights Reserved.
 */
package cn.sedu.moon;

import cn.sedu.moon.common.enums.ErrorCodeEnum;
import cn.sedu.moon.common.exception.BizException;
import cn.sedu.moon.dao.UserGloryDAO;
import cn.sedu.moon.dao.model.UserGlory;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.springframework.aop.framework.AopContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;

/**
 * @author wukong
 * @version : AopTest.java, v 0.1 2019年10月17日 4:53 PM Exp $
 */
@Slf4j
@Service
public class AopTest extends BaseTest {

    @Autowired
    private UserGloryDAO userGloryDAO;

//    @Test
    @Async
//    @Transactional(rollbackFor = Exception.class)
    public void transactionTest() {

        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            log.error("thread sleep exception", e);
        }

        log.info("transactionTest, start");

        UserGlory saveModel = new UserGlory();
        saveModel.setUserId(2L);
        saveModel.setGlory(0L);
        saveModel.setCreateTime(new Date());
        saveModel.setUpdateTime(new Date());
        saveModel.setDeleted(0);

        int saveResult = userGloryDAO.saveRecord(saveModel);

        throw new BizException(ErrorCodeEnum.SYS_EXP, "测试异常");
    }

    @Test
    @Transactional(rollbackFor = Exception.class)
    public void syncTest() {
        log.info("-------sync test start.");

        ((AopTest)(AopContext.currentProxy())).transactionTest();
//        transactionTest();

        log.info("-------sync test finish.");
    }

}