/*
 * Dian.com Inc.
 * Copyright (c) 2004-2019 All Rights Reserved.
 */
package cn.sedu.moon;

import cn.sedu.moon.common.enums.SchoolPropertyEnum;
import cn.sedu.moon.dao.SchoolDAO;
import cn.sedu.moon.dao.SchoolHotDAO;
import cn.sedu.moon.dao.model.School;
import cn.sedu.moon.dao.model.SchoolHot;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

/**
 * @author wukong
 * @version : SchoolHotTest.java, v 0.1 2019年08月31日 00:01 Exp $
 */

public class SchoolHotTest extends BaseTest {

    // 钱塘外国语小学 49779、育才实验小学 49950、绿城育华小学 49771、新世纪外国语小学 54204 、
    // 江南实验小学 54301、人大附中小学 52121、云谷小学 53986， 崇文实验小学 49885L
    private static final List<Long> minBanSchoolIdList = Arrays.asList(49779L, 49950L, 49771L, 54204L, 54301L, 52121L, 53986L, 49885L);


    // 学军小学 49756L 、天长小学 53854L  、胜利小学 53850L、西湖小学  54182L 、求是小学 53852L 、
    // 文三街小学 54197L、安吉路小学 54174L、保俶塔小学 54180L、采荷二小 54188L
    private static final List<Long> gongBanSchoolIdList = Arrays.asList(49756L, 53854L, 53850L, 54182L, 53852L, 54197L, 54174L, 54180L,
            54188L);


    // 北京热门公办学校: 190 北京第二实验小学, 97 北京市师范大学实验小学（北师大附小）, 89 北京小学本部, 206 北京市中关村第三小学（中关村三小）, 145 北京市中关村第一小学（中关村一小）
    // 109 北京市府学小学（府学胡同小学）,   100 北京市史家小学,   1190 北京市汇文第一小学（汇文一小）（原丁香小学）, 3355 京第一师范学校附属小学（一师附小）
    // 523 北京市育民小学, 1405 北京育才学校小学部  , 963 北京市西城区黄城根小学, 716 中国人民大学附属小学（人大附小）（RDF）
    // 359 北京景山学校小学部, 1355 北京市中关村第二小学（中关村二小）
    private static final List<Long> bjGongBanSchoolIdList = Arrays.asList(190L, 97L, 89L, 206L, 145L, 109L, 100L, 1190L, 3355L,
            523L, 1405L, 963L, 716L, 359L, 1355L);


    // 16908 北京市建华实验学校小学部,  11073 北京拔萃双语学校, 2778 北京私立汇佳学校（小学部), 44319 北京市二十一世纪国际学校（实验学校),
    // 45046 北京市力迈学校，6778 北京市海淀外国语实验学校小学文体特长班， 25000 北大附属实验学校，37369 北京美国英语语言学院，
    // 1140 北京市昌平区天通苑小学，16524   北京市私立星星学校小学部
    private static final List<Long> bjMinBanSchoolIdList = Arrays.asList(16908L, 11073L, 2778L, 44319L, 45046L, 6778L, 25000L, 37369L, 1140L, 16524L);


    // 衢师二附小、衢州市实验学校 79884、新华小学79819、鹿鸣小学79819L、新星学校79885L、巨化第一小学79834L、浙师大附属衢州白云学校 80088
    private static final List<Long> quZhouGongBanSchoolIdList = Arrays.asList(79884L, 79819L, 79885L, 79834L, 80088L);

    // 衢州市衢江区第一小学、江山实验小学、城南小学(塘源口分校)，城南小学(北校区),    江山市中山小学、江山市解放路小学,
    private static final List<Long> quZhouMinBanSchoolIdList = Arrays.asList(79929L, 79847L, 79849L, 79932L, 79806L, 79835L);

    @Autowired
    private SchoolDAO schoolDAO;

    @Autowired
    private SchoolHotDAO schoolHotDAO;

    private static final Integer CITY_HOT = 1;
    private static final Integer AREA_HOT = 2;

    @Test
    public void initHostData() {
//        minBanSchoolIdList.forEach(schoolId -> initHotDataBySchoolId(schoolId, CITY_HOT, SchoolPropertyEnum.MINBAN));
//        gongBanSchoolIdList.forEach(schoolId -> initHotDataBySchoolId(schoolId, CITY_HOT, SchoolPropertyEnum.GONGBAN));
//        bjGongBanSchoolIdList.forEach(schoolId -> initHotDataBySchoolId(schoolId, CITY_HOT, SchoolPropertyEnum.GONGBAN));
        bjMinBanSchoolIdList.forEach(schoolId -> initHotDataBySchoolId(schoolId, CITY_HOT, SchoolPropertyEnum.MINBAN));
        System.out.println("Finish");
    }

    private void initHotDataBySchoolId(Long schoolId, Integer type, SchoolPropertyEnum schoolPropertyEnum) {

        // type 1: 市热门， 2区热门
        School querySchoolModel  = new School();
        querySchoolModel.setId(schoolId);

        School school = schoolDAO.getRecord(querySchoolModel);
        if (school != null) {

            SchoolHot saveSchoolHotModel = new SchoolHot();
            saveSchoolHotModel.setSchoolId(schoolId);
            saveSchoolHotModel.setType(school.getType());
            saveSchoolHotModel.setProperty(schoolPropertyEnum.getCode());
            saveSchoolHotModel.setAreaCode(school.getAreaCode());
            saveSchoolHotModel.setCityCode(school.getCityCode());
            saveSchoolHotModel.setProvinceCode(school.getProvinceCode());
            saveSchoolHotModel.setDeleted(0);
            saveSchoolHotModel.setCityHot(type == CITY_HOT ? 1 : 0);
            saveSchoolHotModel.setAreaHot(type == AREA_HOT ? 1 : 0);
            saveSchoolHotModel.setCreateTime(new Date());
            saveSchoolHotModel.setUpdateTime(new Date());

            int saveResult = schoolHotDAO.saveRecord(saveSchoolHotModel);
            if (saveResult == 0) {
                System.out.println("保存失败");
            }
        }
    }

}