/*
 * Dian.com Inc.
 * Copyright (c) 2004-2019 All Rights Reserved.
 */
package cn.sedu.moon;

import cn.sedu.moon.common.constant.BizConstants;
import cn.sedu.moon.common.properties.WxAppProperties;
import cn.sedu.moon.thirdparty.WechatApi;
import cn.sedu.moon.thirdparty.WechatClient;
import cn.sedu.moon.thirdparty.dto.WxAppSessionKey;
import cn.sedu.moon.thirdparty.response.WxMsgSecCheckResponse;
import com.alibaba.fastjson.JSON;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @author wukong
 * @version : WechatTest.java, v 0.1 2019年08月30日 20:23 Exp $
 */
public class WechatTest extends BaseTest {

    @Autowired
    private WechatApi wechatApi;

    @Autowired
    private WechatClient wechatClient;

    @Autowired
    private WxAppProperties wxAppProperties;

    @Test
    public void contentCheckTest() {
        String content = "学军中学";
//        content = "习大大前妻";
        WxMsgSecCheckResponse wxMsgSecCheckResponse = wechatApi.contentCheck(content);
        System.out.println("response: " + JSON.toJSON(wxMsgSecCheckResponse));
    }

    @Test
    public void code2sessionKeyTest() {
        // 1 获取sessionKey
        String appId = wxAppProperties.getAppId();
        String appSecret = wxAppProperties.getSecret();

        WxAppSessionKey result = wechatClient.code2Session(appId, appSecret, "", BizConstants.WX_APP_GRANT_TYPE_SESSION_KEY);
        System.out.println("wechatClient.code2Session result:" + result);
    }
}