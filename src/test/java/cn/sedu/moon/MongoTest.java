/*
 * Dian.com Inc.
 * Copyright (c) 2004-2019 All Rights Reserved.
 */
package cn.sedu.moon;

import cn.sedu.moon.common.enums.SchoolPropertyEnum;
import cn.sedu.moon.common.enums.SchoolTypeEnum;
import cn.sedu.moon.dao.SchoolDAO;
import cn.sedu.moon.dao.model.School;
import cn.sedu.moon.dao.mongo.SchoolLocation;
import cn.sedu.moon.dao.mongo.SchoolLocationRepository;
import com.alibaba.fastjson.JSON;
import com.github.pagehelper.ISelect;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.geo.Point;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.index.GeospatialIndex;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 *
 * @author ${wukong}
 * @version $Id: MongoTest.java, v 0.1 2019年02月18日 5:47 PM Exp $
 */
public class MongoTest extends BaseTest {

    @Autowired
    private MongoTemplate mongoTemplate;

    @Autowired
    private SchoolDAO schoolDAO;

    @Autowired
    private SchoolLocationRepository locationRepository;

    @Test
    public void test() {
        Set<String> names = mongoTemplate.getCollectionNames();
        System.out.println("names is " + JSON.toJSON(names));
    }

    @Test
    public void initData(){

        // 先清空
        mongoTemplate.dropCollection(SchoolLocation.class);

        // 创建索引
        mongoTemplate.indexOps(SchoolLocation.class).ensureIndex(new GeospatialIndex("position"));

        // 查找学校数据
        School querySchoolModel = new School();

        Integer offset = 0;
        Integer pageSize = 1000;
        Integer count = -1;

        Integer totalCount = 0;

        while (count != 0) {

            System.out.println("offset is " + offset);

            Page<School> page = PageHelper.offsetPage(offset, pageSize).doSelectPage(new ISelect() {
                @Override
                public void doSelect() {
                    schoolDAO.listRecord(querySchoolModel);
                }
            });

            count = page.size();
            totalCount += count;
            List<School> schoolList = page.getResult();

            for (School school : schoolList) {
                if (school.getLng() != null && school.getLat() != null) {
                    System.out.println("save school id is " + school.getId());

                    SchoolLocation schoolLocation = new SchoolLocation();
                    schoolLocation.setPosition(new double[] {school.getLng(), school.getLat()});
                    schoolLocation.setSchoolId(school.getId());
                    schoolLocation.setName(school.getName());

                    // 公办，民办
                    if (school.getProperty() != null) {
                        schoolLocation.setProperty(school.getProperty());
                    } else {
                        schoolLocation.setProperty(0);
                    }

                    // 小学，初中，高中
                    schoolLocation.setType(school.getType());
                    mongoTemplate.save(schoolLocation);
                }
            }

            offset += pageSize;
        }

        System.out.println("total count is " + totalCount);
        System.out.println("Finish");
    }

    @Test
    public void searchGeoTest() {
        //List<SchoolLocation> locations = locationRepository.findCircleNear(new Point(120.114764, 30.274024), 1);

        List<SchoolLocation> locations = locationRepository.findCircleNear(new Point(120.218163, 30.269525), 10,
                SchoolPropertyEnum.GONGBAN, SchoolTypeEnum.XIAOXUE, 0,20);

        System.out.println("locations is " + locations.size());
        List<String> schoolNameList = new ArrayList<>();

        locations.forEach(location -> {
            School queryModel = new School();
            queryModel.setId(location.getSchoolId());
            School school = schoolDAO.getRecord(queryModel);
            schoolNameList.add(school.getName());

            //System.out.println("school location is " + JSON.toJSONString(location));
        });

        schoolNameList.forEach(schoolName -> {
            System.out.println(schoolName);
        });


    }
}