/*
 * Dian.com Inc.
 * Copyright (c) 2004-2019 All Rights Reserved.
 */
package cn.sedu.moon;

import org.junit.Test;

/**
 *
 * @author ${wukong}
 * @version $Id: StringTest.java, v 0.1 2019年07月15日 11:44 AM Exp $
 */
public class StringTest {

    @Test
    public void reTest() {

        String content = "style=1232 width:128px;";
        //content = "important; width:527px";
        String pattern = "width.*?px";
        content = content.replaceAll(pattern, " ");
        pattern = "width=\".*?\"";
        content = content.replaceAll(pattern, " ");
        System.out.println("result is " + content);
    }

    private String processContent(String content) {
        String pattern = "width.*?px;";
        content = content.replaceAll(pattern, " ");
        pattern = "width=\".*?\"";
        content = content.replaceAll(pattern, " ");
        return content;
    }

}