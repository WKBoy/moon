/*
 * Dian.com Inc.
 * Copyright (c) 2004-2019 All Rights Reserved.
 */
package cn.sedu.moon;

import java.text.MessageFormat;

import com.alibaba.fastjson.JSON;

import cn.sedu.moon.controller.response.SopResponse;
import cn.sedu.moon.service.SopService;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author ${wukong}
 * @version $Id: SopTest.java, v 0.1 2019年01月09日 2:54 PM Exp $
 */
public class SopTest extends BaseTest {

    @Autowired
    private SopService sopService;

    @Test
    public void placeHolderTest() {
        String souceTitle = "{0}的学费";
        String title = MessageFormat.format(souceTitle, "庆阳一中");

        System.out.println("title is " + title);
    }

    @Test
    public void getSopTest() {
        SopResponse response = sopService.getSop(101L, 1, 37L);
        System.out.println("response is" + JSON.toJSON(response));
    }

}