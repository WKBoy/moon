/*
 * Dian.com Inc.
 * Copyright (c) 2004-2018 All Rights Reserved.
 */
package cn.sedu.moon;

import cn.sedu.moon.common.enums.AreaLevelEnum;
import cn.sedu.moon.dao.AreaDataDAO;
import cn.sedu.moon.dao.SchoolDAO;
import cn.sedu.moon.dao.model.AreaData;
import cn.sedu.moon.dao.model.School;
import com.alibaba.fastjson.JSON;
import com.github.pagehelper.ISelect;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import org.apache.commons.lang3.StringUtils;
import org.junit.Test;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;

/**
 *
 * @author ${wukong}
 * @version $Id: SchoolTest.java, v 0.1 2018年12月05日 4:44 PM Exp $
 */
public class SchoolTest extends BaseTest {

    @Resource
    private SchoolDAO schoolDAO;

    @Resource
    private AreaDataDAO areaDataDAO;

    @Test
    public void selectSchoolTest() {

        School school = new School();
        school.setId(684L);
        List<School> schools = schoolDAO.listRecord(school);
        if (schools != null && schools.size() > 0) {

        }
    }

    @Test
    public void replaceStringTest() {

        String a = "是吧  确实  然后";
        a = a.replaceAll("\\s", "\r\n");
        System.out.println(a);
    }

    @Test
    public void splitSchoolName() {

        String a = "北京大学附属中学（北大附中）";

        String[] subAuthorNames = a.split("（");
        String[] subs = subAuthorNames[0].split("\\(");
        System.out.println("subs is " + subs);
    }


    @Test
    public void schoolAreaInfoProcess() {

        // 翻页查询数据
        School queryModel = new School();
        Integer offset = 0;
        Integer pageSize = 1000;
        Integer total = schoolDAO.countRecord(queryModel);

        if (total == null || total == 0) {
            System.out.println("数据为空");
            return;
        }

        while (offset < total) {

            Page<School> page = PageHelper.offsetPage(offset, pageSize).doSelectPage(new ISelect() {
                @Override
                public void doSelect() {
                    schoolDAO.listRecord(queryModel);
                }
            });

            if (page != null && page.size() > 0) {
                for (School school : page.getResult()) {

                    String provinceCode = "";
                    String cityCode = "";
                    String areaCode = "";

                    // 处理省信息
                    if (StringUtils.isNotBlank(school.getProvince())) {
                        AreaData queryAreaDataModel = new AreaData();
                        queryAreaDataModel.setAreaName(school.getProvince());
                        queryAreaDataModel.setAreaLevel(AreaLevelEnum.PROVINCE.getCode());
                        AreaData provinceInfo = areaDataDAO.getRecord(queryAreaDataModel);
                        if (provinceInfo != null) {
                            provinceCode = provinceInfo.getAreaCode();
                        }
                    }

                    // 处理市信息
                    if (StringUtils.isNotBlank(school.getCity())) {
                        AreaData queryAreaDataModel = new AreaData();
                        queryAreaDataModel.setAreaName(school.getCity());
                        queryAreaDataModel.setAreaLevel(AreaLevelEnum.CITY.getCode());
                        AreaData cityInfo = areaDataDAO.getRecord(queryAreaDataModel);
                        if (cityInfo != null) {
                            cityCode = cityInfo.getAreaCode();
                        }
                    }

                    // 处理区信息
                    if (StringUtils.isNotBlank(school.getArea())) {
                        AreaData queryAreaDataModel = new AreaData();
                        queryAreaDataModel.setAreaName(school.getArea());
                        queryAreaDataModel.setAreaLevel(AreaLevelEnum.AREA.getCode());
                        AreaData areaInfo = areaDataDAO.getRecord(queryAreaDataModel);
                        if (areaInfo != null) {
                            areaCode = areaInfo.getAreaCode();
                        }
                    }

                    // 更新
                    School updateModel = new School();
                    updateModel.setId(school.getId());
                    updateModel.setProvinceCode(provinceCode);
                    updateModel.setCityCode(cityCode);
                    updateModel.setAreaCode(areaCode);
                    updateModel.setUpdateTime(new Date());

                    int updateResult = schoolDAO.updateRecord(updateModel);
                    if (updateResult == 0) {
                        System.out.println("update area code info error, update model: " + JSON.toJSONString(updateModel));
                    }

                }
            }

            offset = offset + pageSize;
        }
    }
}