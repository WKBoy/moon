/*
 * Dian.com Inc.
 * Copyright (c) 2004-2019 All Rights Reserved.
 */
package cn.sedu.moon;

import cn.sedu.moon.common.util.AuthUtil;
import org.junit.Test;

import java.io.IOException;

/**
 *
 * @author ${wukong}
 * @version $Id: UtilTest.java, v 0.1 2019年03月04日 2:06 PM Exp $
 */
public class UtilTest {

    @Test
    public void hexTest() {

        Long a = 82622L;
        String astr = Long.toHexString(a);
        System.out.println("astr is " + astr);

    }

    @Test
    public void myTest() {
        int i = 1;
        int result = 0;
        switch (i) {
            case 1:
                result += i;
            case 2:
                result += i * 2;
            case 3:
                result += i *3;
        }

        System.out.println("result is " + result);
    }

    @Test
    public void tokenTest() throws IOException {
        String userId = AuthUtil.getUserIdFromToken("MTRBMDExRTA2NjY0OTQwNjQyM0I1MDRCQjRCQkRCM0RGMDNBNTFERjQyMDI=");
        System.out.println("userId: " + userId);
    }




}