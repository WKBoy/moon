/*
 * Dian.com Inc.
 * Copyright (c) 2004-2019 All Rights Reserved.
 */
package cn.sedu.moon;

import cn.sedu.moon.common.enums.AreaLevelEnum;
import cn.sedu.moon.dao.AreaDataDAO;
import cn.sedu.moon.dao.DistrictGdDAO;
import cn.sedu.moon.dao.model.AreaData;
import cn.sedu.moon.dao.model.DistrictGd;
import com.alibaba.fastjson.JSON;
import com.github.pagehelper.ISelect;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import org.apache.commons.lang3.StringUtils;
import org.junit.Test;

import javax.annotation.Resource;
import java.util.Date;

/**
 * @author wukong
 * @version : DistrictTest.java, v 0.1 2019年08月25日 16:34 Exp $
 */
public class DistrictTest extends BaseTest {

    @Resource
    private DistrictGdDAO districtGdDAO;

    @Resource
    private AreaDataDAO areaDataDAO;

    @Test
    public void districtAreaInfoProcess() {

        // 翻页查询数据
        DistrictGd queryModel = new DistrictGd();
        Integer offset = 0;
        Integer pageSize = 1000;
        Integer total = districtGdDAO.countRecord(queryModel);

        if (total == null || total == 0) {
            System.out.println("数据为空");
            return;
        }

        while (offset < total) {

            System.out.println("offset: " + offset + ", total: " + total);

            Page<DistrictGd> page = PageHelper.offsetPage(offset, pageSize).doSelectPage(new ISelect() {
                @Override
                public void doSelect() {
                    districtGdDAO.listRecord(queryModel);
                }
            });

            if (page != null && page.size() > 0) {
                for (DistrictGd districtGd : page.getResult()) {

                    String provinceCode = "";
                    String cityCode = "";
                    String areaCode = "";

                    // 处理省信息
                    if (StringUtils.isNotBlank(districtGd.getProvince())) {
                        AreaData queryAreaDataModel = new AreaData();
                        queryAreaDataModel.setAreaName(districtGd.getProvince());
                        queryAreaDataModel.setAreaLevel(AreaLevelEnum.PROVINCE.getCode());
                        AreaData provinceInfo = areaDataDAO.getRecord(queryAreaDataModel);
                        if (provinceInfo != null) {
                            provinceCode = provinceInfo.getAreaCode();
                        }
                    }

                    // 处理市信息
                    if (StringUtils.isNotBlank(districtGd.getCity())) {
                        AreaData queryAreaDataModel = new AreaData();
                        queryAreaDataModel.setAreaName(districtGd.getCity());
                        queryAreaDataModel.setAreaLevel(AreaLevelEnum.CITY.getCode());
                        AreaData cityInfo = areaDataDAO.getRecord(queryAreaDataModel);
                        if (cityInfo != null) {
                            cityCode = cityInfo.getAreaCode();
                        }
                    }

                    // 处理区信息
                    if (StringUtils.isNotBlank(districtGd.getArea())) {
                        AreaData queryAreaDataModel = new AreaData();
                        queryAreaDataModel.setAreaName(districtGd.getArea());
                        queryAreaDataModel.setAreaLevel(AreaLevelEnum.AREA.getCode());
                        AreaData areaInfo = areaDataDAO.getRecord(queryAreaDataModel);
                        if (areaInfo != null) {
                            areaCode = areaInfo.getAreaCode();
                        }
                    }

                    // 更新
                    DistrictGd updateModel = new DistrictGd();
                    updateModel.setId(districtGd.getId());
                    updateModel.setProvinceCode(provinceCode);
                    updateModel.setCityCode(cityCode);
                    updateModel.setAreaCode(areaCode);
                    updateModel.setUpdateTime(new Date());

                    int updateResult = districtGdDAO.updateRecord(updateModel);
                    if (updateResult == 0) {
                        System.out.println("update area code info error, update model: " + JSON.toJSONString(updateModel));
                    }

                }
            }

            offset = offset + pageSize;
        }
    }


}