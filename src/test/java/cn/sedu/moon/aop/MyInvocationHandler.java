/*
 * Dian.com Inc.
 * Copyright (c) 2004-2019 All Rights Reserved.
 */
package cn.sedu.moon.aop;

import com.alibaba.fastjson.JSON;
import org.aopalliance.aop.Advice;
import org.springframework.aop.MethodBeforeAdvice;
import org.springframework.aop.framework.ProxyFactory;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

/**
 * @author wukong
 * @version : MyInvocationHandler.java, v 0.1 2019年10月21日 11:33 PM Exp $
 */
public class MyInvocationHandler implements InvocationHandler {

    private Object object;

    public MyInvocationHandler(final Object object) {
        this.object = object;
    }

    @Override
    public Object invoke(final Object proxy, final Method method, final Object[] args) throws Throwable {

        System.out.println("method: " + method.getName() + "is invoked ");
        return method.invoke(object, args);
    }

    public static void main(String[] args) throws Exception {
//        MusicPlayer musicPlayer = (MusicPlayer) Proxy.newProxyInstance(MyInvocationHandler.class.getClassLoader(), new Class<?>[]{MusicPlayer.class}, new MyInvocationHandler(new MusicPlayerImpl()));
//        musicPlayer.play();


        ProxyFactory proxyFactory = new ProxyFactory(new Demo());
        Advice advice = new MethodBeforeAdvice() {
            @Override
            public void before(final Method method, final Object[] args, final Object target) throws Throwable {
                System.out.println("你被拦截了: 方法名: " + method.getName() + ", 参数为:" + JSON.toJSONString(args));
            }
        };

        proxyFactory.addAdvice(advice);
        DemoInterface demoInterface = (DemoInterface)proxyFactory.getProxy();
        demoInterface.hello();
    }
}

interface DemoInterface {
    void hello();
}

class Demo implements DemoInterface {
    @Override
    public void hello() {
        System.out.println("this demo show");
    }
}