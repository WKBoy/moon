/*
 * Dian.com Inc.
 * Copyright (c) 2004-2019 All Rights Reserved.
 */
package cn.sedu.moon.aop;

/**
 * @author wukong
 * @version : MusicPlayer.java, v 0.1 2019年10月21日 11:37 PM Exp $
 */
public interface MusicPlayer {

    void play();
}