/*
 * Dian.com Inc.
 * Copyright (c) 2004-2019 All Rights Reserved.
 */
package cn.sedu.moon.aop;

/**
 * @author wukong
 * @version : MusicPlayerImpl.java, v 0.1 2019年10月21日 11:38 PM Exp $
 */
public class MusicPlayerImpl implements MusicPlayer {

    @Override
    public void play() {
        System.out.println("music player playing");
    }
}