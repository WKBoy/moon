/*
 * Dian.com Inc.
 * Copyright (c) 2004-2017 All Rights Reserved.
 */
package cn.sedu.moon;

import lombok.extern.slf4j.Slf4j;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicLong;

/**
 * 测试基类
 *
 * @author ${wukong}
 * @version $Id: BaseController.java, v 0.1 2018年12月5日 下午9:03 Exp $
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = MoonApplication.class)
@Slf4j
@EnableAsync
public class BaseTest {

    /**
     * 线程池服务
     */
    static ExecutorService arynExecutorService = new ThreadPoolExecutor(60, 60, 0L,
            TimeUnit.SECONDS, new LinkedBlockingQueue<>(), new ThreadPoolExecutor.CallerRunsPolicy());
    /**
     * 总执行请求次数
     */
    static AtomicLong      requestCount        = new AtomicLong(0);

    /**
     * RT总和
     */
    static AtomicLong      rtTotalCount        = new AtomicLong(0);

    /**
     * 失败次数
     */
    static AtomicLong      failCount           = new AtomicLong(0);

    @Autowired
    public WebApplicationContext context;
    public MockMvc               mvc;

    @Before
    public void setUp() throws Exception {
        mvc = MockMvcBuilders.webAppContextSetup(context).build();//建议使用这种
    }
}