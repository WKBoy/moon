/*
 * Dian.com Inc.
 * Copyright (c) 2004-2019 All Rights Reserved.
 */
package cn.sedu.moon;

import com.alibaba.fastjson.JSON;

import cn.sedu.moon.service.MessageTaskService;
import cn.sedu.moon.thirdparty.WechatApi;
import cn.sedu.moon.thirdparty.WechatPushApi;
import cn.sedu.moon.thirdparty.request.WxNotifyMessagePushRequest;
import cn.sedu.moon.thirdparty.response.WxGetAccessTokenResponse;
import cn.sedu.moon.thirdparty.response.WxNotifyMessagePushResponse;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author ${wukong}
 * @version $Id: MessageTest.java, v 0.1 2019年04月02日 6:17 PM Exp $
 */
public class MessageTest extends BaseTest {

    @Autowired
    private WechatPushApi wechatPushApi;

    @Autowired
    private WechatApi wechatApi;

    @Autowired
    private MessageTaskService messageTaskService;

    @Test
    public void sendTemplateMessage() {
        WxNotifyMessagePushRequest request = new WxNotifyMessagePushRequest();
        request.setFormId("");
        WxNotifyMessagePushResponse response = wechatPushApi.sendNotifyPushMessage("", request);
        System.out.println("response is " + JSON.toJSONString(response));
    }

    @Test
    public void getAccessToken() {

        WxGetAccessTokenResponse response = wechatApi.getAccessToken();
        System.out.println("response is " + JSON.toJSON(response));
    }

    @Test
    public void askPushTest() {
        messageTaskService.askPushTemplateMessage(2489L);
    }

}