/*
 * Dian.com Inc.
 * Copyright (c) 2004-2019 All Rights Reserved.
 */
package cn.sedu.moon;

import cn.sedu.moon.service.NewsService;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author ${wukong}
 * @version $Id: NewsTest.java, v 0.1 2019年03月12日 12:46 AM Exp $
 */
public class NewsTest extends BaseTest{

    @Autowired
    private NewsService newsService;

    @Test
    public void newsDaoTest() {

        List<String> schoolNameList = new ArrayList<>();
        schoolNameList.add("学军小学");
        schoolNameList.add("文一街道小学");

        //List<News> newsList = newsDAO.listRecordBySchoolNameList(schoolNameList);

        System.out.println("Finish");
    }

    @Test
    public void stickLastDayNewsTest() {
        newsService.stickLastDayCreatedNews();
    }
}